const Sequelize = require('sequelize');
const PracticesModel = require('./rest/practices/model');
const UsersModel = require('./rest/users/model');
const ClientsModel = require('./rest/clients/model');
const CasesModel = require('./rest/cases/model');
const DocumentsModel = require('./rest/documents/model');
const OrdersModel = require('./rest/orders/model');
const LegalFormsModel = require('./rest/legalforms/model');
const FormsModel = require('./rest/forms/model');
const FormotpModel = require('./rest/formotp/model');
const SettingsModel = require('./rest/settings/model');
const HipaaTermsModel = require('./rest/hipaa_terms/model');
const FeeTermsModel = require('./rest/fee_terms/model');
const AdminObjectionsModel = require('./rest/admin_objections/model');
const CustomerObjectionsModel = require('./rest/customer_objections/model');
const MedicalHistoryModel = require('./rest/medical_history/model');
const MedicalHistorySummeryModel = require('./rest/medical_history_summery/model');
const DocumentUploadProgressModel = require('./rest/document_upload_progress/model');
const HelpRequestModel = require('./rest/help_request/model');
const QuestionsTranslationsModel = require('./rest/questions_translations/model');
const VersionHistoryModel = require('./rest/version_history/model');
const PasswordHistoryModel = require('./rest/users/password_history_model');
const ClientSignatureModel = require('./rest/client_signature/model');
const PracticeSettingsModel = require('./rest/practice_settings/model');
const PlansModel = require('./rest/plans/model');
const SubscriptionsModel = require('./rest/subscriptions/model');
const SubscriptionHistoryModel = require('./rest/subscriptions/subscription_history_model');
const SubscriptionHistoryWebhookModel = require('./rest/subscriptions/subscription_history_webhook_model');
const OtherPartiesModel = require('./rest/other_parties/model');
const OtherPartiesFormsModel = require('./rest/other_parties_forms/model');
const OtherPartiesFormsOtpModel = require('./rest/other_parties_formotp/model');
const PracticesTemplatesModel = require('./rest/practices_templates/model');
const InvoiceModel = require('./rest/invoice/model');
const StatesModel = require('./rest/states/model');
const MedicalDocumentPropertyModel = require('./rest/medical_document_properties/model');
const CaseArchiveModel = require('./rest/archive/model');
const FeeWaiverModel = require('./rest/fee_waiver/model');
const PropoundTemplatesModel = require('./rest/propound_templates/model');
const PropoundDocumentModel = require('./rest/propound_template_upload_progress/model');
const PropoundFormsModel = require('./rest/propound_forms/model');
const PropoundResponderModel = require('./rest/propound_responder/model')
const FrogsTemplateModel = require('./rest/frogs_templates/model');
const PropoundHelpRequestModel = require('./rest/propound_help_request/model');
const DiscountCodeModel = require('./rest/discount_codes/model');
const StripeBillingHistoryModel = require('./rest/stripe_webhooks/stripe_billing_history');
const FilevineModel = require('./rest/integrations/filevine/model');
const MycaseModel = require('./rest/integrations/mycase/model');
const DiscountHistoryModel = require('./rest/discount_codes/discount_code_history_model');
const DocumentExtractionProgressModel = require('./rest/document_extraction_progress/model');
const LawyerObjectionsModel = require('./rest/lawyer_objection/model');
const FormsResponseHistoryModel = require('./rest/forms_response_history/model');
const ClioModel = require('./rest/integrations/clio/model');
const MeetingsModel = require('./rest/video_call/meetings_model');
const MeetingAttendeesModel = require('./rest/video_call/meeting_attendees_model');
const HashedFilesModel = require('./rest/hashed_files/model');
const DocTextractRegionModel = require('./rest/docTextractRegion/model');
const DocumentVersioningModel = require('./rest/document_versioning/model');

const Op = Sequelize.Op;

const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
        dialect: 'mysql',
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialectOptions: {decimalNumbers: true},
        logging: false,
    }
);
const Practices = PracticesModel(sequelize, Sequelize);
const Users = UsersModel(sequelize, Sequelize);
const Clients = ClientsModel(sequelize, Sequelize);
const Cases = CasesModel(sequelize, Sequelize);
const Documents = DocumentsModel(sequelize, Sequelize);
const Orders = OrdersModel(sequelize, Sequelize);
const LegalForms = LegalFormsModel(sequelize, Sequelize);
const Forms = FormsModel(sequelize, Sequelize);
const Formotp = FormotpModel(sequelize, Sequelize);
const Settings = SettingsModel(sequelize, Sequelize);
const HipaaTerms = HipaaTermsModel(sequelize, Sequelize);
const FeeTerms = FeeTermsModel(sequelize, Sequelize);
const AdminObjections = AdminObjectionsModel(sequelize, Sequelize);
const CustomerObjections = CustomerObjectionsModel(sequelize, Sequelize);
const MedicalHistory = MedicalHistoryModel(sequelize, Sequelize);
const MedicalHistorySummery = MedicalHistorySummeryModel(sequelize, Sequelize);
const DocumentUploadProgress = DocumentUploadProgressModel(sequelize, Sequelize);
const HelpRequest = HelpRequestModel(sequelize, Sequelize);
const QuestionsTranslations = QuestionsTranslationsModel(sequelize, Sequelize);
const VersionHistory = VersionHistoryModel(sequelize, Sequelize);
const PasswordHistory = PasswordHistoryModel(sequelize, Sequelize);
const ClientSignature = ClientSignatureModel(sequelize, Sequelize);
const PracticeSettings = PracticeSettingsModel(sequelize, Sequelize);
const Plans = PlansModel(sequelize, Sequelize);
const Subscriptions = SubscriptionsModel(sequelize, Sequelize);
const SubscriptionHistory = SubscriptionHistoryModel(sequelize, Sequelize);
const SubscriptionHistoryWebhook = SubscriptionHistoryWebhookModel(sequelize, Sequelize);
const OtherParties = OtherPartiesModel(sequelize, Sequelize);
const OtherPartiesForms = OtherPartiesFormsModel(sequelize, Sequelize);
const OtherPartiesFormsOtp = OtherPartiesFormsOtpModel(sequelize, Sequelize);
const PracticesTemplates = PracticesTemplatesModel(sequelize, Sequelize);
const Invoices = InvoiceModel(sequelize, Sequelize);
const States = StatesModel(sequelize, Sequelize);
const MedicalDocumentProperty = MedicalDocumentPropertyModel(sequelize, Sequelize);
const CaseArchive = CaseArchiveModel(sequelize, Sequelize);
const Feewaiver = FeeWaiverModel(sequelize, Sequelize);
const PropoundTemplates = PropoundTemplatesModel(sequelize, Sequelize);
const PropoundDocumentUploadProgress = PropoundDocumentModel(sequelize, Sequelize);
const PropoundForms = PropoundFormsModel(sequelize, Sequelize);
const PropoundResponder = PropoundResponderModel(sequelize, Sequelize);
const FrogsTemplates = FrogsTemplateModel(sequelize, Sequelize);
const PropoundHelpRequest = PropoundHelpRequestModel(sequelize, Sequelize);
const DiscountCode = DiscountCodeModel(sequelize, Sequelize);
const StripeBillingHistory = StripeBillingHistoryModel(sequelize, Sequelize);
const Filevine = FilevineModel(sequelize, Sequelize);
const Mycase = MycaseModel(sequelize, Sequelize);
const DiscountHistory = DiscountHistoryModel(sequelize, Sequelize);
const DocumentExtractionProgress = DocumentExtractionProgressModel(sequelize, Sequelize);
const LawyerObjections = LawyerObjectionsModel(sequelize, Sequelize);
const FormsResponseHistory = FormsResponseHistoryModel(sequelize, Sequelize);
const Clio = ClioModel(sequelize, Sequelize);
const Meetings = MeetingsModel(sequelize, Sequelize);
const MeetingAttendees = MeetingAttendeesModel(sequelize, Sequelize);
const HashedFiles = HashedFilesModel(sequelize, Sequelize);
const DocTextractRegion = DocTextractRegionModel(sequelize, Sequelize);
const DocumentVersioning = DocumentVersioningModel(sequelize, Sequelize);

Practices.hasMany(Users, {foreignKey: 'practice_id'});
Users.belongsTo(Practices, {foreignKey: 'practice_id'});

Forms.hasMany(OtherPartiesForms, {foreignKey: 'form_id'});
OtherPartiesForms.belongsTo(Forms, {foreignKey: 'form_id'});


const Models = {
    Op,
    Practices,
    Users,
    sequelize,
    Clients,
    Cases,
    Documents,
    Orders,
    LegalForms,
    Forms,
    Formotp,
    Settings,
    HipaaTerms,
    FeeTerms,
    AdminObjections,
    CustomerObjections,
    MedicalHistory,
    MedicalHistorySummery,
    DocumentUploadProgress,
    HelpRequest,
    QuestionsTranslations,
    VersionHistory,
    PasswordHistory,
    ClientSignature,
    PracticeSettings,
    Plans,
    Subscriptions,
    SubscriptionHistory,
    SubscriptionHistoryWebhook,
    OtherParties,
    OtherPartiesForms,
    OtherPartiesFormsOtp,
    PracticesTemplates,
    Invoices,
    States,
    MedicalDocumentProperty,
    CaseArchive,
    Feewaiver,
    PropoundTemplates,
    PropoundDocumentUploadProgress,
    PropoundForms,
    PropoundResponder,
    FrogsTemplates,
    PropoundHelpRequest,
    DiscountCode,
    StripeBillingHistory,
    Filevine,
    Mycase,
    DiscountHistory,
    DocumentExtractionProgress,
    LawyerObjections,
    FormsResponseHistory,
    Clio,
    Meetings,
    MeetingAttendees,
    HashedFiles,
    DocTextractRegion,
    DocumentVersioning
};
const connection = {};

module.exports = async () => {
    if (connection.isConnected) {
        console.log('=> Using existing connection.');
        return Models;
    }
    await sequelize.sync();
    await sequelize.authenticate();
    connection.isConnected = true;
    console.log('=> Created a new connection.');
    return Models;
};
