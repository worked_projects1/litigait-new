const nodemailer = require('nodemailer');

async function sendEmail(to, subject, content, FROM_NAME, attachments) {
    const smtpConfig = {
        host: process.env.AWS_SES_HOST,
        port: process.env.AWS_SES_PORT,
        // secure: true, // use SSL
        auth: {
            user: process.env.AWS_SES_SMTP_USER,
            pass: process.env.AWS_SES_SMTP_PASSWORD,
        },
    };
    const transporter = nodemailer.createTransport(smtpConfig);
    return await new Promise((resolve, reject) => {
        if (!to) {
            resolve(false);
        }
        const mailOptions = {
            from: `"${FROM_NAME || process.env.AWS_SES_FROM_NAME}" <${process.env.AWS_SES_FROM_EMAIL}>`, // sender address
            to, // list of receivers
            subject, // Subject line
            text: '', // plaintext body
            html: content,
        };
        if (attachments && attachments.length) {
            mailOptions.attachments = attachments;
        }
        if (!to) {
            return resolve(true);
        }
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(`error is ${error}`);
                reject(error); // or use rejcet(false) but then you will have to handle errors
            } else {
                console.log(`Email sent: ${info.response}`);
                resolve(info);
            }
        });
    });
}

async function sendEmailwithCC(to, cc, subject, content, FROM_NAME, attachments) {
    const smtpConfig = {
        host: process.env.AWS_SES_HOST,
        port: process.env.AWS_SES_PORT,
        // secure: true, // use SSL
        auth: {
            user: process.env.AWS_SES_SMTP_USER,
            pass: process.env.AWS_SES_SMTP_PASSWORD,
        },
    };
    const transporter = nodemailer.createTransport(smtpConfig);
    return await new Promise((resolve, reject) => {
        if (!to) {
            resolve(false);
        }
        const mailOptions = {
            from: `"${FROM_NAME || process.env.AWS_SES_FROM_NAME}" <${process.env.AWS_SES_FROM_EMAIL}>`, // sender address
            to, // list of receivers
            cc, // list of cc receivers
            subject, // Subject line
            text: '', // plaintext body
            html: content,
        };
        if (attachments && attachments.length) {
            mailOptions.attachments = attachments;
        }
        if (!to) {
            return resolve(true);
        }
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(`error is ${error}`);
                reject(error); // or use rejcet(false) but then you will have to handle errors
            } else {
                console.log(`Email sent: ${info.response}`);
                resolve(info);
            }
        });
    });
}

module.exports = {
    sendEmail,
    sendEmailwithCC
};
