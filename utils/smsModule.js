const AWS = require('aws-sdk');
// const { getSecrets } = require('../rest/helpers/secrets.helper');

async function sendSMS(to, subject, content) {
    AWS.config.region = 'us-west-2';
    const sns = new AWS.SNS();
    const params = {
        Message: content,
        MessageStructure: 'string',
        PhoneNumber: `${to}`,
        Subject: subject,
    };
    await sns.publish(params).promise();
}



// async function sendSMS(to, subject, content) {
//     const accountSid = process?.env?.TWILIO_SID ? process?.env?.TWILIO_SID : await getSecrets('TWILIO_SID');
//     const authToken = process?.env?.TWILIO_AUTHTOKEN ? process?.env?.TWILIO_AUTHTOKEN : await getSecrets('TWILIO_AUTHTOKEN');;
//     const client = require('twilio')(accountSid, authToken);
//     try {
//         let response = await client.messages
//         .create({
//             body: `${content}`,
//             to: `+${to}`, // Text your number
//             from: '+19383333995', // From a valid Twilio number
//         });
//         console.log(response);   
//     } catch (err) {
//         console.log(err);
//     }
// }



module.exports = {
    sendSMS,
};
