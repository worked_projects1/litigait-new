
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Read Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }
  test('read orders by superAdmin (no skip, limit, filters, sort)', async () => {
    const response = await server.get('/orders')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({

      });
    expect(response.statusCode).toBe(200);
    expect(response.body.length).toBe(4);
  });

  test('read orders with skip and limit', async () => {
    const response = await server.get('/orders')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .query({
        offset: 1,
        limit: 1,
      });
    expect(response.statusCode).toBe(200);
    expect(response.body.length).toBe(1);
  });

  test('read orders filtered by name', async () => {
    const response = await server.get('/orders')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .query({
        where: JSON.stringify({ practice_id: 'sample_practice_id' }),
      });
    expect(response.statusCode).toBe(200);
    expect(response.body.length).toBe(1);
  });
});
