
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }

/*

        order_date: new Date(),
        status: 'completed',
        practice_id: 'sample_practice_id',
        user_id: 'sample_user_id',
        case_id: 'sample_case_id',
        document_id: 'sample_document_id',
        document_type: 'FROGS',
        amount_charged: 12,
        charge_id: 'abcdefrgsj',

*/


  test('create order with missing order date', async () => {
    const response = await server.post('/orders')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        status: 'completed',
        practice_id: 'sample_practice_id',
        user_id: 'sample_user_id',
        case_id: 'sample_case_id',
        document_id: 'sample_document_id',
        document_type: 'FROGS',
        amount_charged: 12,
        charge_id: 'abcdefrgsj',
      });
    expect(response.statusCode).toBe(400);
  });

  test('create order with invalid order_date', async () => {
    const response = await server.post('/orders')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        order_date: 'invalid date',
        status: 'completed',
        practice_id: 'sample_practice_id',
        user_id: 'sample_user_id',
        case_id: 'sample_case_id',
        document_id: 'sample_document_id',
        document_type: 'FROGS',
        amount_charged: 12,
        charge_id: 'abcdefrgsj',
      });
    expect(response.statusCode).toBe(400);
  });

});
