
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }


  test('create cases with only required fields (client_id, start_date)', async () => {
    const response = await server.post('/cases')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        client_id: 'client_id_1',
        start_date: new Date(),
        case_title: 'Sample Case 1',
        case_number: 'xyzcase123',
        status: 'new',
      });
    expect(response.statusCode).toBe(200);
  });

  test('create cases with only required fields (name, address) with ID', async () => {
    const response = await server.post('/cases')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: 'specific_case_for_test',
        client_id: 'client_id_2',
        start_date: new Date(),
        case_title: 'Sample Case 2',
        case_number: 'xyzcase12334',
        status: 'new',
      });
    expect(response.statusCode).toBe(200);
  });

  test('create user with all fields at min value/length', async () => {
    const response = await server.post('/cases')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        client_id: 'abc2',
        start_date: new Date(),
        case_title: config.text4,
        case_number: config.text4,
        status: 'new',
      });
    expect(response.statusCode).toBe(200);
  });

  test('create user with all fields at max value/length', async () => {
    const response = await server.post('/cases')
        .set('content-type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', process.env.authToken)
        .send({
          client_id: config.text64,
          start_date: new Date(),
          case_title: config.text64,
          case_number: config.text64,
          status: 'new',
        });
    expect(response.statusCode).toBe(200);
  });
});
