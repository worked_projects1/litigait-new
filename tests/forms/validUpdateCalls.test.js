
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Update Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }
  test('update form case_id by superAdmin', async () => {
    const response = await server.put('/forms/specific_form_for_test')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_special_23',
      });
    expect(response.statusCode).toBe(200);
  });

  test('update form document_type to operator by superAdmin', async () => {
    const response = await server.put('/forms/specific_form_for_test')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        document_type: 'SPROGS',
      });
    expect(response.statusCode).toBe(200);
  });
});
