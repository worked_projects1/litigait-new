
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }


  test('create user with missing name', async () => {
    const response = await server.post('/clients')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        address: 'justanyuser@test.com',
        logoFile: 'testlink',
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with invalid name', async () => {
    const response = await server.post('/clients')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        name: 'asa',
        address: 'justanyuser@test.com',
        logoFile: 'testlink',
      });
    expect(response.statusCode).toBe(400);
  });


  test('create user with missing address', async () => {
    const response = await server.post('/clients')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        name: 'valid name',
        logoFile: 'testlink',
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with invalid address', async () => {
    const response = await server.post('/clients')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        name: 'valid name',
        address: 'ds',
        logoFile: 'testlink',
      });
    expect(response.statusCode).toBe(400);
  });
});
