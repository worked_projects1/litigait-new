
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }

  test('create document with only required fields (case_id, document_id, document_type, created_date, created_by )', async () => {
    const response = await server.post('/documents')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_1',
        document_id: 'sample_document_id_1',
        document_type: 'FROGS',
        created_date: new Date(),
        created_by: 'sample_user_1',
      });
    expect(response.statusCode).toBe(200);
  });

  test('create document with manual id with only required fields (name, address)', async () => {
    const response = await server.post('/documents')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: 'specific_document_for_test',
        case_id: 'sample_case_id_1',
        document_id: 'sample_document_id_2',
        document_type: 'FROGS',
        created_date: new Date(),
        created_by: 'sample_user_1',
      });
    expect(response.statusCode).toBe(200);
  });

  test('create user with all fields at min value/length', async () => {
    const response = await server.post('/documents')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: config.text4,
        document_id: config.text4,
        document_type: 'FROGS',
        created_date: new Date(),
        created_by: config.text4,
      });
    expect(response.statusCode).toBe(200);
  });

  test('create user with all fields at max value/length', async () => {
    const response = await server.post('/documents')
        .set('content-type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', process.env.authToken)
        .send({
          case_id: config.text64,
          document_id: config.text64,
          document_type: 'FROGS',
          created_date: new Date(),
          created_by: config.text64,
        });
    expect(response.statusCode).toBe(200);
  });
});
