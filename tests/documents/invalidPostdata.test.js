
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }


  // case_id: type.STRING,
  // document_id: type.STRING,
  // document_type: type.STRING, // (FROGS, SPROGS, RFPD, RFA)
  // created_date: type.DATE,
  // created_by: type.STRING,

  // case_id: 'sample_case_id_1',
  //       document_id: 'sample_document_id_1',
  //       document_type: 'FROGS',
  //       created_date: new Date(),
  //       created_by: 'sample_user_1',


  test('Create document with missing case_id', async () => {
    const response = await server.post('/documents')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        document_id: 'sample_document_id_1',
        document_type: 'FROGS',
        created_date: new Date(),
        created_by: 'sample_user_1',
      });
    expect(response.statusCode).toBe(400);
  });

  test('Create document with invalid document_id', async () => {
    const response = await server.post('/documents')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'ab',
        document_id: 'sample_document_id_1',
        document_type: 'FROGS',
        created_date: new Date(),
        created_by: 'sample_user_1',
      });
    expect(response.statusCode).toBe(400);
  });


  test('Create document with invaid document_type', async () => {
    const response = await server.post('/documents')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id',
        document_id: 'sample_document_id_1',
        document_type: 'unknown type',
        created_date: new Date(),
        created_by: 'sample_user_1',
      });
    expect(response.statusCode).toBe(400);
  });

  test('Create document with missing document_type', async () => {
    const response = await server.post('/documents')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_',
        document_id: 'sample_document_id_1',
        created_date: new Date(),
        created_by: 'sample_user_1',
      });
    expect(response.statusCode).toBe(400);
  });

  test('Create document with invaid created_date', async () => {
    const response = await server.post('/documents')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id',
        document_id: 'sample_document_id_1',
        document_type: 'FROGS',
        created_date: 'invalid date',
        created_by: 'sample_user_1',
      });
    expect(response.statusCode).toBe(400);
  });
  test('Create document with invaid created_by', async () => {
    const response = await server.post('/documents')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id',
        document_id: 'sample_document_id_1',
        document_type: 'FROGS',
        created_date: new Date(),
        created_by: 'ab',
      });
    expect(response.statusCode).toBe(400);
  });

  test('Create document with missing created_by', async () => {
    const response = await server.post('/documents')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id',
        document_id: 'sample_document_id_1',
        document_type: 'FROGS',
        created_date: new Date(),
      });
    expect(response.statusCode).toBe(400);
  });
});
