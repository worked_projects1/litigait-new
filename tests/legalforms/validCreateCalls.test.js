
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }

  // practice_id: type.STRING,
  // case_id: type.STRING,
  // client_id: type.STRING,
  // form_id: type.STRING,
  // status: type.STRING, // pending, complete
  // json_data: type.JSON,


  test('create legalforms with only required fields (practice_id, case_id, client_id, form_id, status)', async () => {
    const response = await server.post('/legalforms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        practice_id: 'sample_practice_id',
        case_id: 'sample_case_id',
        client_id: 'sample_client_id',
        form_id: 'sample_form_id',
        status: 'pending', // pending, complete
      });
    expect(response.statusCode).toBe(200);
  });

  test('create legalforms with only required fields (name, address)', async () => {
    const response = await server.post('/legalforms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: 'specific_legalform_for_test',
        practice_id: 'sample_practice_id_1',
        case_id: 'sample_case_id_1',
        client_id: 'sample_client_id_1',
        form_id: 'sample_form_id_1',
        status: 'pending', // pending, complete
      });
    expect(response.statusCode).toBe(200);
  });

  test('create user with all fields at min value/length', async () => {
    const response = await server.post('/legalforms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        practice_id: config.text4,
        case_id: config.text4,
        client_id: config.text4,
        form_id: config.text4,
        status: 'pending', // pending, complete
      });
    expect(response.statusCode).toBe(200);
  });

  test('create user with all fields at max value/length', async () => {
    const response = await server.post('/legalforms')
        .set('content-type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', process.env.authToken)
        .send({
          practice_id: config.text64,
          case_id: config.text64,
          client_id: config.text64,
          form_id: config.text64,
          status: 'pending', // pending, complete
        });
    expect(response.statusCode).toBe(200);
  });
});
