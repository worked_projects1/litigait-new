

const request = require('supertest');
const { config } = require('../global');
const EnvVariables = require('../../configs/secrets.test.json');

process.env = Object.assign(process.env, EnvVariables);
const connectToDatabase = require('../../db');

describe('Clean up', () => {
  test('Clean Up LegalForms Table', async () => {
    const { LegalForms, sequelize } = await connectToDatabase();

    const result = await LegalForms.destroy({ where: { } });

    expect(result).not.toBe(null);
    const count = await LegalForms.count({
      where: {
      },
    });
    expect(count).toBe(0);
    //sequelize.close();
  });
});
