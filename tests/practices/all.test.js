process.env.viaAll = true;
require('./cleanup.test');
require('../users/login.test');
require('./validCreateCalls.test');
require('./validReadCalls.test');
require('./validUpdateCalls.test');
require('./validDeleteCalls.test');
require('./invalidParams.test');
require('./invalidPostdata.test');
