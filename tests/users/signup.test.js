

const request = require('supertest');
const { config } = require('../global');
const EnvVariables = require('../../configs/secrets.test.json');

process.env = Object.assign(process.env, EnvVariables);
const connectToDatabase = require('../../db');

const server = request(config.server_url);

describe('Sign up', () => {
  test('Clean Up User Table', async () => {
    const { Users, sequelize } = await connectToDatabase();

    const result = await Users.destroy({ where: { } });

    expect(result).not.toBe(null);
    const count = await Users.count({
      where: {
      },
    });
    // sequelize.close();
    expect(count).toBe(0);
  });

  test('Sign-up a user', async () => {
    const response = await server.post('/users/signup')
    .set('content-type', 'application/json')
    .set('Accept', 'application/json')
    .send({
      practice_name: 'Test Practice',
      name: 'Sandip Das',
      email: 'sandip.sandip.das11@gmail.com',
      password: 'simple12345',
      role: 'superAdmin',
    });
    expect(response.statusCode).toBe(200);
    expect(response.body.authToken).not.toBe('');
  });
});
