
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }


  test('create user with missing name', async () => {
    const response = await server.post('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        email: 'justanyuser@test.com',
        password: 'old_password',
        role: 'superAdmin',
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with invalid name', async () => {
    const response = await server.post('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        name: 'asa',
        email: 'justanyuser@test.com',
        password: 'old_password',
        role: 'superAdmin',
      });
    expect(response.statusCode).toBe(400);
  });


  test('create user with missing email', async () => {
    const response = await server.post('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        name: 'Second Admin',
        password: 'old_password',
        role: 'superAdmin',
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with invalid email', async () => {
    const response = await server.post('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        name: 'Second Admin',
        email: 'abc_test_invalid_email',
        password: 'old_password',
        role: 'superAdmin',
      });
    expect(response.statusCode).toBe(400);
  });

  test('create users with missing password', async () => {
    const response = await server.post('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        name: 'Second Admin',
        email: 'justanyuser@test.com',
        role: 'superAdmin',
      });
    expect(response.statusCode).toBe(400);
  });
  test('create users with Invalid Password', async () => {
    const response = await server.post('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        name: 'Second Admin',
        email: 'justanyuser@test.com',
        password: 'cdc',
        role: 'superAdmin',
      });
    expect(response.statusCode).toBe(400);
  });
  test('create user with missing role', async () => {
    const response = await server.post('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        name: 'Second Admin',
        email: 'justanyuser@test.com',
        password: 'old_password',
      });
    expect(response.statusCode).toBe(400);
  });
  test('create user with invalid role', async () => {
    const response = await server.post('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        name: 'Second Admin',
        email: 'justanyuser@test.com',
        password: 'old_password',
        role: 'unknown',
      });
    expect(response.statusCode).toBe(400);
  });
});
