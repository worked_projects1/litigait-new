
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }


  test('create second superAdmin user with all fields for test', async () => {
    const response = await server.post('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: 'specific_superAdmin_user_for_test',
        name: 'Second Admin',
        email: 'secondsuperAdmin@test.com',
        password: 'old_password',
        role: 'superAdmin',
      });
    expect(response.statusCode).toBe(200);
  });

  test('create user with only required fields (username, password, role, name)', async () => {
    const response = await server.post('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: '',
        name: 'Required User',
        email: 'req_fields_user@email.com',
        password: 'req_fields_user',
        role: 'superAdmin',
      });
    expect(response.statusCode).toBe(200);
  });

  test('create user with all fields at min value/length', async () => {
    const response = await server.post('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: '',
        name: config.shortName,
        email: config.shortEmail,
        password: config.text4,
        role: 'superAdmin',
      });
    expect(response.statusCode).toBe(200);
  });

    test('create user with all fields at max value/length', async () => {
      const response = await server.post('/users')
        .set('content-type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', process.env.authToken)
        .send({
          id: '',
          name: config.longName,
          email: config.longEmail,
          password: config.text64,
          role: 'superAdmin',
        });
      expect(response.statusCode).toBe(200);
    });

});
