
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Un Authenticated test', () => {

  test('create user without authentication', async () => {
    const response = await server.post('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        id: 'specific_superAdmin_user_for_test',
        name: 'Second Admin',
        email: 'secondsuperAdmin@test.com',
        password: 'old_password',
        role: 'superAdmin',
      });
    expect(response.statusCode).toBe(401);
  });

  test('read users without authentication', async () => {
    const response = await server.get('/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({

      });
    expect(response.statusCode).toBe(401);
  });

  test('update user without authentication', async () => {
    const response = await server.put('/users/specific_superAdmin_user_for_test')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        password: 'new_password',
      });
    expect(response.statusCode).toBe(401);
  });

  test('Delete user without authentication', async () => {
    const response = await server.delete('/users/specific_superAdmin_user_for_test')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
      });
    expect(response.statusCode).toBe(401);
  });

});
