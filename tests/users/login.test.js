

const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Log in', () => {
  test('login-in a user', async () => {
    const response = await server.post('/login')
    .set('content-type', 'application/json')
    .set('Accept', 'application/json')
    .send({
      email: 'sandip.sandip.das11@gmail.com',
      password: 'simple12345',
    });
    expect(response.statusCode).toBe(200);
    expect(response.body.authToken).not.toBe('');
    process.env.authToken = response.body.authToken;
  });
});
