module.exports = (sequelize, type) => sequelize.define('DiscountCode', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    name: type.STRING,
    discount_code: type.STRING,
    discount_percentage: type.STRING,
    status: type.STRING,
    plan_type: type.STRING,
    stripe_obj: type.TEXT,
    createdBy: type.STRING,
    is_deleted: type.BOOLEAN
});

/* activation fee coupon code dont have any stripe obj. */