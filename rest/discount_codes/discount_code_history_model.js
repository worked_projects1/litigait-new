module.exports = (sequelize, type) => sequelize.define('DiscountCodeHistory', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    subscription_id: type.STRING,
    plan_type: type.STRING,
    plan_category: type.STRING,
    discount_for: type.STRING,
    promocode: type.STRING,
    discount_percentage: type.STRING,
    base_price: type.STRING, //actual_price
    discounted_price: type.STRING, //discount_price:
    discount_amount: type.STRING, //discount_price:
});
/* If discount for is activation fee subscription history id will be stored in subscription id column. */