const {HTTPError} = require('../../utils/httpResp');

const adminInternalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const practiceRoles = ['lawyer', 'paralegal'];
const litigaitRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician', 'lawyer', 'paralegal'];

exports.authorizeCreate = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetAll = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetOne = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeDestroy = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdate = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetAllHistory = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeValidateDiscountCode = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};