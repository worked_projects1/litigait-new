const {HTTPError} = require('../../utils/httpResp');

const adminInternalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const practiceRoles = ['lawyer', 'paralegal'];
const internalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const superAdminRole = ['superAdmin'];
const normalUserRole = ['superAdmin', 'manager', 'operator'];
const litigaitRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician', 'lawyer', 'paralegal']

exports.authorizeCreate = function (user) {
    if (!normalUserRole.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetOne = function (user) {
    if (!litigaitRoles.includes(user.role)) {

        throw new HTTPError(403, 'forbidden');
    }
};
exports.authorizeGetAll = function (user) {
    if (!normalUserRole.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdate = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeDestroy = function (user) {
    if (!normalUserRole.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUploadUrl = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUploadFile = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeAdminGetFiles = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUploadPrivateFile = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetSecuredPublicUrlForPrivateFile = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetSecuredPublicUrlForFormFile = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeResetBillingData = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeObjectionStatusUpdate = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetAllPracticeInfoForDashboard = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};


