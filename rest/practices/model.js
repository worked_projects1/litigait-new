module.exports = (sequelize, type) => {
    const Practices = sequelize.define('Practices', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    name: type.STRING,
    address: type.STRING,
    street: type.STRING,
    city: type.STRING,
    state: type.STRING,
    zip_code: type.STRING,
    logoFile: type.STRING,
    stripe_customer_id: type.STRING,
    phone: type.STRING,
    fax: type.STRING,
    is_deleted: type.BOOLEAN,
    objections: type.BOOLEAN,
    no_of_users: {
        type:type.INTEGER,
        defaultValue: 0,
    },
    no_of_orders: {
        type:type.INTEGER,
        defaultValue: 0,
    },
    recent_login_ts: type.DATE, 
    license_count: type.INTEGER,
    one_time_activation_fee: type.BOOLEAN, // true - have to pay amount, false - payment completed or no need to pay
    is_yearly_free_trial_available : type.BOOLEAN, // 0 - no , 1 - yes
    is_propounding_canceled : type.BOOLEAN,
    global_attorney_response_tracking: type.BOOLEAN,
    is_vip_account: {
        type: type.BOOLEAN,
        defaultValue: false,
    },
    billing_type: {
        type: type.STRING,
        defaultValue: 'limited_users_billing',
    },
},
{
    indexes: [
        {
            name: 'is_deleted',
            fields: ['is_deleted']
        },
        {
            name: 'createdAt',
            fields: ['createdAt']
        },
        {
            name: 'state',
            fields: ['state']
        }
    ]
});
    Practices.beforeCreate(async (practice, options) => {

        const global_settings = await sequelize.models.Settings.findOne({ where: { key: 'global_settings' }, raw: true });
        const global_settings_obj = JSON.parse(global_settings.value);
        if (!global_settings_obj?.free_user_limit) throw new Error('Users limit not found.please contact support.');
        practice.license_count = global_settings_obj?.free_user_limit;
    });

    return Practices;
};
