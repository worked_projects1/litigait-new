module.exports = (sequelize, type) => sequelize.define('MedicalHistory', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    client_id: type.STRING,
    case_id: type.STRING,
    s3_file_key: type.TEXT,
    file_name: type.TEXT,
    comment: type.TEXT,
    pages: type.INTEGER,
    keys: type.BOOLEAN,
    textract_status: type.STRING,
    order_date: type.DATE,
    summery_doc_status: type.STRING,
    status: type.STRING, // NEW REQUEST, TEXTRACT, FILE_PARTITION, PREPARE_PARTITION_SUMMERY, FINAL_SUMMERY, PREPARE_SUMMARY_INPUT, COMPLETED
    extracted_doc_data: type.TEXT('long'),
    reason: type.STRING,
    file_splits_arr: type.TEXT('long'),
    completed_splits_summery: type.TEXT('long'),
    final_summery: type.TEXT('long'),
    patient_details: type.STRING,
    ai_billing_summery: type.TEXT('long'),
    textract_job_id: type.STRING
});
