const uuid = require('uuid');
const AWS = require('aws-sdk');

AWS.config.update({ region: process.env.REGION || 'us-east-1' });
const s3 = new AWS.S3();

const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const {
    validateCreateMedicalHistory,
    validateGetOneMedicalHistory,
    validateUpdateMedicalHistory,
    validateDeleteMedicalHistory,
    validateReadMedicalHistory,
} = require('./validation');
const { text } = require('body-parser');
const create = async (event) => {
    try {
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        if (event.user.practice_id) {
            input.practice_id = event.user.practice_id;
        }
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;

        // validateCreateMedicalHistory(dataObject);
        const { MedicalHistory } = await connectToDatabase();
        if (Array.isArray(input)) {
            const processedObjectArray = [];
            for (let i = 0; i < input.length; i += 1) {
                const objectItem = input[i];
                objectItem.id = `${uuid.v4()}${i}`;
                objectItem.practice_id = event.user.practice_id;
                objectItem.status = 'NEW_REQUEST';
                processedObjectArray.push(objectItem);
            }
            await MedicalHistory.bulkCreate(processedObjectArray);
        } else {
            const dataObject = Object.assign(input, { id: id || uuid.v4(), practice_id: event.user.practice_id, status: 'NEW_REQUEST' });
            await MedicalHistory.create(dataObject);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Medical history data created successfully',
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ err, error: err.message || 'Could not create the medicalHistoryDetails.' }),
        };
    }
};
const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateGetOneMedicalHistory(params);
        const { MedicalHistory } = await connectToDatabase();
        const medicalHistoryDetails = await MedicalHistory.findOne({ where: { id: params.id } });
        if (!medicalHistoryDetails) throw new HTTPError(404, `MedicalHistory with id: ${params.id} was not found`);
        const plainMedicalHistory = medicalHistoryDetails.get({ plain: true });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainMedicalHistory),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the MedicalHistory.' }),
        };
    }
};

const getAll_new = async (event) => {
    try {
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        query.raw = true;
        query.order = [
            ['createdAt', 'DESC'],
        ];
        validateReadMedicalHistory(query);
        const { MedicalHistory } = await connectToDatabase();
        const medicalHistoryDetails = await MedicalHistory.findAll(query);
        const medicalHistoryCount = await MedicalHistory.count({ where: { practice_id: event.user.practice_id } });
        medicalHistoryDetails[medicalHistoryDetails] = {
            TotalRowCount: medicalHistoryDetails.length
        };
        let totalPages, currentPage;
        if (query.limit && (medicalHistoryDetails.length > 0)) {
            totalPages = Math.ceil(medicalHistoryCount / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const medicalHistoryData = {
            total_items: medicalHistoryCount,
            response: medicalHistoryDetails,
            total_pages: totalPages,
            current_page: currentPage
        };
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(medicalHistoryData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the medicalHistoryDetails.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        let header = event.headers;
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        query.raw = true;
        query.order = [
            ['createdAt', 'DESC'],
        ];
        validateReadMedicalHistory(query);
        const { MedicalHistory } = await connectToDatabase();
        const medicalHistoryDetails = await MedicalHistory.findAll(query);
        medicalHistoryDetails[medicalHistoryDetails] = {
            TotalRowCount: medicalHistoryDetails.length
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(medicalHistoryDetails),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the medicalHistoryDetails.' }),
        };
    }
};
const update = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        validateUpdateMedicalHistory(input);
        const { MedicalHistory } = await connectToDatabase();
        const medicalHistoryDetails = await MedicalHistory.findOne({ where: { id: params.id } });
        if (!medicalHistoryDetails) throw new HTTPError(404, `Medical History with id: ${params.id} was not found`);
        const updatedModel = Object.assign(medicalHistoryDetails, input);
        let saveModel = await updatedModel.save();
        const plainMedicalHistory = saveModel.get({ plain: true });

        plainMedicalHistory.keys = plainMedicalHistory.keys;

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainMedicalHistory),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the MedicalHistory.' }),
        };
    }
};
const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateDeleteMedicalHistory(params);
        const { MedicalHistory } = await connectToDatabase();
        const medicalHistoryDetails = await MedicalHistory.findOne({ where: { id: params.id } });
        if (!medicalHistoryDetails) throw new HTTPError(404, `MedicalHistory with id: ${params.id} was not found`);
        const s3Params = {
            Bucket: process.env.S3_BUCKET_FOR_PRIVATE_ASSETS,
            Key: medicalHistoryDetails.s3_file_key,
        };
        await s3.deleteObject(s3Params).promise();
        await medicalHistoryDetails.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(medicalHistoryDetails),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy fetch the MedicalHistory.' }),
        };
    }
};
const updateJSONKeyValue = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { MedicalHistory } = await connectToDatabase();
        const params = event.params || event.pathParameters;
        const medicalObj = await MedicalHistory.findOne({ where: { id: params.id } });
        if (!medicalObj) throw new HTTPError(404, `MedicalHistory with id: ${params.id} was not found`);
        medicalObj.order_date = input.order_date;
        medicalObj.keys = input.keys;
        await medicalObj.save();
        const medicalObjRawText = await MedicalHistory.findOne({ where: { id: params.id }, raw: true });
        medicalObjRawText.keys = medicalObjRawText.keys;
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(medicalObjRawText),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not add the MedicalHistory JSOn Text.' }),
        };
    }
};
const getStatus = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateGetOneMedicalHistory(params);
        const { MedicalHistory } = await connectToDatabase();
        const medicalHistoryDetails = await MedicalHistory.findOne({
            where: { id: params.id },
            attributes: ['textract_status'],
            raw: true
        });
        if (!medicalHistoryDetails) throw new HTTPError(404, `MedicalHistory with id: ${params.id} was not found`);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(medicalHistoryDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not get Document status.' }),
        };
    }
}

const getAllMedicalExperts = async () => {
    try {
        const { Users, Op } = await connectToDatabase();

        const medicalExperts = await Users.findAll({
    where:{ role:'medicalExpert', is_deleted: { [Op.not]: true }},
    attributes: ['id','name','email','practice_id','role','is_email_verified','is_admin','last_login_ts','archived','state_bar_number','signature','color_code']
    , raw: true});
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(medicalExperts),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Failed to fetch medical experts details' }),
        };
    }
}
module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;
module.exports.updateJSONKeyValue = updateJSONKeyValue;
module.exports.getStatus = getStatus;
module.exports.getAllMedicalExperts = getAllMedicalExperts;
