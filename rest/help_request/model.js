module.exports = (sequelize, type) => sequelize.define('HelpRequests', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    user_email: type.STRING,
    practice_id: type.STRING,
    hash_id: type.STRING,
    practice_name: type.STRING,
    client_id: type.STRING,
    client_name: type.STRING,
    case_id: type.STRING,
    case_title: type.STRING,
    case_number: type.STRING,
    legalforms_id: type.STRING,
    filename: type.STRING,
    document_s3_key: type.TEXT,
    document_type: type.STRING,
});
