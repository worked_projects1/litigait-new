const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const { sendEmail } = require('../../utils/mailModule');
const {
    authorizeGetAll,
} = require('./authorize');


const sendHelpRequest = async (event) => {
    try {
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        // const dataObject = input;
        // helpRequestValidation(dataObject);
        if (!input.document_s3_key) throw new HTTPError(400, 'Document link missing in request');
        const { Practices, Clients, Cases, LegalForms } = await connectToDatabase();
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        const clientObject = await Clients.findOne({ where: { id: input.client_id } });
        const caseObject = await Cases.findOne({ where: { id: input.case_id } });
        const LegalFormsObject = await LegalForms.findOne({ where: { id: input.legalforms_id } });
        let email = ''
        if (process.env.CODE_ENV == "staging" || process.env.CODE_ENV == "local") {
            email = 'siva@miotiv.com,bhuvaneshwari.rifluxyss@gmail.com';
        } else {
            email = process.env.SUPPORT_EMAIL_RECEIPIENT;
        }

        let template = 'Document Generation Help Requested';
        if (event?.user?.name) { template = template + ' By ' + event.user.name; }
        await sendEmail(email, template, `
      User Email : ${event.user.email}<br/>
      Practice Name : ${practiceObject.name}<br/>
      Client Name : ${clientObject.name}<br/>
      Case Title : ${caseObject.case_title}<br/>
      Case Number : ${caseObject.case_number}<br/>
      Document Type: ${input.document_type}<br/>
      Document S3 Key: ${input.document_s3_key}<br/>
      `);

        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObjectFinal = Object.assign({
            user_email: event.user.email,
            practice_id: event.user.practice_id,
            hash_id: input.hash_id,
            client_id: input.client_id,
            case_id: input.case_id,
            document_type: input.document_type,
            document_s3_key: input.document_s3_key,
            practice_name: practiceObject.name,
            client_name: clientObject.name,
            case_title: caseObject.case_title,
            legalforms_id: input.legalforms_id,
            filename: LegalFormsObject.filename,
            case_number: caseObject.case_number,
        }, { id: id || uuid.v4() });
        const { HelpRequest } = await connectToDatabase();
        const helpRequest = await HelpRequest.create(dataObjectFinal);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Help Request Sent',
                helpRequest,
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not resolve.' }),
        };
    }
};

const getAll_new = async (event) => {
    try {
        authorizeGetAll(event.user);
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        // query.where.practice_id = event.user.practice_id;
        query.order = [
            ['createdAt', 'DESC'],
        ];
        // validateReadCustomerObjections(query);
        const { HelpRequest } = await connectToDatabase();
        const helpRequests = await HelpRequest.findAll(query);
        const helpRequestsCount = await HelpRequest.count();
        let totalPages, currentPage;
        if (query.limit && (helpRequests.length > 0)) {
            totalPages = Math.ceil(helpRequestsCount / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const helpRequestDetails = {
            total_items: helpRequestsCount,
            response: helpRequests,
            total_pages: totalPages,
            current_page: currentPage
        };
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(helpRequestDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the customerObjectionsss.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        authorizeGetAll(event.user);
        const query = event.params || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        // query.where.practice_id = event.user.practice_id;
        query.order = [
            ['createdAt', 'DESC'],
        ];
        // validateReadCustomerObjections(query);
        const { HelpRequest } = await connectToDatabase();
        const helpRequests = await HelpRequest.findAll(query);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(helpRequests),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the customerObjectionsss.' }),
        };
    }
};

const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const { HelpRequest } = await connectToDatabase();
        const helpRequestObject = await HelpRequest.findOne({ where: { id: params.id } });
        if (!helpRequestObject) throw new HTTPError(404, `HelpRequest with id: ${params.id} was not found`);
        await helpRequestObject.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'support request  removed'
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy fetch the HelpRequest.' }),
        };
    }
};

const getOne = async (event) => {
    try {
        authorizeGetAll(event.user);
        const params = event.params || event.pathParameters;
        const { HelpRequest } = await connectToDatabase();
        const helpRequestobj = await HelpRequest.findOne({ where: { id: params.id } });
        if (!helpRequestobj) throw new HTTPError(404, `Help Request with id: ${params.id} was not found`);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(helpRequestobj),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Help Request.' }),
        };
    }
};
const update = async (event) => {
    try {
        authorizeGetAll(event.user);
        const params = event.params || event.pathParameters;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { HelpRequest } = await connectToDatabase();
        const helpRequestobj = await HelpRequest.findOne({ where: { id: params.id } });
        if (!helpRequestobj) throw new HTTPError(404, `Help Request with id: ${params.id} was not found`);
        const helpRequestModel = Object.assign(helpRequestobj, input);
        await helpRequestModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(helpRequestModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the helpRequest.' }),
        };
    }
};

const destroySupportRequest = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const { type, id, reason } = params;
        const { HelpRequest, PropoundHelpRequest, DocumentExtractionProgress, HashedFiles } = await connectToDatabase();

        if (!params) throw new HTTPError(400, `Inpput parameters not provided in params`);
        if (!type) throw new HTTPError(400, `type not provided in params`);
        if (!id) throw new HTTPError(400, `id not provided in params`);
        if (!reason) throw new HTTPError(400, `Reason not provided in params`);

        let hashId;
        if (type == 'respond') {
            const respondingHelpRequestObj = await HelpRequest.destroy({ where: { id: params.id } });
            if (!respondingHelpRequestObj) throw new HTTPError(404, `Responding help request with id: ${id} was not found`);
            hashId = respondingHelpRequestObj?.hash_id;
        } else if (type == 'propound') {
            const propoundHelpRequestObj = await PropoundHelpRequest.destroy({ where: { id: params.id } });
            if (!propoundHelpRequestObj) throw new HTTPError(404, `Propounding help request with id: ${id} was not found`);
            hashId = propoundHelpRequestObj?.hash_id;
        } else if (type == 'extraction') {
            const quickCreateHelpRequestObj = await DocumentExtractionProgress.destroy({ where: { id: params.id } });
            if (!quickCreateHelpRequestObj) throw new HTTPError(404, `Quick create help request with id: ${id} was not found`);
            hashId = quickCreateHelpRequestObj?.hash_id;
        } else {
            throw new HTTPError(400, `provided type doesn't match`);
        }
        if (hashId) {
            const hashData = await HashedFiles.findOne({ where: { hash_id: hashId } });
            if (!hashData) throw new HTTPError(404, `Hashed data was not found`);
            if (reason == 'Duplicate') {
                await HashedFiles.destroy({ where: { hash_id: hashId } });
            }
            if (reason == 'NoQuestions') {
                hashData.questions_available = 'NoQuestions';
                await hashData.save();
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ message: "Support request file has been successfully destroyed" }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the helpRequest.' }),
        };
    }
};

module.exports.sendHelpRequest = sendHelpRequest;
module.exports.getAll = getAll;
module.exports.destroy = destroy;
module.exports.getOne = getOne;
module.exports.update = update;
module.exports.destroySupportRequest = destroySupportRequest;
