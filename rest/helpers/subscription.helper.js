


const calculateRespondingAmount = () => {
    try {

    } catch (err) {
        console.log('Subscription Helper : calculate Responding amount');
        console.log(err);
    }
}

const usersLicencePurchase = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { Users, Practices, PasswordHistory, Op, Subscriptions, Feewaiver, Settings, Plans } = await connectToDatabase();
        // const { practice_id } = input;
        const practice_id = input?.practice_id || 'b9860262-05e3-4952-8ce0-399fdedb24fb';
        const users_count = await Users.count({ where: { practice_id, is_deleted: { [Op.not]: true }, role: { [Op.in]: ['lawyer', 'paralegal'] } } });
        const subscriptionObj = await Subscriptions.findAll({
            where: { practice_id, plan_id: { [Op.not]: null }, stripe_subscription_id: { [Op.not]: null } },
            raw: true
        });
        const discount_status = users_count > 5;

        for (let i = 0; i < subscriptionObj.length; i++) {
            const plansObj = await Plans.findOne({ where: { plan_id: subscriptionObj[i]?.plan_id, active: true }, raw: true });
            const stripe_subscription = JSON.parse(subscriptionObj[i].stripe_subscription_data);
            const subscription_date = subscriptionObj[i].subscribed_on;
            const plan_id = plansObj?.plan_id;

            const subscription = await stripe.subscriptions.create({
                customer: 'cus_JVlNVTYo8zgX8m',
                items: [
                    { price: 'price_1NPhFXABxGGVMf064YXaSv7y' }, //1020

                ],
            });

            console.log(subscription.items.data);
            const subscriptionUp = await stripe.subscriptions.update(subscription.id, {
                items: [
                    { price: 'price_1NQ5flABxGGVMf06IRlnpOMT' }, //948
                ],
                proration_behavior: 'none',
            });

            console.log(JSON.stringify(subscriptionUp));
        }
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({ test: 'Testing' }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not create the users.",
            }),
        };
    }
}