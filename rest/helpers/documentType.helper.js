const { HTTPError } = require('../../utils/httpResp');
const { getSecrets } = require('../helpers/secrets.helper')

const documentTypes = async (type) => {
    try {
        const { state, outputType, letterCase } = type;
        if (!state) throw new HTTPError(400, 'State missing in helper function');
        if (!outputType) throw new HTTPError(400, 'Output type missing in helper function');
        if (state == 'all') {
            let allDocumentTypes = process?.env?.SUPPORTED_DOCUMENT_TYPES ? process?.env?.SUPPORTED_DOCUMENT_TYPES : await getSecrets('SUPPORTED_DOCUMENT_TYPES');
            if (letterCase == 'small') allDocumentTypes = allDocumentTypes.toLowerCase();
            const availableFormDocumenttype = allDocumentTypes.split(',');
            if (outputType == 'array') {
                const formArray = {};
                for (let i = 0; i < availableFormDocumenttype.length; i++) {
                    formArray[availableFormDocumenttype[i]] = [];
                }
                return formArray;
            }
            if (outputType == 'types') { return availableFormDocumenttype };
        }
        if (state != 'all') {
            const RequiredDocumentTypes = process?.env?.[`${state}_SUPPORTED_DOCUMENT_TYPES`];
            console.log(process?.env?.MI_SUPPORTED_DOCUMENT_TYPES);
            let allDocumentTypes = RequiredDocumentTypes ? RequiredDocumentTypes : await getSecrets(`${state}_SUPPORTED_DOCUMENT_TYPES`);
            console.log(allDocumentTypes);
            if (letterCase == 'small') allDocumentTypes = allDocumentTypes.toLowerCase();
            const availableFormDocumenttype = allDocumentTypes.split(',');
            if (outputType == 'array') {
                const formArray = {};
                for (let i = 0; i < availableFormDocumenttype.length; i++) {
                    formArray[availableFormDocumenttype[i]] = [];
                }
                return formArray;
            }
            if (outputType == 'types') { return availableFormDocumenttype };
        }
    }
    catch (err) {
        console.error(err);
        return (err);
    }
}

module.exports = {
    documentTypes
}