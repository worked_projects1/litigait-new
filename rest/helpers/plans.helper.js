/* 
Refer TODO Docs
UserBased Billing

  Responding Pricing:
    Billed Monthly - $495/month for 5 users. Every additional user is $89/user/month 
    Billed Annually - $5100/yr for 5 users.  Every additional user is  $79/user/month (billed at $948/yr)

  Propounding Pricing: 
    Billed Monthly - $199/month for 5 users. Every additional user is $39/user/month
    Billed Annually - $2149.20/yr for 5 users. Every additional user is $35/user/month (billed at $ 420/yr)
*/

const respondingPlans = [
    'responding_monthly_495', 'monthly', 'responding_monthly_349',
    'responding_yearly_5100', 'yearly', 'responding_yearly_3490',
    'users_responding_monthly_license_495', 'users_responding_monthly_license_89',
    'users_responding_yearly_license_5100', 'users_responding_yearly_license_948'
];

const respondingMonthlyPlans = [
    'responding_monthly_495', 'monthly', 'responding_monthly_349', 'users_responding_monthly_license_495', 'users_responding_monthly_license_89'
];

const respondingYearlyPlans = [
    'responding_yearly_5100', 'yearly', 'responding_yearly_3490', 'users_responding_yearly_license_5100', 'users_responding_yearly_license_948'
];

const propoundingPlans = ['propounding_monthly_199', 'propounding_yearly_2199',
    'users_propounding_monthly_license_199', 'users_propounding_monthly_license_39',
    'users_propounding_yearly_license_2149_20', 'users_propounding_yearly_license_420'
];

const propoundingMonthlyPlans = [
    'propounding_monthly_199', 'users_propounding_monthly_license_199', 'users_propounding_monthly_license_39'
];

const propoundingYearlyPlans = [
    'propounding_yearly_2199', 'users_propounding_yearly_license_2149_20', 'users_propounding_yearly_license_420'
];

const subscriptionPlans = [
    'responding_monthly_495', 'monthly', 'responding_monthly_349',
    'responding_yearly_5100', 'yearly', 'responding_yearly_3490',
    'users_responding_monthly_license_495', 'users_responding_monthly_license_89',
    'users_responding_yearly_license_5100', 'users_responding_yearly_license_948',
    'propounding_monthly_199', 'propounding_yearly_2199',
    'users_propounding_monthly_license_199', 'users_propounding_monthly_license_39',
    'users_propounding_yearly_license_2149_20', 'users_propounding_yearly_license_420'
];

const monthlyPlans = ['responding_monthly_495', 'monthly', 'responding_monthly_349', 'users_responding_monthly_license_495',
    'users_responding_monthly_license_89', 'propounding_monthly_199', 'users_propounding_monthly_license_199', 'users_propounding_monthly_license_39'
];
const yearlyPlans = [
    'responding_yearly_5100', 'yearly', 'responding_yearly_3490',
    'users_responding_yearly_license_5100', 'users_responding_yearly_license_948', 'propounding_yearly_2199',
    'users_propounding_yearly_license_2149_20', 'users_propounding_yearly_license_420'
];

module.exports = {
    subscriptionPlans,
    propoundingYearlyPlans,
    propoundingMonthlyPlans,
    propoundingPlans,
    respondingYearlyPlans,
    respondingMonthlyPlans,
    respondingPlans,
    monthlyPlans,
    yearlyPlans
}