const {Base64} = require('js-base64');
const CryptoJS = require("crypto-js");

const decrypted = (token, userId) =>{
    const bytesToken = CryptoJS.AES.decrypt(token, userId);
    const base64Token = bytesToken.toString(CryptoJS.enc.Utf8);
    const decryptedToken = Base64.decode(base64Token);
    return decryptedToken;
}

const encrypted = (token, userId) =>{
    const encodedToken = Base64.encode(token);
    const tokenString = encodedToken.toString(CryptoJS.enc.Utf8);
    const encryptedToken = CryptoJS.AES.encrypt(tokenString, userId);
    return encryptedToken;
}

module.exports = {
    decrypted,
    encrypted
}