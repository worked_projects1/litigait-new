const { default: OpenAI } = require("openai");
const MODEL = 'gpt-4';
const MAXIMUM_TOKEN = 6000;
const { getSecrets } = require('../helpers/secrets.helper')

const getResponseHistory = async (practice_id, case_id, form_id, legalforms_id, FormsResponseHistory) => {
    try {
        let lawyer_response = [];
        const response_history = await FormsResponseHistory.findOne({
            where: { practice_id: practice_id, case_id: case_id, form_id: form_id, legalforms_id: legalforms_id },
            raw: true
        });
        if (response_history?.response) {
            lawyer_response = JSON.parse(response_history.response);
            lawyer_response.sort((a, b) => b.version - a.version);
        }
        return lawyer_response;
    } catch (err) {
        console.log('Helper Function Error: getResponseHistory');
        console.log(err);
        throw new Error(err);
    }
}

const getOpposingCounsilDetails = async (paragraph) => {
    try {
        const OPENAI_API_KEY = process.env?.OPENAI_API_KEY ? process.env.OPENAI_API_KEY : await getSecrets('OPENAI_API_KEY');
        const openai = new OpenAI({ apiKey: OPENAI_API_KEY });
        const questionWithText = `${paragraph}\n\n Extract the practice name, attorney name, street, city, state, zipcode and email from above text.`;
        const completion = await openai.chat.completions.create({
            model: MODEL,
            temperature: 0,
            n: 1,
            max_tokens: 2000,
            messages: [{
                role: "system", content: questionWithText
            }],
        });

        const text = completion?.choices[0]?.message?.content;

        const practiceInfo = {};

        // practice name
        const practiceNameRegex = /Practice Name: (.+)/;
        const practiceNameMatch = text.match(practiceNameRegex);
        if (practiceNameMatch) {
            practiceInfo.opposing_counsel_office_name = practiceNameMatch[1];
        }

        // attorney name
        const attorneyNameRegex = /Attorney Name: (.+)/;
        const attorneyNameMatch = text.match(attorneyNameRegex);
        if (attorneyNameMatch) {
            practiceInfo.opposing_counsel_attorney_name = attorneyNameMatch[1];
        }

        // street
        const streetRegex = /Street: (.+)/;
        const streetMatch = text.match(streetRegex);
        if (streetMatch) {
            practiceInfo.opposing_counsel_street = streetMatch[1];
        }

        // city
        const cityRegex = /City: (.+)/;
        const cityMatch = text.match(cityRegex);
        if (cityMatch) {
            practiceInfo.opposing_counsel_city = cityMatch[1];
        }

        // state
        const stateRegex = /State: (.+)/;
        const stateMatch = text.match(stateRegex);
        if (stateMatch) {
            practiceInfo.opposing_counsel_state = stateMatch[1];
        }

        // zip code
        const zipRegex = /Zipcode: (\d+)/;
        const zipMatch = text.match(zipRegex);
        if (zipMatch) {
            practiceInfo.opposing_counsel_zip_code = zipMatch[1];
        }

        // email
        const emailRegex = /Email: ([^\s@]+@[^\s@]+\.[^\s@]+)/;
        const emailMatch = text.match(emailRegex);
        if (emailMatch) {
            practiceInfo.opposing_counsel_email = emailMatch[1];
        } else {
            practiceInfo.opposing_counsel_email = null;
        }

        return practiceInfo;

    } catch (err) {
        console.log(err);
        throw new Error('Could not get opposing counsil details.');
    }
}

module.exports = {
    getResponseHistory,
    getOpposingCounsilDetails
}