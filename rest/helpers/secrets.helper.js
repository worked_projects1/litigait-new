const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION || 'us-west-2'});
const secretManager = new AWS.SecretsManager();

const getSecrets = async(key) => {
    try {
        const response = await secretManager.getSecretValue({ SecretId: process.env.SECRET_MANAGER_URL}).promise();
        if (response?.SecretString) {
          const secretValue = JSON.parse(response.SecretString);
          return secretValue[key];
        }
      } catch (error) {
        console.error('Error retrieving secret:', error);
      }
}

module.exports.getSecrets = getSecrets;