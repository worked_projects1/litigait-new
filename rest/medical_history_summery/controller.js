const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const { sendEmail } = require('../../utils/mailModule');

const {
    validateCreateMedicalHistorySummery,
    validateGetOneMedicalHistorySummery,
    validateUpdateMedicalHistorySummery,
    validateDeleteMedicalHistorySummery,
    validateReadMedicalHistorySummery,
} = require('./validation');

const { authorizeCreate, authorizeGetOne, authorizeGetAll, authorizeUpdate, authorizeDestroy } = require('./authorize');
// const { startMedicalSummarybackgroundProcess } = require('../medical_history_summery/openAiController');

const { QueryTypes } = require('sequelize');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const { respondingPlans, respondingYearlyPlans, respondingMonthlyPlans, propoundingMonthlyPlans,
    propoundingPlans, propoundingYearlyPlans
} = require('../helpers/plans.helper');
const create = async (event) => {
    try {
        //authorizeCreate(event.user);
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        if (event.user.practice_id) {
            input.practice_id = event.user.practice_id;
        }
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, { id: id || uuid.v4(), scheduler_email: true, ai_scheduler: true, ai_status: 'New Request' });
        validateCreateMedicalHistorySummery(dataObject);
        const {
            MedicalHistory,
            MedicalHistorySummery,
            Orders,
            Practices,
            Settings,
            Cases,
            Clients,
            PracticeSettings,
            Subscriptions,
            Plans,
            Invoices
        } = await connectToDatabase();
        let validSubscriptionFeatures = [];
        const existingSubscriptionDetails = await Subscriptions.findOne({
            where: { practice_id: event.user.practice_id, plan_category: 'responding' },
            order: [['createdAt', 'DESC']],
            raw: true
        });
        let plan_type;
        if (input.plan_type == 'free_trial') {
            plan_type = 'FREE TRIAL';
        } else if (input.plan_type == 'pay_as_you_go' || input.plan_type == 'tek_as_you_go') {
            plan_type = 'TEK AS-YOU-GO';
        } else if (respondingMonthlyPlans.includes(input.plan_type)) {
            plan_type = 'MONTHLY';
        } else if (respondingYearlyPlans.includes(input.plan_type)) {
            plan_type = 'YEARLY';
        } else if (propoundingMonthlyPlans.includes(input.plan_type)) {
            plan_type = 'MONTHLY PROPOUNDING';
        } else if (propoundingYearlyPlans.includes(input.plan_type)) {
            plan_type = 'YEARLY PROPOUNDING';
        } else if (['responding_vip', 'propounding_vip'].includes(input.plan_type)) {
            plan_type = 'VIP';
        }
        if (!plan_type) throw new HTTPError(400, `Plan Type not found.`);
        if (existingSubscriptionDetails && existingSubscriptionDetails.stripe_subscription_data) {
            const stripeSubscriptionDataObject = JSON.parse(existingSubscriptionDetails.stripe_subscription_data);
            const subscriptionValidity = new Date(parseInt(stripeSubscriptionDataObject.current_period_end) * 1000);
            const today = new Date();
            if (subscriptionValidity > today) {
                const planDetails = await Plans.findOne({
                    raw: true,
                    where: { stripe_product_id: existingSubscriptionDetails.stripe_product_id },
                });
                validSubscriptionFeatures = planDetails.features_included.split(',');
            }
        }

        let medicalHistorySummeryDetailsPlainText;
        const MedicalHistoryRecords = await MedicalHistory.findAll({
            where: { case_id: input.case_id },
            order: [
                ['createdAt', 'DESC'],
            ],
            raw: true
        });

        const MedicalHistoryPendingRecords = await MedicalHistory.findAll({
            where: { case_id: input.case_id, summery_doc_status: 'pending' },
            order: [
                ['createdAt', 'DESC'],
            ],
            raw: true
        });


        const MedicalHistorySummeryPreviousRecords = await MedicalHistorySummery.findOne({
            where: { case_id: input.case_id },
            order: [
                ['createdAt', 'DESC'],
            ]
        });
        let pagesCount = 0;
        let previousPageCount = 0;
        let medicalHistory_previous_price = 0;
        let medicalhistory_first_set = true;

        if (MedicalHistorySummeryPreviousRecords) {
            previousPageCount = MedicalHistorySummeryPreviousRecords.pages;
            medicalHistory_previous_price = parseFloat(MedicalHistorySummeryPreviousRecords.amount_charged);
            medicalhistory_first_set = false;
        }
        console.log('Preivious Page Count' + previousPageCount, ": dataObject?.ai_scheduler", dataObject?.ai_scheduler);
        for (let i = 0; i < MedicalHistoryRecords.length; i += 1) {
            if (MedicalHistoryRecords[i].pages) {
                pagesCount += MedicalHistoryRecords[i].pages;
            }
            if (MedicalHistoryRecords[i].pages > 1000 && dataObject?.ai_scheduler) {
                dataObject.ai_scheduler = false;
            }
            const updateMedicalObj = await MedicalHistory.findOne({ where: { id: MedicalHistoryRecords[i].id } });
            updateMedicalObj.summery_doc_status = 'ready';
            await updateMedicalObj.save();
        }
        console.log(": dataObject?.ai_scheduler", dataObject?.ai_scheduler);
        console.log('Total Page Count ' + pagesCount);
        const actualPagesCount = pagesCount;
        const clientDetails = await Clients.findOne({ where: { id: input.client_id } });

        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

        const orderData = {
            id: uuid.v4(),
            order_date: new Date(),
            status: 'completed', // completed, pending
            practice_id: event.user.practice_id,
            user_id: event.user.id,
            case_id: input.case_id,
            client_id: input.client_id,
            document_type: 'MEDICAL_HISTORY', // (FROGS, SPROGS, RFPD, RFA)
            document_generation_type: 'medical', // template or final_doc
            amount_charged: 0,
            charge_id: '',
            case_title: input.case_title,
            client_name: clientDetails.name,
            pages: actualPagesCount,
            plan_type: plan_type,
            scheduler_email: true,
            billing_type: practiceObject.billing_type || null
        };

        const settingsObject = await Settings.findOne({ where: { key: 'global_settings' }, raw: true, });

        if (!settingsObject) throw new HTTPError(400, 'Settings data not found');
        let settings = JSON.parse(settingsObject.value);

        const practiceSettings = await PracticeSettings.findOne({
            where: { practice_id: event.user.practice_id, },
            raw: true,
        });
        if (practiceSettings) {
            const practiceSettingsObject = JSON.parse(practiceSettings.value);
            settings = Object.assign(settings, practiceSettingsObject);
        }
        let price = 0;
        let free_tier_available = true;
        let custom_quote_needed = false;
        if (settings.medical_history_pricing_tier) {
            for (let i = 0; i < settings.medical_history_pricing_tier.length; i += 1) {
                const pricingPLan = settings.medical_history_pricing_tier[i];
                if (pagesCount >= pricingPLan.from && pricingPLan.custom_quote_needed) {
                    custom_quote_needed = true;
                } else if (pagesCount >= pricingPLan.from && pagesCount <= pricingPLan.to) {
                    if (!pricingPLan.price) {
                        free_tier_available = true;
                    } else {
                        free_tier_available = false;
                    }
                    price = parseFloat(pricingPLan.price) * pagesCount;
                }
            }
            if (settings.medical_history_minimum_pricing) {
                const minimumPricing = parseFloat(settings.medical_history_minimum_pricing);
                if (price < minimumPricing) {
                    price = minimumPricing;
                }
            }

            if (price) {
                price = parseFloat(price.toFixed(2));
            }
        } else {
            return {
                statusCode: 400,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify({
                    status: 'errpr',
                    message: 'Pricing tier data not available',
                }),
            };
        }

        if (!medicalhistory_first_set) {
            if (validSubscriptionFeatures.length && price) {
                price *= 0.90; //10 % discound for Yearly Subscription
            }
            price = parseFloat(price).toFixed(2);
            price = parseFloat(price);
            console.log('second set price ' + price);
            if (price > medicalHistory_previous_price) {
                price = price - medicalHistory_previous_price;
                price = parseFloat(price);
                console.log('second set price inside if ' + price);
            } else {
                price = 0;
            }
        } else {
            console.log('first set price inside if ' + price);
            if (validSubscriptionFeatures.length && price) {
                price *= 0.90; //10 % discound for Yearly Subscription
            }
            price = parseFloat(price).toFixed(2);
            console.log('first set price after discount ' + price);
        }

        if (custom_quote_needed) {
            return {
                statusCode: 400,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify({
                    status: 'errpr',
                    message: 'Custom quote required',
                }),
            };
        }

        let credit_card_details_available = false;

        if (practiceObject && practiceObject.stripe_customer_id) {
            credit_card_details_available = true;
        }

        if (price <= 0) {
            const orderDataModel = await Orders.create(orderData);
            if (price < 0) {
                price = 0;
            }
            //medicalHistorySummeryDetails
            if (MedicalHistorySummeryPreviousRecords) {

                let currentPageCount = pagesCount - previousPageCount;
                if (currentPageCount < 0) currentPageCount = -1 * currentPageCount;
                previousPageCount = currentPageCount + previousPageCount;

                medicalHistory_previous_price += parseFloat(price);
                medicalHistory_previous_price = parseFloat(medicalHistory_previous_price).toFixed(2);
                MedicalHistorySummeryPreviousRecords.amount_charged = medicalHistory_previous_price;
                MedicalHistorySummeryPreviousRecords.pages = previousPageCount;
                MedicalHistorySummeryPreviousRecords.order_id = orderDataModel.id;
                MedicalHistorySummeryPreviousRecords.request_date = dataObject.request_date;

                /* scheduler changes selva */
                MedicalHistorySummeryPreviousRecords.ai_scheduler = dataObject?.ai_scheduler
                MedicalHistorySummeryPreviousRecords.ai_status = dataObject?.ai_status


                await MedicalHistorySummeryPreviousRecords.save();
                medicalHistorySummeryDetailsPlainText = await MedicalHistorySummeryPreviousRecords.get({ plain: true });
                console.log("Summery previous 1");
            } else {
                let medicalHistorySummeryDetailsFreeTier = await MedicalHistorySummery.create(dataObject);
                medicalHistorySummeryDetailsFreeTier.amount_charged = price;
                medicalHistorySummeryDetailsFreeTier.pages = pagesCount;
                medicalHistorySummeryDetailsFreeTier.order_id = orderDataModel.id;
                await medicalHistorySummeryDetailsFreeTier.save();
                medicalHistorySummeryDetailsPlainText = await medicalHistorySummeryDetailsFreeTier.get({ plain: true });
            }

        } else {
            if (!practiceObject.stripe_customer_id) {
                throw new HTTPError(400, 'Free tire exceeded but payment method is not added yet');
            }
            const paymentMethods = await stripe.paymentMethods.list({
                customer: practiceObject.stripe_customer_id,
                type: 'card',
            });
            if (!paymentMethods.data.length) {
                throw new HTTPError(400, 'Free tire exceeded but payment method not added');
            }

            if (price && price > 0 && price < 0.50) {
                price = 0.50;
            }

            const paymentMethod = paymentMethods.data[0];
            const paymentIntent = await stripe.paymentIntents.create({
                amount: parseInt(price * 100),
                currency: 'usd',
                customer: practiceObject.stripe_customer_id,
                payment_method: paymentMethod.id,
                off_session: true,
                confirm: true,
            });
            orderData.charge_id = paymentIntent.id;
            orderData.amount_charged = price;
            const orderDataModel = await Orders.create(orderData);
            console.log(`Amount charged is: ${price}`);

            if (MedicalHistorySummeryPreviousRecords) {
                let currentPageCount = pagesCount - previousPageCount;
                if (currentPageCount < 0) currentPageCount = -1 * currentPageCount;
                previousPageCount = currentPageCount + previousPageCount;

                medicalHistory_previous_price += parseFloat(price);
                medicalHistory_previous_price = parseFloat(medicalHistory_previous_price).toFixed(2);
                MedicalHistorySummeryPreviousRecords.amount_charged = medicalHistory_previous_price;
                MedicalHistorySummeryPreviousRecords.pages = previousPageCount;
                MedicalHistorySummeryPreviousRecords.order_id = orderDataModel.id;
                MedicalHistorySummeryPreviousRecords.request_date = dataObject.request_date;

                /* scheduler changes selva */
                MedicalHistorySummeryPreviousRecords.ai_scheduler = dataObject?.ai_scheduler
                MedicalHistorySummeryPreviousRecords.ai_status = dataObject?.ai_status

                await MedicalHistorySummeryPreviousRecords.save();
                medicalHistorySummeryDetailsPlainText = await MedicalHistorySummeryPreviousRecords.get({ plain: true });
                console.log("Summery previous 2");
            } else {
                let medicalHistorySummeryDetailsPayable = await MedicalHistorySummery.create(dataObject);
                medicalHistorySummeryDetailsPayable.amount_charged = price;
                medicalHistorySummeryDetailsPayable.pages = pagesCount;
                medicalHistorySummeryDetailsPayable.order_id = orderDataModel.id;
                await medicalHistorySummeryDetailsPayable.save();
                medicalHistorySummeryDetailsPlainText = await medicalHistorySummeryDetailsPayable.get({ plain: true });
            }
        }
        /* Invoce Deatails  */
        // medicalHistorySummeryDetailsPlainText = medicalHistorySummeryDetails;
        if (medicalHistorySummeryDetailsPlainText.pages > 0 &&
            medicalHistorySummeryDetailsPlainText.amount_charged > 0) {
            //practiceObject
            const caseDetails = await Cases.findOne({ where: { id: medicalHistorySummeryDetailsPlainText.case_id } });
            const ordersDetails = await Orders.findOne({ where: { id: medicalHistorySummeryDetailsPlainText.order_id } });
            const description = caseDetails.case_title + " " + "(Medical History)";
            const invoiceData = {};
            invoiceData.id = uuid.v4();
            invoiceData.user_id = event.user.id;
            invoiceData.medical_summery_id = medicalHistorySummeryDetailsPlainText.id;
            invoiceData.practice_id = medicalHistorySummeryDetailsPlainText.practice_id;
            invoiceData.client_id = medicalHistorySummeryDetailsPlainText.client_id;
            invoiceData.case_id = medicalHistorySummeryDetailsPlainText.case_id;
            invoiceData.order_id = medicalHistorySummeryDetailsPlainText.order_id;
            invoiceData.invoice_date = new Date();
            invoiceData.invoice_type = 'Paid';
            invoiceData.qty = MedicalHistoryPendingRecords.length;
            invoiceData.description = description;
            invoiceData.total = ordersDetails.amount_charged;
            invoiceData.subtotal = ordersDetails.amount_charged;
            invoiceData.total_due = ordersDetails.amount_charged;
            const invoiceDetails = await Invoices.create(invoiceData);

            const fileupload = {};
            fileupload.file_name = caseDetails.case_title;
            fileupload.invoice_no = invoiceDetails.invoice_no;
            fileupload.practice_id = invoiceDetails.practice_id;
            const link = await UploadInvoiceFile(fileupload);

            invoiceDetails.invoice_name = caseDetails.case_title + " " + invoiceDetails.invoice_no + ".pdf";
            invoiceDetails.s3_file_key = link.s3_file_key;
            const invoiceDataSave = await invoiceDetails.save();
            const invoicePlainTet = invoiceDataSave.get({ plain: true });
            /* Date  */
            const invoicefullDate = new Date(invoicePlainTet.invoice_date);

            let invoiceDate = invoicefullDate.getDate();
            let invoiceMonth = invoicefullDate.getMonth() + 1;
            if (invoiceMonth < 10) invoiceMonth = '0' + invoiceMonth;
            let invoiceYear = invoicefullDate.getFullYear();
            invoiceYear = invoiceYear.toString().substr(-2);

            invoicePlainTet.invoice_date = invoiceMonth + '/' + invoiceDate + '/' + invoiceYear;
            invoicePlainTet.uploadURL = link.uploadURL;
            invoicePlainTet.public_url = link.public_url;
            invoicePlainTet.name = practiceObject.name;
            invoicePlainTet.phone_no = practiceObject.phone;
            invoicePlainTet.practice_address = practiceObject.address;
            invoicePlainTet.practice_street = practiceObject.street;
            invoicePlainTet.city = practiceObject.city;
            invoicePlainTet.state = practiceObject.state;
            invoicePlainTet.zip_code = practiceObject.zip_code;
            medicalHistorySummeryDetailsPlainText.invoice_pdf_data = invoicePlainTet;
            console.log("Inside if");
        }
        /* selva changes starts here */
        // new Promise((resolve, reject) => {
        //     startMedicalSummarybackgroundProcess(event, function (err, data) {
        //         if (err) {
        //             reject(err)
        //         } else {
        //             console.log("background process: ", data)
        //             resolve(data)
        //         }
        //     })
        // });
        /* selva changes starts here */

        console.log("dataObject?.ai_scheduler", JSON.stringify(dataObject))
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(medicalHistorySummeryDetailsPlainText),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ err, error: err.message || 'Could not create the medicalHistorySummeryDetails.' }),
        };
    }
};

const getSecuredPublicUrlForPrivateFile = async (medical_doc) => {
    try {
        AWS.config.update({ region: process.env.S3_CLIENT_SIGNATURE_BUCKET_REGION });
        const publicUrl = await s3.getSignedUrlPromise('getObject', {
            Bucket: process.env.S3_BUCKET_FOR_PRIVATE_ASSETS,
            Expires: 60 * 60 * 1,
            Key: medical_doc.s3_file_key,
        });
        let res = {
            public_url: publicUrl,
            file_name: medical_doc.file_name,
            s3_file_key: medical_doc.s3_file_key
        };
        return res;
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }
};

const getOne = async (event) => {
    try {
        authorizeGetOne(event.user);
        const params = event.params || event.pathParameters;
        validateGetOneMedicalHistorySummery(params);
        const { MedicalHistorySummery, MedicalHistory, Clients, Cases } = await connectToDatabase();
        const medicalHistorySummeryDetails = await MedicalHistorySummery.findOne({ where: { id: params.id } });
        if (!medicalHistorySummeryDetails) throw new HTTPError(404, `MedicalHistorySummery with id: ${params.id} was not found`);
        const plainMedicalHistorySummery = medicalHistorySummeryDetails.get({ plain: true });
        const medical_doc_array = [];
        const medicalHistory = await MedicalHistory.findAll({
            where: { case_id: plainMedicalHistorySummery.case_id, summery_doc_status: 'ready' },
            attributes: ['id', 'practice_id', 'client_id', 'createdAt', 'case_id', 's3_file_key', 'file_name', 'comment', 'pages', 'keys', 'textract_status', 'order_date', 'summery_doc_status', 'status'],
            order: [
                ['createdAt', 'DESC'],
            ],
            raw: true,
        });
        for (let i = 0; i < medicalHistory.length; i++) {
            let obj = await getSecuredPublicUrlForPrivateFile(medicalHistory[i]);
            medical_doc_array.push(obj);
        }
        const clinetDetails = await Clients.findOne({ where: { id: plainMedicalHistorySummery.client_id }, raw: true });
        const caseDetails = await Cases.findOne({ where: { id: plainMedicalHistorySummery.case_id }, raw: true });
        plainMedicalHistorySummery.medicalHistory = medicalHistory;
        plainMedicalHistorySummery.client_name = clinetDetails?.name;
        plainMedicalHistorySummery.dob = clinetDetails?.dob;
        plainMedicalHistorySummery.date_of_loss = caseDetails?.date_of_loss;
        plainMedicalHistorySummery.medicalDoc_public_url = medical_doc_array;
        if (plainMedicalHistorySummery.summery_documents) {
            plainMedicalHistorySummery.summery_documents = JSON.parse(plainMedicalHistorySummery.summery_documents);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainMedicalHistorySummery),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the MedicalHistorySummery.' }),
        };
    }
};

/* const getAll = async (event) => {
  try {
    //authorizeGetAll(event.user);
    const query = event.queryStringParameters || {};
    if (query.offset) query.offset = parseInt(query.offset, 10);
    if (query.limit) query.limit = parseInt(query.limit, 10);
    if (query.where) query.where = JSON.parse(query.where);
    if (!query.where) {
      query.where = {};
    }
    // query.where.practice_id = event.user.practice_id;
    query.order = [
      ['createdAt', 'DESC'],
    ];
    validateReadMedicalHistorySummery(query);
    query.raw = true;
    const { MedicalHistorySummery } = await connectToDatabase();
    const medicalHistorySummeryDetails = await MedicalHistorySummery.findAll(query);
    for(let i=0;i<medicalHistorySummeryDetails.length;i++){
      if(medicalHistorySummeryDetails[i].summery_documents){
        medicalHistorySummeryDetails[i].summery_documents = JSON.parse(medicalHistorySummeryDetails[i].summery_documents);
      }
    }
    return {
      statusCode: 200,
      headers: { 'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true },
      body: JSON.stringify(medicalHistorySummeryDetails),
    };
  } catch (err) {
    console.log(err);
        return {
      statusCode: err.statusCode || 500,
      headers: { 'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true },
      body: JSON.stringify({ error: err.message || 'Could not fetch the medicalHistorySummeryDetails.' }),
    };
  }
}; */

/* list all optimize start*/

const getAll_new_pagination = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let filterQuery = '';
        let sortQuery = '';
        let searchQuery = '';
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let codeSnip = '';
        let codeSnip2 = '';
        let where = `WHERE `;
        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' createdAt >= "' + event?.query?.from_date + '" AND createdAt <= "' + event?.query?.to_date + '" AND ';
        }
        /**Filter**/
        if (query.filter != 'false') {
            query.filter = JSON.parse(query.filter);
            filterKey.status = query.filter.status;
            if (filterKey.status != 'all' && filterKey.status != '' && filterKey.status) {
                filterQuery += `status LIKE '%${filterKey.status}%'`;
            }
        }
        /**Sort**/
        if (query.sort == 'false') {
            // sortQuery += ` ORDER BY createdAt DESC`
            sortQuery += ` ORDER BY request_date DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` practice_name LIKE '%${searchKey}%' OR status LIKE '%${searchKey}%' OR assigned_to LIKE '%${searchKey}%' OR request_date LIKE '%${searchKey}%' OR case_number LIKE '%${searchKey}%' OR case_title LIKE '%${searchKey}%' OR pages LIKE '%${searchKey}%' OR amount_charged LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + sortQuery;
            codeSnip2 = where + filterQuery + sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery != '') {
            codeSnip = sortQuery;
            codeSnip2 = sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + '(' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + '(' + searchQuery + ')' + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where + searchQuery;
            codeSnip2 = where + searchQuery;
        } else if (searchQuery != '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')';
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery;
            codeSnip2 = where + filterQuery;
        }

        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let sqlQuery = 'select * from MedicalHistorySummeries ' + codeSnip;

        let sqlQueryCount = 'select * from MedicalHistorySummeries ' + codeSnip2;

        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });
        for (let i = 0; i < serverData.length; i++) {
            if (serverData[i].summery_documents) {
                serverData[i].summery_documents = JSON.parse(serverData[i].summery_documents);
            }
        }
        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });

        let totalPages, currentPage;
        if (query.limit && (serverData.length > 0)) {
            totalPages = Math.ceil(TableDataCount.length / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const serverDetails = {
            total_items: TableDataCount.length,
            response: serverData,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let filterQuery = '';
        let sortQuery = '';
        let searchQuery = '';
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let codeSnip = '';
        let codeSnip2 = '';
        let where = `WHERE `;
        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' createdAt >= "' + event?.query?.from_date + '" AND createdAt <= "' + event?.query?.to_date + '" AND ';
        }
        console.log(query);

        /**Filter**/
        if (query.filter != 'false') {
            query.filter = JSON.parse(query.filter);
            filterKey.status = query.filter.status;
            if (filterKey.status != 'all' && filterKey.status != '' && filterKey.status) {
                filterQuery += `status LIKE '%${filterKey.status}%'`;
            }
        }
        /**Sort**/
        if (query.sort == 'false') {
            // sortQuery += ` ORDER BY createdAt DESC`
            sortQuery += ` ORDER BY request_date DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` practice_name LIKE '%${searchKey}%' OR status LIKE '%${searchKey}%' OR assigned_to LIKE '%${searchKey}%' OR request_date LIKE '%${searchKey}%' OR case_number LIKE '%${searchKey}%' OR case_title LIKE '%${searchKey}%' OR pages LIKE '%${searchKey}%' OR amount_charged LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + sortQuery;
            codeSnip2 = where + filterQuery + sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery != '') {
            codeSnip = sortQuery;
            codeSnip2 = sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + '(' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + '(' + searchQuery + ')' + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where + searchQuery;
            codeSnip2 = where + searchQuery;
        } else if (searchQuery != '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')';
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery;
            codeSnip2 = where + filterQuery;
        }

        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let sqlQuery = 'select id, practice_id, case_id, practice_name, status, assigned_to, request_date, case_number, case_title, pages, amount_charged  from MedicalHistorySummeries ' + codeSnip;

        let sqlQueryCount = 'select id, practice_id, case_id, practice_name, status, assigned_to, request_date, case_number, case_title, pages, amount_charged from MedicalHistorySummeries ' + codeSnip2;

        console.log(sqlQuery);
        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });
        for (let i = 0; i < serverData.length; i++) {
            if (serverData[i].summery_documents) {
                serverData[i].summery_documents = JSON.parse(serverData[i].summery_documents);
            }
        }
        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
};
/*list all optimize end*/

const update = async (event) => {
    try {
        authorizeUpdate(event.user);
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        validateUpdateMedicalHistorySummery(input);
        const { MedicalHistorySummery, Orders, Users, Op } = await connectToDatabase();
        const medicalHistorySummeryDetails = await MedicalHistorySummery.findOne({ where: { id: params.id } });
        if (!medicalHistorySummeryDetails) throw new HTTPError(404, `Medical History with id: ${params.id} was not found`);
        /*  if (input.summery_url) {
           input.summery_url = input.summery_url;
           } */
        if (input.summery_documents) {
            input.summery_documents = JSON.stringify(input.summery_documents);
        }
        const updatedModel = Object.assign(medicalHistorySummeryDetails, input);
        const updateData = await updatedModel.save();
        const updatePlainDetails = updateData.get({ plain: true });
        let summerdocument_length = 0;
        if (updatePlainDetails.summery_documents) {
            updatePlainDetails.summery_documents = JSON.parse(updatePlainDetails.summery_documents);
            summerdocument_length = Object.keys(updatePlainDetails.summery_documents).length;
        }
        console.log(updatePlainDetails);
        console.log(summerdocument_length);

        if (updatePlainDetails.status == 'Complete' && summerdocument_length != 0) {
            const orderDetails = await Orders.findOne({ where: { id: medicalHistorySummeryDetails.order_id } });
            if (orderDetails && orderDetails.user_id) {
                const userObject = await Users.findOne({
                    where: {
                        id: orderDetails.user_id,
                        is_deleted: { [Op.not]: true }
                    }
                })
                if (userObject && userObject.email) {
                    console.log(userObject.email);
                    let template = 'Hi';
                    if (userObject?.name) template = template + ' ' + userObject.name;
                    await sendEmail(userObject.email, 'Medical Summary Document Updated',
                        `${template}, <br/><br/> EsquireTek Medical Expert has created a medical summary document for case “${medicalHistorySummeryDetails.case_title}”. Document is available for download from case detail page. <br/><br/> Notification from EsquireTek.`, 'EsquireTek');
                }
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatePlainDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the MedicalHistorySummery.' }),
        };
    }
};

const destroy = async (event) => {
    try {
        authorizeDestroy(event.user);
        const params = event.params || event.pathParameters;
        validateDeleteMedicalHistorySummery(params);
        const { MedicalHistorySummery } = await connectToDatabase();
        const medicalHistorySummeryDetails = await MedicalHistorySummery.findOne({ where: { id: params.id } });
        if (!medicalHistorySummeryDetails) throw new HTTPError(404, `MedicalHistorySummery with id: ${params.id} was not found`);
        await medicalHistorySummeryDetails.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(medicalHistorySummeryDetails),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy fetch the MedicalHistorySummery.' }),
        };
    }
};


const getUploadURL = async (input) => {
    let fileExtention = '.pdf';
    const s3Params = {
        Bucket: process.env.S3_BUCKET_FOR_INVOICES,
        Key: `${input.practice_id}/${input.file_name}` + " " + `${input.invoice_no}` + `${fileExtention}`,
        /* ContentType: input.content_type, */
        ContentType: "application/pdf",
        ACL: 'public-read',
    };
    console.log(s3Params);
    return new Promise((resolve, reject) => {
        const uploadURL = s3.getSignedUrl('putObject', s3Params);
        resolve({
            uploadURL,
            s3_file_key: s3Params.Key,
            public_url: `https://${s3Params.Bucket}.s3.amazonaws.com/${s3Params.Key}`,
        });
    });
};
const UploadInvoiceFile = async (event) => {
    try {
        const uploadURLObject = await getUploadURL(event);
        const response = {};
        response.uploadURL = uploadURLObject.uploadURL;
        response.s3_file_key = uploadURLObject.s3_file_key;
        response.public_url = uploadURLObject.public_url;
        return response;
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }
};

module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;