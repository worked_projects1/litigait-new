const { OpenAIApi, default: OpenAI } = require("openai");
const AWS = require('aws-sdk')
const s3 = new AWS.S3();
const fs = require('fs');
const { HTTPError } = require("../../utils/httpResp");
const { encode, decode } = require('gpt-3-encoder')
const axios = require('axios');
const connectToDataBase = require('../../db');
const { sendEmail } = require('../../utils/mailModule');
const { texractFromAWS, JSONmerger } = require('./openaiHelper');
const { QueryTypes } = require('sequelize');
const { getSecrets } = require('../helpers/secrets.helper')
const pluralize = require('pluralize');


const MODEL = 'gpt-4o'; // previously used model: 'gpt-4'
const MAXIMUM_TOKEN = 3400;
const OVERLAP_TOKEN = 1000;
const toEmailIds = ["r.aravinth.rifluxyss@gmail.com", "prithiviraj.rifluxyss@gmail.com","bhuvaneshwari.rifluxyss@gmail.com"];
const reminderEmail = ["siva@miotiv.com"];
const expertEmailIds = process.env.MEDICAL_EXPERTS;

const validateToken = function (text) {
    try {
        const encoded = encode(text);
        if (MAXIMUM_TOKEN < encoded.length) {
            throw new HTTPError(400, `File too large`);
        }
    } catch (err) {
        console.log(err);
        return err;
    }

}

const getObject = async (fileKey) => {
    try {
        let params = {
            Bucket: process.env.S3_BUCKET_FOR_DUPLICATE_MEDICAL_SUMMERY,
            Key: fileKey
        };
        let response = await s3.getObject(params).promise();
        return response.Body;
    } catch (err) {
        console.log(err);
        return err;
    }
}

const extractTextFromPdf = async (file_key) => {
    try {
        console.log("^^^^^^^^^^^^^^^^ Intside Extract Request ^^^^^^^^^^^^^^^^^");
        let fileText =
            // const pdf2image = require('pdf-img-convert');

            // const publicUrl = await s3.getSignedUrlPromise('getObject', {
            //     Bucket: process.env.S3_BUCKET_FOR_PRIVATE_ASSETS,
            //     Expires: 60 * 60 * 1,
            //     Key: file_key,
            // });
            // console.log("publicUrl:", publicUrl);
            // const pages = await pdf2image.convert(publicUrl, { width: 1024, height: 768 });
            // console.log("???????????????? After Pages ?????????????");
            // let i = 0;
            // const pageBlocks = [];
            // for (let t = 0; t < pages.length; t++) {
            //     console.log(' Pages : ' + (t + 1));
            //     let blocks = await textract.detectDocumentText({ Document: { Bytes: pages[t] } }).promise();
            //     pageBlocks.push(blocks);
            // }
            // // const pageBlocks = pages.map(async (pages) => {
            // //     console.log(' Pages : '+(i+1));
            // //     i++;
            // //     let blocks = await textract.detectDocumentText({ Document: { Bytes: pages } }).promise();
            // //     return blocks;
            // // });
            // console.log(pageBlocks);
            // //const pageBlocks = blocksPromise;
            // console.log(" @#@##@#@#@#@#@#@#@#@ After Page Blocks @#@##@#@#@#@#@#@#@#@");

            // const fileText = pageBlocks.map((blocksObj) => {
            //     let text = blocksObj.Blocks.filter(block => block.BlockType == 'LINE').map(i => i.Text).join('\n');
            //     return text;
            // }).join('');
            console.log("@@@@@@@@@@@@@@@ Extraction Completed @@@@@@@@@@@@@@@@@@");
        return fileText;
    } catch (err) {
        console.log('===== Question Extraction Error =======');
        console.log(err);
    }
}

// const extractTextFromPdf = async (event) => {
//     try {
//         const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
//         const startCommand = new StartDocumentTextDetectionCommand({
//             DocumentLocation: {
//                 S3Object: {
//                     Bucket: process.env.S3_BUCKET_FOR_DUPLICATE_MEDICAL_SUMMERY,
//                     Name: input.file_key,
//                 },
//             },
//             FeatureTypes: ["TABLES", "FORMS"], // Optional: Specify the features you want to extract
//         });
//         const startResponse = await client.send(startCommand);
//         const jobId = startResponse.JobId;
//         const getCommand = new GetDocumentTextDetectionCommand({ JobId: jobId });
//         const getResponse = await client.send(getCommand);
//         console.log('getResponse:', getResponse)
//         // Access the extracted text
//         const checkStatus = async (getResponse) => {
//             return new Promise((resolve, reject) => {
//                 // Assume you have an object called 'data' that contains the status
//                 if (getResponse.JobStatus === 'SUCCESS') {
//                     resolve(getResponse);
//                 } else if (getResponse.status === 'IN_PROGRESS') {
//                     reject('pending...');
//                 } else {
//                     setTimeout(checkStatus(getResponse), 1000); // Check again after 1 second
//                 }
//             });
//         };
//         const newRes = await checkStatus(getResponse);
//         // console.log("newRes:", newRes)
//         const textDetections = getResponse.Blocks.filter(block => block.BlockType === 'LINE');
//         const extractedText = textDetections.map(block => block.Text).join(' ');
//         console.log(extractedText);
//         return {
//             statusCode: 200,
//             headers: {
//                 'Content-Type': 'text/plain',
//                 'Access-Control-Allow-Origin': '*',
//                 'Access-Control-Allow-Credentials': true
//             },
//             body: JSON.stringify({ extractedText })
//         }
//     } catch (err) {
//         console.log(err);
//         return {
//             statusCode: err.statusCode || 500,
//             headers: {
//                 'Content-Type': 'text/plain',
//                 'Access-Control-Allow-Origin': '*',
//                 'Access-Control-Allow-Credentials': false
//             },
//             body: JSON.stringify({ error: err.message || 'Could not fetch the Users.' }),
//         };
//     }
// }


const getFileText = async (file_key) => {
    try {
        const buffer = fs.readFileSync(file_key); // for local
        return buffer.toString();
    } catch (err) {
        console.log(err);
        throw new HTTPError(400, `Unable to read the file: '${file_key}'.`)
    }

}

// For siva
// input
// @param 'file' - local file path
const getSummaryDetails = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const file_path = input.file_path;
        const fileText = await getFileText(file_path);
        //validateToken(fileText);
        const gptResponse = await getAnswers(fileText);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(JSON.parse(gptResponse))
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': false
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Users.' }),
        };
    }
}

const getBillingSummaryInput = async (case_id) => {
    const { MedicalHistory } = await connectToDataBase();
    try {
        let billingSummaryObj = {
            client_name: "",
            date_of_injury: ""
        };
        const medicalRecords = await MedicalHistory.findAll({
            where: { case_id: case_id }
        });
        
         let totalBillingDetails = medicalRecords.map((doc) => {
            try {
                let billingDetails = doc.ai_billing_summery && typeof doc.ai_billing_summery == 'string' ? JSON.parse(doc.ai_billing_summery) : [];
                return billingDetails[0];
            } catch (err) {
                return null;
            }
         }).filter((val) => val != null);
         billingSummaryObj.billing = totalBillingDetails;
         return billingSummaryObj;
    } catch (err) {
        console.log("totalBillingDetails:", err)
    }
}

const getBillingSummary = async (billingSummaryObj, summery_request_id) => {
    const { MedicalHistorySummery } = await connectToDataBase();
    try {
        const SCANNED_URL = process.env?.SCANNED_URL ? process.env.SCANNED_URL : await getSecrets('SCANNED_URL');
        console.log("billingSummaryObj: ", billingSummaryObj)
        const billingSummaryRes = await axios.post(`${SCANNED_URL}/medicalBillGen`, billingSummaryObj)
            .then((response) => response.data)
            .catch((error) => {
                throw new Error(`Failed to get Billing Summary url: ${error}`);
            });
        return billingSummaryRes;
    } catch (err) {
        console.log('Error while getting billing summary doc url: ', err);
        await MedicalHistorySummery.update({ ai_scheduler: false, ai_status: 'Failed', reason: 'Failed while getting Billing Summary url' }, { where: { id: summery_request_id } });
    }
}

const getMedicalDocUrl = async (summaryDocObj, summery_request_id) => {
    const SCANNED_URL = process.env?.SCANNED_URL ? process.env.SCANNED_URL : await getSecrets('SCANNED_URL');
    const { MedicalHistorySummery } = await connectToDataBase();
    try {
        console.log("summaryDocObj: ", summaryDocObj)
        const summaryDocRes = await axios.post(`${SCANNED_URL}/medicalDocGen`, summaryDocObj)
            .then((response) => response.data)
            .catch((error) => {
                throw new Error(`Failed to get Medical Document url: ${error}`);
            });
        return summaryDocRes;
    } catch (err) {
        console.log('Error while getting summary doc url: ', err);
        await MedicalHistorySummery.update({ ai_scheduler: false, ai_status: 'Failed' }, { where: { id: summery_request_id } });
    }
}

const applyStylesForTreatments = async (treatments, procedure) => {
    try {
        const { Settings } = await connectToDataBase();
        const settingsObj = await Settings.findOne({
            where: { key: 'global_settings' }
        });

        // removing the headings
        // let headingsToIgnore = [];
        // if (JSON.parse(settingsObj.value).headings_to_ignore) {
        //     headingsToIgnore = JSON.parse(settingsObj.value).headings_to_ignore.length > 0 ? JSON.parse(settingsObj.value).headings_to_ignore : [];
        //     headingsToIgnore = headingsToIgnore.split(',')
        // }

        // if (headingsToIgnore.length > 0) {
        //     headingsToIgnore = headingsToIgnore.map((headings) => headings.toLowerCase())
        // }

        // Adding only the given hadings in the ai prompt settings - 'Headings to Add'
        let headingsToAdd = [];
        if (JSON.parse(settingsObj.value).headings_to_add) {
            headingsToAdd = JSON.parse(settingsObj.value).headings_to_add;
            headingsToAdd = JSON.parse(headingsToAdd);
            headingsToAdd = headingsToAdd.map(heading => heading.toLowerCase().trim());
        }


        let treatmentString = procedure && treatments.length > 0 ? `<p><strong>Procedure</strong></p><p>${procedure}</p><p></p>` : '';
        treatments.forEach((content) => {
            if( content && content.length > 1 && content[1] && (headingsToAdd.includes(content[0].toLowerCase()) || headingsToAdd.includes(content[0].toLowerCase().slice(0, -1)) || headingsToAdd.includes(pluralize.singular(content[0]).toLowerCase()) || 
            headingsToAdd.includes(pluralize.plural(content[0]).toLowerCase()) )) {
                console.log("Adding:", content[0])
                treatmentString += `<p><strong>${content[0]}</strong></p><p>${content[1]}</p><p></p>`
            }
        });
        return treatmentString;
    } catch (err) {
        // await MedicalHistorySummery.update({ ai_status: 'Failed', reason: 'Error while applying styles' }, { where: { id: summery_request_id } });
        console.log('err:', err)
        throw new HTTPError(400, `Getting wrong answer format from AI: ${treatments}`)
    }
}

const prepareSummaryDocInput = async (answers, summery_request_id) => {
    const { MedicalHistorySummery } = await connectToDataBase();
    try {
        let columns = new Map([[1, "date"], [2, "facility"], [3, "treatment"], [4, "link"]]);
        const result = [];

        for (let i = 0; i < answers.length; i++) {
            let answer = answers[i];
            try {
                answer = typeof answer == 'string' ? JSON.parse(answer) : answer; 
                // we need to parse twice because of getting string type response from AI
                answer = typeof answer == 'string' ? JSON.parse(answer) : answer;
                if (!answer) {
                    console.log("Skipped answer:", answer)
                    continue;
                }
            } catch (err) {
                console.log("Skipped answer:", answer)
                continue;
            }
            if (answer?.date_of_services) {
                let promises = answer.date_of_services.map( async (services) => {
                    let newObj = {};
                    let treatment = await applyStylesForTreatments(services.treatment, services.procedure)
                    Object.assign(newObj, {
                        date: services.date_of_service,
                        facility: answer.hospital_name,
                        treatment: treatment,
                        link: `${answer.file_name}`
                        // link: `<a href='#'>${answer.file_name}</a>`
                    });
                    if (treatment) result.push(newObj);
                    return newObj;
                });
                await Promise.all(promises);
            }

        }
        const summaryDocInput = [];
        result.forEach((resultObj, row_id) => {
            columns.forEach((column_name, column_id) => {
                summaryDocInput.push({
                    "row_id": row_id + 1,
                    "row_index": row_id + 1,
                    "row_section_id": column_id,
                    "key": column_name,
                    "extracted_text": [
                        result[row_id][column_name]
                    ]
                });
            })
        });

        // const summaryDocInputResponse = {
        //     client_name: 'MONICA OCHOA',
        //     date_of_injury: JSON.parse(answers[0]).date_of_injury,
        //     dob: '07/18/1979',
        //     key: summaryDocTableData
        // }
        let date_of_injury = JSON.parse(answers[0])?.date_of_injury || "";
        return { summaryDocInput, date_of_injury: date_of_injury };
    } catch (err) {
        await MedicalHistorySummery.update({ ai_status: 'Failed' }, { where: { id: summery_request_id } });
        console.log('err:', err)
        throw new HTTPError(400, `Getting wrong answer format from AI: ${answers}`)
    }
}

const getAnswers = async (file) => {
    try {
        console.log('Inside AI Request');
        const OPENAI_API_KEY = process.env?.OPENAI_API_KEY ? process.env.OPENAI_API_KEY : await getSecrets('OPENAI_API_KEY');
        const openai = new OpenAI({ apiKey: OPENAI_API_KEY });
        const completion = await openai.chat.completions.create({
            model: MODEL,
            temperature: 0,
            messages: [{
                role: "user", content: `${file} 
    
                Please split the above context by its Date Of Service (DOS) and get the answer for the below question under each Date Of Services. 
    Save the answers in json format like below and add '<br></br>' tag before and after the <CONTENT>. Please set the value/<content> as 'NO', If no answers found.
                
    1.Get the 'Patient Name:'.
    2.Get the patient 'Date of Birth:' or 'DOB'.
    3.Get the patient 'Date of Injury' or 'DOI'.
    4.Get the Hospital name.
    5.Get the content under 'Cheif Complaint:'.
    6.Get the content under 'History Of Present Illness:' or 'HPI'.
    7.Get the content under 'Medical Decision Making:'.
    8.Get the content under 'Findings:'.
    9.Get the content under 'Impression:'.
    10.Get the content under 'Diagnosis:'.
    11.Get the content under 'Disposition:'.
    13.Get the content under 'Narrative'.
    14.Get the content under 'Objective'.
    15.Get the content under 'Assessment'.
    16.Get the content under 'Plan'.
                
    {
    "patient_name": "",
    "date_of_birth": "",
    "date_of_injury": "",
    "hospital_name": "",
    "date_of_services": [{
    "date_of_service": "",
    "treatment": "<strong>Chief Complaint:</strong> <br></br><CONTENT><br></br> <strong>History Of Present Illness:</strong>  <br></br><CONTENT><br></br>  <strong>Medical Decision Making:</strong> <br></br><CONTENT><br></br> <strong>Findings:</strong> <br></br><CONTENT><br></br>  <strong>Impression:</strong> <br></br><CONTENT><br></br> <strong>Diagnosis:</strong> <br></br><CONTENT><br></br> <strong>Disposition:</strong>  <br></br><CONTENT><br></br> <strong>Narrative:</strong>  <br></br><CONTENT><br></br> <strong>Objective:</strong>  <br></br><CONTENT><br></br> <strong>Assessment:</strong>  <br></br><CONTENT><br></br> <strong>Plan:</strong>  <br></br><CONTENT><br></br>"
    }]
    }`}],
        });
        const answer = completion.choices[0].message.content;
        console.log('........... Inside AI Request Completed ...........');
        return answer;
    } catch (err) {
        console.log('===================== AI Error Block=================');
        console.log(err);
    }
}

// const getTextByPdfFileKey = async (file_key) => {
//     // const res = await axios.post(`${process.env.SCANNED_URL}/medicalDocConv?key=${encodeURIComponent(file_key)}`, {});
//     const response = await axios.post(`${process.env.SCANNED_URL}/medicalDocConv?key=${encodeURIComponent(file_key)}`, {})
//         .then((response) => response.data)
//         .catch((error) => {
//             throw new Error(`Failed to extract pdf: ${error.message}`);
//         });
//     const textRes = await getObject(response.data.s3_key);
//     let text = textRes.toString();
//     return text;
// }

const getDownloadUrl = async (s3_file_key) => {
    const publicUrlRes = await s3.getSignedUrlPromise('getObject', {
        Bucket: process.env.S3_BUCKET_FOR_DUPLICATE_MEDICAL_SUMMERY,
        Expires: 60 * 60 * 1,
        Key: s3_file_key,
    });
    return publicUrlRes
}

// @param - list of file_keys
const getSummaryDoc = async (file_keys) => {
    try {
        // const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
        // const file_keys = input.file_keys;
        let answers = [];
        console.log(' File length :  ' + file_keys.length);
        let fileText = ''
        for (let i = 0; i < file_keys.length; i++) {
            console.log(`Processing file ${i + 1}`);
            fileText = await texractFromAWS(process.env.S3_BUCKET_FOR_PRIVATE_ASSETS, file_keys[i]);

            fileText = fileText.replace('MONICA OCHOA', fileText)
            fileText = fileText.replace('OCHOA, MONICA', fileText)
            fileText = fileText.replace('07/18/1979', fileText)

            //validateToken(fileText);
            let answer = await getAnswers(fileText);
            console.log("****************** Answers ****************");
            console.log(answer);
            answers.push(answer);
        }
        const summaryDocInput = await prepareSummaryDocInput(answers);
        const summaryDocResponse = await getMedicalDocUrl(summaryDocInput);
        return summaryDocResponse.url;
    } catch (err) {
        console.log(err);
        // throw new Error(' Summery Doc Error ' + err);
    }
}


// const startMedicalSummarybackgroundProcess = async (event) => {
//     try {

//         await sendEmail('r.aravinth.rifluxyss@gmail.com,selva.rifluxyss@gmail.com', `Schedular Started`, `Schedulare Testing`, 'EsquireTek');

//         const { MedicalHistory, MedicalHistorySummery } = await connectToDataBase();

//         const medicalSummaryRec = await MedicalHistorySummery.findOne({
//             where: { ai_status: 'New Request', ai_scheduler: true }, raw: true, logging: console.log
//         });
//         console.log("Total requests:", JSON.stringify(medicalSummaryRec));
//         if (!medicalSummaryRec) {
//             return {
//                 statusCode: 200,
//                 headers: {
//                     'Content-Type': 'text/plain',
//                     'Access-Control-Allow-Origin': '*',
//                     'Access-Control-Allow-Credentials': false
//                 },
//                 body: JSON.stringify({ status: 'Ok', message: 'No documents to process' }),
//             };
//         }

//         const medicalDocuments = await MedicalHistory.findAll({
//             where: { case_id: medicalSummaryRec.case_id },
//             raw: true
//         });

//         // const caseObj = await Cases.findOne({ where: { id: medicalSummaryRec.case_id } });
//         await MedicalHistorySummery.update({ ai_status: 'In Progress' }, { where: { id: medicalSummaryRec.id } });
//         const file_keys = medicalDocuments.map((document) => { return document.s3_file_key; });

//         console.log("Case Title: ", medicalSummaryRec.case_title);
//         console.log("File Keys: ", JSON.stringify(file_keys));

//         const medicalDocUrl = await getSummaryDoc(file_keys);


//         const emailSubject = `Summary Draft Generated for ${medicalSummaryRec.practice_name}`

//         if (!medicalDocUrl) {
//             await sendEmail(toEmailIds, `ERROR: Summary Draft Generation for ${medicalSummaryRec.practice_name}`,
//                 `Could not generate Medical Summary Draft for the below case <br/><br/> <br/><br/> Practice Name - ${medicalSummaryRec.practice_name} <br/><br/> Case Number - ${medicalSummaryRec.case_number} <br/><br/> Case Title - ${medicalSummaryRec.case_title} <br/><br/> Total Pages - ${medicalSummaryRec.pages}`, 'EsquireTek');

//             await MedicalHistorySummery.update({ ai_status: 'Failed', summary_doc_url_ai: medicalDocUrl, ai_scheduler: false }, { where: { id: medicalSummaryRec.id } });

//             throw new HTTPError(400, `Unable to generate Summary Draft.`)

//         }

//         await MedicalHistorySummery.update({ ai_status: 'Complete', summary_doc_url_ai: medicalDocUrl, ai_scheduler: false }, { where: { id: medicalSummaryRec.id } });


//         if (medicalDocUrl) {
//             await sendEmail(expertEmailIds, emailSubject,
//                 `Practice Name - ${medicalSummaryRec.practice_name} <br/><br/> Case Number - ${medicalSummaryRec.case_number} <br/><br/> Case Title - ${medicalSummaryRec.case_title} <br/><br/> Total Pages - ${medicalSummaryRec.pages}`, 'EsquireTek');
//         }

//         console.log("medicalDocUrl", medicalDocUrl);
//         await sendEmail('r.aravinth.rifluxyss@gmail.com,selva.rifluxyss@gmail.com', `Scheduler Ended`, `Scheduler Testing`, 'EsquireTek');
//         return {
//             statusCode: 200,
//             headers: {
//                 'Content-Type': 'text/plain',
//                 'Access-Control-Allow-Origin': '*',
//                 'Access-Control-Allow-Credentials': false
//             },
//             body: JSON.stringify({ status: 'Ok', message: 'Success' }),
//         };
//     } catch (err) {
//         console.log(`Error throwing from try block:`, err);
//         return {
//             statusCode: err.statusCode || 500,
//             headers: {
//                 'Content-Type': 'text/plain',
//                 'Access-Control-Allow-Origin': '*',
//                 'Access-Control-Allow-Credentials': false
//             },
//             body: JSON.stringify({ error: err.message || 'Could not process Medical Summary Document Generation.' }),
//         };
//     }
// }

const getTokens = function (fileText) {
        const splitSize = 10000; // getting each 10,000 chars and encoding the chars
        const iterations = Math.ceil(fileText.length / splitSize)
        let tokensList = [];

        for (let i = 0; i < iterations; i++) {
            let tokens = encode(fileText.slice(i * splitSize, (i + 1) * splitSize));
            tokensList.push(tokens);
        }
        tokensList = tokensList.flat();
        return tokensList;
}


const splitFileTexts = async (fileText, medical_doc_id) => {
    const { MedicalHistory } = await connectToDataBase();
    try {
        console.log('=============== File Split Process ===============');
        console.log('fileText:', fileText);
        // const tokens = encode(fileText); get error because of file text too large, so used the below method
        const tokens = getTokens(fileText);
        const noOfSplits = Math.ceil(tokens.length / MAXIMUM_TOKEN);

        const fileSplits = [];
        for (let i = 0; i < noOfSplits; i++) {

            const encodedTexts = tokens.slice(MAXIMUM_TOKEN * i, MAXIMUM_TOKEN * (i + 1));
            let obj = { status: 'pending', text: decode(encodedTexts) };
            fileSplits.push(obj);

            if (i != noOfSplits - 1) {
                const overlapEncodedTexts = tokens.slice((MAXIMUM_TOKEN * (i + 1)) - OVERLAP_TOKEN, (MAXIMUM_TOKEN * (i + 1)) + OVERLAP_TOKEN);
                let overlapObj = { status: 'pending', type: 'overlap', text: decode(overlapEncodedTexts) };
                fileSplits.push(overlapObj);
            }
        }

        // let split = 9500;
        // let noOfSplits = Math.ceil(fileText.length/split);
        // let overlap_split = 2500;

        // const fileSplits = [];
        // for (let i = 0; i < noOfSplits; i++) {
        //     let setOfText = fileText.slice(i * split, (i + 1) * split);
        //     let obj = { status: 'pending', text: setOfText };
        //     fileSplits.push(obj);
        //     console.log("*", i, ":", encode(setOfText).length)

        //     if (i != noOfSplits - 1) {
        //         let overlapTexts = fileText.slice((split * (i + 1)) - overlap_split, (split * (i + 1)) + overlap_split)
        //         let overlapObj = { status: 'pending', type: 'overlap', text: overlapTexts };
        //         fileSplits.push(overlapObj);
        //         console.log("overlap length", i, ":", encode(overlapTexts).length)
        //     }
        // }

        console.log('=============== Completed File Split Process ===============');
        return fileSplits;
    } catch (err) {
        await MedicalHistory.update({ status: 'FILE_PARTITION_FAILED', reason: err.message }, { where: { id: medical_doc_id } });
        console.log(err);
        throw new HTTPError(500, err.message);
    }
}

const getPdfDetails = async (s3_key, password) => {
    const apiUrl = await getSecrets('PDF_CONVERSION_API_URL');
    const response = await axios.request({ 
        url: `${apiUrl}/protected-pdf/check-file-status`, 
        method: "post", 
        data: { env: process.env.CODE_ENV, s3_key, password } 
    })
    .then((response) => response)
    .catch((err) => {
        console.log("Error:", err);
        throw new Error(400, `Error while getting pdf details`)
    }); 
    console.log("response:", response.data)
    return response.data;
}

const decryptPdf = async (s3_key, password) => {
    let decryptedKey = '';
    const apiUrl = await getSecrets('PDF_CONVERSION_API_URL');

    const response = await axios.request({ 
        url: `${apiUrl}/protected-pdf/extract-data`, 
        method: "post", 
        data: { env: process.env.CODE_ENV, s3_key, password } 
    })
    .then((response) => response.data)
    .catch((err) => {
        console.log("Error:", err);
        throw new Error(400, `Error while getting pdf details`)
    });

    console.log("response:", response.data);
    decryptedKey = response.s3_key
    return decryptedKey;
}

const DocTextExtract = async (medical_doc_id, file_key) => {
    const { MedicalHistory } = await connectToDataBase();
    try {
        let decrypted_s3_key = '';
        console.log('=============== Doc Extract Process ===============');
        const medicalObj = await MedicalHistory.findOne({ where: { id: medical_doc_id } });
        let password = medicalObj.comment ? medicalObj.comment : "duplicatePassword";
        const { encryption_status } = await getPdfDetails(file_key, password);

        if (encryption_status) {
            decrypted_s3_key = await decryptPdf(file_key, password);
            const textractObj = await texractFromAWS(process.env.S3_BUCKET_FOR_PRIVATE_ASSETS, decrypted_s3_key, medical_doc_id);
            console.log('=============== Doc Extract Process Completed ===============');
            return textractObj;
        }

        const textractObj = await texractFromAWS(process.env.S3_BUCKET_FOR_PRIVATE_ASSETS, file_key, medical_doc_id);
        console.log('=============== Doc Extract Process Completed ===============');
        return textractObj;

    } catch (err) {
        await MedicalHistory.update({ status: 'EXTRACTION_FAILED', reason: err.message }, { where: { id: medical_doc_id } });
        console.log(err);
        throw new HTTPError(500, err.message);
    }

}

const fetchUpdatedRow = async (id) => {
    try {
        const { MedicalHistory } = await connectToDataBase();
        const medicalDoc = await MedicalHistory.findOne({ where: { id }, raw: true });
        return medicalDoc;
    } catch (err) {
        throw new HTTPError(500, err.message);
    }
}

const fetchSummeryRequestRow = async (id) => {
    try {
        const { MedicalHistorySummery } = await connectToDataBase();
        const medicalSummery = await MedicalHistorySummery.findOne({ where: { id }, raw: true });
        return medicalSummery;
    } catch (err) {
        throw new HTTPError(500, err.message);
    }
}

const updateBillingDetails = async (answer, medical_doc_id) => {
    const { MedicalHistory } = await connectToDataBase();
    try {
        const medicalDoc = await MedicalHistory.findOne({ where: { id: medical_doc_id } })
        const billingDetails = medicalDoc.ai_billing_summery ? JSON.parse(medicalDoc.ai_billing_summery) : [];
        answer = typeof answer == 'string' ? JSON.parse(answer) : {};
        if (answer && answer.billings) {
            // answer.date_of_services.forEach((service) => {
                if (answer.billings && answer.billings.length > 0) {
                    //adding provider name in billing data
                    answer.billings = answer.billings.map((billingData) => {
                        billingData.provider = answer.hospital_name ? answer.hospital_name : '';
                        return billingData;
                    });
                    billingDetails.push(answer.billings);
                    console.log("billing details found.")
                }
            // })
        }
        medicalDoc.ai_billing_summery = JSON.stringify(billingDetails);
        await medicalDoc.save();
    } catch (err) {
        console.log(err)
    }
}

const getAnswersForSplittedText = async (file_splits_arr, completed_splits_summery, medical_doc_id, schedulerStartTime) => {
    const { Settings, MedicalHistory, PracticeSettings } = await connectToDataBase();
    try {
        console.log('=============== Getting Answer for splitted text ===============', medical_doc_id);
        let summary_question = ''

        // const medicalDoc = await MedicalHistory.findOne({ where: { id: medical_doc_id } });
        // const practiceSettings = await PracticeSettings.findOne({ where: { practice_id: medicalDoc.practice_id } })
        // summary_question = JSON.parse(practiceSettings.value).summary_question;
        const settingsObj = await Settings.findOne({ where: { key: 'global_settings' } });
        let headings = JSON.parse(settingsObj.value).headings_to_add;
        if (!summary_question) {
            summary_question = JSON.parse(settingsObj.value).summary_question;
        }

        const splited_files = JSON.parse(file_splits_arr);
        // check existing ai response
        let ai_response = completed_splits_summery ? JSON.parse(completed_splits_summery) : [];
        for (let i = 0; i < splited_files.length; i++) {
            const currentTime = new Date().getTime();
            const timeDifference = (currentTime - schedulerStartTime) / (1000 * 60);
            console.log(`${i + 1}: requesting before AI:`, new Date(currentTime).toString());
            if (timeDifference > 10.5) { // break the loop if not enough time get response (4.5 minutes)
                console.log(`${i + 1}: Not enough to get splitted response from AI.`, timeDifference);
                return false;
            }
            console.log(timeDifference, "continue answer for splitted text...")
            console.log("*************************************************")
            const { text, status } = splited_files[i];
            if (status == 'completed') continue;


            console.log("####summary_question####:", summary_question);

            let questionWithText = '';
            if (summary_question.includes('{pdftext}')) {
                questionWithText = summary_question.replace('{pdftext}', text);
            } else {
                questionWithText = `${text}: ${summary_question}`
            }
            // console.log("Spltted text:", text);
            const OPENAI_API_KEY = process.env?.OPENAI_API_KEY ? process.env.OPENAI_API_KEY : await getSecrets('OPENAI_API_KEY');
            const openai = new OpenAI({ apiKey: OPENAI_API_KEY });
            const completion = await openai.chat.completions.create({
                model: MODEL,
                response_format: { type: 'json_object' },
                temperature: 0,
                // top_p: 1,
                max_tokens: 3500,
                messages: [
                    { role: "system", content: `You are a medical summary report generator assistant. Your task is to generate summary used the given headings in the desired json format.\n\nAdditional headings to consider:\n${headings.replace(/\n/g, "")}` },
                    { role: "user", content: questionWithText }
                ],
            });
            const answer = completion.choices[0].message.content;
            console.log(`Answer for splitted text: ${i + 1} of ${splited_files.length}: id: ${medical_doc_id} answer:`, answer);

            ai_response.push(answer);
            splited_files[i].status = 'completed';
            await MedicalHistory.update(
                { file_splits_arr: JSON.stringify(splited_files), completed_splits_summery: JSON.stringify(ai_response) },
                {
                    where: { id: medical_doc_id }
                });
        }
        console.log('=============== Completed Answers For Splitted Text ===============', medical_doc_id);
        return true;
    } catch (err) {
        console.log('Error from getAnswersForSplittedText method', err);
        const ingnoreStatusCode = [500, 502, 503];
        if (!ingnoreStatusCode.includes(err.response?.status)) {
            await MedicalHistory.update({ status: 'PARTITION_SUMMERY_FAILED', reason: err.message }, { where: { id: medical_doc_id } });

            console.log('===================== AI Error Block=================');
            console.log('Error response from AI: ', err.response?.statusText);
            await sendEmail(toEmailIds, `Error from AI - ${process.env.CODE_ENV}`, `Getting Error from AI: Summary request: ${medical_doc_id} \n ${err}`, 'EsquireTek');
            throw new HTTPError(400, `The document summary retrieval failed.`)
        }
    }
}
const getFinalSummery = async (splited_ai_response, medical_doc_id, final_summery_arr, schedulerStartTime) => {

    const { Settings, MedicalHistory, PracticeSettings } = await connectToDataBase();
    try {
        console.log('=============== Getting Final Summary ===============', medical_doc_id);

        // Combaining Responses : [1,2,3,4,5] => [[1,2,3],[4,5]];
        const final_summery_length = final_summery_arr.length; 
        console.log('=============== SPLIT ===============', final_summery_length);
        const response_iteration = Math.ceil(splited_ai_response.length/3 ); 
        for (let i = final_summery_length; i < response_iteration; i++) {

            const currentTime = new Date().getTime();
            const timeDifference = (currentTime - schedulerStartTime) / (1000 * 60);
            console.log(`${i}: requesting before AI:`, new Date(currentTime).toString());
            if (timeDifference > 10.5) { // break the loop if not enough time get response (4.5 minutes)
                console.log(`${i}: Not enough to get final response from AI.`, timeDifference);
                break;
            }
            console.log(timeDifference, "continue answer for final response...");

            let getting_ai_response =  await AIResponseGenerator(splited_ai_response.slice( i* 5 , (i+1) * 5), medical_doc_id, splited_ai_response.length);
            final_summery_arr.push(getting_ai_response);
            await MedicalHistory.update({ final_summery: JSON.stringify(final_summery_arr) }, { where: { id: medical_doc_id } });
        }
        if(final_summery_arr.length ==  response_iteration){
            await MedicalHistory.update({status: 'PREPARE_SUMMARY_INPUT' }, { where: { id: medical_doc_id } });
        }
        return true;
    } catch (err) {
        console.log(err);
        await MedicalHistory.update({ status: 'FINAL_SUMMERY_FAILED_STAGE_1', reason: err.message }, { where: { id: medical_doc_id } });
        throw new HTTPError(400, `The document summary retrieval failed.`)
    }
}

const AIResponseGenerator = async (response, medical_doc_id, responseLength) => {
    const { Settings, Op, MedicalHistory } = await connectToDataBase();
    try {
        let summary_template = ''
        const settingsObj = await Settings.findOne({ where: { key: 'global_settings' } });
        summary_template = JSON.parse(settingsObj.value).summary_template;
        let headings = JSON.parse(settingsObj.value).headings_to_add;

        let questionWithText = '';
        let model_name = MODEL;
        let system = `You are a medical summary report generator assistant. Your task is to generate summary used the given headings in the desired json format.\n\nAdditional headings to consider:\n${headings.replace(/\n/g, "")}`;
        console.log("System content:", system)
        let max_token = 3500;
        let file = '';
        // if(response.length > 1) {
        //     let answers = response.map((res, index) => { return `RESPONSE ${index + 1}: ` + res })
        //     file = answers.join('\n\n');
        // } else {
        //     file = response.join('\n\n');
        // }

        if (response.length > 1) {
            // let summary_final_question = JSON.parse(settingsObj.value).summary_final_questions;
            // questionWithText = summary_final_question.replace('{answers}', file);
            // max_token = 7500;
            // model_name = 'gpt-3.5-turbo-16k'
            // system = `You are a JSON merger assistant. Merge all the treatment/billing sections from the given RESPONSES based on the procedure and date_of_services. Update the values and maintain the large values if the keys are matched. Get the response in JSON format.`
            const mergedSummaries = await JSONmerger(response);
            await updateBillingDetails(mergedSummaries, medical_doc_id);
            console.log('=============== Final Summary Process completed ===============');
            return mergedSummaries;
        } else if (responseLength != 1) { // returns the last answer
            if (response[0]) {
                try {
                    let answer = JSON.parse(response[0]);
                    await updateBillingDetails(answer, medical_doc_id);
                    console.log('=============== <Final Summary Process completed> ===============');
                    return answer;
                } catch (err) {
                    return ""
                }
            } else {
                return ""
            }

        }
        file = response.join('\n\n');
        if (!summary_template.includes('{pdftext}')) summary_template += '{pdftext}';
        questionWithText = summary_template.replace('{pdftext}', file)
        console.log("**final response question:**", questionWithText);

        const OPENAI_API_KEY = process.env?.OPENAI_API_KEY ? process.env.OPENAI_API_KEY : await getSecrets('OPENAI_API_KEY');
        const openai = new OpenAI({ apiKey: OPENAI_API_KEY });
        // const openai = new OpenAIApi(configuration);
        const completion = await openai.chat.completions.create({
            model: model_name,
            temperature: 0,
            response_format: { type: 'json_object' },
            // top_p: 1,
            max_tokens: max_token,
            messages: [
                { role: "system", content: system },
                { role: "user", content: questionWithText }
            ],
        });
        const answer = completion.choices[0].message.content;
        //saving billing details
        await updateBillingDetails(answer, medical_doc_id);
        console.log("Final Answer: ", answer)
        console.log('=============== Final Summary Process completed ===============');
        return answer;
    } catch (err) {
        console.log(err);
        const ingnoreStatusCode = [500, 502, 503];
        if (!ingnoreStatusCode.includes(err.response?.status)) {
            await MedicalHistory.update({ status: 'FINAL_SUMMERY_FAILED_STAGE_2', reason: err.message }, { where: { id: medical_doc_id } });
            throw new HTTPError(400, `The document summary retrieval failed.`)
        }
    }
}

const getFinalSummery_old = async (completed_splits_summery, medical_doc_id) => {

    const { Settings, MedicalHistory, PracticeSettings } = await connectToDataBase();
    try {
        console.log('=============== Getting Final Summary ===============', medical_doc_id);
        let summary_template = ''
        // const patientSummary = await PatientSummaryRequests.findOne({ where: { id: medical_doc_id } });
        // const practiceSettings = await PracticeSettings.findOne({ where: { practice_id: patientSummary.practice_id } })
        // summary_template = JSON.parse(practiceSettings.value).summary_template;

        // if (!summary_template) {
        const settingsObj = await Settings.findOne({ where: { key: 'global_settings' } });
        summary_template = JSON.parse(settingsObj.value).summary_template;
        // }

        const splited_ai_response = JSON.parse(completed_splits_summery);
        if (splited_ai_response.length <= 0) {
            await MedicalHistory.update({ status: 'FINAL_SUMMERY_FAILED', reason: 'Splits ai summery empty.' }, { where: { id: medical_doc_id } });
            throw new HTTPError(500, 'Splits ai summery empty.')
        }

        let file = splited_ai_response.join('\n\n');


        let questionWithText = '';
        let model_name = MODEL;
        let max_token = 3000;
        if (splited_ai_response.length > 1) {
            // summary_template = summary_template.replace(':{pdftext}', '');
            // summary_template = summary_template.replace('{pdftext}:', '');
            // summary_template = summary_template.replace('{pdftext}', '');
            let summary_final_questions = JSON.parse(settingsObj.value).summary_final_questions;
            questionWithText = summary_final_questions.replace('{answers}', file);

            max_token = 6000;
            model_name = 'gpt-3.5-turbo-16k'
        } else {
            if (!summary_template.includes('{pdftext}')) summary_template += '{pdftext}';
            questionWithText = summary_template.replace('{pdftext}', file)
            // questionWithText = `Here is the sample output template:${summary_template}:${file}`
        }

        console.log("**final response question:**", questionWithText);

        const OPENAI_API_KEY = process.env?.OPENAI_API_KEY ? process.env.OPENAI_API_KEY : await getSecrets('OPENAI_API_KEY');
        const openai = new OpenAI({ apiKey: OPENAI_API_KEY });
        const completion = await openai.chat.completions.create({
            // model: MODEL,
            model: model_name,
            temperature: 0,
            top_p: 1,
            max_tokens: max_token,
            messages: [{
                role: "system", content: questionWithText
            }],
        });
        const answer = completion.choices[0].message.content;
        console.log("Final Answer: ", answer)
        console.log('=============== Final Summary Process completed ===============', medical_doc_id);
        return answer;
    } catch (err) {
        console.log(err);
        await MedicalHistory.update({ status: 'FINAL_SUMMERY_FAILED', reason: err.message }, { where: { id: medical_doc_id } });
        throw new HTTPError(400, `The document summary retrieval failed.`)
    }
}


const generateMedicalSummary = async (event) => {
    try {
        const schedulerStartTime = new Date().getTime();
        console.log("Scheduler started:", new Date().toTimeString());
        const { MedicalHistory, MedicalHistorySummery, sequelize, Op } = await connectToDataBase();

        const query = `SELECT * FROM MedicalHistorySummeries WHERE ai_status IN ('New Request', 'Prepare Input', 'Generate Summery') AND ai_scheduler IS TRUE AND case_id NOT IN (SELECT case_id FROM MedicalHistories WHERE practice_id IS NOT NULL AND case_id IS NOT NULL AND pages > 1000) ORDER BY createdAt DESC LIMIT 5;`;
        const summaryRequests = await sequelize.query(query, {
            type: QueryTypes.SELECT,
            logging: console.log
        });
        console.log("summaryRequests:", summaryRequests.length);

        for (let i = 0; i < summaryRequests.length; i++) {
            try {
                if (process.env.CODE_ENV != 'local') {
                    await sendEmail(toEmailIds, `Scheduler started - ${process.env.CODE_ENV}`,
                    `Medical summary scheduler started`, 'EsquireTek');
                    await sendEmail(reminderEmail, `Scheduler started`,
                    `Medical summary scheduler started`, 'EsquireTek');

                    const medicalHistories = await MedicalHistory.findAll({
                        where: { 
                            case_id: summaryRequests[i].case_id,
                            status: { [Op.in]: ['NEW_REQUEST', 'TEXTRACT', 'FILE_PARTITION', 'PREPARE_PARTITION_SUMMERY', 'FINAL_SUMMERY'] },
                            summery_doc_status: { [Op.in]: ['ready'] } 
                        },
                        order: [['createdAt', 'ASC']],
                        logging: console.log
                    });

                    for (let j = 0; j < medicalHistories.length; j++) {

                        let medical_doc_id = medicalHistories[j].id;
                        let medicalDoc = await fetchUpdatedRow(medical_doc_id);

                        /* TEXT EXTRACTION */
                        const currentTime = new Date().getTime();
                        const timeDifference = (currentTime - schedulerStartTime) / (1000 * 60);
                        console.log("timeDifference TEXTRACT:", timeDifference)
                        if ((medicalDoc?.status == 'TEXTRACT' || medicalDoc.status == 'NEW_REQUEST') && timeDifference < 10) { // has five minutes?
                            await MedicalHistory.update({ status: 'TEXTRACT' }, { where: { id: medical_doc_id } });

                            const { textData, patientName, dateOfBirth, claims_number } = await DocTextExtract(medical_doc_id, medicalDoc.s3_file_key);

                            const patient_details = {
                                patient_name: patientName,
                                date_of_birth: dateOfBirth,
                                claims_number: claims_number
                            }

                            await MedicalHistory.update({ status: 'FILE_PARTITION', patient_details: JSON.stringify(patient_details), extracted_doc_data: textData }, { where: { id: medical_doc_id } });
                        }

                        /* FILE SPLITTING */
                        medicalDoc = await fetchUpdatedRow(medical_doc_id);
                        if (medicalDoc?.status == 'FILE_PARTITION') {
                            const partition_arr = await splitFileTexts(medicalDoc.extracted_doc_data, medical_doc_id);

                            if (partition_arr.length > 1) {

                                await MedicalHistory.update({ status: 'PREPARE_PARTITION_SUMMERY', file_splits_arr: JSON.stringify(partition_arr) }, { where: { id: medical_doc_id } });

                            } else if (partition_arr.length == 1) {

                                // it will skip PREPARE_PARTITION_SUMMERY process. * update completed_splits_summery column.
                                let summary_arr = [];
                                summary_arr.push(partition_arr[0].text);
                                await MedicalHistory.update({
                                    status: 'FINAL_SUMMERY', file_splits_arr: JSON.stringify(partition_arr),
                                    completed_splits_summery: JSON.stringify(summary_arr)
                                }, { where: { id: medical_doc_id } });
                            }
                        }


                        /* ANSWER FOR SPLITTED TEXT'S */
                        medicalDoc = await fetchUpdatedRow(medical_doc_id);
                        if (medicalDoc?.status == 'PREPARE_PARTITION_SUMMERY') {
                            let res = await getAnswersForSplittedText(medicalDoc?.file_splits_arr, medicalDoc?.completed_splits_summery, medical_doc_id, schedulerStartTime);
                            if (res) await MedicalHistory.update({ status: 'FINAL_SUMMERY' }, { where: { id: medical_doc_id } });
                        }


                        /* ANSWER FOR FINAL SUMMARY */
                        medicalDoc = await fetchUpdatedRow(medical_doc_id);
                        if (medicalDoc?.status == 'FINAL_SUMMERY') {
                            console.log("****FINAL_SUMMERY******")
                            console.log("****Completed summary******", typeof medicalDoc.completed_splits_summery)
                            const splited_ai_response = medicalDoc?.completed_splits_summery ?  JSON.parse(medicalDoc?.completed_splits_summery) : [];
                            if (splited_ai_response.length <= 0) {
                                await MedicalHistory.update({ status: 'FINAL_SUMMERY_FAILED', reason: 'Splits ai summery empty.' }, { where: { id: medical_doc_id } });
                                throw new HTTPError(500, 'Splits ai summery empty.')
                            }
                            const final_summery_arr = medicalDoc?.final_summery ?  JSON.parse(medicalDoc?.final_summery) : [];
                            let response = await getFinalSummery(splited_ai_response, medical_doc_id,final_summery_arr, schedulerStartTime);
                        }
                    }
                    const pendingMedicalRecords = await MedicalHistory.findOne({
                        where: { 
                            case_id: summaryRequests[i].case_id, status: { [Op.not]: ['PREPARE_SUMMARY_INPUT', 'COMPLETED'] },
                            summery_doc_status: { [Op.in]: ['ready'] } 
                        },
                        logging: console.log , raw: true
                    });
                    // console.log("pendingMedicalRecords:", pendingMedicalRecords);
                    /* PREPARE SUMMARY DOC INPUT */
                    if (!pendingMedicalRecords?.id) {
                        console.log('has pedning')
                        await MedicalHistorySummery.update({ ai_status: 'Prepare Input' }, { where: { id: summaryRequests[i].id } })

                        const completedMedicalRecords = await MedicalHistory.findAll({
                            where: { 
                                case_id: summaryRequests[i].case_id, status: { [Op.in]: ['PREPARE_SUMMARY_INPUT', 'COMPLETED'] },
                                summery_doc_status: { [Op.in]: ['ready'] } 
                            }
                        });
 
                        let documentSummaries = completedMedicalRecords.map((docs) => {
                            console.log('docs.file_name:', docs.file_name);
                             
                            console.log("docs.final_summery:", docs.final_summery);
                            const final_summery = JSON.parse(docs.final_summery);
                            let list_of_final_summeries = final_summery.map((row)=>{
                                // console.log("ROW:", row);
                                try {
                                    let summaryObj = typeof row == 'string' ? JSON.parse(row) : row; //JSON.parse(row);
                                    summaryObj.file_name = docs.file_name;

                                    //replacing patient name
                                    let patientName = "";
                                    if(docs.patient_details) {
                                        let patientObj = typeof docs.patient_details == 'string' ? JSON.parse(docs.patient_details) : {}
                                        patientName = patientObj.patient_name || ""
                                    }

                                    let summaryObjString = JSON.stringify(summaryObj);
                                    if(patientName) {
                                        const patientCodeRegex = new RegExp("@#@#PatientName@#@#", "g");
                                        summaryObjString = summaryObjString.replace(patientCodeRegex, patientName);
                                    }
                                    return summaryObjString;
                                } catch (err) {
                                    console.log("*****ERROR*****", err, "value:", row)
                                    return null;
                                }
                            });
                            list_of_final_summeries = list_of_final_summeries.filter((summary) => summary != null);
                            return list_of_final_summeries
                        });

                        // removing null values
                        let flatteredList = documentSummaries.flat();
                        if (flatteredList.length != 0) {
                            const { summaryDocInput, date_of_injury } = await prepareSummaryDocInput(flatteredList, summaryRequests[i].id);


                            const patientDetailsObj = completedMedicalRecords[0]?.patient_details ? JSON.parse(completedMedicalRecords[0].patient_details) : {}
                            const summaryDocInputObj = {
                                client_name: patientDetailsObj?.patient_name || "",
                                date_of_injury: date_of_injury,
                                dob: patientDetailsObj?.date_of_birth,
                                key: summaryDocInput
                            }
                            await MedicalHistorySummery.update({ ai_status: 'Generate Summery', medical_doc_input: JSON.stringify(summaryDocInputObj) }, { where: { id: summaryRequests[i].id } });
                        }else{
                            await MedicalHistorySummery.update({ ai_status: 'Generate Summery', medical_doc_input: JSON.stringify({}) }, { where: { id: summaryRequests[i].id } });
                        }
                        

                        /* GENERATE SUMMARY */
                        let summaryRequest = await fetchSummeryRequestRow(summaryRequests[i].id);
                        if (summaryRequest.ai_status == 'Generate Summery') {

                            console.log('********Getting Medical summary url*******');
                            const response = await getMedicalDocUrl(summaryRequest.medical_doc_input, summaryRequests[i].id);
                            let billing_attachment = {};
                            try {
                                const billingSummaryInput = await getBillingSummaryInput(summaryRequests[i].case_id)
                                const billingSummary = await getBillingSummary( JSON.stringify(billingSummaryInput), summaryRequests[i].id);
                                console.log("billingSummaryUrl:", billingSummary)
                                let billingSummaryUrl = billingSummary.url

                                if (billingSummaryUrl) {
                                    billing_attachment = {
                                        filename: billingSummaryUrl.slice( billingSummaryUrl.lastIndexOf('/') + 1 , billingSummaryUrl.length),
                                        href: billingSummaryUrl,
                                        contentType: 'application/vnd.ms-excel',
                                    }
                                }
                                

                            } catch (err) {
                                console.log("Error while getting billing summary.");
                                await sendEmail(toEmailIds, `Error: Generating billing summary - ${process.env.CODE_ENV}`,
                                `Error while generating billing summary for request id: ${summaryRequests[i].id}`, 'EsquireTek');
                            }
                            

                            if (response?.url) {

                                const docUrl = response.url;
                                const attachment = [
                                    {
                                    filename: docUrl.slice( docUrl.lastIndexOf('/') + 1 , docUrl.length),
                                    href: docUrl,
                                    contentType: 'application/pdf',
                                    },
                                    billing_attachment
                                ];
                                console.log("attachment:", attachment)
                                await sendEmail(expertEmailIds, `Summary Draft Generated for ${summaryRequests[i].practice_name}`,
                                `Practice Name - ${summaryRequests[i].practice_name} <br/> Case Number - ${summaryRequests[i].case_number} <br/> Case Title - ${summaryRequests[i].case_title} <br/> Total Pages Uploaded - ${summaryRequests[i].pages}`, 'EsquireTek', attachment);
                                await sendEmail(toEmailIds, `Summary Draft Generated for ${summaryRequests[i].practice_name} - ${process.env.CODE_ENV}`,
                                `Practice Name - ${summaryRequests[i].practice_name} <br/> Case Number - ${summaryRequests[i].case_number} <br/> Case Title - ${summaryRequests[i].case_title} <br/> Total Pages Uploaded - ${summaryRequests[i].pages}`, 'EsquireTek', attachment);

                                await MedicalHistorySummery.update({ ai_status: 'Completed', ai_scheduler: false, summary_doc_url_ai: response.url, billing_summary_url_ai: billing_attachment?.href || "" }, { where: { id: summaryRequests[i].id } });
                                await MedicalHistory.update({ status: 'COMPLETED' }, {
                                    where: { 
                                        case_id: summaryRequests[i].case_id,
                                        summery_doc_status: { [Op.in]: ['ready'] } 
                                    }
                                });
                            }

                        }
                    }
                    await sendEmail(toEmailIds, `Scheduler ended - ${process.env.CODE_ENV}`,
                        `Medical summary scheduler ended`, 'EsquireTek');
                    await sendEmail(reminderEmail, `Scheduler ended`,
                        `Medical summary scheduler ended`, 'EsquireTek');
                }

            } catch (error) {
                console.log("Outside error:", error)
                await MedicalHistorySummery.update({ ai_status: 'Failed', ai_scheduler: false }, { where: { id: summaryRequests[i].id } });
                await sendEmail(toEmailIds, `Scheduler Failed - ${process.env.CODE_ENV}`,
                    `Medical summary scheduler Failed`, 'EsquireTek');
            }

        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({})
        }
    } catch (err) {
        console.log('ALL ERRORS WILL BE CATCHED HERE', err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': false
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Users.' }),
        };
    }
}

const retryGenerateMedicalSummary = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const id = params.id;

        const { MedicalHistorySummery, MedicalHistory, Op } = await connectToDataBase();

        const medicalSummaryRequest = await MedicalHistorySummery.findOne({ where: { id: id } });
        if (!medicalSummaryRequest) throw new HTTPError(404, `Medical History not found.`)

        medicalSummaryRequest.ai_status = 'New Request';
        medicalSummaryRequest.ai_scheduler = true;
        medicalSummaryRequest.summary_doc_url_ai = null;
        medicalSummaryRequest.medical_doc_input = null;
        medicalSummaryRequest.save();

        await MedicalHistory.update({
            status: 'NEW_REQUEST',
            extracted_doc_data: null,
            reason: null,
            file_splits_arr: null,
            completed_splits_summery: null,
            final_summery: null,
            patient_details: null
        }, { where: { case_id: medicalSummaryRequest.case_id, status: { [Op.in]: ['FINAL_SUMMERY_FAILED', 'PARTITION_SUMMERY_FAILED', 'EXTRACTION_FAILED', 'FILE_PARTITION_FAILED','FINAL_SUMMERY_FAILED_STAGE_1', 'FINAL_SUMMERY_FAILED_STAGE_2'] } } });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'Ok', message: 'Success' })
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': false
            },
            body: JSON.stringify({ error: err.message || 'Could not Medical Summary Request.' }),
        };
    }
}

module.exports.getSummaryDoc = getSummaryDoc;
module.exports.getSummaryDetails = getSummaryDetails;
// module.exports.startMedicalSummarybackgroundProcess = startMedicalSummarybackgroundProcess;
module.exports.generateMedicalSummary = generateMedicalSummary;
module.exports.retryGenerateMedicalSummary = retryGenerateMedicalSummary;