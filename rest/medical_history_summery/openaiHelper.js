const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-west-2' });
const textract = new AWS.Textract();
const { TextractClient, StartDocumentAnalysisCommand } = require("@aws-sdk/client-textract");
const { HTTPError } = require('../../utils/httpResp');
const connectToDataBase = require('../../db');

const client = new TextractClient({
    region: 'us-west-2',
});


const dos_key_split = "<key_to_split_date_of_service>";

// as of now adding all the billing object details without checking duplicates.
const mergeBillingDetails = function (completedSplitSummaries) {
    let uniqueBillingMap = new Map();

    completedSplitSummaries.forEach((summaryResponse) => {
        if (summaryResponse && summaryResponse.billings && summaryResponse.billings.length > 0) {
            // billingDetails = billingDetails.concat(summaryResponse.billings)
            summaryResponse.billings.forEach((billingData) => {
                let key = `${billingData.date}, ${billingData.description}`
                if (!uniqueBillingMap.has(key)) {
                    uniqueBillingMap.set(key, billingData);
                }
            })
        }
    });
    return Array.from(uniqueBillingMap.values());
}

const mergeTreatmentSection = function (listOfDos) {
    let combinedTreatments = listOfDos.map((dos) => {
        return dos.treatment;
    });
    let resultTreatment = [];
    combinedTreatments = combinedTreatments.flat();

    for (let i = 0; i < combinedTreatments.length; i++) {
        let alreadyAdded = false;
        for (let j = 0; j < resultTreatment.length; j++) {
            if ((resultTreatment[j][0] == combinedTreatments[i][0])) { // merge heading already present
                resultTreatment[j] = combinedTreatments[i];
                alreadyAdded = true;
            }
        }
        if (!alreadyAdded) {
            resultTreatment.push(combinedTreatments[i]);
        }
    }
    return resultTreatment;
}

const getUniqueDosMap = function (completedSplitSummaries) {
    let uniqueDos = new Map();
    completedSplitSummaries.forEach((summaryJson) => {
        let summaryObj = typeof summaryJson == 'string' ? JSON.parse(summaryJson) : summaryJson;
        summaryObj.date_of_services.forEach((dosObj) => {
            let key = `${dosObj.date_of_service}${dos_key_split}${dosObj.procedure}`.toLocaleLowerCase();
            if (!uniqueDos.has(key)) {
                uniqueDos.set(key, [dosObj])
            } else {
                uniqueDos.set(key, [...(uniqueDos.get(key)), dosObj]);
            }
        });
    });
    return uniqueDos;
}

const updateDateofServices = async (completedSplitSummaries) => {
    const uniqueDosMap = getUniqueDosMap(completedSplitSummaries);
    let final_date_of_services = [];
    for (let key of uniqueDosMap.keys()) {
        let dos = key.split(dos_key_split)[0];
        let procedure = key.split(dos_key_split)[1];
        let mergedTreatment = mergeTreatmentSection(uniqueDosMap.get(key));
        if (mergedTreatment.length > 0) { // adding dos (row in medical summary) object only if treatment data is present
            final_date_of_services.push({
                date_of_service: dos,
                procedure: procedure,
                treatment: mergedTreatment
            })
        }
    }

    return final_date_of_services;
}

const JSONmerger = async (completedSplitSummaries) => {
    try {
        if (completedSplitSummaries.length < 1) {
            return [];
        }
        completedSplitSummaries = completedSplitSummaries.map((summaryString, index) => {
            try {
                let summaryOb = JSON.parse(summaryString);
                return summaryOb;
            } catch (err) {
                console.log("skipped:", index);
                return null;
            }
        }).filter((obj) => obj != null)

        let finalJson = {};
        const noResults = ["not mentioned", "not specified", "not provided", "n/a", ""]
        let date_of_injury = '';
        let hospital_name = '';

        for (let i = 0; i < completedSplitSummaries.length; i++) {
            if (completedSplitSummaries[i].date_of_injury && !noResults.includes(completedSplitSummaries[i].date_of_injury.toLocaleLowerCase()) && (!date_of_injury)) {
                date_of_injury = completedSplitSummaries[i].date_of_injury;
            }

            if (completedSplitSummaries[i].hospital_name && !noResults.includes(completedSplitSummaries[i].hospital_name.toLocaleLowerCase()) && (!hospital_name)) {
                hospital_name = completedSplitSummaries[i].hospital_name;
            }
        }
        finalJson.date_of_injury = date_of_injury;
        finalJson.hospital_name = hospital_name;
        // finalJson.date_of_injury = completedSplitSummaries[0]?.date_of_injury;
        // if (!finalJson.date_of_injury && completedSplitSummaries.length > 1) {
        //     finalJson.date_of_injury = completedSplitSummaries[1]?.date_of_injury
        // }
        // if (!finalJson.date_of_injury && completedSplitSummaries.length > 1) {
        //     finalJson.date_of_injury = completedSplitSummaries[2]?.date_of_injury
        // }

        // finalJson.hospital_name = completedSplitSummaries[0]?.hospital_name;
        // if (!finalJson.hospital_name && completedSplitSummaries.length > 1) {
        //     finalJson.hospital_name = completedSplitSummaries[1]?.hospital_name
        // }
        // if (!finalJson.hospital_name && completedSplitSummaries.length > 1) {
        //     finalJson.hospital_name = completedSplitSummaries[2]?.hospital_name
        // }

        // updating billing sections.
        finalJson.billings = mergeBillingDetails(completedSplitSummaries);

        // update date_of_service
        finalJson.date_of_services = await updateDateofServices(completedSplitSummaries);

        return finalJson;

    } catch (err) {
        console.log(err);
        throw new HTTPError(400, `Error while merging summarries.`)
    }
}

const getDocumentAnalysisResults = async (jobId) => {
    let response;
    let pageNumber = 1;
    const allResults = [];

    while (true) {

        response = await textract.getDocumentAnalysis({
            JobId: jobId,
            MaxResults: 1000,
            NextToken: response ? response.NextToken : undefined
        }).promise();

        const { JobStatus } = response;

        if (JobStatus === 'SUCCEEDED') {

            const analysisResults = response.Blocks;
            allResults.push(...analysisResults);

            if (!response.NextToken || pageNumber >= response.DocumentMetadata.Pages) {
                // Exit the loop if there are no more pages or reached the maximum page limit
                break;
            } else {
                pageNumber++;
            }
        }
        else if (JobStatus === "FAILED" || JobStatus === "PARTIAL_SUCCESS") {
            console.log(`response: `, response)
            throw new Error(`Document analysis failed with JobStatus: ${JobStatus}`);
        }

        // Wait for some time before making the next request
        await new Promise((resolve) => setTimeout(resolve, 500));
    }

    return allResults;

};


const replaceDummyInfo = async (output, filetext) => {

    const patientName = Object.keys(output).length && output[`What's the patient name?`] ? output[`What's the patient name?`][0] : "";
    const dateOfBirth = Object.keys(output).length && output[`What's the patient Date of Birth (D.O.B)?`] ? output[`What's the patient Date of Birth (D.O.B)?`][0] : "";
    const claims_number = Object.keys(output).length && output[`What's the patient Claim number?`] ? output[`What's the patient Claim number?`][0] : "";

    let textData = filetext;

    if (patientName && patientName.length >= 3) {
        const patientNameRegex = new RegExp(patientName, "g");
        textData = textData.replace(patientNameRegex, "@#@#PatientName@#@#");
        if (patientName.includes(', ')) {
            let firstNameLastName = patientName.split(', ').reverse().join(' ')
            let firstNameLastNameRegex = new RegExp(` ${firstNameLastName} `, "g");
            textData = textData.replace(firstNameLastNameRegex, " @#@#PatientName@#@# ");
        } else if (patientName.split(' ').length == 2) {
            let lastNameFirstName = patientName.split(' ').reverse().join(', ');
            let lastNameFirstNameRegex = new RegExp(` ${lastNameFirstName} `, "g");
            textData = textData.replace(lastNameFirstNameRegex, " @#@#PatientName@#@# ");
        }
    }

    if (dateOfBirth) {
        const date_of_birthRegex = new RegExp(dateOfBirth, "g");
        textData = textData.replace(date_of_birthRegex, "@#@#DateofBirth@#@#");
    }

    if (claims_number) {
        const claims_numberRegex = new RegExp(claims_number, "g");
        textData = textData.replace(claims_numberRegex, "@#@#ClaimNumber@#@#");
    }

    return { textData, patientName, dateOfBirth, claims_number };

}




const texractFromAWS = async (bucketname, fileKey, medical_doc_id) => {
    try {
        const params = {
            DocumentLocation: {
                S3Object: {
                    Bucket: bucketname,
                    Name: fileKey,
                },
            },
            FeatureTypes: ["QUERIES"],
            QueriesConfig: {
                Queries: [

                    {
                        Text: "What's the patient name?",
                        Alias: "Patient Name",
                        Pages: ["1"],
                        limit: '1000'
                    },
                    {
                        Text: "What's the patient Date of Birth (D.O.B)?",
                        Alias: "Date of Birth/D.O.B/Birthday/BirthDate/DOB",
                        Pages: ["1"],
                        limit: '1000'
                    },
                    {
                        Text: "What's the patient Claim number?",
                        Alias: "Claim number",
                        Pages: ["1"],
                        limit: '1000'
                    }
                ],
            }
        };

        // console.log(params);

        // checking if job is already running or not.
        const { MedicalHistory } = await connectToDataBase();
        const medicalRecord = await MedicalHistory.findOne({
            where: { id: medical_doc_id }
        });
        if(!medicalRecord) throw new HTTPError(404, `Record with id: '${medical_doc_id}' not found`)

        let Blocks = [];
        if (!(medicalRecord?.textract_job_id)) {
            const command = new StartDocumentAnalysisCommand(params);
            const response = await client.send(command);
            console.log('response:', response);
            const jobId = response && response.JobId;

            // saving the jobId
            if (response.JobId) {
                medicalRecord.textract_job_id = response.JobId;
                await medicalRecord.save();
                console.log("Job id saved", response.JobId)
            }
            Blocks = await getDocumentAnalysisResults(jobId);
        } else {
            console.log("using existing job id", medicalRecord?.textract_job_id)
            Blocks = await getDocumentAnalysisResults(medicalRecord.textract_job_id);
        }

        const fileTexts = Blocks.filter((block) => block.BlockType === 'LINE').map(block => block.Text).join('\n');

        // // Replace the patient name and date of birth
        // const questions = Blocks.filter((block) => block.BlockType === 'QUERY')
        // const answers = Blocks.filter((block) => block.BlockType === 'QUERY_RESULTS')

        // // questions.map((question) => {
        //     // question.Query.Text == 
        //     // let answerId = question.Relationships[0].Ids[0];
        // // })

        // console.log(answers, questions);

        let formsData = [];
        let queriesDatas = [];

        Blocks.forEach((block) => {

            if (block.BlockType == 'KEY_VALUE_SET') {
                if (block.EntityTypes.includes('KEY')) {
                    const keyId = block.Relationships[1] ? block.Relationships[1].Ids[0] : "";
                    const filteredKeyBlock = Blocks.filter((val) => val.Relationships && val.Relationships[0].Ids[0] === keyId) || "";
                    const keyText = filteredKeyBlock.length && filteredKeyBlock[0].Text || '';

                    const valueBlockId = block.Relationships[0].Ids[0];
                    const fileredValueBlock = Blocks.filter((Val) => Val.Id === valueBlockId);


                    const valueId = fileredValueBlock[0].Relationships && fileredValueBlock[0].Relationships[0].Ids[0] || '';
                    const sortedValueBlock = Blocks.filter((e) => e.Relationships && e.Relationships[0].Ids[0] == valueId);
                    const valueText = sortedValueBlock.length && sortedValueBlock[0].Text ? sortedValueBlock[0].Text : "";

                    const obj = Object.assign({ [keyText]: valueText });
                    formsData.push(obj);
                }
            }

            if (block.BlockType == 'QUERY') {

                const queryText = block.Query.Text;

                const queryKeyId = block.Relationships && block.Relationships[0].Ids[0] || '';
                const queryResBlock = Blocks.filter((data) => data.BlockType == 'QUERY_RESULT' && data.Id == queryKeyId);
                const queryResValue = queryResBlock.length && queryResBlock[0].Text || '';

                const obj = Object.assign({ [queryText]: queryResValue });
                queriesDatas.push(obj);

            }
        })


        const output = {};

        queriesDatas.forEach(obj => {
            for (let key in obj) {
                if (obj[key] !== "") {
                    if (output[key]) {
                        if (!output[key].includes(obj[key])) {
                            output[key].push(obj[key]);
                        }
                    } else {
                        output[key] = [obj[key]];
                    }
                }
            }
        });

        const { textData, patientName, dateOfBirth, claims_number } = await replaceDummyInfo(output, fileTexts);
        console.log('****************************************')
        // console.log("textData: ", textData)
        return { textData, patientName, dateOfBirth, claims_number };
        // return output;
    } catch (err) {
        console.log("Error while extracting text:", err);
        throw new HTTPError(400, `Failed to extract text from the document.`)
    }
}

module.exports = {
    JSONmerger,
    texractFromAWS
}