module.exports = (sequelize, type) => sequelize.define('MedicalHistorySummeryAi', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    client_id: type.STRING,
    case_id: type.STRING,
    case_number: type.STRING,
    case_title: type.STRING,
    status: type.STRING, // "New Request", "Assigned", "In Progress", "Complete"
    assigned_to: type.TEXT,
    request_date: type.DATE,
    practice_name: type.STRING,
    summery_url: type.TEXT,
    summery_documents: type.TEXT,
    summery_id: {
        type: type.INTEGER,
        autoIncrement: true,
        unique: true,
    },
    amount_charged: type.FLOAT,
    pages: {
        type: type.INTEGER,
    },
    order_id: type.STRING,
    scheduler_email: type.BOOLEAN,
});
