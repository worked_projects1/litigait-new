module.exports = (sequelize, type) => sequelize.define('MedicalHistorySummery', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    client_id: type.STRING,
    case_id: type.STRING,
    case_number: type.STRING,
    case_title: type.STRING,
    status: type.STRING, // "New Request", "Assigned", "In Progress", "Complete"
    assigned_to: type.TEXT,
    request_date: type.DATE,
    practice_name: type.STRING,
    summery_url: type.TEXT,
    summery_documents: type.TEXT,
    summery_id: {
        type: type.INTEGER,
        autoIncrement: true,
        unique: true,
    },
    amount_charged: type.FLOAT,
    pages: {
        type: type.INTEGER,
    },
    order_id: type.STRING,
    scheduler_email: type.BOOLEAN,
    summary_doc_url_ai: type.STRING,
    billing_summary_url_ai: type.STRING,
    ai_status: type.STRING,
    ai_scheduler: {
        type: type.BOOLEAN
    },
    medical_doc_input: type.TEXT('long')
},{
    indexes: [
        {
            name: 'getAllIndex',
            fields: ['practice_id', 'client_id', 'case_id', 'case_number','case_title','createdAt']
        }
    ]
});
