const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const { sendEmail } = require('../../utils/mailModule');
const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.REGION || "us-west-2" });
const s3 = new AWS.S3();

const sendPropoundHelpRequest = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

        if (!input.document_s3_key) throw new HTTPError(400, 'Document link missing in request');
        const { Practices, Op, PropoundHelpRequest } = await connectToDatabase();
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });

        let email = ''
        if (process.env.CODE_ENV == "staging" || process.env.CODE_ENV == "local") {
            email = 'siva@miotiv.com';
        } else {
            email = process.env.SUPPORT_EMAIL_RECEIPIENT;
        }

        let template = 'Propound Document Generation Help Requested';
        if (event?.user?.name) template = template + ' By ' + event?.user?.name;
        await sendEmail(email, `${template}`, `
      User Email : ${event.user.email}<br/>
      Practice Name : ${practiceObject.name}<br/>
      State: ${input.state}<br/>
      File Name: ${input.file_name}<br/>
      Document Type: ${input.document_type}<br/>
      Document S3 Key: ${input.document_s3_key}<br/>
      `);

        const dataObjectFinal = {
            id: uuid.v4(),
            user_email: event.user.email,
            practice_id: event.user.practice_id,
            practice_name: practiceObject.name,
            state: input.state,
            file_name: input.file_name,
            upload_document_name: input.upload_document_name,
            document_type: input.document_type,
            document_s3_key: input.document_s3_key,
            propound_template_id: input.propound_template_id,
        };

        const helpRequest = await PropoundHelpRequest.create(dataObjectFinal);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Propound Help Request Sent',
                helpRequest,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not resolve.' }),
        };
    }
};

const getAll_new = async (event) => {
    try {
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.order = [
            ['createdAt', 'DESC'],
        ];
        const { PropoundHelpRequest, Op } = await connectToDatabase();
        const helpRequests = await PropoundHelpRequest.findAll(query);
        const helpRequestsCount = await PropoundHelpRequest.count();

        let totalPages, currentPage;
        if (query.limit && (helpRequests.length > 0)) {
            totalPages = Math.ceil(helpRequestsCount / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const helpRequestsDetails = {
            total_items: helpRequestsCount,
            response: helpRequests,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(helpRequestsDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Propound Help Requests.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        const query = event.params || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.order = [
            ['createdAt', 'DESC'],
        ];
        const { PropoundHelpRequest, Op } = await connectToDatabase();
        const helpRequests = await PropoundHelpRequest.findAll(query);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(helpRequests),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Propound Help Requests.' }),
        };
    }
};
const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const { PropoundHelpRequest, Op } = await connectToDatabase();
        const propoundHelpRequestObject = await PropoundHelpRequest.findOne({ where: { id: params.id } });
        if (!propoundHelpRequestObject) throw new HTTPError(404, `HelpRequest with id: ${params.id} was not found`);
        await propoundHelpRequestObject.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'support request  removed'
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy fetch the Propound HelpRequest.' }),
        };
    }
};
const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const { PropoundHelpRequest, Op } = await connectToDatabase();
        const propoundHelpRequestobj = await PropoundHelpRequest.findOne({ where: { id: params.id } });
        if (!propoundHelpRequestobj) throw new HTTPError(404, `Propound Help Request with id: ${params.id} was not found`);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(propoundHelpRequestobj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Propound Help Request.' }),
        };
    }
};
const update = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { PropoundHelpRequest, Op } = await connectToDatabase();
        const propoundHelpRequestobj = await PropoundHelpRequest.findOne({ where: { id: params.id } });
        if (!propoundHelpRequestobj) throw new HTTPError(404, `Propound Help Request with id: ${params.id} was not found`);
        const helpRequestModel = Object.assign(propoundHelpRequestobj, input);
        await helpRequestModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(helpRequestModel),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the propound helpRequest.' }),
        };
    }
};
const getSecuredPublicUrlForPropound = async (event) => {
    try {
        const input = event.body;
        AWS.config.update({ region: process.env.S3_BUCKET_FOR_FORMS_REGION });
        const publicUrl = await s3.getSignedUrlPromise('getObject', {
            Bucket: process.env.S3_BUCKET_FOR_PROPOUND_SETTINGS_DOCUMENTS,
            Expires: 60 * 60 * 1,
            Key: input.s3_file_key,
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                publicUrl,
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }
};
module.exports.sendPropoundHelpRequest = sendPropoundHelpRequest;
module.exports.getAll = getAll;
module.exports.destroy = destroy;
module.exports.getOne = getOne;
module.exports.update = update;
module.exports.getSecuredPublicUrlForPropound = getSecuredPublicUrlForPropound;