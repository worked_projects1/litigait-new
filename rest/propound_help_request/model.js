module.exports = (sequelize, type) => sequelize.define('PropoundHelpRequests', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    user_email: type.STRING,
    practice_id: type.STRING,
    hash_id: type.STRING,
    practice_name: type.STRING,
    file_name: type.STRING,
    upload_document_name: type.STRING,
    state: type.STRING,
    document_type: type.STRING,
    document_s3_key: type.TEXT,    
});
