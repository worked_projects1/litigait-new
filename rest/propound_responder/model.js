module.exports = (sequelize, type) =>
    sequelize.define("PropoundResponder", {
        id: {
            type: type.STRING,
            primaryKey: true,
        },
        propound_form_id: type.STRING,
        responder_email: type.STRING,
        responder_practice_name: type.STRING,
        responder_practice_id: type.STRING,
        responder_case_id: type.STRING,
        responder_user_name: type.STRING, // attorney_name
        responder_state_bar_no: type.STRING,
        document_type: type.STRING,
        questions: type.TEXT("long"),
        number_of_questions_sent: type.STRING,
        token_id: type.STRING, //UUID.V4()
        propounder_practice_id: type.STRING,
        propounder_case_id: type.STRING,
        is_new_user: type.BOOLEAN,
        is_template_used_by_responder: type.BOOLEAN,
    });
/* First we have to check responder email is existing or not if email is existing have can get that user practice id 
  then we can store responder_practice_id else user have to create practice using this email then we can store responder_practice_id
  */
/* Once Propound case is assigned to responder case we can get responder case_id  */
