module.exports = (sequelize, type) => sequelize.define('FormsResponseHistory', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    case_id: type.STRING,
    form_id: type.STRING,
    legalforms_id: type.STRING,
    document_type: type.STRING,
    response: type.TEXT('long')
},
{
    indexes: [
        {
            name: 'practice_id',
            fields: ['practice_id']
        },
        {
            name: 'case_id',
            fields: ['case_id']
        },
        {
            name: 'form_id',
            fields: ['form_id']
        },,
        {
            name: 'legalforms_id',
            fields: ['legalforms_id']
        },
        {
            name: 'document_type',
            fields: ['document_type']
        },
        {
            name: 'pr_ca_fo_le_doc',
            fields: ['practice_id','case_id','form_id','legalforms_id','document_type']
        },
        {
            name: 'ca_fo_le_doc',
            fields: ['case_id','form_id','legalforms_id','document_type']
        }
    ]
}
);
