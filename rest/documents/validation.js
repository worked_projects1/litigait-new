const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');

exports.validateCreateDocuments = function (data) {
    const rules = {
        case_id: 'required|min:4|max:64',
        document_id: 'required|min:4|max:64',
        document_type: 'required|min:3|max:64',
        created_date: 'date',
        created_by: 'required|min:4|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }

    const validDocumentTypes = ['FROGS', 'SPROGS', 'RFPD', 'RFA'];
    if (!validDocumentTypes.includes(data.document_type)) throw new HTTPError(400, 'Invalid document type');
};

exports.validateGetOneDocument = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateUpdateDocuments = function (data) {
    const rules = {
        case_id: 'min:4|max:64',
        document_id: 'min:4|max:64',
        document_type: 'min:3|max:64',
        created_date: 'date',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }

    const validDocumentTypes = ['FROGS', 'SPROGS', 'RFPD', 'RFA'];
    if (data.document_type && !validDocumentTypes.includes(data.document_type)) throw new HTTPError(400, 'Invalid document type');
};

exports.validateDeleteDocument = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateReadDocuments = function (data) {
    const rules = {
        offset: 'numeric',
        limit: 'numeric',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};
