module.exports = (sequelize, type) => sequelize.define('Documents', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    case_id: type.STRING,
    document_id: type.STRING,
    document_type: type.STRING, // (FROGS, SPROGS, RFPD, RFA)
    created_date: type.DATE,
    created_by: type.STRING,
});
