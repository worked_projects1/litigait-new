module.exports = (sequelize, type) => sequelize.define('PracticeSettings', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    value: type.TEXT,
});
