const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const AWS = require('aws-sdk');
const { sendEmail } = require('../../utils/mailModule');

AWS.config.update({ region: process.env.REGION || 'us-east-1' });

const saveClientSignatureByOtp = async (event) => {
    try {
        const { Formotp, Op, ClientSignature } = await connectToDatabase();
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const otp_code = input.otp_code;
        if (!otp_code) throw new HTTPError(400, 'Missing OTP code');

        const otpDetails = await Formotp.findOne({
            where: { otp_code },
        });
        if (!otpDetails) throw new HTTPError(404, 'Invalid OTP code');
        const clientSignatureObject = await ClientSignature.findOne({
            where: {
                case_id: otpDetails.case_id,
                document_type: otpDetails.document_type,
                legalforms_id: otpDetails.legalforms_id,
            }
        });
        if (clientSignatureObject) {
            clientSignatureObject.client_signature = input.client_signature;
            clientSignatureObject.client_signature_date = new Date();
            await clientSignatureObject.save();
        } else {
            const dataObject = Object.assign({
                client_signature: input.client_signature,
                case_id: otpDetails.case_id,
                client_id: otpDetails.client_id,
                legalforms_id: otpDetails.legalforms_id,
                document_type: otpDetails.document_type,
                client_signature_date: new Date(),
            }, { id: uuid.v4() });
            await ClientSignature.create(dataObject);
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Signature data saved',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the forms' }),
        };
    }
};

const saveClientSignatureInS3ByOtp = async (event) => {
    try {
        const s3 = new AWS.S3({ region: 'us-west-2', signatureVersion: 'v4' });
        const { Formotp, Op, ClientSignature, Users, Clients, Practices } = await connectToDatabase();
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const otp_code = input.otp_code;
        if (!otp_code) throw new HTTPError(400, 'Missing OTP code');
        if (!input) throw new HTTPError(400, 'Missing http body params');
        if (!input.client_signature_s3_key) throw new HTTPError(400, 'Missing client_signature_s3_key');

        const otpDetails = await Formotp.findOne({
            where: { otp_code },
        });
        if (!otpDetails) throw new HTTPError(404, 'Invalid OTP code');
        let clientSignatureObject = await ClientSignature.findOne({
            where: {
                case_id: otpDetails.case_id,
                document_type: otpDetails.document_type,
                legalforms_id: otpDetails.legalforms_id,
            }
        });
        if (clientSignatureObject) {
            clientSignatureObject.client_signature_s3_key = input.client_signature_s3_key;
            clientSignatureObject.client_signature_date = new Date();
            clientSignatureObject.client_signature_data = input.client_signature_data;
            await clientSignatureObject.save();
        } else {
            const dataObject = Object.assign({
                client_signature_s3_key: input.client_signature_s3_key,
                case_id: otpDetails.case_id,
                client_id: otpDetails.client_id,
                legalforms_id: otpDetails.legalforms_id,
                document_type: otpDetails.document_type,
                client_signature_date: new Date(),
                client_signature_data: input.client_signature_data,
            }, { id: uuid.v4() });
            clientSignatureObject = await ClientSignature.create(dataObject);
        }

        const clientModel = await Clients.findOne({ where: { id: otpDetails.client_id } });
        if (clientModel) {
            let practiceName = '';
            const practiceDetails = await Practices.findOne({ where: { id: clientModel.practice_id } });
            if (practiceDetails) {
                practiceName = practiceDetails.name;
            }
            //const userModel = await Users.findOne({where:{id:otpDetails.sent_by}});
            const userModel = await Users.findOne({ where: { id: otpDetails.sent_by, is_deleted: { [Op.not]: true } }, logging: console.log });
            if (userModel) {
                const s3BucketParams = { Bucket: process.env.CLIENT_SIGNATURE_BUCKET, Expires: 60 * 60 * 1 };
                if (clientSignatureObject.client_signature_s3_key) {
                    s3BucketParams.Key = clientSignatureObject.client_signature_s3_key;
                    const pdfLink = await s3.getSignedUrlPromise('getObject', s3BucketParams);
                    const attachments = [
                        {
                            filename: 'Signature.pdf',
                            href: pdfLink, // URL of document save in the cloud.
                            contentType: 'application/pdf',
                        },
                    ];
                    await sendEmail(`${userModel.email},${clientModel.email}`, 'TekSign for your lawsuit',
                        `Client, ${clientModel.name} has verified the attorney responses for the questions and signed.`,
                        practiceName,
                        attachments,
                    );
                }
            }
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Signature data saved',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the forms' }),
        };
    }
};


const saveVerificationSignatureInS3ByOtp = async (event) => {
    try {
        const { Formotp, Op, ClientSignature } = await connectToDatabase();
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const otp_code = input.otp_code;
        if (!otp_code) throw new HTTPError(400, 'Missing OTP code');

        const otpDetails = await Formotp.findOne({
            where: { otp_code },
        });
        if (!otpDetails) throw new HTTPError(404, 'Invalid OTP code');
        const clientSignatureObject = await ClientSignature.findOne({
            where: {
                case_id: otpDetails.case_id,
                document_type: otpDetails.document_type,
                legalforms_id: otpDetails.legalforms_id,
            }
        });
        if (clientSignatureObject) {
            clientSignatureObject.client_verification_signature_s3_key = input.client_verification_signature_s3_key;
            clientSignatureObject.client_verification_signature_date = new Date();
            await clientSignatureObject.save();
        } else {
            const dataObject = Object.assign({
                client_verification_signature_s3_key: input.client_verification_signature_s3_key,
                case_id: otpDetails.case_id,
                client_id: otpDetails.client_id,
                legalforms_id: otpDetails.legalforms_id,
                document_type: otpDetails.document_type,
                client_verification_signature_date: new Date(),
            }, { id: uuid.v4() });
            await ClientSignature.create(dataObject);
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Signature data saved',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the forms' }),
        };
    }
};

const getSignatureUploadURL = async (input, otpDetails, signatureType) => {
    const s3 = new AWS.S3({ region: 'us-west-2', signatureVersion: 'v4' });
    let fileExtention = '';
    if (input.file_name) {
        fileExtention = `.${input.file_name.split('.').pop()}`;
    }
    const s3Params = {
        Bucket: process.env.CLIENT_SIGNATURE_BUCKET,
        Key: `${otpDetails.client_id}${otpDetails.legalforms_id}${otpDetails.document_type}${signatureType}${fileExtention}`,
        ContentType: input.content_type,
        ACL: 'private',
        Metadata: {
            case_id: otpDetails.case_id,
            client_id: otpDetails.client_id,
            document_type: otpDetails.document_type,
            legalforms_id: otpDetails.legalforms_id
        },
    };

    return new Promise((resolve, reject) => {
        const uploadURL = s3.getSignedUrl('putObject', s3Params);
        resolve({
            uploadURL,
            s3_file_key: s3Params.Key,
        });
    });
};

const getClientSignatureUploadUrlByOtp = async (event) => {
    try {
        const { Formotp } = await connectToDatabase();
        const input = event.query;
        const otp_code = input.otp_code;
        if (!otp_code) throw new HTTPError(400, 'Missing OTP code');

        const otpDetails = await Formotp.findOne({
            where: { otp_code },
        });
        if (!otpDetails) throw new HTTPError(404, 'Invalid OTP code');
        const uploadURLObject = await getSignatureUploadURL(input, otpDetails, 'signature');
        const uploadURL = uploadURLObject.uploadURL;
        const s3_file_key = uploadURLObject.s3_file_key;


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                data: {
                    uploadURL,
                    s3_file_key,
                },
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the forms' }),
        };
    }
};

const getClientVerificationSignatureUploadUrlByOtp = async (event) => {
    try {
        const { Formotp } = await connectToDatabase();
        const input = event.query;
        const otp_code = input.otp_code;
        if (!otp_code) throw new HTTPError(400, 'Missing OTP code');

        const otpDetails = await Formotp.findOne({
            where: { otp_code },
        });
        if (!otpDetails) throw new HTTPError(404, 'Invalid OTP code');
        const uploadURLObject = await getSignatureUploadURL(input, otpDetails, 'verification_signature');
        const uploadURL = uploadURLObject.uploadURL;
        const s3_file_key = uploadURLObject.s3_file_key;


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                data: {
                    uploadURL,
                    s3_file_key,
                },
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the forms' }),
        };
    }
};

const teksignCorrection = async (event) => {
    try {
        const { Formotp, Op, ClientSignature, Users, Clients, Practices, Cases } = await connectToDatabase();
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const otp_code = input.otp_code;
        if (!otp_code) throw new HTTPError(400, 'Missing OTP code');
        if (!input) throw new HTTPError(400, 'Missing http body params');
        const message = input.message;

        const otpDetails = await Formotp.findOne({
            where: { otp_code },
        });
        const clientsDetails = await Clients.findOne({ where: { id: otpDetails.client_id } });
        const caseDetails = await Cases.findOne({ where: { id: otpDetails.case_id } });
        const userModel = await Users.findOne({ where: { id: otpDetails.sent_by, is_deleted: { [Op.not]: true } } });
        const practiceDetails = await Practices.findOne({ where: { id: userModel.practice_id } });
        if (!userModel) throw new HTTPError(400, 'Cannot find Users Deatils.');
        // let message_template = 'Hi' + userModel.name?(' '+userModel.name+','):',' + ' Below is the message from Client (' + clientsDetails.name + ') for the case - ' + caseDetails.case_title + '.' + '<br/><br/>' + message;
        let template = 'Hi';
        if (userModel?.name) template = template + ' ' + userModel.name;
        let message_template = template + ', Below is the message from Client (' + clientsDetails.name + ') for the case - ' + caseDetails.case_title + '.' + '<br/><br/>' + message;
        await sendEmail(`${userModel.email}`, 'Needs Correction for TekSign', message_template, practiceDetails.name);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Email send successfully.',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the users.' }),
        };
    }
};

module.exports.saveClientSignatureByOtp = saveClientSignatureByOtp;
module.exports.saveClientSignatureInS3ByOtp = saveClientSignatureInS3ByOtp;
module.exports.saveVerificationSignatureInS3ByOtp = saveVerificationSignatureInS3ByOtp;
module.exports.getClientSignatureUploadUrlByOtp = getClientSignatureUploadUrlByOtp;
module.exports.getClientVerificationSignatureUploadUrlByOtp = getClientVerificationSignatureUploadUrlByOtp;
module.exports.teksignCorrection = teksignCorrection;
