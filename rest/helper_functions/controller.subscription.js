const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const connectToDatabase = require('../../db');

const getPropoundingPlanType = (plan_type) => {
    try {
        let propounding_plan;
        if (plan_type == 'users_responding_monthly_license_495') {
            propounding_plan = 'users_propounding_monthly_license_199';
        } else if (plan_type == 'users_responding_yearly_license_5100') {
            propounding_plan = 'users_propounding_yearly_license_2149_20';
        }
        return propounding_plan;
    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
}

const getPlansByplanType = async (type, plan_category, plan_type) => {
    try {
        /*
        Refer TODO Docs
        UserBased Billing
        Responding Pricing:

        Billed Monthly - $495/month for 5 users. Every additional user is $89/user/month
        Billed Annually - $5100/yr for 5 users.  Every additional user is  $79/user/month (billed at $948/yr)

        Propounding Pricing:

        Billed Monthly - $199/month for 5 users. Every additional user is $39/user/month
        Billed Annually - $2149.20/yr for 5 users. Every additional user is $35/user/month (billed at $ 420/yr)
        */
        const { Plans, Op } = await connectToDatabase();
        const plans_details = {
            standard_plans: { //paid
                responding: {
                    'users_responding_monthly_license_495': 'users_responding_monthly_license_89',
                    'users_responding_yearly_license_5100': 'users_responding_yearly_license_948',
                },
                propounding: {
                    'users_propounding_monthly_license_199': 'users_propounding_monthly_license_39',
                    'users_propounding_yearly_license_2149_20': 'users_propounding_yearly_license_420',
                },
            },
            complimentary_plans: { //free
                responding: {
                    'users_responding_monthly_license_495': 'users_responding_monthly_free_license_495',
                    'users_responding_yearly_license_5100': 'users_responding_yearly_free_license_5100',
                },
                propounding: {
                    'users_propounding_monthly_license_199': 'users_propounding_monthly_free_license_199',
                    'users_propounding_yearly_license_2149_20': 'users_propounding_yearly_free_license_2149_20',
                },
            }
        };

        const plan = plans_details?.[type]?.[plan_category]?.[plan_type];
        const plan_obj = await Plans.findOne({ where: { plan_type: plan, plan_category: plan_category }, raw: true });
        return plan_obj;
    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
}

const validateDiscountCode = async (discount_code) => {
    try {
        const { Plans, Op, DiscountCode } = await connectToDatabase();
        const discountObj = await DiscountCode.findOne({ where: { discount_code: discount_code, is_deleted: { [Op.not]: true } }, raw: true });
        if (!discountObj?.id) throw new Error(`Invalid discount code ${discount_code}.`);
        return discountObj;
    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
}

const createSubscriptionMetaObj = (price, quantity, meta, subscription_for, proration_status = true) => {
    try {
        let metaObj = {
            items: [{ price, quantity }],
            proration_behavior: proration_status && subscription_for == 'additional_user_subscription' ? 'always_invoice' : 'none',
            metadata: {
                ...meta,
                latest_subscriber_count: quantity,
                subscription_for: subscription_for,
                date: new Date()
            }
        }
        return metaObj;
    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
}

const calculateOneTimeFeeDiscount = async (discount_code, discount_percentage = 0) => {
    try {
        const { Op, DiscountCode } = await connectToDatabase();
        const discountObj = await DiscountCode.findOne({
            where: { discount_code: discount_code, is_deleted: { [Op.not]: true } },
            raw: true
        });

        if (!discountObj) throw new Error(`Invalid discount code ${discount_code}.`);
        const stripe_couponcode_obj = discountObj?.stripe_obj ? JSON.parse(discountObj.stripe_obj) : {};

        const { plan_type } = discountObj;
        const isEligibleFor = plan_type.split(',');
        let discounted_price = 249;
        let discount_amount = 0;

        const response = {
            is_eligible_for: isEligibleFor
        }

        if (isEligibleFor.includes('activation_fee')) {
            const percentage = 1 - (discountObj?.discount_percentage / 100);
            discounted_price = Math.ceil((percentage * discounted_price).toFixed(2));
            discount_amount = (249 - discounted_price).toFixed(2);
            console.log(discount_code);
            response.id = stripe_couponcode_obj?.id ? stripe_couponcode_obj?.id : discount_code.toLowerCase();
            response.name = stripe_couponcode_obj?.name ? stripe_couponcode_obj?.name : discount_code;
            response.percent_off = stripe_couponcode_obj?.percent_off ? stripe_couponcode_obj?.percent_off : discountObj?.discount_percentage;
        }
        response.discounted_price = discounted_price;
        response.discount_amount = discount_amount;

        return response

    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
}
module.exports = {
    getPlansByplanType,
    validateDiscountCode,
    createSubscriptionMetaObj,
    calculateOneTimeFeeDiscount,
    getPropoundingPlanType
}