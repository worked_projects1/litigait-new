






const getExistingSubscription = async (subscriptionId) => {
    try{
        const existingSubscription = await stripe.subscriptions.retrieve(subscriptionId);
        return existingSubscription;
    }catch(err){
        throw new Error(err);
    }
}

const createSubscription = async (stripe_customer_id, stripe_price_id, total_license, metadata) => {
    try {
        const create_subscription = await stripe.subscriptions.create({
            customer: stripe_customer_id,
            items: [{ 
                    price: stripe_price_id, quantity: total_license, 
                }],
            metadata,
        });
        return create_subscription;
    } catch (err) {
        throw new Error(err);
    }
}

const updateSubscription = async (subscriptionId, item_id, price, metadata) => {
    try {
        const updatedSubscription = await stripe.subscriptions.update(subscriptionId, {
            //billing_cycle_anchor: 'now',
            cancel_at_period_end: false,
            proration_behavior: 'create_prorations',
            items: [{
                id: item_id,
                price,
            }],
            metadata,
        });
        return updatedSubscription;
    } catch (err) {
        throw new Error(err);
    }
}

const cancelSubscriptionWithPartialRefund = async (subscriptionId) => {
    try {
        const subscription = await stripe.subscriptions.retrieve(subscriptionId);
        const refundAmount = Math.floor(subscription.plan.amount * subscription.days_until_due / 30);
        const refund = await stripe.refunds.create({
            charge: subscription.latest_invoice.payment_intent.charges.data[0].id,
            amount: refundAmount
        });
        const canceledSubscription = await stripe.subscriptions.del(subscriptionId, { at_period_end: true });
        return { refund, canceledSubscription };
    } catch (err) {
        throw new Error(err);
    }
}

const calculateDaysDifference = (subscriptionDate) =>{
    try {
        const oneDayMs = 24 * 60 * 60 * 1000;
        const currentDate = new Date();
        const subscriptionDateMs = subscriptionDate * 1000;
        const daysDifference = Math.abs(Math.ceil((currentDate - subscriptionDateMs) / oneDayMs));
        return daysDifference;
    } catch (err) {
        throw new Error(err);
    }
}

const createPaymentIntent = async (subscription_price, stripe_customer_id, payment_method_id) => {
    try {
        const licence_subscription_obj = {
            amount: parseInt(subscription_price * 100), currency: 'usd', customer: stripe_customer_id,
            payment_method: payment_method_id, off_session: true, confirm: true,
        }
        const payment_intent_obj = await stripe.paymentIntents.create(licence_subscription_obj);
        return payment_intent_obj;
    } catch (err) {
        throw new Error(err);
    }
}

const caluclateOneDayPriceByPlanId = async (planId) => {
    try {
        const plan = await stripe.plans.retrieve(planId);
        const amount = plan.amount; // Monthly amount in cents
        console.log('Amount : ' + amount);
        const intervalCount = plan.interval_count; // 1
        console.log('Interval count : ' + intervalCount);
        const dailyAmount = Math.round(amount / (intervalCount)); // Amount for one day in cents
        console.log(`The prorated amount for one day is $${(dailyAmount / 100).toFixed(2)}.`);
        const prorated_amount = (dailyAmount / 100).toFixed(2);
        return parseFloat(prorated_amount);
    } catch (err) {
        throw new Error(err);
    }
}

const getChargeIdUsingSubscriptionID = async (subscription_id) => {
    try {
        const subscription = await stripe.subscriptions.retrieve(subscription_id);

        // Retrieve Invoice object by ID
        const invoiceId = subscription.latest_invoice;
        const invoice = await stripe.invoices.retrieve(invoiceId);

        // Get Charge ID
        const chargeId = invoice.charge;
        return chargeId;

    } catch (err) {
        throw new Error(err);
    }
}
module.exports = {
    getExistingSubscription,
    updateSubscription,
    cancelSubscriptionWithPartialRefund,
    createSubscription,
    calculateDaysDifference,
    createPaymentIntent,
    getChargeIdUsingSubscriptionID,
    caluclateOneDayPriceByPlanId
}