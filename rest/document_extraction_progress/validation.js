const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');

exports.validateUploadFiles = function (data) {
    const rules = {
        practice_id: 'required',
        user_id: 'required',
        pdf_filename: 'required',
        status: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateExtractedData = function (data) {
    const rules = {
        practice_id: 'required',
        user_id: 'required',
        id: 'required',
        document_data: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateCreateClients = function (data) {
    const rules = {
        name: 'required|min:4|max:200',
        //phone: 'required|numeric',
        practice_id: 'required',
        client_from:'required'
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};
exports.validateCreateCases = function (data) {
    const rules = {
        client_id: 'required',
        practice_id: 'required',
        // case_title: 'required',
        // case_number: 'required',
        // case_plaintiff_name:'required',
        // case_defendant_name:'required',
         state: 'required',
        // county: 'required',
    };
    const validation = new Validator(data, rules);
    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};
exports.validateCreateLegalForms = function (data) {
    const rules = {
        practice_id: 'required|min:4|max:64',
        case_id: 'required|min:4|max:64',
        client_id: 'required|min:4|max:64',
        filename: 'required',
        document_type: 'required',
        // set_number: 'required'
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateForms = function (data) {
    const rules = {
        question_text: 'required',
        question_number_text: 'required',
        question_id: 'required',
        question_number: 'required',
        id: 'required',
        case_id: 'required',
        practice_id: 'required',
        client_id: 'required',
        legalforms_id: 'required',
        document_type: 'required',
        lawyer_response_status: 'required',
        client_response_status: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateGetOneCase = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateUpdateCases = function (data) {
    const rules = {
        client_id: 'min:4|max:64',
        start_date: 'date',
        case_number: 'min:1|max:64',
        status: 'min:3|max:64',
        state: 'required|min:2|max:64',
    };
    const validation = new Validator(data, rules);
    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateDeleteCase = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateReadCases = function (data) {
    const rules = {
        offset: 'numeric',
        limit: 'numeric',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateCreateParties = (data) => {
    const rules = {
        case_id: 'required',
        name: 'min:4|max:64|required',
        email: 'email|min:4|max:64',
        phone: 'required|numeric',
        address: 'min:4|max:64',
    };
    const validation = new Validator(data, rules);
    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
}

exports.validateCreatePartiesForms = (data) => {
    const rules = {
        case_id: 'required',
        party_id: 'required',
        legalforms_id: 'required',
        question_id: 'required',
        document_type: 'required',
    };
    const validation = new Validator(data, rules);
    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
}

exports.validateCreateClients = function (data) {
    const rules = {
        //email: 'email|min:4|max:64',
        //phone: 'required|numeric',
        address: 'min:4|max:64',
        hipaa_acceptance_status: 'boolean',
        hipaa_acceptance_date: 'date',
        fee_acceptance_status: 'boolean',
        fee_acceptance_date: 'date',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateDuplicateCaseClientCreate = function (data) {
    const rules = {
        name: 'min:4|max:64',
        email: 'email|min:4|max:64',
        phone: 'required|numeric',
        address: 'min:4|max:64',
        dob: 'min:4|max:64',
        case_id: 'min:4|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};