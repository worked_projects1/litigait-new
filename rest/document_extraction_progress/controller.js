const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION || 'us-east-1' });
const s3 = new AWS.S3();
const {
    validateCreateClients, validateCreateCases, validateCreateLegalForms, validateForms, validateUploadFiles, validateExtractedData
} = require('./validation');
const { sendEmail } = require('../../utils/mailModule');
const { getOpposingCounsilDetails } = require('../helpers/forms.helper');

const getPrivateUploadURL = async (input) => {
    let fileExtention = '';
    let newfilename = '';
    if (input.pdf_filename) {
        const filterarr = input.pdf_filename.split('.');
        fileExtention = `.` + filterarr.pop();
        newfilename = filterarr.join("_");
        newfilename = `${input.practice_id}/${input.user_id}/${input.id}/${newfilename}${fileExtention}`;
    }

    const s3Params = {
        Bucket: process.env.S3_BUCKET_FOR_FORMS,
        Key: `${newfilename}`,
        ContentType: input.content_type,
        Expires: 60 * 60 * 1,
        ACL: 'private',
    };
    const publicUrl = await s3.getSignedUrlPromise('getObject', {
        Bucket: process.env.S3_BUCKET_FOR_FORMS,
        Expires: 60 * 60 * 12,
        Key: newfilename,
    });
    const uploadURL = s3.getSignedUrl('putObject', s3Params);

    return { uploadURL, s3_file_key: s3Params.Key, publicUrl };
}
const uploadPrivateFile = async (event) => {
    try {
        const input = event.body; /* practice_id, user_id, session_user_id */
        const content_type = input.content_type;
        const { DocumentExtractionProgress, LegalForms, Op, sequelize, DocTextractRegion } = await connectToDatabase();
        const dataObject = Object.assign(input, { id: uuid.v4(), status: 'FileProgress' });
        validateUploadFiles(dataObject);
        let response = {};

        //Upload region and its count starts
        let index = 0;
        let region = null;
        const currentTime = new Date();

        const allRegion = (process.env.DOCUMENT_EXTRACT_REGION).split(',');
        const regionDocDataLegalFroms = await DocTextractRegion.findAll({
            where: {
                doc_textract_region: allRegion,
                createdAt: {
                    [Op.lt]: new Date(),
                    [Op.gt]: new Date(new Date() - 10 * 60 * 1000)
                }
            },
            attributes: [
                'doc_textract_region',
                [sequelize.fn('COUNT', sequelize.col('doc_textract_region')), 'type_count'],
            ],
            group: ['doc_textract_region'],
            order: [[sequelize.col('type_count'), 'ASC']],
            raw: true
        });
        if (regionDocDataLegalFroms.length < allRegion.length) {
            { region = allRegion[regionDocDataLegalFroms.length] };
        }
        response.doc_textract_region = region || regionDocDataLegalFroms[index]?.doc_textract_region || 'us-east-1';
        response.doc_textract_region_count = region ? 0 : regionDocDataLegalFroms[index]?.type_count;

        dataObject.doc_textract_region = response.doc_textract_region;
        dataObject.latest_region_timestamp = currentTime;
        const extractionObj = await DocumentExtractionProgress.create(dataObject);
        extractionObj.content_type = content_type;
        const uploadURLObject = await getPrivateUploadURL(extractionObj);

        await DocumentExtractionProgress.update({ s3_file_key: uploadURLObject.s3_file_key }, { where: { id: extractionObj.id, practice_id: extractionObj.practice_id, user_id: extractionObj.user_id } });

        const getExtractionObj = await DocumentExtractionProgress.findOne({ where: { id: extractionObj.id, practice_id: extractionObj.practice_id, user_id: extractionObj.user_id }, raw: true });
        response.extraction_data = {};
        Object.assign(response.extraction_data, getExtractionObj);
        if (uploadURLObject?.uploadURL) response.uploadURL = uploadURLObject.uploadURL;
        if (uploadURLObject?.s3_file_key) response.s3_file_key = uploadURLObject.s3_file_key;
        if (uploadURLObject?.publicUrl) response.publicUrl = uploadURLObject.publicUrl;

        const regionObj = Object.assign({}, {
            id: uuid.v4(),
            practice_id: event.user.practice_id,
            user_id: event.user.id,
            document_extraction_id: extractionObj.id,
            region_sent_timestamp: currentTime,
            doc_textract_region: response.doc_textract_region,
            uploaded_type: 'QuickCreate'
        });
        const textractRegionData = await DocTextractRegion.create(regionObj);
        response.doc_textract_region_id = textractRegionData.id;

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(response),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload documents.' }),
        };
    }
};
const getStatus = async (event) => {
    try {
        const input = event.query;
        const { DocumentExtractionProgress, Op } = await connectToDatabase();
        const { practice_id, id, user_id } = input;
        if (!practice_id || !id || !user_id) throw new HTTPError(404, `Required details not found.`);
        const DocumentExtractionProgressObject = await DocumentExtractionProgress.findOne({
            where: { practice_id, id, user_id },
            attributes: ['id', 'practice_id', 'user_id', 'legalform_filename', 'pdf_filename', 's3_file_key', 'status', 'createdAt', 'legalforms_id']
            , raw: true
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', data: DocumentExtractionProgressObject, input }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch document extraction status.' }),
        };
    }
};
const setStatus = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { DocumentExtractionProgress, Op } = await connectToDatabase();
        const { practice_id, id, user_id } = input;
        if (!practice_id || !id || !user_id) throw new HTTPError(404, `Required details not found.`);
        let DocumentExtractionProgressObject = await DocumentExtractionProgress.findOne({ where: { practice_id, id, user_id } });

        if (DocumentExtractionProgressObject?.status != 'cancelled') {
            const updatedModel = Object.assign(DocumentExtractionProgressObject, input);
            await updatedModel.save();
            DocumentExtractionProgressObject = await DocumentExtractionProgress.findOne({
                where: { practice_id, id, user_id },
                attributes: ['id', 'practice_id', 'user_id', 'session_user_id', 'legalform_filename', 'pdf_filename', 's3_file_key', 'set_number', 'state', 'document_type', 'status', 'createdAt']
            });
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', data: DocumentExtractionProgressObject }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create document extraction status.' }),
        };
    }
};
const updateDocumentData = async (event) => {
    try {
        const input = event.body; /* practice_id, user_id, session_user_id */
        const { DocumentExtractionProgress, HashedFiles, Op } = await connectToDatabase();
        const params = event.params || event.pathParameters;
        const query_params = event.query || {};
        const extractionObj = await DocumentExtractionProgress.findOne({ where: { id: params.id } });
        if (!extractionObj) throw new HTTPError(404, `Document extraction with id: ${params.id} was not found`);
        validateExtractedData(input);
        const { id, practice_id, user_id, hash_id, document_data, state, set_number, document_type, defendant_practice_details } = input;

        if (query_params?.question_encode) {
            const question = document_data?.questions;
            question.map(row => {
                return row.question_text = Buffer.from(row.question_text, 'base64').toString('utf8');
            });
            document_data.questions = question;
        }
        if (input?.defendant_practice_details) {
            document_data.defendant_practice_details = defendant_practice_details;
        }

        await DocumentExtractionProgress.update({ document_data: JSON.stringify(document_data), set_number, document_type }, { where: { id, practice_id, user_id } });

        // if(hash_id){
        //     document_data.set_number = set_number;
        //     const hashedData = await HashedFiles.findOne({where:{ id:hash_id }});
        //     if(!hashedData) throw new HTTPError(400, 'hash data not found');
        //     hashedData.questions = JSON.stringify(document_data.questions);
        //     hashedData.document_data = JSON.stringify(document_data);
        //     hashedData.questions_available = 'true';
        //     hashedData.document_type = document_type;
        //     hashedData.state = state;
        //     await hashedData.save();
        //     }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'Ok', message: 'extracted questions saved successfully.' }),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch document extraction data.' }),
        };
    }
}
const saveExtractionData = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { Clients, Cases, LegalForms, Forms, Op, DocumentExtractionProgress, Practices, Users } = await connectToDatabase();
        let { client_info, case_info, questions, practice_id, document_type, s3_file_key, set_number, legalform_filename, user_id, extraction_id } = input;
        if (input?.state_bar_number) {
            await Users.update({ state_bar_number: input.state_bar_number }, { where: { id: event.user.id } });
        }
        /* Client */
        client_info = Object.assign(client_info, { id: uuid.v4(), practice_id: practice_id, client_from: 'quick_create' });
        validateCreateClients(client_info);
        let name = '';
        if (!client_info?.name && client_info?.first_name) {
            name = client_info?.first_name;
        }
        if (!client_info?.name && client_info.middle_name) {
            name = name + ' ' + client_info.middle_name;
        }
        if (!client_info?.name && client_info.last_name) {
            name = name + ' ' + client_info.last_name;
        }
        if (!client_info?.name) client_info.name = name;
        const clientsObj = await Clients.create(client_info);
        if (!clientsObj) throw new HTTPError(404, `could not create client details.`);

        /* Case */
        case_info = Object.assign(case_info, { id: uuid.v4(), client_id: clientsObj.id, practice_id: practice_id, case_from: 'quick_create' });
        if (input?.opposing_counsel) {
            case_info.opposing_counsel = JSON.stringify(input.opposing_counsel);
            case_info.opposing_counsel_info = true;
        }
        validateCreateCases(case_info);
        const caseObj = await Cases.create(case_info);
        if (!caseObj) throw new HTTPError(404, `could not create case details.`);

        /* LegalForms */
        const legalforms_info = {
            id: uuid.v4(), client_id: clientsObj.id, case_id: caseObj.id, practice_id: practice_id,
            filename: legalform_filename, document_type: document_type, set_number: set_number, s3_file_key: s3_file_key
        };
        validateCreateLegalForms(legalforms_info);
        const legalformsObj = await LegalForms.create(legalforms_info);
        if (!legalformsObj) throw new HTTPError(404, `could not create Legalforms details.`);

        /* Create Forms */
        questions = input?.questions && typeof input?.questions === 'string' ? JSON.parse(input?.questions) : input?.questions;
        let modified_question = [];
        for (let i = 0; i < questions.length; i++) {
            const question = questions[i];
            const dataObject = {
                id: uuid.v4(),
                case_id: legalformsObj.case_id,
                practice_id: legalformsObj.practice_id,
                client_id: legalformsObj.client_id,
                legalforms_id: legalformsObj.id,
                document_type: legalformsObj.document_type, // FROGS, SPROGS, RFPD, RFA
                /*  */
                question_text: question.question_text,
                question_number_text: question.question_number,
                question_options: JSON.stringify(question.question_options),
                question_id: parseFloat(question.question_id),
                question_number: parseFloat(question.question_number),
                /*  */
                lawyer_response_status: 'NotStarted', // (NotStarted, Draft, Final)
                lawyer_objection_status: 'NotStarted', // (NotStarted, Draft, Final)
                client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
            };
            if (question.question_section_id) dataObject.question_section_id = question.question_section_id;
            if (question.question_section) dataObject.question_section = question.question_section;
            if (question.question_section_text) dataObject.question_section_text = question.question_section_text;

            if (question.is_consultation_set) dataObject.is_consultation_set = question.is_consultation_set;
            if (question.consultation_set_no) dataObject.consultation_set_no = question.consultation_set_no;
            validateForms(dataObject);
            modified_question.push(dataObject);
        }
        await Forms.bulkCreate(modified_question);
        await DocumentExtractionProgress.update({ legalforms_id: legalformsObj.id }, { where: { id: extraction_id } });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                case_id: legalformsObj.case_id,
                practice_id: legalformsObj.practice_id,
                client_id: legalformsObj.client_id,
                legalforms_id: legalformsObj.id,
                filename: legalformsObj.filename,
                document_type: legalformsObj.document_type,
                state: caseObj.state,
                message: 'Form data processing complete',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not save extraction document data.' }),
        };
    }
};
const createClientCaseAndLegalforms = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { Clients, Cases, LegalForms, Forms, Op, DocumentExtractionProgress, Practices, Users } = await connectToDatabase();
        let { client_info, case_info, practice_id, document_type, set_number, legalform_filename, extraction_id } = input;
        /* Client */
        client_info = Object.assign(client_info, { id: uuid.v4(), practice_id: practice_id, client_from: 'quick_create' });
        validateCreateClients(client_info);
        let name;
        if (client_info.middle_name) { name = client_info.first_name + ' ' + client_info.middle_name + ' ' + client_info.last_name; }
        else {
            name = client_info.first_name + ' ' + client_info.last_name;
        };
        client_info.name = name;
        const clientsObj = await Clients.create(client_info);
        if (!clientsObj) throw new HTTPError(404, `could not create client details.`);

        /* Case */
        case_info = Object.assign(case_info, { id: uuid.v4(), client_id: clientsObj.id, practice_id: practice_id, case_from: 'quick_create' });
        validateCreateCases(case_info);
        const caseObj = await Cases.create(case_info);
        if (!caseObj) throw new HTTPError(404, `could not create case details.`);

        /* LegalForms */
        const legalforms_info = {
            id: uuid.v4(), client_id: clientsObj.id, case_id: caseObj.id, practice_id: practice_id,
            filename: legalform_filename, document_type: document_type, set_number: set_number
        };
        validateCreateLegalForms(legalforms_info);
        const legalformsObj = await LegalForms.create(legalforms_info);
        if (!legalformsObj) throw new HTTPError(404, `could not create Legalforms details.`);
        await DocumentExtractionProgress.update({ legalforms_id: legalformsObj.id }, { where: { id: extraction_id } });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                case_id: legalformsObj.case_id,
                practice_id: legalformsObj.practice_id,
                client_id: legalformsObj.client_id,
                legalforms_id: legalformsObj.id,
                filename: legalformsObj.filename,
                document_type: legalformsObj.document_type,
                state: caseObj.state,
                message: 'Form data processing complete',
            }),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create,Cases and Legalforms details.' }),
        };
    }
}
const getAll = async (event) => {
    try {
        const { DocumentExtractionProgress, Op } = await connectToDatabase();
        const extractionObj = await DocumentExtractionProgress.findAll({
            where: { practice_id: event.user.practice_id },
            attributes: ['id', 'practice_id', 'user_id', 'session_user_id', 'legalform_filename', 'pdf_filename', 's3_file_key', 'set_number', 'state', 'document_type', 'status', 'createdAt']
            , raw: true
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(extractionObj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch document extraction data.' }),
        };
    }
}
const getOne = async (event) => {
    try {
        const { DocumentExtractionProgress, Op, LegalForms, Cases, Practices } = await connectToDatabase();
        const params = event.params || event.pathParameters;
        const extractionObj = await DocumentExtractionProgress.findOne({ where: { id: params.id }, logging: console.log });
        if (!extractionObj) throw new HTTPError(404, `Document extraction with id: ${params.id} was not found`);
        const plaintext = extractionObj.get({ plain: true });
        const practiceObj = await Practices.findOne({ where: { id: plaintext.practice_id }, logging: console.log, raw: true });
        if (plaintext?.document_data) {
            plaintext.document_data = JSON.parse(plaintext.document_data);
            if (!plaintext?.defendant_practice_info) {
                const opposing_counsel_info = plaintext?.document_data?.defendant_practice_details ? await getOpposingCounsilDetails(plaintext?.document_data?.defendant_practice_details) : null;
                // plaintext.document_data.defendant_practice_info =  opposing_counsel_info;
                extractionObj.defendant_practice_info = JSON.stringify(opposing_counsel_info);
                await extractionObj.save();
            }
            if (plaintext?.defendant_practice_info) plaintext.document_data.defendant_practice_info = plaintext.defendant_practice_info;
            if (plaintext?.document_data?.case_info?.case_number) {
                const caseData = await Cases.findOne({ where: { case_number: plaintext.document_data.case_info.case_number, practice_id: plaintext.practice_id }, order: [['createdAt', 'DESC']], logging: console.log });
                if (caseData) {
                    plaintext.case_exists = true;
                    plaintext.existing_case_state = caseData.state;
                }
            }
        }
        if (plaintext?.s3_file_key) {
            const publicUrl = await s3.getSignedUrlPromise('getObject', {
                Bucket: process.env.S3_BUCKET_FOR_FORMS,
                Expires: 60 * 60 * 12,
                Key: plaintext.s3_file_key,
            });
            plaintext.public_url = publicUrl;
        }

        plaintext.is_client_available = false;
        plaintext.is_case_available = false;
        plaintext.practice_name = practiceObj.name;
        if (plaintext?.legalforms_id) {
            const legalformsObj = await LegalForms.findOne({ where: { id: plaintext.legalforms_id }, raw: true, logging: console.log });
            if (legalformsObj?.id) {
                const caseObj = await Cases.findOne({ where: { id: legalformsObj.case_id }, raw: true });
                plaintext.client_id = legalformsObj.client_id;
                plaintext.case_id = legalformsObj.case_id;
                plaintext.set_number = legalformsObj.set_number;
                plaintext.document_type = legalformsObj.document_type;
                plaintext.is_client_available = true;
                plaintext.is_case_available = true;
                plaintext.state = caseObj.state;
                plaintext.case_number = caseObj.case_number;
            }
        }

        if (plaintext?.document_data?.defendant_practice_info) plaintext.document_data.defendant_practice_info = typeof (plaintext.document_data.defendant_practice_info) === "string" ? JSON.parse(plaintext.document_data.defendant_practice_info) : plaintext.document_data.defendant_practice_info;
        console.log(plaintext);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plaintext),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch document extraction data.' }),
        };
    }
}
const destroy = async (event) => {
    try {
        const { DocumentExtractionProgress, Op } = await connectToDatabase();
        const params = event.params || event.pathParameters;
        const extractionObj = await DocumentExtractionProgress.findOne({ where: { id: params.id } });
        let res = {};
        if (extractionObj?.id) {
            res.status = 'Ok.'
            res.message = 'Extracted data removed successfully.'
            await DocumentExtractionProgress.destroy({ where: { id: params.id } });
        } else {
            res.status = 'Ok.'
            res.message = 'extraction data not found.'
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(res),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch document extraction data.' }),
        };
    }
}
const updateExtractionFormsQuestions = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { Forms, Op, DocumentExtractionProgress, Users, Cases } = await connectToDatabase();
        let { practice_id, client_id, case_id, legalforms_id, questions, document_type, extraction_id } = input;
        /* Create Forms */
        let modified_question = [];
        for (let i = 0; i < questions.length; i++) {
            const question = questions[i];
            const dataObject = {
                id: uuid.v4(), case_id: case_id, practice_id: practice_id, client_id: client_id, legalforms_id: legalforms_id, document_type: document_type,
                question_text: question.question_text, question_number_text: question.question_number, question_options: JSON.stringify(question.question_options),
                question_id: parseFloat(question.question_id), question_number: parseFloat(question.question_number),
                lawyer_response_status: 'NotStarted', client_response_status: 'NotSetToClient',
            };
            if (question.question_section_id) dataObject.question_section_id = question.question_section_id;
            if (question.question_section) dataObject.question_section = question.question_section;
            if (question.question_section_text) dataObject.question_section_text = question.question_section_text;

            if (question.is_consultation_set) dataObject.is_consultation_set = question.is_consultation_set;
            if (question.consultation_set_no) dataObject.consultation_set_no = question.consultation_set_no;
            validateForms(dataObject);
            modified_question.push(dataObject);
        }
        await Forms.destroy({ where: { case_id, practice_id, client_id, legalforms_id, document_type } });
        await Forms.bulkCreate(modified_question);
        const extractionObj = await DocumentExtractionProgress.findOne({ where: { id: extraction_id } });
        const usersObj = await Users.findOne({ where: { id: extractionObj.user_id } });
        await extractionObj.destroy();
        const caseObj = await Cases.findOne({ where: { id: case_id } });
        let template = 'Hi';
        if (usersObj?.name) template = template + ' ' + usersObj.name;
        await sendEmail(usersObj.email, 'Important: Questions updated for your case', `${template},<br/>
        Your case, ${caseObj.case_title} has been updated with the questions. <br/>
        Notification from EsquireTek`);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'Ok', message: 'Form data processing complete',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not save extraction document data.' }),
        };
    }
}
const getFailedDocuments = async (event) => {
    try {
        const { DocumentExtractionProgress, Op, Practices, Users } = await connectToDatabase();
        const extractionObj = await DocumentExtractionProgress.findAll({
            where: { status: 'Failed' },
            order: [['createdAt', 'DESC']],
            logging: console.log,
            attributes: ['id', 'practice_id', 'user_id', 'legalform_filename', 'pdf_filename', 's3_file_key', 'set_number', 'state', 'document_type', 'status', 'createdAt']
            , raw: true
        });

        for (let i = 0; i < extractionObj.length; i++) {
            const row = extractionObj[i];
            if (row?.s3_file_key) {
                const publicUrl = await s3.getSignedUrlPromise('getObject', {
                    Bucket: process.env.S3_BUCKET_FOR_FORMS,
                    Expires: 60 * 60 * 1,
                    Key: row.s3_file_key,
                });
                extractionObj[i].public_url = publicUrl;
            }

            const practiceObj = await Practices.findOne({ where: { id: row.practice_id }, raw: true });
            if (practiceObj?.name) extractionObj[i].practice_name = practiceObj.name;
            const usersObj = await Users.findOne({ where: { id: row.user_id }, raw: true });
            if (usersObj?.name) extractionObj[i].user_name = usersObj.name;
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(extractionObj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch document extraction data.' }),
        };
    }
}
const quickCreateHelpRequest = async (event) => {
    try {
        const { DocumentExtractionProgress, Op, Practices } = await connectToDatabase();
        const params = event.params || event.pathParameters;
        const extractionObj = await DocumentExtractionProgress.findOne({ where: { id: params.id } });
        if (!extractionObj) throw new HTTPError(404, `Document extraction with id: ${params.id} was not found`);
        extractionObj.status = 'Failed';
        let email = ''
        if (process.env.CODE_ENV == "staging" || process.env.CODE_ENV == "local") {
            email = 'siva@miotiv.com,bhuvaneshwari.rifluxyss@gmail.com,r.aravinth.rifluxyss@gmail.com';
        } else {
            email = process.env.SUPPORT_EMAIL_RECEIPIENT;
        }
        console.log(extractionObj.practice_id);
        const practiceObj = await Practices.findOne({ where: { id: extractionObj.practice_id } });
        let subject = `Quick Create Document Generation Help Requested `;
        if (event?.user?.name) { subject = subject + 'By ' + event.user.name; };
        await sendEmail(email, subject, `
      User Email : ${event.user.email}<br/>
      Practice Name : ${practiceObj.name}<br/>
      File Name : ${extractionObj.legalform_filename}<br/>
      Document S3 Key : ${extractionObj.s3_file_key}<br/>
      `, 'EsquireTek');
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', message: 'Help request created successfully.' }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create help request.' }),
        };
    }
}
module.exports.setStatus = setStatus;
module.exports.getStatus = getStatus;
module.exports.uploadPrivateFile = uploadPrivateFile;
module.exports.saveExtractionData = saveExtractionData;
module.exports.getAll = getAll;
module.exports.getOne = getOne;
module.exports.destroy = destroy;
module.exports.updateDocumentData = updateDocumentData;
module.exports.updateExtractionFormsQuestions = updateExtractionFormsQuestions;
module.exports.getFailedDocuments = getFailedDocuments;
module.exports.createClientCaseAndLegalforms = createClientCaseAndLegalforms;
module.exports.quickCreateHelpRequest = quickCreateHelpRequest;