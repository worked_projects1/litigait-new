const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');

exports.validateCreateLegalForms = function (data) {
    const rules = {
        practice_id: 'required|min:4|max:64',
        case_id: 'required|min:4|max:64',
        client_id: 'required|min:4|max:64',
        form_id: 'min:4|max:64',
        status: 'required|min:4|max:64',
        filename: 'required'
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateFilename = function (data) {
    const rules = {
        filename: 'required',
        case_id: 'required',
        document_type: 'required'
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateGetOneLegalForms = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateUpdateLegalForms = function (data) {
    const rules = {
        practice_id: 'min:4|max:64',
        case_id: 'min:4|max:64',
        client_id: 'min:4|max:64',
        form_id: 'min:4|max:64',
        status: 'min:4|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateDeleteLegalForms = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateReadLegalForms = function (data) {
    const rules = {
        offset: 'numeric',
        limit: 'numeric',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateDeleteFormsDetails = function (data) {
    const rules = {
        document_type: 'required',
        legalforms_id: 'required',
        case_id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateResponseDate = function (data) {
    const rules = {
        start_date: 'required',
        end_date: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};