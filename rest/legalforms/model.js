module.exports = (sequelize, type) => sequelize.define('LegalForms', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    case_id: type.STRING,
    client_id: type.STRING,
    form_id: type.STRING,
    doc_version_id: type.STRING,
    filename: type.STRING,
    doc_textract_region: type.STRING,
    latest_region_timestamp: type.DATE,   // doc_textract_region latest updated timestamp
    pdf_filename: type.STRING,
    doc_filename: type.TEXT,
    plaintiff_name: type.STRING,
    defendant_name: type.STRING,
    defendant_practice_details: type.TEXT,
    defendant_practice_info: type.TEXT('long'),
    status: type.STRING, // pending, complete
    json_data: type.TEXT,
    document_type: type.STRING, // FROGS, SPROGS, RFPD, RFA
    set_number: type.STRING,
    response_deadline_startdate: type.DATEONLY,
    response_deadline_enddate: type.DATEONLY,
    s3_file_key: type.STRING,
    pdf_s3_file_key: type.TEXT('long'),
    pdf_discovery_status: type.STRING,
    email_sent_date: type.DATE,
    email_sent_to: type.TEXT,
    email_viewed_date: type.DATE,
    email_verification_id: type.STRING,
    attach_documents: type.TEXT('long'),
    generated_document: type.TEXT,
    generated_document_s3_key: type.TEXT,
    final_document: type.TEXT,
    final_document_s3_key: type.TEXT('long'),
    final_doc_edited_At: type.DATE,
    sfdt_s3key: type.TEXT('long'),
    pos_document: type.TEXT,
    pos_document_s3_key: type.TEXT,
    pos_signature: type.TEXT('long'),
    serving_attorney_name: type.STRING,
    serving_attorney_street: type.STRING,
    serving_attorney_city: type.STRING,
    serving_attorney_state: type.STRING,
    serving_attorney_zip_code: type.STRING,
    serving_attorney_email: type.STRING,
    serving_date: type.DATE,
},
{
    indexes: [
        {
            name: 'pdtli',
            fields: ['practice_id', 'case_id', 'client_id', 'document_type', 'createdAt']
        },
        {
            name: 'ccdt',
            fields: ['case_id', 'client_id', 'document_type']
        },
        {
            name: 'pcc',
            fields: ['practice_id', 'case_id', 'client_id']
        },
        {
            name: 'pccc',
            fields: ['practice_id', 'case_id', 'client_id', 'createdAt']
        },
        {
            name: 'dt',
            fields: ['document_type']
        }
    ]
});

/*
FROGS - Form Interrogatories (or) initial Disclosure
SPROGS - Specially Prepared Interrogatories (or) Interrogatories
RFPD - Requests For Production of Documents
RFA - Request For Admissions
BOP - Bill of Particulars
ODD - Omnibus Discovery Demands
NDI - Notice for Discovery and Inspection
NTA - Notice to Admit
*/