const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const AWS = require('aws-sdk');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const phoneUtil = require("google-libphonenumber").PhoneNumberUtil.getInstance();
const { sendEmail, sendEmailwithCC } = require("../../utils/mailModule");
AWS.config.update({ region: process.env.REGION || 'us-east-1' });
const s3 = new AWS.S3();
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const authMiddleware = require('../../auth');
const { QueryTypes } = require('sequelize');
const { generateRandomString } = require('../../utils/randomStringGenerator');
const { documentTypes } = require('../../rest/helpers/documentType.helper');

const {
    validateCreateLegalForms,
    validateGetOneLegalForms,
    validateUpdateLegalForms,
    validateDeleteLegalForms,
    validateReadLegalForms,
    validateFilename,
    validateDeleteFormsDetails,
    validateResponseDate
} = require('./validation');
const { includes, random } = require('lodash');
const { respondingPlans, respondingYearlyPlans, respondingMonthlyPlans, propoundingMonthlyPlans,
    propoundingPlans, propoundingYearlyPlans
} = require('../helpers/plans.helper');

const create = async (event) => {
    try {
        let id;
        const input = JSON.parse(event.body);
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, { id: id || uuid.v4() });
        validateCreateLegalForms(dataObject);
        const { LegalForms } = await connectToDatabase();
        const legalForms = await LegalForms.create(dataObject);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(legalForms),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the legalForms.' }),
        };
    }
};
const getOne = async (event) => {
    try {
        validateGetOneLegalForms(event.pathParameters);
        const { LegalForms } = await connectToDatabase();
        const legalForms = await LegalForms.findOne({ where: { id: event.pathParameters.id } });
        if (!legalForms) throw new HTTPError(404, `LegalForms with id: ${event.pathParameters.id} was not found`);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(legalForms),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the LegalForms.' }),
        };
    }
};

const getAll_new = async (event) => {
    try {
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        validateReadLegalForms(query);
        const { LegalForms } = await connectToDatabase();
        const legalForms = await LegalForms.findAll(query);
        const legalFormsCount = await LegalForms.count();
        let totalPages, currentPage;
        if (query.limit && (legalForms.length > 0)) {
            totalPages = Math.ceil(legalFormsCount / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const legalFormsDetails = {
            total_items: legalFormsCount,
            response: legalForms,
            total_pages: totalPages,
            current_page: currentPage
        };
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(legalFormsDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the legalForms.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        validateReadLegalForms(query);
        const { LegalForms } = await connectToDatabase();
        const legalForms = await LegalForms.findAll(query);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(legalForms),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the legalForms.' }),
        };
    }
};
const update = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateUpdateLegalForms(input);
        const { LegalForms } = await connectToDatabase();
        const legalForms = await LegalForms.findOne({ where: { id: event.pathParameters.id } });
        if (!legalForms) throw new HTTPError(404, `LegalForms with id: ${event.pathParameters.id} was not found`);
        const updatedModel = Object.assign(legalForms, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the LegalForms.' }),
        };
    }
};
const destroy = async (event) => {
    try {
        validateDeleteLegalForms(event.pathParameters);
        const { LegalForms } = await connectToDatabase();
        const legalForms = await LegalForms.findOne({ where: { id: event.pathParameters.id } });
        if (!legalForms) throw new HTTPError(404, `LegalForms with id: ${event.pathParameters.id} was not found`);
        await legalForms.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(legalForms),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy the LegalForms' }),
        };
    }
};
const getUploadURL = async (input, legalForms) => {
    let fileExtention = '';
    let documentType = '';
    if (input.file_name) {
        fileExtention = `.${input.file_name.split('.').pop()}`;
    }
    if (input.document_type) {
        documentType = `_${input.document_type}`;
    }
    const s3Params = {
        Bucket: process.env.S3_BUCKET_FOR_FORMS,
        Key: `${legalForms.id}${documentType}${fileExtention}`,
        ContentType: input.content_type,
        ACL: 'private',
        Metadata: {
            legal_form_id: legalForms.id,
            practice_id: legalForms.practice_id,
            case_id: legalForms.case_id,
            client_id: legalForms.client_id,
        },
    };
    return new Promise((resolve, reject) => {
        const uploadURL = s3.getSignedUrl('putObject', s3Params);
        resolve({
            uploadURL,
            s3_file_key: s3Params.Key,
        });
    });
};
const uploadLegalForm = async (event) => {
    try {
        let id;
        const input = JSON.parse(event.body);
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, { id: id || uuid.v4(), status: 'pending' });
        validateCreateLegalForms(dataObject);
        const { LegalForms, Forms, OtherPartiesForms, Op, sequelize, DocTextractRegion } = await connectToDatabase();
        const existing_legalforms_id = input.legalforms_id;
        if (existing_legalforms_id) {
            const existingLegalForm = await LegalForms.findOne({
                where: {
                    id: existing_legalforms_id,
                    practice_id: input.practice_id,
                    case_id: input.case_id,
                    client_id: input.client_id,
                }, logging: console.log, raw: true
            });
            if (existingLegalForm) {
                await Forms.destroy({
                    where: {
                        legalforms_id: existingLegalForm.id,
                        client_id: input.client_id,
                        case_id: input.case_id
                    }, logging: console.log
                });
                await OtherPartiesForms.destroy({
                    where: {
                        legalforms_id: existingLegalForm.id,
                        practice_id: input.practice_id,
                        case_id: input.case_id,
                    }, logging: console.log
                });
                await LegalForms.destroy({
                    where: {
                        id: existingLegalForm.id,
                        practice_id: input.practice_id,
                        case_id: input.case_id,
                        client_id: input.client_id,
                    }, logging: console.log
                });
            }
        }
        let response = {};
        //Upload region and its count starts
        let index = 0;
        let region = null;
        const currentTime = new Date();
        const allRegion = (process.env.DOCUMENT_EXTRACT_REGION).split(',');
        const regionDocDataLegalFroms = await DocTextractRegion.findAll({
            where: {
                doc_textract_region: allRegion,
                createdAt: {
                    [Op.lt]: new Date(),
                    [Op.gt]: new Date(new Date() - 10 * 60 * 1000)
                }
            },
            attributes: [
                'doc_textract_region',
                [sequelize.fn('COUNT', sequelize.col('doc_textract_region')), 'type_count'],
            ],
            group: ['doc_textract_region'],
            order: [[sequelize.col('type_count'), 'ASC']],
            raw: true
        });
        if (regionDocDataLegalFroms.length < allRegion.length) {
            { region = allRegion[regionDocDataLegalFroms.length] };
        }
        response.doc_textract_region = region || regionDocDataLegalFroms[index]?.doc_textract_region || 'us-east-1';
        response.doc_textract_region_count = region ? 0 : regionDocDataLegalFroms[index]?.type_count;

        dataObject.doc_textract_region = response.doc_textract_region;
        dataObject.latest_region_timestamp = currentTime;

        const legalForms = await LegalForms.create(dataObject);
        uploadURLObject = await getUploadURL(input, legalForms);
        legalForms.s3_file_key = uploadURLObject.s3_file_key;
        await legalForms.save();
        response.legalForms = legalForms;
        response.uploadURL = uploadURLObject.uploadURL;
        response.s3_file_key = uploadURLObject.s3_file_key;

        const regionObj = Object.assign({}, {
            id: uuid.v4(),
            practice_id: event.user.practice_id,
            user_id: event.user.id,
            legalforms_id: legalForms.id,
            region_sent_timestamp: currentTime,
            doc_textract_region: response.doc_textract_region,
            uploaded_type: 'Responding'
        });
        const textractRegionData = await DocTextractRegion.create(regionObj);
        response.doc_textract_region_id = textractRegionData.id;

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(response),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }
};

const createLegalformDetailsforHelpRequest = async (event) => {
    try {
        const input = JSON.parse(event.body);

        const dataObject = Object.assign(input, { id: uuid.v4(), status: 'pending' });
        validateCreateLegalForms(dataObject);
        const { LegalForms, Forms, OtherPartiesForms } = await connectToDatabase();

        let existingLegalformID = input.legalforms_id;
        if (existingLegalformID) {
            await Forms.destroy({ where: { legalforms_id: existingLegalformID.id } });
            await OtherPartiesForms.destroy({ where: { legalforms_id: existingLegalformID.id } });
            await LegalForms.destroy({ where: { id: existingLegalformID.id } });
        }
        const legalForms = await LegalForms.create(dataObject);
        uploadURLObject = await getUploadURL(input, legalForms);
        legalForms.s3_file_key = uploadURLObject.s3_file_key;
        await legalForms.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                legalForms,
                uploadURL: uploadURLObject.uploadURL,
                s3_file_key: uploadURLObject.s3_file_key,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }

};
const saveFormData = async (event) => {
    try {
        const { Forms, LegalForms, HelpRequest, OtherPartiesForms, OtherParties, Users, Op, HashedFiles } = await connectToDatabase();
        const input = JSON.parse(event.body);
        console.log(input);
        const { client_id, document_type, plaintiff_name, defendant_practice_details, defendant_name, questions, action_by, legalforms_id, hash_id, state, doc_textract_region } = input;
        const queryParams = event.queryStringParameters || {};
        const legalForms = await LegalForms.findOne({
            where: {
                id: event.pathParameters.id,
                client_id: input.client_id,
                document_type: input.document_type
            }, logging: console.log
        });
        const query = {};
        query.raw = true;
        const question_encode = queryParams.question_encode;

        const formsBulkData = [];
        const otherPartiesFormsBulkData = [];
        const hashQuestions = [];

        if (!legalForms) throw new HTTPError(404, `LegalForms with id: ${event.pathParameters.id} was not found`);
        if (plaintiff_name) {
            legalForms.plaintiff_name = plaintiff_name;
        }
        if (defendant_practice_details) {
            legalForms.defendant_practice_details = defendant_practice_details;
        }
        if (defendant_name) {
            legalForms.defendant_name = defendant_name;
        }
        if ((plaintiff_name && defendant_name) || (defendant_practice_details)) {
            await legalForms.save();
        }

        const otherPartiesModel = await OtherParties.findAll({
            where: {
                case_id: legalForms.case_id,
                practice_id: legalForms.practice_id
            }
        });
        for (let i = 0; i < questions.length; i += 1) {
            if (question_encode == 'true') {
                questions[i].question_text = Buffer.from(questions[i].question_text, 'base64').toString('utf8')
            }
            
            const questionText = questions[i]?.question_text;
            questions[i].question_text = questionText.replaceAll('\u00a7','§');
            questions[i].question_text = questionText.replaceAll('\\u00a7','§');

            let question_id = uuid.v4();
            let question_category = null;
            let question_category_id = null;
            if(document_type.toUpperCase() == 'ODD'){
            question_category = questions[i]?.question_category || null;
            question_category_id = questions[i]?.question_category_id || null;
            }
            const dataObject = {
                id: question_id,
                case_id: legalForms.case_id,
                practice_id: legalForms.practice_id,
                client_id: legalForms.client_id,
                legalforms_id: event.pathParameters.id,
                document_type: document_type, // FROGS, SPROGS, RFPD, RFA
                question_type: questions[i].question_type, // only for initial discloser forms (FROGS, SPROGS, RFPD, RFA)
                duplicate_set_no: questions[i].duplicate_set_no || 1,
                is_duplicate: questions[i].is_duplicate || false,
                question_number: parseFloat(questions[i].question_number),
                question_number_text: questions[i].question_number,
                question_id: parseFloat(questions[i].question_id),
                question_category: question_category,
                question_category_id: question_category_id,
                question_text: questions[i].question_text,
                question_section_id: questions[i].question_section_id,
                question_section: questions[i].question_section,
                question_section_text: questions[i].question_section_text,
                question_options: JSON.stringify(questions[i].question_options),
                is_consultation_set: questions[i].is_consultation_set || false,
                consultation_set_no: questions[i].consultation_set_no || 1,
                lawyer_response_status: 'NotStarted', // (NotStarted, Draft, Final)
                lawyer_objection_status: 'NotStarted', // (NotStarted, Draft, Final)
                client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
            };

            formsBulkData.push(dataObject);
            hashQuestions.push(questions[i]);
            if (otherPartiesModel.length != 0) {
                console.log('--Inside Other parties--');
                // for(let n=0;n<otherPartiesModel.length;n++)
                for (let n = 0; n < otherPartiesModel.length; n++) //4
                {
                    let otherPartiesFormRow = Object.assign({}, dataObject, {
                        form_id: question_id,
                        id: uuid.v4(),
                        party_id: otherPartiesModel[n].id
                    });
                    otherPartiesFormsBulkData.push(otherPartiesFormRow);
                    // otherPartiesFormsBulkData[otherPartiesFormsBulkData.length] = otherPartiesFormRow;
                }
            }
        }
        await Forms.bulkCreate(formsBulkData);
        if (otherPartiesFormsBulkData.length > 0) {
            await OtherPartiesForms.bulkCreate(otherPartiesFormsBulkData);
        }
        const adminRoles = ['superAdmin', 'QualityTechnician'];
        if (action_by && adminRoles.includes(action_by)) {
            const helpRequestObject = await HelpRequest.findOne({
                where: {
                    case_id: legalForms.case_id,
                    document_type: document_type
                }
            });
            if (helpRequestObject) {
                const userObject = await Users.findOne({
                    where: {
                        email: helpRequestObject.user_email,
                        is_deleted: { [Op.not]: true }
                    }
                });
                if (userObject) {
                    let template = 'Hi';
                    if (userObject?.name) template = template + ' ' + userObject?.name;
                    await sendEmail(userObject.email, 'Important: Questions updated for your case', `${template},<br/>
          Your case, ${helpRequestObject.case_title} has been updated with the questions. <br/>
          Notification from EsquireTek`);
                    await HelpRequest.destroy({
                        where: {
                            case_id: legalForms.case_id,
                            document_type: document_type,
                            client_id: client_id,
                            legalforms_id: legalforms_id
                        }
                    });
                }
            }
        }
        // let hashDataId = null;
        // if(hash_id){
        // const hashedData = await HashedFiles.findOne({where:{ id:hash_id }});
        // if(!hashedData) throw new HTTPError(400, 'hash data not found');
        // hashedData.questions = JSON.stringify(hashQuestions);
        // hashedData.questions_available = 'true';
        // hashedData.document_type = document_type;
        // hashedData.state = state;
        // await hashedData.save();
        // hashDataId = hashedData?.id;
        // }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                legal_form_id: event.pathParameters.id,
                // hash_id: hashDataId,`
                message: 'Form data processing complete',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the LegalForms.' }),
        };
    }
};

const saveGeneratedDocument = async (event) => {
    try {
        const { LegalForms, Orders, Practices, Settings, Cases, Clients, Op, PracticeSettings, Subscriptions, Plans } = await connectToDatabase();
        let validSubscriptionFeatures = [];
        const subscriptionDetails = await Subscriptions.findOne({
            where: { practice_id: event.user.practice_id, plan_category: 'responding' },
            order: [['createdAt', 'DESC']],
            logging: console.log,
            raw: true
        });
        let subscriptionValidity, sub_plain = undefined;
        const subscriptionId = subscriptionDetails?.stripe_subscription_id;
        if (subscriptionDetails && subscriptionDetails.stripe_subscription_data && subscriptionDetails.plan_id) {
            const stripeSubscriptionDataObject = JSON.parse(subscriptionDetails.stripe_subscription_data);
            subscriptionValidity = new Date(parseInt(stripeSubscriptionDataObject.current_period_end) * 1000);
        }
        const today = new Date();
        if ((subscriptionValidity > today) || ['responding_vip', 'propounding_vip'].includes(subscriptionDetails?.plan_id)) {
            let query = {};
            if (subscriptionDetails?.plan_id == 'responding_vip') query.plan_id = 'responding_vip';
            else if (subscriptionDetails?.plan_id == 'propounding_vip') query.plan_id = 'propounding_vip';
            else query.stripe_product_id = subscriptionDetails.stripe_product_id;
            planDetails = await Plans.findOne({ raw: true, where: query, logging: console.log });
            validSubscriptionFeatures = planDetails.features_included.split(',');
        }
        const input = JSON.parse(event.body);

        let plan_type;
        if (input.plan_type == 'free_trial') {
            plan_type = 'FREE TRIAL';
        } else if (input.plan_type == 'pay_as_you_go' || input.plan_type == 'tek_as_you_go') {
            plan_type = 'TEK AS-YOU-GO';
        } else if (respondingMonthlyPlans.includes(input.plan_type)) {
            plan_type = 'MONTHLY';
        } else if (respondingYearlyPlans.includes(input.plan_type)) {
            plan_type = 'YEARLY';
        } else if (propoundingMonthlyPlans.includes(input.plan_type)) {
            plan_type = 'MONTHLY PROPOUNDING';
        } else if (propoundingYearlyPlans.includes(input.plan_type)) {
            plan_type = 'YEARLY PROPOUNDING';
        } else if (['responding_vip', 'propounding_vip'].includes(input.plan_type)) {
            plan_type = 'VIP';
        }

        if (!plan_type) throw new HTTPError(400, `Plan Type not found.`);
        if (!input.legalforms_id) throw new HTTPError(400, 'Legalform id was not found');
        const legalForm = await LegalForms.findOne({ where: { id: input.legalforms_id } });

        if (!legalForm) throw new HTTPError(400, 'Relevant previously generated legal form data not found');
        
        if(input?.doc_filename)legalForm.doc_filename = input.doc_filename;
        if (input.document_generation_type === 'final') {
            input.document_generation_type = 'final_doc';
        }
        if (input?.pos_signature) legalForm.pos_signature = input.pos_signature;
        if (input.document_generation_type === 'final_doc') {
            legalForm.final_document = input.final_document;
            legalForm.final_document_s3_key = input.final_document_s3_key;
        } else if (input.document_generation_type === 'pos') {
            legalForm.pos_document = input.pos_document;
            legalForm.pos_document_s3_key = input.pos_document_s3_key;
        } else {
            input.document_generation_type = 'template';
            legalForm.generated_document = input.generated_document;
            legalForm.generated_document_s3_key = input.generated_document_s3_key;
        }
        legalForm.serving_attorney_name = input.serving_attorney_name;
        legalForm.serving_attorney_street = input.serving_attorney_street;
        legalForm.serving_attorney_city = input.serving_attorney_city;
        legalForm.serving_attorney_state = input.serving_attorney_state;
        legalForm.serving_attorney_zip_code = input.serving_attorney_zip_code;
        legalForm.serving_attorney_email = input.serving_attorney_email;
        legalForm.serving_date = input.serving_date;

        const CaseDetails = await Cases.findOne({ where: { id: input.case_id } });
        const clientDetails = await Clients.findOne({ where: { id: CaseDetails.client_id } });

        const orderData = {
            id: uuid.v4(),
            order_date: new Date(),
            status: 'completed', // completed, pending
            practice_id: event.user.practice_id,
            user_id: event.user.id,
            case_id: input.case_id,
            client_id: CaseDetails.client_id,
            document_type: input.document_type, // (FROGS, SPROGS, RFPD, RFA)
            document_generation_type: input.document_generation_type, // template,pos or final_doc
            amount_charged: 0,
            charge_id: '',
            case_title: CaseDetails.case_title,
            client_name: clientDetails.name,
            legalforms_id: input.legalforms_id,
            filename: legalForm.filename,
            plan_type: plan_type
        };

        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);
        orderData.billing_type = practiceObject.billing_type || null;

        const settingsObject = await Settings.findOne({ where: { key: 'global_settings' }, raw: true, });

        if (!settingsObject) throw new HTTPError(400, 'Settings data not found');
        let settings = JSON.parse(settingsObject.value);

        const practiceSettings = await PracticeSettings.findOne({ where: { practice_id: event.user.practice_id, }, raw: true, });
        if (practiceSettings) {
            const practiceSettingsObject = JSON.parse(practiceSettings.value);
            settings = Object.assign(settings, practiceSettingsObject);
        }

        const document_type = input.document_type;
        const document_generation_type = input.document_generation_type;
        const availableFormDocumenttype = await documentTypes({ state: 'all', outputType: 'types' });

        const freeQuotaUseageCount = await Orders.count({
            where: {
                practice_id: event.user.practice_id,
                document_type: { [Op.in]: availableFormDocumenttype },
                amount_charged: 0,
            }
        });

        let free_tier_available = true;
        if (freeQuotaUseageCount >= parseInt(settings.no_of_docs_free_tier, 10)) {
            free_tier_available = false;
        }

        let price = 0;
        if (document_generation_type == 'pos' && settings) {
            if (!settings.pos) throw new HTTPError(400, 'POS Data not found in Settings');
            price = settings.pos;
        } else {
            price = parseFloat(settings[`${document_generation_type}_generation_price`][document_type]);
        }

        let credit_card_details_available = false;

        if (practiceObject && practiceObject.stripe_customer_id) {
            credit_card_details_available = true;
        }
        if (validSubscriptionFeatures.includes('shell')) {
            price = 0;
        }
        if (validSubscriptionFeatures.includes('discovery')) {
            price = 0;
        }
        if (validSubscriptionFeatures.includes('pos')) {
            price = 0;
        }
        console.log(validSubscriptionFeatures);
        console.log('price : ' + price);
        console.log('free_tier_available : ' + free_tier_available);
        if (free_tier_available || price == 0) {
            await Orders.create(orderData);
        } else {
            if (!practiceObject.stripe_customer_id) {
                throw new HTTPError(400, 'Free tire exceeded but payment method is not added yet');
            }
            const paymentMethods = await stripe.paymentMethods.list({
                customer: practiceObject.stripe_customer_id,
                type: 'card',
            });
            if (!paymentMethods.data.length) {
                throw new HTTPError(400, 'Free tire exceeded but payment method not added');
            }

            if (price && price > 0 && price < 0.50) {
                price = 0.50;   //minimum pricing charge
            }

            const paymentMethod = paymentMethods.data[0];
            const paymentIntent = await stripe.paymentIntents.create({
                amount: parseInt(price * 100),
                currency: 'usd',
                customer: practiceObject.stripe_customer_id,
                payment_method: paymentMethod.id,
                off_session: true,
                confirm: true,
            });
            orderData.charge_id = paymentIntent.id;
            orderData.amount_charged = price;
            await Orders.create(orderData);
        }
        legalForm.pdf_discovery_status = 'new';
        await legalForm.save();
        const legalFormRawText = await LegalForms.findOne({ where: { id: input.legalforms_id } });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                legalFormRawText,
                message: 'Generated document saved',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the LegalForms.' }),
        };
    }
};
const deleteForm = async (event) => {
    try {
        const input = JSON.parse(event.body);
        const { Forms, LegalForms, OtherPartiesForms } = await connectToDatabase();

        validateDeleteFormsDetails(input);
        const caseId = input.case_id;
        const legalforms_id = input.legalforms_id;
        const documentType = input.document_type;
        await Forms.destroy({ where: { case_id: caseId, document_type: documentType, legalforms_id: legalforms_id } });
        await LegalForms.destroy({ where: { case_id: caseId, document_type: documentType, id: legalforms_id } });
        await OtherPartiesForms.destroy({
            where: {
                case_id: caseId,
                document_type: documentType,
                legalforms_id: legalforms_id
            }
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'data removed successfully',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy the LegalForms' }),
        };
    }
};
const shredForm = async (event) => {
    try {
        const input = event.body;
        const { LegalForms } = await connectToDatabase();
        const LegalFormsDetails = await LegalForms.findOne({ where: { id: input.id } });
        if (!LegalFormsDetails) throw new HTTPError(404, `LegalForms with id: ${input.id} was not found`);
        if (LegalFormsDetails.final_document) {
            let legalFormsPlainText = LegalFormsDetails.get({ plain: true });
            const s3Params = {
                Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
                Key: LegalFormsDetails.final_document_s3_key,
            };
            const aws_status = await s3.deleteObject(s3Params).promise();
            legalFormsPlainText.final_document_s3_key = null;
            legalFormsPlainText.final_document = null;
            if (LegalFormsDetails.pdf_s3_file_key) {
                await s3.deleteObject({ Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS, Key: LegalFormsDetails.pdf_s3_file_key, }).promise();
                legalFormsPlainText.pdf_s3_file_key = null;
            }
            await LegalForms.update(legalFormsPlainText, { where: { id: input.id } });
        } else {
            throw new HTTPError(404, `Final Doucument not found this Case`);
        }
        const LegalFormsObj = await LegalForms.findOne({ where: { id: input.id } });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(LegalFormsObj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the LegaForms.' }),
        };
    }
};
const updateFilename = async (event) => {
    try {
        const input = event.body;
        const { LegalForms } = await connectToDatabase();
        const legalforms_id = event.params.id;
        const filename = input.filename;
        validateFilename(input);
        const existingLegalform = await LegalForms.findAll({
            where: {
                practice_id: event.user.practice_id,
                case_id: input.case_id,
                filename: input.filename,
                document_type: input.document_type
            }
        });
        if (existingLegalform.length) throw new HTTPError(404, `File Name  ${input.filename} has been already existing in this Doument Type `);
        const legalformsObj = await LegalForms.update({ filename: filename }, { where: { id: legalforms_id } });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ success: 'Filename has been updated successfully' }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the LegaForms.' }),
        };
    }
};
const updateFormsResponsedeadlineDate = async (event) => {
    try {
        const input = event.body;
        validateResponseDate(input);
        const { LegalForms, sequelize, Op } = await connectToDatabase();
        if (!input.legalforms_id) throw new HTTPError(404, 'Legalforms id was  not found');
        const updateColumn = {};
        const updateQuery = {};
        updateColumn.response_deadline_startdate = input.start_date;
        updateColumn.response_deadline_enddate = input.end_date;
        updateQuery.id = input.legalforms_id;
        updateQuery.case_id = input.case_id;
        updateQuery.client_id = input.client_id;
        updateQuery.practice_id = input.practice_id;
        const form = await LegalForms.update(updateColumn, { where: updateQuery });
        let sqlQuery = 'SELECT Cases.*,Clients.name as client_name, ' +
            '(SELECT SUM(amount_charged) amount_charged FROM Orders WHERE case_id = ' + "'" + input.case_id + "'" + ' ) AS total_cost, ' +
            '(SELECT MIN(response_deadline_enddate) FROM LegalForms WHERE case_id = ' + "'" + input.case_id + "'" + ' AND date(response_deadline_enddate) >= date(now())) AS due_date ' +
            'FROM (Clients INNER JOIN Cases ON Clients.id = Cases.client_id) WHERE Cases.is_deleted IS NOT true AND Cases.practice_id =' + "'" + input.practice_id + "'" + ' order by createdAt desc limit 1';
        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: "ok", due_date: serverData[0].due_date }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Forms.' }),
        };
    }
};

const sendEmailForms = async (event) => {
    try {
        const { LegalForms, Practices, Users, Cases, Op } = await connectToDatabase();

        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;

        const to_opposing_counsel_email_arr = input?.to_opposing_counsel_email ? input.to_opposing_counsel_email.split(",") : [];
        if (to_opposing_counsel_email_arr.length === 0) throw new HTTPError(400, "No email details found");

        const attach_documents = input.attach_documents;

        if (attach_documents.length > 0) {
            await LegalForms.update({ attach_documents: JSON.stringify(input.attach_documents) }, {
                where: {
                    id: input.attach_document_id,
                    practice_id: event.user.practice_id,
                    case_id: input.case_id,
                },
            });
        }
        if (attach_documents.length == 0) {
            await LegalForms.update({ attach_documents: null }, {
                where: {
                    id: input.attach_document_id,
                    practice_id: event.user.practice_id,
                    case_id: input.case_id,
                },
            });
        }

        let email_template, forms_obj;
        let cc_email = undefined;

        const casesObj = await Cases.findOne({
            where: { id: input.case_id },
            is_deleted: { [Op.not]: true },
            logging: console.log,
            raw: true,
        });
        if (!casesObj) throw new HTTPError(400, "case not found");

        const opposingCounsel = JSON.parse(casesObj.opposing_counsel);
        const emailAndName = [];
        const emailSentTo = [];

        for (let i = 0; i < opposingCounsel.length; i++) {
            console.log(opposingCounsel[i]);
            let emails = opposingCounsel[i].opposing_counsel_email.split(',');
            let nameArr = opposingCounsel[i].opposing_counsel_attorney_name.split(',');
            let opposingPractice = opposingCounsel[i].opposing_counsel_office_name;
            for (let k = 0; k < emails.length; k++) {
                emailAndName.push({
                    email: emails[k],
                    name: nameArr.length == 1 ? nameArr[0] : opposingPractice
                });
                emailSentTo.push(emails[k]);
            }
        };

        email_template = "<ul>";

        const selectedForms = input.selected_forms;
        const formsLength = selectedForms.length;
        if (formsLength == 0) throw new HTTPError(400, "Legal forms id not provided");

        const formsDocTypeWithCount = {};
        const availableFormDocumenttype = await documentTypes({ state: casesObj.state, outputType: 'types', letterCase: 'small' });
        for (let i = 0; i < availableFormDocumenttype.length; i++) {
            const DocType = availableFormDocumenttype[i];
            formsDocTypeWithCount[DocType] = selectedForms.filter(form => form.document_type == DocType).length;
        }
        for (let j = 0; j < formsLength; j++) {
            const formDocType = selectedForms[j].document_type;
            const orderId = selectedForms[j].orderId;
            forms_obj = await LegalForms.findOne({
                where: {
                    id: selectedForms[j].id,
                    practice_id: event.user.practice_id,
                    case_id: input.case_id,
                },
            });
            if (!forms_obj) {
                throw new HTTPError(
                    404,
                    `form with id: ${selectedForms[j].id} was not found`
                );
            }

            if (!forms_obj?.pdf_s3_file_key) throw new HTTPError(400, "cannot find pdf s3 file key");
            if (forms_obj && !forms_obj.s3_file_key) {
                throw new HTTPError(400, "Document details not found.");
            }
            const email_verification_id = generateRandomString(12);

            const Download_url = `${process.env.API_URL}/rest/legalforms/email-document-download?id=${selectedForms[j].id}&verification=${email_verification_id}`;

            if (!forms_obj?.pdf_filename) throw new HTTPError(400, "Pdf filename not found");

            email_template += `<li><a href=${Download_url}>${forms_obj.pdf_filename}${formsDocTypeWithCount[formDocType] == '1' ? '' : ' FORM ' + orderId}</a></li>`;
            const emailSent = {
                emails: emailSentTo,
                cc_email: null
            }

            if (input.cc_responder_email) cc_email = emailSent.cc_email = input.cc_responder_email;
            if (forms_obj?.email_viewed_date) forms_obj.email_viewed_date = null;
            forms_obj.email_verification_id = email_verification_id;
            forms_obj.email_sent_date = new Date();
            forms_obj.email_sent_to = JSON.stringify(emailSent);
            await forms_obj.save();

        }

        if (attach_documents?.length > 0) {
            email_template += "<br>Other Documents:<br><br>";
        }

        for (let q = 0; q < attach_documents.length; q++) {
            email_template += `<li><a href=${attach_documents[q]?.public_url}>${attach_documents[q]?.filename}</a></li>`;
        }

        email_template += "</ul>";

        const practice_details = await Practices.findOne({
            where: {
                is_deleted: { [Op.not]: true },
                id: event.user.practice_id,
            },
            raw: true,
        });

        const user_details = await Users.findOne({
            where: { id: event.user.id, is_deleted: { [Op.not]: true } },
            raw: true,
        });

        // let to_email_string = to_opposing_counsel_email_arr;

        let practice_address = practice_details.name + ",";
        if (practice_details.street)
            practice_address += "<br>" + practice_details.street;
        if (practice_details.city)
            practice_address += "<br>" + practice_details.city + ",";
        if (practice_details.state)
            practice_address += "&nbsp;" + practice_details.state;
        if (practice_details.zip_code)
            practice_address += "&nbsp;" + practice_details.zip_code;
        if (practice_details.phone) {
            const Mobile = phoneUtil.parseAndKeepRawInput(
                practice_details.phone,
                "US"
            );
            const MobileNumber = phoneUtil.formatInOriginalFormat(Mobile, "US");
            practice_address += "<br>" + "Telephone: " + MobileNumber;
        }
        if (practice_details.fax) {
            const Fax = phoneUtil.parseAndKeepRawInput(
                practice_details.fax,
                "US"
            );
            const FaxNumber = phoneUtil.formatInOriginalFormat(Fax, "US");
            practice_address += "<br>" + "Fax: " + FaxNumber;
        }
        if (user_details.email) practice_address += "<br>" + "Email: " + user_details.email;

        for (let l = 0; l < emailAndName.length; l++) {
            if (cc_email) {
                await sendEmailwithCC(
                    emailAndName[l].email,
                    cc_email,
                    `ELECTRONIC SERVICE - Responding ${formsLength > 1 ? "docs" : "doc"} for ${casesObj.case_title}`,
                    `Dear ${emailAndName[l].name},<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Please click the link below to download the electronic service in the above referenced matter: ${email_template}Should you have any questions, please don't hesitate to contact me at the number listed below.<br><br>Sincerely,<br>${practice_address}`,
                    practice_details.name,
                );
            } else {
                await sendEmail(
                    emailAndName[l].email,
                    `ELECTRONIC SERVICE - Responding ${formsLength > 1 ? "docs" : "doc"} for ${casesObj.case_title}`,
                    `Dear ${emailAndName[l].name},<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Please click the link below to download the electronic service in the above referenced matter: ${email_template}Should you have any questions, please don't hesitate to contact me at the number listed below.<br><br>Sincerely,<br>${practice_address}`,
                    practice_details.name,
                );
            }
        }
        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                status: "Ok",
                message: "Email sent successfully.",
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error:
                    err.message || "could not send propound forms details to responder.",
            }),
        };
    }
}

const saveS3FileKeyForPdf = async (event) => {
    try {
        const { LegalForms, Op } = await connectToDatabase();
        const input =
            typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const queryParams = event.pathParameters || event.params;

        const formsObj = await LegalForms.findOne({
            where: { id: queryParams.id },
        });

        if (!formsObj)
            throw new HTTPError(404, `Legal forms id: ${queryParams.id} was not found`);

        if(input?.s3_file_key) formsObj.final_document_s3_key = input.s3_file_key;
        const pdfS3FileKey = input?.pdf_s3_file_key;
        if (pdfS3FileKey) {
            formsObj.pdf_s3_file_key = pdfS3FileKey;
            const pdfName = pdfS3FileKey.split('.pdf').join('').split('/').pop();
            formsObj.pdf_filename = pdfName;
            formsObj.pdf_discovery_status = 'Completed';
        }
        const saveObj = await formsObj.save();
        const plaintext = saveObj.get({ plain: true });
        console.log(plaintext);
        if(plaintext?.public_url){
            plaintext.public_url = await s3.getSignedUrlPromise("getObject", {
                Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
                Expires: 60 * 60 * 1,
                Key: plaintext.pdf_s3_file_key,
            });
        }

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify(plaintext),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error: err.message || "could not fetch legal forms details.",
            }),
        };
    }
};

const QuickCreateFormsCreate = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const caseNumber = input?.case_number;
        const { Forms, Op, LegalForms, Cases, DocumentExtractionProgress } = await connectToDatabase();

        const CaseData = await Cases.findOne({
            where: {
                case_number: caseNumber,
                practice_id: event.user.practice_id,
            }, order: [['createdAt', 'DESC']]
        });

        if (!CaseData) { throw new HTTPError(400, 'case not found') };

        const legalFormsObject = Object.assign({}, {
            id: uuid.v4(),
            client_id: CaseData.client_id,
            case_id: CaseData.id,
            practice_id: event.user.practice_id,
            filename: input.legalform_filename,
            status: 'pending',
            document_type: input.document_type,
            s3_file_key: input.s3_file_key

        });

        const legalformsObj = await LegalForms.create(legalFormsObject);
        if (!input?.extraction_id) throw new HTTPError(400, 'Extraction Id not found.');
        await DocumentExtractionProgress.update({ legalforms_id: legalformsObj.id }, { where: { id: input.extraction_id } });
        const questions_count = input?.questions?.length;
        const questions = input?.questions;
        for (let i = 0; i < questions_count; i++) {
            const formsObject = Object.assign(questions[i], {
                id: uuid.v4(),
                client_id: CaseData.client_id,
                case_id: CaseData.id,
                practice_id: event.user.practice_id,
                document_type: input.document_type,
                legalforms_id: legalformsObj.id,
                lawyer_response_status: "NotStarted",
                lawyer_objection_status: "NotStarted",
                client_response_status: "NotSetToClient"
            });
            const formData = await Forms.create(formsObject);
        }

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                case_id: legalformsObj.case_id,
                practice_id: legalformsObj.practice_id,
                client_id: legalformsObj.client_id,
                legalforms_id: legalformsObj.id,
                filename: legalformsObj.filename,
                document_type: legalformsObj.document_type,
                state: CaseData.state,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error: err.message || "Could not create forms.",
            }),
        };
    }
}
const emailDocumentDownload = async (event) => {
    try {
        const params = event.query || event.params;
        const { LegalForms } = await connectToDatabase();

        const legalFormsData = await LegalForms.findOne({ where: { id: params?.id } });
        const email_verification_id = params?.verification;
        if (!legalFormsData) throw new HTTPError(400, "LegalForms not found");
        if (!legalFormsData?.pdf_s3_file_key) throw new HTTPError(400, "Pdf file not found");
        if (!legalFormsData?.email_viewed_date) legalFormsData.email_viewed_date = new Date();
        if (!legalFormsData?.email_verification_id) throw new HTTPError(400, "LegalForms authorization id not found");
        if (legalFormsData?.email_verification_id != email_verification_id) throw new HTTPError(400, "Pdf Download url Expired");
        await legalFormsData.save();
        const public_url = await s3.getSignedUrlPromise("getObject", {
            Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
            Key: legalFormsData?.pdf_s3_file_key,
        });
        if (!public_url) throw new HTTPError(400, "Public URL not found");

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: public_url,
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({ error: err.message || "unauthorised access to this file" }),
        };
    }

};

const getQuestionsUsingLeagalFormID = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const legalformsArr = input?.legalforms_id;
        const case_id = input?.case_id;
        const { Forms, Op, LegalForms, Cases } = await connectToDatabase();
        const caseObj = await Cases.findOne({ where: { id: case_id, state: 'CA', is_deleted: { [Op.not]: true }, is_archived: { [Op.not]: true } }, raw: true });
        if (!caseObj) throw new HTTPError(404, `Invalid request.`);
        const formsResponse = [];
        for (let i = 0; i < legalformsArr.length; i++) {
            let response = {};
            const legalforms_id = legalformsArr[i];
            const legalformsObj = await LegalForms.findOne({
                where: {
                    id: legalforms_id, case_id: case_id,
                    client_id: caseObj.client_id, practice_id: caseObj.practice_id,
                }, raw: true
            });
            const formsObj = await Forms.findAll({
                where: {
                    legalforms_id: legalforms_id, case_id: case_id,
                    client_id: caseObj.client_id, practice_id: caseObj.practice_id,
                    document_type: 'FROGS', question_number_text: '17.1'
                },
                order: [['question_section_id', 'ASC']],
                raw: true
            });
            response.legalforms_id = legalformsObj?.id;
            response.filename = legalformsObj?.filename;
            response.questions = formsObj;
            formsResponse.push(response);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(formsResponse),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({ error: err.message || "could not get forms questions." }),
        };
    }
}
module.exports.create = middy(create).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAll = middy(getAll).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.uploadLegalForm = middy(uploadLegalForm).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.saveFormData = saveFormData;
module.exports.saveGeneratedDocument = middy(saveGeneratedDocument).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.deleteForm = middy(deleteForm).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.shredForm = shredForm;
module.exports.updateFilename = updateFilename;
module.exports.updateFormsResponsedeadlineDate = updateFormsResponsedeadlineDate;
module.exports.sendEmailForms = sendEmailForms;
module.exports.saveS3FileKeyForPdf = saveS3FileKeyForPdf;
module.exports.QuickCreateFormsCreate = QuickCreateFormsCreate;
module.exports.emailDocumentDownload = emailDocumentDownload;
module.exports.getQuestionsUsingLeagalFormID = getQuestionsUsingLeagalFormID;