const msal = require('@azure/msal-node');
const axios = require("axios");
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const decode = require("urldecode");
const jwt = require('jsonwebtoken');
const uuid = require("uuid");
const { sendEmail } = require("../../utils/mailModule");
const { generateRandomString } = require('../../utils/randomStringGenerator');

const config = {
    auth: {
        clientId: process.env.MICROSOFT_CLIENT_ID,
        authority: process.env.MICROSOFT_AUTHORITY,
        clientSecret: process.env.MICROSOFT_CLIENT_SECRET
    }
}

const clientApplication = new msal.ConfidentialClientApplication(config);

const authCodeUrlParameters = {
    scopes: ["user.read"],
    redirectUri: `${process.env.API_URL}/rest/microsoft/oauth-redirect`, // node url
};

const microsoftCallback = async () => {
    try {

        const url = await clientApplication.getAuthCodeUrl(authCodeUrlParameters);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: url
        };
    }
    catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || "invalid request" }),
        };
    }
}

const microsoftGetRedirect = async (event) => {
    try {
        const input = event.query.code;
        let redirectUrl;

        if (input) {
            redirectUrl = `${process.env.APP_URL}/microsoft-oauth2?code=${input}`;
        } else {
            redirectUrl = `${process.env.APP_URL}`;
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: redirectUrl,
        };
    }
    catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || "invalid Request" }),
        };
    }
}

const microsoftSignin = async (UserData) => {
    try {
        const { Settings, Users, Op } = await connectToDatabase();


        const tokenUser = {
            id: UserData.id,
            role: UserData.role,
        };
        const token = jwt.sign(
            tokenUser,
            process.env.JWT_SECRET,
            {
                expiresIn: process.env.JWT_EXPIRATION_TIME,
            }
        );

        const settingsObject = await Settings.findOne({
            where: {
                key: 'global_settings',
            }
        });

        const responseData = {
            //authToken: `JWT ${token}`,
            user: {
                name: UserData.name,
                role: UserData.role,
                email: UserData.email,
            },
        };
        if (UserData.twofactor_status) {
            const otpSecret = generateRandomString(4);
            const userotpUpdate = await Users.update({ twofactor_otp: otpSecret }, {
                where: {
                    email: responseData.user.email,
                    is_deleted: { [Op.not]: true }
                }
            });
            await sendEmail(responseData.user.email, 'EsquireTek Two Factor Authentication', ` Your OTP code is <b>${otpSecret}</b>`);
            responseData.user.otpstatus = 'Otp code send Successfully';

            responseData.user.two_factor = true;
            responseData.authToken = `JWT ${token}`;
        } else {
            responseData.authToken = `JWT ${token}`;
        }

        responseData.user.newUserLogin = false;
        responseData.user.signingMethod = 'signin';
        responseData.user.user_from = 'microsoft';

        if (settingsObject) {
            const plainSettingsObject = settingsObject.get({ plain: true });
            const settings = JSON.parse(plainSettingsObject.value);
            responseData.session_timeout = settings.session_timeout;
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(responseData),
        };
    }
    catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || "invalid token" }),
        };
    }
}


const microsoftAuthentication = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;

        const code = input.code;

        const { Users, Op } = await connectToDatabase();

        const tokenRequest = {
            code: code,
            scopes: ["user.read"],
            redirectUri: `${process.env.API_URL}/rest/microsoft/oauth-redirect`,
        };

        const token_result = await clientApplication.acquireTokenByCode(tokenRequest);

        const access_token = token_result.accessToken;

        const headers = {
            Authorization: "Bearer " + access_token,
        };

        const userinfo_url = `${process.env.MICROSOFT_USERINFO_URL}`;

        const userInfo_result = await axios.request({ url: userinfo_url, method: "get", headers }).then((response) => response.data)
            .catch((error) => {
                console.log(error);
                throw new Error(`Mircosoft authentication failed : ${error.message}`);
            });
        const userdetails = {};

        userdetails.email = userInfo_result.userPrincipalName;

        if (process.env.NODE_ENV === "test" && event.body.id) {
            userdetails.id = event.body.id;
        }

        if (userInfo_result.displayName) {
            userdetails.name = userInfo_result.displayName;
        }

        const UserData = await Users.findOne({
            where: {
                email: userdetails.email,
                is_deleted: { [Op.not]: true }
            },
        });

        const responseData = {
            user: {
                name: userdetails.name,
                email: userdetails.email,
                newUserLogin: true,
                signingMethod: 'signup',
                user_from: 'microsoft',
            }
        }

        if (UserData) {

            UserData.last_login_ts = new Date();
            UserData.device_id = input.device_id;
            await UserData.save();

            return microsoftSignin(UserData);

        } else {
            return {
                statusCode: 200, headers: {
                    "Content-Type": "text/plain",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Credentials": true,
                }, body: JSON.stringify(responseData),
            };
        }

    }
    catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || "invalid request" }),
        };
    }
}


module.exports.microsoftCallback = microsoftCallback;
module.exports.microsoftGetRedirect = microsoftGetRedirect;
module.exports.microsoftAuthentication = microsoftAuthentication;
