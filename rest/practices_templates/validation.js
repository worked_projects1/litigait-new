const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');
exports.validateCreatePracticeTemplates = function (data) {
    const rules = {
        practice_id: 'required|min:4|max:64',
        filename: 'required'
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};
exports.validateModifiedTemplate = function (data) {
    const rules = {
        modified_template: 'required',
        modified_template_s3_file_key: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};