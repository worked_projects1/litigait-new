const {Types} = require("aws-sdk/clients/acm");
const sequelize = require("sequelize");

module.exports = (sequelize, type) => sequelize.define('TemplateSubscriptionHistories', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    order_date: type.DATE,
    practice_id: type.STRING,
    user_id: type.STRING,
    subscribed_by: type.STRING,
    price: type.FLOAT,
    subscribed_on: type.DATE,
});