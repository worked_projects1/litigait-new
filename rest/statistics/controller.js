const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const { validateReadCustomerObjections } = require('./validation');
const { QueryTypes } = require('sequelize');
const { customDate } = require('../../utils/timeStamp');
const { customDateandTime } = require('../../utils/datetimeModule');
const { documentTypes } = require('../../rest/helpers/documentType.helper');

const getAll = async (event) => {
    try {
        const { Op, Forms, Cases, SubscriptionHistory, DiscountHistory, LegalForms, Clients, Practices, Users, HelpRequest, Orders, sequelize } = await connectToDatabase();
        let from_date, to_date;
        const d = new Date();
        d.setMonth(d.getMonth() - 1);
        const filter = event?.query?.filter ? event.query.filter : 'last_30_days';
        if (filter != 'custom_filter') {
            let dates = customDateandTime(filter);
            from_date = dates.from_date;
            to_date = dates.to_date;
            filter_query = { [Op.gte]: dates.from_date, [Op.lte]: dates.to_date };
        } else if (filter == 'custom_filter') {
            filter_query = { [Op.gte]: event.query.from_date, [Op.lte]: event.query.to_date };
            from_date = event.query.from_date;
            to_date = event.query.to_date;
        }

        const productsByCategory = await Orders.findAll({
            where: { createdAt: filter_query },
            group: ['document_generation_type'],
            attributes: ['document_generation_type', [sequelize.fn('COUNT', sequelize.col('document_type')), 'count'], [sequelize.fn('sum', sequelize.col('amount_charged')), 'revenue']],
            order: [[sequelize.literal('count'), 'DESC']],
            raw: true,
        });

        // const revenueQuery = "select max(sh.order_date) as order_date,sh.practice_id,sh.plan_type, sh.plan_type, sh.price AS `revenue`, sh.status, sh.payment_type, '1' as counts from Subscriptions s, SubscriptionHistories sh " +
        //     "where  s.practice_id=sh.practice_id and s.stripe_subscription_id=sh.stripe_subscription_id " +
        //     "and s.plan_id is not null and s.stripe_subscription_id is not null " +
        //     "and sh.status='Success' and sh.payment_type != 'Subscription Canceled'and sh.plan_category is not null " +
        //     "and (sh.createdAt >= '" + from_date + "' AND sh.createdAt <= '" + to_date + "') " +
        //     "and s.stripe_product_id=sh.stripe_product_id and s.subscribed_valid_till >= current_date() and s.practice_id !='4a5daca9-869c-48c4-b474-45732f8d36fa' " +
        //     "group by sh.practice_id,sh.plan_category " +
        //     "order by sh.practice_id,sh.plan_category; ";

        const revenueQuery = "select sh.order_date as order_date,sh.practice_id,sh.plan_type,  sum(sh.price) AS `revenue`, " +
            "sh.status, sh.payment_type, '1' as counts from SubscriptionHistories sh " +
            "where  sh.plan_id is not null and sh.stripe_subscription_id is not null and sh.status='Success' and " +
            "sh.payment_type != 'Subscription Canceled'and sh.plan_category is not null and " +
            "(sh.createdAt >= '" + from_date + "' AND sh.createdAt <= '" + to_date + "') and " +
            "sh.stripe_product_id is not null and " +
            "sh.subscription_valid_till >= current_date() and " +
            "sh.practice_id !='4a5daca9-869c-48c4-b474-45732f8d36fa' group by sh.practice_id,sh.plan_category " +
            "order by sh.practice_id,sh.plan_category;";

        console.log(revenueQuery);

        const revenueByProduct = await sequelize.query(revenueQuery, { type: QueryTypes.SELECT });

        const docTypeInterpretation = {
            final_doc: 'discovery',
            medical: 'medical_history',
            pos: 'pos',
            propound_doc: 'propounding',
            teksign: 'teksign',
            template: 'shell',
        };

        let salesByProductGrandTotal = 0;
        const productTypeInterpretation = {
            monthly: 'monthly',
            yearly: 'yearly',
            activationFee: 'activationFee',
            propounding_monthly_199: 'propounding_monthly_199',
            propounding_yearly_2199: 'propounding_yearly_2199',
            responding_monthly_349: 'responding_monthly_349',
            responding_yearly_3490: 'responding_yearly_3490',
            responding_monthly_495: 'responding_monthly_495',
            responding_yearly_5100: 'responding_yearly_5100',
            users_responding_monthly_license_495: 'users_responding_monthly_license_495',
            users_responding_yearly_license_5100: 'users_responding_yearly_license_5100',
            users_propounding_monthly_license_199: 'users_propounding_monthly_license_199',
            users_propounding_yearly_license_2149_20: 'users_propounding_yearly_license_2149_20',
            monthly_grand_total: 'monthly_grand_total',
            yearly_grand_total: 'yearly_grand_total'
        };
        const revenueByProductFinal = { total: 0, medical: 0 };

        const salesByProductFinal = { total: 0, };
        for (let i = 0; i < productsByCategory.length; i += 1) {
            const product = productsByCategory[i];
            salesByProductFinal[docTypeInterpretation[product.document_generation_type]] = product.count;
            salesByProductGrandTotal += parseInt(product.count);
            if (product.document_generation_type == 'medical') {
                revenueByProductFinal.medical = parseFloat(revenueByProductFinal.medical) + parseFloat(product.revenue.toFixed(2));
                revenueByProductFinal.medical = revenueByProductFinal.medical.toFixed(2);
            }
            salesByProductFinal.total = salesByProductGrandTotal;
        }

        let revenueByProductGrandTotal = 0;
        revenueByProductGrandTotal += parseFloat(revenueByProductFinal.medical);
        const monthlyBreakDownFinal = { new_monthly: 0, renewal_monthly: 0, discount_applied: 0, monthly_canceled_count: 0 };
        const yearlyBreakDownFinal = { new_yearly: 0, renewal_yearly: 0, discount_applied: 0, yearly_canceled_count: 0 };

        console.log('********* Activation fee ************');
        const activationFee = await SubscriptionHistory.findAll({
            where: { createdAt: filter_query, plan_type: 'activationFee', price: { [Op.gt]: 0 } },
            group: ['plan_type'],
            attributes: ['plan_type', [sequelize.fn('COUNT', sequelize.col('plan_type')), 'count'], [sequelize.fn('sum', sequelize.col('price')), 'revenue']],
            order: [[sequelize.literal('count'), 'DESC']],
            raw: true,
            logging: console.log
        });

        for (let i = 0; i < revenueByProduct.length; i += 1) {
            const product = revenueByProduct[i];
            if (['responding_monthly_349', 'responding_monthly_495', 'monthly', 'propounding_monthly_199', 'users_propounding_monthly_license_199', 'users_responding_monthly_license_495'].includes(product.plan_type)) {
                revenueByProductGrandTotal += parseFloat(parseFloat(product.revenue).toFixed(2));
                revenueByProductFinal.total = revenueByProductGrandTotal.toFixed(2);
                if (product.status == 'Success' && product.payment_type == 'Renewal') {
                    /* Monthly Breakdown - Renewal Subscriptions*/
                    monthlyBreakDownFinal.renewal_monthly = parseFloat(monthlyBreakDownFinal.renewal_monthly) + parseFloat(parseFloat(product.revenue).toFixed(2));;
                    monthlyBreakDownFinal.renewal_monthly = monthlyBreakDownFinal.renewal_monthly.toFixed(2);
                } else if (product.status == 'Success' && product.payment_type == 'New') {
                    /* Monthly Breakdown - New Subscriptions*/
                    monthlyBreakDownFinal.new_monthly = parseFloat(monthlyBreakDownFinal.new_monthly) + parseFloat(parseFloat(product.revenue).toFixed(2));;
                    monthlyBreakDownFinal.new_monthly = monthlyBreakDownFinal.new_monthly.toFixed(2);
                }
                let monthly_grand_total = revenueByProductFinal[productTypeInterpretation['monthly_grand_total']] ? revenueByProductFinal[productTypeInterpretation['monthly_grand_total']] : 0;
                monthly_grand_total = parseFloat(monthly_grand_total) + parseFloat(parseFloat(product.revenue).toFixed(2));
                revenueByProductFinal[productTypeInterpretation['monthly_grand_total']] = monthly_grand_total.toFixed(2);
            } else if (['responding_yearly_3490', 'responding_yearly_5100', 'yearly', 'propounding_yearly_2199', 'users_propounding_yearly_license_2149_20', 'users_responding_yearly_license_5100'].includes(product.plan_type)) {
                revenueByProductGrandTotal += parseFloat(parseFloat(product.revenue).toFixed(2));
                revenueByProductFinal.total = revenueByProductGrandTotal.toFixed(2);
                if (product.status == 'Success' && product.payment_type == 'Renewal') {
                    /* Yearly Breakdown - Renewal Subscriptions*/
                    yearlyBreakDownFinal.renewal_yearly = parseFloat(yearlyBreakDownFinal.renewal_yearly) + parseFloat(parseFloat(product.revenue).toFixed(2));;
                    yearlyBreakDownFinal.renewal_yearly = yearlyBreakDownFinal.renewal_yearly.toFixed(2);
                } else if (product.status == 'Success' && product.payment_type == 'New') {
                    /* Yearly Breakdown - New Subscriptions*/
                    yearlyBreakDownFinal.new_yearly = parseFloat(yearlyBreakDownFinal.new_yearly) + parseFloat(parseFloat(product.revenue).toFixed(2));;
                    yearlyBreakDownFinal.new_yearly = yearlyBreakDownFinal.new_yearly.toFixed(2);
                }
                let yearly_grand_total = revenueByProductFinal[productTypeInterpretation['yearly_grand_total']] ? revenueByProductFinal[productTypeInterpretation['yearly_grand_total']] : 0;
                yearly_grand_total = parseFloat(yearly_grand_total) + parseFloat(parseFloat(product.revenue).toFixed(2));
                revenueByProductFinal[productTypeInterpretation['yearly_grand_total']] = yearly_grand_total.toFixed(2);
            }
            let totalRevenuePerProduct = revenueByProductFinal[productTypeInterpretation[product.plan_type]] ? revenueByProductFinal[productTypeInterpretation[product.plan_type]] : 0;
            totalRevenuePerProduct = parseFloat(totalRevenuePerProduct) + parseFloat(parseFloat(product.revenue).toFixed(2));
            revenueByProductFinal[productTypeInterpretation[product.plan_type]] = totalRevenuePerProduct.toFixed(2);
        }
        console.log(revenueByProductFinal);
        for (let i = 0; i < activationFee.length; i += 1) {
            const product = activationFee[i];
            if (product.plan_type == 'activationFee') {
                revenueByProductGrandTotal += parseFloat(product.revenue.toFixed(2));
                revenueByProductFinal.total = revenueByProductGrandTotal.toFixed(2);
            }
            revenueByProductFinal[productTypeInterpretation[product.plan_type]] = product.revenue.toFixed(2);
        }
        /* Discount History */
        const discounthistory = await DiscountHistory.findAll({
            where: { createdAt: filter_query, discount_for: { [Op.ne]: 'activationFee' }, plan_type: { [Op.ne]: null } },
            group: ['plan_type'],
            attributes: ['plan_type', [sequelize.fn('COUNT', sequelize.col('plan_type')), 'count'], [sequelize.fn('sum', sequelize.col('discount_amount')), 'total_discount']],
            order: [[sequelize.literal('count'), 'DESC']],
            raw: true,
            logging: console.log
        });

        for (let i = 0; i < discounthistory.length; i++) {
            const product = discounthistory[i];
            if (['responding_monthly_349', 'responding_monthly_495', 'monthly', 'propounding_monthly_199', 'users_responding_monthly_license_495', 'users_propounding_monthly_license_199'].includes(product.plan_type)) {
                monthlyBreakDownFinal.discount_applied = parseFloat(monthlyBreakDownFinal.discount_applied) + parseFloat(product.total_discount.toFixed(2));
                monthlyBreakDownFinal.discount_applied = monthlyBreakDownFinal.discount_applied.toFixed(2);
            } else if (['responding_yearly_3490', 'responding_yearly_5100', 'yearly', 'propounding_yearly_2199', 'users_responding_yearly_license_5100', 'users_propounding_yearly_license_2149_20'].includes(product.plan_type)) {
                yearlyBreakDownFinal.discount_applied = parseFloat(yearlyBreakDownFinal.discount_applied) + parseFloat(product.total_discount.toFixed(2));
                yearlyBreakDownFinal.discount_applied = yearlyBreakDownFinal.discount_applied.toFixed(2);
            }
        }
        /* Subscription Canceled */
        console.log('******** Subscription Canceled ***********');
        const subscriptionCancel = await SubscriptionHistory.findAll({
            where: { createdAt: filter_query, status: 'Success', payment_type: 'Subscription Canceled' },
            group: ['plan_type'],
            attributes: ['plan_type', [sequelize.fn('COUNT', sequelize.col('plan_type')), 'count']],
            order: [[sequelize.literal('count'), 'DESC']],
            raw: true,
            logging: console.log
        });

        for (let i = 0; i < subscriptionCancel.length; i++) {
            const product = subscriptionCancel[i];
            if (['responding_monthly_349', 'responding_monthly_495', 'monthly', 'propounding_monthly_199', 'users_responding_monthly_license_495', 'users_propounding_monthly_license_199'].includes(product.plan_type)) {
                monthlyBreakDownFinal.monthly_canceled_count = monthlyBreakDownFinal.monthly_canceled_count + product.count;
                monthlyBreakDownFinal.monthly_canceled_count = monthlyBreakDownFinal.monthly_canceled_count;
            } else if (['responding_yearly_3490', 'responding_yearly_5100', 'yearly', 'propounding_yearly_2199', 'users_responding_yearly_license_5100', 'users_propounding_yearly_license_2149_20'].includes(product.plan_type)) {
                yearlyBreakDownFinal.yearly_canceled_count = yearlyBreakDownFinal.yearly_canceled_count + product.count;
                yearlyBreakDownFinal.yearly_canceled_count = yearlyBreakDownFinal.yearly_canceled_count;
            }
        }

        /** Practice **/
        const totalNumberOfPractices = await Practices.count({ where: { is_deleted: { [Op.not]: true } } });
        const totalActivePractices = await Practices.count({ where: { createdAt: filter_query, is_deleted: { [Op.not]: true } }, include: [{ model: Users, where: { last_login_ts: filter_query, is_deleted: { [Op.not]: true } }, }] });
        const totalNewPractices = await Practices.count({ where: { createdAt: filter_query, is_deleted: { [Op.not]: true } } });
        const practices = { active: totalActivePractices, new: totalNewPractices, total: totalNumberOfPractices };

        /** Users **/
        const totalNumberOfUsers = await Users.count({ where: { practice_id: { [Op.not]: null }, is_deleted: { [Op.not]: true }, role: { [Op.notIn]: ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'] } }, logging: console.log });
        const totalActiveUsers = await Users.count({ where: { createdAt: filter_query, practice_id: { [Op.not]: null }, last_login_ts: filter_query, is_deleted: { [Op.not]: true } }, logging: console.log });
        const totalNewUsers = await Users.count({ where: { createdAt: filter_query, practice_id: { [Op.not]: null }, is_deleted: { [Op.not]: true } }, logging: console.log });
        const users = { active: totalActiveUsers, new: totalNewUsers, total: totalNumberOfUsers };

        /* Cases */
        const totalCases = await Cases.count();
        const totalActiveCases = await Cases.count({ where: { createdAt: filter_query, is_deleted: { [Op.not]: true } }, });
        const totalNewCases = await Cases.count({ where: { createdAt: filter_query }, });
        const cases = { active: totalActiveCases, new: totalNewCases, total: totalCases };

        /* Clients */
        const totalClients = await Clients.count();
        const totalActiveClients = await Clients.count({ where: { createdAt: filter_query, is_deleted: { [Op.not]: true } }, });
        const totalNewClients = await Clients.count({ where: { createdAt: filter_query }, });
        const clients = { active: totalActiveClients, new: totalNewClients, total: totalClients };

        const getAllStatisticsData = { practices, users, cases, clients };
        const availableFormDocumenttype = await documentTypes({ state: 'all', outputType: 'types' });

        for (let i = 0; i < availableFormDocumenttype.length; i++) {

            const doc_type = availableFormDocumenttype[i];
            const QuestionsSentToClient = await Forms.count({
                where: {
                    createdAt: filter_query,
                    document_type: doc_type,
                    client_response_status: { [Op.in]: ['SentToClient', 'ClientResponseAvailable'] }
                }
            });

            const QuestionsRespondedByClient = await Forms.count({ where: { createdAt: filter_query, document_type: doc_type, client_response_status: 'ClientResponseAvailable' } });

            const GeneratedDocuments = await LegalForms.count({ where: { createdAt: filter_query, document_type: doc_type, generated_document: { [Op.ne]: null } } });

            const FinalDocuments = await LegalForms.count({ where: { createdAt: filter_query, document_type: doc_type, final_document: { [Op.ne]: null } } });

            getAllStatisticsData[doc_type.toLowerCase()] = {
                QuestionsSentToClient, QuestionsRespondedByClient, GeneratedDocuments, FinalDocuments
            };
        }

        const subscriptionTypeInterpretation = {
            monthly: 'monthly_299',
            yearly: 'yearly_2999',
            free_trial: 'free_trial',
            propounding_monthly_199: 'propounding_monthly_199',
            propounding_yearly_2199: 'propounding_yearly_2199',
            responding_monthly_349: 'responding_monthly_349',
            responding_yearly_3490: 'responding_yearly_3490',
            responding_monthly_495: 'responding_monthly_495',
            responding_yearly_5100: 'responding_yearly_5100',
            users_responding_monthly_license_495: 'users_responding_monthly_license_495',
            users_responding_yearly_license_5100: 'users_responding_yearly_license_5100',
            users_propounding_monthly_license_199: 'users_propounding_monthly_license_199',
            users_propounding_yearly_license_2149_20: 'users_propounding_yearly_license_2149_20'
        };
        const subscriptionByProductFinal = { monthly: 0, yearly: 0, activationFee: 0, activationFeeWaived: 0 };

        for (let i = 0; i < revenueByProduct.length; i += 1) {
            const product = revenueByProduct[i];
            if (['responding_monthly_349', 'responding_monthly_495', 'monthly', 'propounding_monthly_199', 'users_responding_monthly_license_495', 'users_propounding_monthly_license_199'].includes(product.plan_type)) {
                subscriptionByProductFinal.monthly = parseInt(subscriptionByProductFinal.monthly) + parseInt(product.counts);
                subscriptionByProductFinal.monthly = subscriptionByProductFinal.monthly;
            } else if (['responding_yearly_3490', 'responding_yearly_5100', 'yearly', 'propounding_yearly_2199', 'users_responding_yearly_license_5100', 'users_propounding_yearly_license_2149_20'].includes(product.plan_type)) {
                subscriptionByProductFinal.yearly = parseInt(subscriptionByProductFinal.yearly) + parseInt(product.counts);
                subscriptionByProductFinal.yearly = subscriptionByProductFinal.yearly;
            }
            subscriptionByProductFinal[subscriptionTypeInterpretation[product.plan_type]] = product.count;
        }

        if (activationFee.length) subscriptionByProductFinal.activationFee = activationFee[0].count;
        console.log('********* Activation feewaived ************');
        const activationFeeWaived = await SubscriptionHistory.findAll({
            where: { createdAt: filter_query, plan_type: 'activationFee', price: 0 },
            group: ['plan_type'],
            attributes: ['plan_type', [sequelize.fn('COUNT', sequelize.col('plan_type')), 'count']],
            order: [[sequelize.literal('count'), 'DESC']],
            raw: true,
            logging: console.log
        });
        if (activationFeeWaived.length) subscriptionByProductFinal.activationFeeWaived = activationFeeWaived[0].count;

        getAllStatisticsData.salesByProduct = salesByProductFinal;
        getAllStatisticsData.revenueByProduct = revenueByProductFinal;
        getAllStatisticsData.monthlyBreakDown = monthlyBreakDownFinal;
        getAllStatisticsData.yearlyBreakDown = yearlyBreakDownFinal;
        getAllStatisticsData.subscription = subscriptionByProductFinal;

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(getAllStatisticsData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the customerObjectionsss.' }),
        };
    }
};

const getPracticesAndRevenueStat = async (event) => {
    try {
        const { Op, Practices, Orders, sequelize } = await connectToDatabase();

        const filter = event.query.filter;

        let date = new Date(new Date().getFullYear() - 1, 0, 1);
        let date2 = new Date(new Date().getFullYear(), 0, 1);
        if (filter) {
            date = new Date(parseInt(filter), 0, 1);
            date2 = new Date(parseInt(filter) + 1, 0, 1);
        }

        const filter_query = { [Op.gte]: date, [Op.lte]: date2 };


        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        const practices = await Practices.findAll({
            where: { createdAt: filter_query },
            group: [sequelize.fn("month", sequelize.col("createdAt"))],
            attributes: [
                [sequelize.fn("COUNT", "*"), "new"],
                [sequelize.fn("count", "*"), "total"],
                [sequelize.fn("month", sequelize.col("createdAt")), "month"],
            ],
            raw: true,
        });

        const revenue = await Orders.findAll({
            where: { createdAt: filter_query },
            attributes: [
                [sequelize.fn('sum', sequelize.col('amount_charged')), 'new'], [sequelize.fn('month', sequelize.col('createdAt')), 'month'],
            ],
            group: [sequelize.fn('month', sequelize.col('createdAt'))],
            logging: console.log,
            raw: true,
        });

        const pacticesMetricsData = {};
        const revenueMtericsData = {};

        for (let i = 0; i < months.length; i += 1) {
            const monthValue = months[i];
            if (!pacticesMetricsData[monthValue]) {
                pacticesMetricsData[monthValue] = {};
                pacticesMetricsData[monthValue].new = 0;
                if (i <= 0) {
                    pacticesMetricsData[monthValue].total = 0;
                } else {
                    pacticesMetricsData[monthValue].total = pacticesMetricsData[months[i - 1]].total;
                }
            }
            if (!revenueMtericsData[monthValue]) {
                revenueMtericsData[monthValue] = {};
                revenueMtericsData[monthValue].new = 0;
                if (i <= 0) {
                    revenueMtericsData[monthValue].total = 0;
                } else {
                    revenueMtericsData[monthValue].total = revenueMtericsData[months[i - 1]].total;
                }
            }
        }
        let totalPracticeCount = 0;
        for (let i = 0; i < practices.length; i += 1) {
            const practiceObject = practices[i];
            if (!practiceObject.new) {
                practiceObject.new = 0;
            }
            if (i > 0) {
                totalPracticeCount += practiceObject.new;
            } else {
                totalPracticeCount = practiceObject.new;
            }
            pacticesMetricsData[months[practiceObject.month - 1]] = {
                new: practiceObject.new,
                //total: i > 0 ? practices[i - 1].new + practiceObject.new : practiceObject.new,
                total: totalPracticeCount,
            };
        }


        let revenueTotal = 0;
        for (let i = 0; i < revenue.length; i += 1) {
            const revenueObject = revenue[i];
            if (!revenueObject.new) {
                revenueObject.new = 0;
            }
            revenueTotal += parseFloat(revenueObject.new);
            revenueMtericsData[months[revenueObject.month - 1]] = {
                new: parseFloat(revenueObject.new.toFixed(2)),
                total: parseFloat(revenueTotal.toFixed(2)),
            };
        }

        for (let i = 0; i < months.length; i += 1) {
            const monthValue = months[i];
            if (i > 0) {
                if (!pacticesMetricsData[monthValue].total) {
                    pacticesMetricsData[monthValue].total = pacticesMetricsData[months[i - 1]].total;
                }
                if (!revenueMtericsData[monthValue].total) {
                    revenueMtericsData[monthValue].total = revenueMtericsData[months[i - 1]].total;
                }
            }
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                practices: pacticesMetricsData,
                revenue: revenueMtericsData,
                range: { from: date, to: date2 },
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the customerObjections.' }),
        };
    }
};

module.exports.getAll = getAll;
module.exports.getPracticesAndRevenueStat = getPracticesAndRevenueStat;

