const {HTTPError} = require('../../utils/httpResp');

const adminInternalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const practiceRoles = ['lawyer', 'paralegal'];
const litigaitRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician', 'lawyer', 'paralegal'];

exports.authorizeCreate = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetOne = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};
exports.authorizeGetAll = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdate = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeDestroy = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetFormDates = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};


exports.authorizeGetCaseDetails = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeCloningCases = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetCaseDetailsOptimze = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeDuplicateCaseClientCreate = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdateClientAndCase = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};