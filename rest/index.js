const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');
const healthCheckApi = require('../handler');
const authMiddleware = require('../authExpress');
const feeTermsAPi = require('../rest/fee_terms/controller');
const hipaaTermsAPi = require('../rest/hipaa_terms/controller');
const ClientsApi = require('../rest/clients/controller');
const UsersApi = require('../rest/users/controller');
const AdminObjectionsApi = require('../rest/admin_objections/controller');
const CustomerObjectionsApi = require('../rest/customer_objections/controller');
const OredrApi = require('../rest/orders/controller');
const MedicalHistoryApi = require('../rest/medical_history/controller');
const PracticesApi = require('../rest/practices/controller');
const MedicalHistorySummeryApi = require('../rest/medical_history_summery/controller');
const SupportApi = require('../rest/support/controller');
const documentUploadProgressApi = require('../rest/document_upload_progress/controller');
const helpRequestApi = require('../rest/help_request/controller');
const translateApi = require('../rest/translation/controller');
const questionTranslationsApi = require('../rest/questions_translations/controller');
const versionHistoryApi = require('../rest/version_history/controller');
const clientSignatureApi = require('../rest/client_signature/controller');
const statisticsApi = require('../rest/statistics/controller');
const formsApi = require('../rest/forms/controller');
const formotpApi = require('../rest/formotp/controller');
const practiceSettingsApi = require('../rest/practice_settings/controller');
const casesApi = require('../rest/cases/controller');
const plansApi = require('../rest/plans/controller');
const subscriptionsApi = require('../rest/subscriptions/controller');
const OtherPartiesApi = require('../rest/other_parties/controller');
const billingApi = require('../rest/billing/controller');
const LegalformsApi = require('../rest/legalforms/controller');
const practiceTemplateApi = require('../rest/practices_templates/controller');
const invoiceApi = require('../rest/invoice/controller');
const migrationScriptApi = require('../rest/migration_script/migration_script');
const mycaseApi = require('../rest/integrations/mycase/controller');
const filevineApi = require('../rest/integrations/filevine/controller');
const cronApi = require('../rest/cronTasks/cronTask');
const stateApi = require('../rest/states/controller');
const OtherPartiesFormsOtpsApi = require('../rest/other_parties_formotp/controller');
const medicalDocumentProperties = require('../rest/medical_document_properties/controller');
const archiveApi = require('../rest/archive/controller');
const scriptApi = require('../rest/scripts/controller');
const feewaiverApi = require('../rest/fee_waiver/controller');
const propoundApi = require('../rest/propound_templates/controller');
const propoundDocumentUploadProgressApi = require('../rest/propound_template_upload_progress/controller');
const propoundFormsApi = require('../rest/propound_forms/controller');
const PropoundResponderDetailsApi = require('../rest/propound_responder/controller');
const PropoundHelpRequestApi = require('../rest/propound_help_request/controller');
const frogsTemplateApi = require('../rest/frogs_templates/controller');
const discountApi = require('../rest/discount_codes/controller');
const stripeAPI = require('../rest/stripe_webhooks/controller');
const pricingChangesApi = require('../rest/pricing_changes_2022/controller');
const DocumentExtractionProgressApi = require('../rest/document_extraction_progress/controller');
const GoogleApi = require('../rest/google/controller');
const MicrosoftApi = require('../rest/microsoft/controller');
const LoginApi = require('../rest/login/controller');
const LawyerObjectionsApi = require('../rest/lawyer_objection/controller');
const FormsResponseHistoryApi = require('../rest/forms_response_history/controller');
const ClioApi = require('./integrations/clio/controller');
const LitifyApi = require('./integrations/Litify/controller');
const OpenAiApi = require('../rest/medical_history_summery/openAiController');
const automation = require('../rest/forms/automation_controller');
const WebsiteAuthenticationApi = require('../rest/website_authentication/controller');
const usersLicenseAPI = require('../rest/subscriptions/user_based/controller');
const VideoCallsApi = require('../rest/video_call/controller');
const DocEditorAPI = require('../rest/doc_editor/controller');
const HashedFilesAPI = require('../rest/hashed_files/controller');
const paymentGatewayAPI = require('../rest/stripe_webhooks/api');
const documentVersioningAPI = require('../rest/document_versioning/controller');

const app = express();
app.use(bodyParser.json({ strict: false, limit: '50mb' })); /* to avoid payload large error. */
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true })); /* to avoid payload large error. */
app.use(express.json());
app.use(cors());
app.options('*', cors());

app.get('/rest/', authMiddleware, async (req, res) => {
    try {
        const responseData = await healthCheckApi.healthCheck(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Hippa - Terms */
app.get('/rest/hipaa-terms', authMiddleware, async (req, res) => {
    try {
        const responseData = await hipaaTermsAPi.getHipaaTermsExpress(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/hipaa-terms', authMiddleware, async (req, res) => {
    try {
        const responseData = await hipaaTermsAPi.setHipaaTermsExpress(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Fee-Terms */
app.get('/rest/fee-terms', authMiddleware, async (req, res) => {
    try {
        const responseData = await feeTermsAPi.getFeeTermsExpress(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/fee-terms', authMiddleware, async (req, res) => {
    try {
        const responseData = await feeTermsAPi.setFeeTermsExpress(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/fee-terms/get-terms-by-clientid-caseid', async (req, res) => {
    try {
        const responseData = await feeTermsAPi.getFeeTermsByClientId(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Clients */
app.post('/rest/clients/send-hipaa-terms/:clientId/:caseId', authMiddleware, async (req, res) => {
    try {
        const responseData = await ClientsApi.sendHipaaTermsToClient(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/clients/send-fee-terms/:clientId/:caseId', authMiddleware, async (req, res) => {
    try {
        const responseData = await ClientsApi.sendFeeTermsToClient(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Users */
app.put('/rest/users/change-password', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.changePassword(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/users/reset-login-attempt', async (req, res) => {
    try {
        const responseData = await UsersApi.resetLoginAttempt(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/users/two-factor-settings', async (req, res) => {
    try {
        const responseData = await UsersApi.twofactorSetting(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/users/subscription-validate', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.checkSubscriptionValidation(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/users/session', async (req, res) => {
    try {
        const responseData = await UsersApi.sessionTokenGenerate(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/users/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/users', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.getAll(req);
        return res.status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/users/dashboard/admin', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.getAllUsersForDashboard(req);
        return res.status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/users/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/users/qc-notification/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.updateQCNotificationStatus(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/users/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/users', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/user/update-signature', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.updateSignature(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/user/update-other-signup-details', async (req, res) => {
    try {
        const responseData = await UsersApi.updateForOtherSignUps(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/user/update-details', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.updateNameAndStateBarNumber(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/user/user-exists', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.userExists(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/users/license-purchase', authMiddleware, async (req, res) => {
    try {
        const responseData = await UsersApi.usersLicencePurchase(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Admin - Objections */
app.post('/rest/admin-objections', authMiddleware, async (req, res) => {
    try {
        const responseData = await AdminObjectionsApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/customer-objections/push-to-all-practices', authMiddleware, async (req, res) => {
    try {
        const responseData = await AdminObjectionsApi.pushToAllPractices(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/admin-objections', authMiddleware, async (req, res) => {
    try {
        const responseData = await AdminObjectionsApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/admin-objections/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await AdminObjectionsApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/admin-objections/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await AdminObjectionsApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/admin-objections/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await AdminObjectionsApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Customer - Objections */
app.post('/rest/customer-objections', authMiddleware, async (req, res) => {
    try {
        const responseData = await CustomerObjectionsApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/customer-objections', authMiddleware, async (req, res) => {
    try {
        const responseData = await CustomerObjectionsApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/customer-objections/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await CustomerObjectionsApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/get-all-customer-objection-by-state', authMiddleware, async (req, res) => {
    try {
        const responseData = await CustomerObjectionsApi.getAllObjectionByState(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/customer-objections', authMiddleware, async (req, res) => {
    try {
        const responseData = await CustomerObjectionsApi.updateMany(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/customer-objections/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await CustomerObjectionsApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/customer-objections/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await CustomerObjectionsApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Orders */
app.get('/rest/admin/orders/search', authMiddleware, async (req, res) => {
    try {
        const responseData = await OredrApi.ordersSearch(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/orders/practice-search', authMiddleware, async (req, res) => {
    try {
        const responseData = await OredrApi.PracticeordersSearch(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/admin/orders/search', authMiddleware, async (req, res) => {
    try {
        const responseData = await OredrApi.ordersSearch(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/admin/orders', authMiddleware, async (req, res) => {
    try {
        const responseData = await OredrApi.adminGetAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Medical-History */
app.post('/rest/medical-history', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistoryApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/medical-history', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistoryApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/medical-history/get-status/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistoryApi.getStatus(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/medical-history/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistoryApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/medical-history/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistoryApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/medical-history/key-value/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistoryApi.updateJSONKeyValue(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/medical-history/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistoryApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/medical-history/invoice', async (req, res) => {
    try {
        const responseData = await MedicalHistoryApi.emailInvoice(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/medical-experts', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistoryApi.getAllMedicalExperts(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Medical-History-Summery */
app.post('/rest/medical-history-summery', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistorySummeryApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/medical-history-summery', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistorySummeryApi.getAll(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/medical-history-summery/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistorySummeryApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/medical-history-summery/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistorySummeryApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/medical-history-summery/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await MedicalHistorySummeryApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Practices */
app.put('/rest/practices/update-objection-status', authMiddleware, async (req, res) => {
    try {
        const responseData = await PracticesApi.objectionStatusUpdate(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/practices', authMiddleware, async (req, res) => {
    try {
        const responseData = await PracticesApi.getAll(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/dashboard/practices', authMiddleware, async (req, res) => {
    try {
        const responseData = await PracticesApi.getAllPracticeInfoForDashboard(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.post('/rest/practices/upload-private-file', authMiddleware, async (req, res) => {
    try {
        const responseData = await PracticesApi.uploadPrivateFile(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/practices/get-secure-public-url-of-private-file', authMiddleware, async (req, res) => {
    try {
        const responseData = await PracticesApi.getSecuredPublicUrlForPrivateFile(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/practices/get-secure-public-url-of-form-file', authMiddleware, async (req, res) => {
    try {
        const responseData = await PracticesApi.getSecuredPublicUrlForFormFile(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/practices/reset-billing-data', authMiddleware, async (req, res) => {
    try {
        const responseData = await PracticesApi.resetBillingData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.post('/rest/practices/switch', async (req, res) => {
    try {
        const responseData = await PracticesApi.practiceSwitch(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/practices/details-fetch', authMiddleware, async (req, res) => {
    try {
        const responseData = await PracticesApi.practiceDetailsFetch(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});


/* practice settings */
app.get('/rest/practice-settings', async (req, res) => {
    try {
        const responseData = await practiceSettingsApi.getSettings(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/practice-settings', async (req, res) => {
    try {
        const responseData = await practiceSettingsApi.setSettings(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Support */
app.post('/rest/support/support-request', async (req, res) => {
    try {
        const responseData = await SupportApi.supportMail(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/support/demo-request', async (req, res) => {
    try {
        const responseData = await SupportApi.demoMail(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Document-Upload-Progreess */
app.put('/rest/document-upload-progreess/set-status', async (req, res) => {
    try {
        const responseData = await documentUploadProgressApi.setStatus(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/document-upload-progreess/get-status', async (req, res) => {
    try {
        const responseData = await documentUploadProgressApi.getStatus(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Help - Request */
app.post('/rest/help-request/send-help-request', authMiddleware, async (req, res) => {
    try {
        const responseData = await helpRequestApi.sendHelpRequest(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/help-request', authMiddleware, async (req, res) => {
    try {
        const responseData = await helpRequestApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/help-request/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await helpRequestApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/help-request/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await helpRequestApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/help-request/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await helpRequestApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/admin-help-request/:id/:type/:reason', authMiddleware, async (req, res) => {
    try {
        const responseData = await helpRequestApi.destroySupportRequest(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Question Translate */
app.post('/rest/translate', async (req, res) => {
    try {
        const responseData = await translateApi.translate(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/questions-translations', authMiddleware, async (req, res) => {
    try {
        const responseData = await questionTranslationsApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/questions-translations/save-multiple', authMiddleware, async (req, res) => {
    try {
        const responseData = await questionTranslationsApi.saveMultiple(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/questions-translations', authMiddleware, async (req, res) => {
    try {
        const responseData = await questionTranslationsApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/questions-translations/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await questionTranslationsApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/questions-translations/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await questionTranslationsApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Version - History */
app.get('/rest/version-history', async (req, res) => {
    try {
        const responseData = await versionHistoryApi.getVersionHistory(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/version-history', authMiddleware, async (req, res) => {
    try {
        const responseData = await versionHistoryApi.setVersionHistory(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Client - Signature */
app.put('/rest/client-signature', async (req, res) => {
    try {
        const responseData = await clientSignatureApi.saveClientSignatureByOtp(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/client-signature/get-signature-upload-url-by-otp', async (req, res) => {
    try {
        const responseData = await clientSignatureApi.getClientSignatureUploadUrlByOtp(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/client-signature/get-verification-signature-upload-url-by-otp', async (req, res) => {
    try {
        const responseData = await clientSignatureApi.getClientVerificationSignatureUploadUrlByOtp(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/client-signature/save-signature-in-s3', async (req, res) => {
    try {
        const responseData = await clientSignatureApi.saveClientSignatureInS3ByOtp(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/client-signature/save-verification-signature-in-s3', async (req, res) => {
    try {
        const responseData = await clientSignatureApi.saveVerificationSignatureInS3ByOtp(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/client-signature/teksign-correction', async (req, res) => {
    try {
        const responseData = await clientSignatureApi.teksignCorrection(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Statistics */
app.get('/rest/statistics/practices-and-revenue', async (req, res) => {
    try {
        const responseData = await statisticsApi.getPracticesAndRevenueStat(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/statistics', async (req, res) => {
    try {
        const responseData = await statisticsApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Forms */
app.post('/rest/forms/send-layer-answer-verification-to-client', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.sendVerificationRequestToClient(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/forms/get-laywer-response-data-by-otp', async (req, res) => {
    try {
        const responseData = await formsApi.getLawyerResponseDataByOtp(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/forms/upload-file', async (req, res) => {
    try {
        const responseData = await formsApi.userUploadFile(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/forms/resend-previous-questions', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.sendPreviousSetQuestions(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/forms/translation-question', async (req, res) => {
    try {
        const responseData = await formsApi.translationTest(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/forms/saveall-lawyer-response', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.saveAlllawyerResponseByCaseidAndDocumentType(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/forms/update-subgroup', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.updateSubgroupStatus(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/forms/add-new-consultation', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.saveNewConsultation(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/forms/remove-consultation', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.removeConsultation(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/forms/remove-consultation-client', async (req, res) => {
    try {
        const responseData = await formsApi.removeFromClientConsultation(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/forms/add-new-client-consultation', async (req, res) => {
    try {
        const responseData = await formsApi.saveNewConsultationCreatedByClient(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/forms/update-client-response-edited-status', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.updateClientResponseEditStatus(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/forms/lawyer-response-auto-save', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.lawyerResponseAutoSave(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/forms/save-lawyer-objections', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.saveLawyerObjections(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/forms/save-multiple-lawyer-objections', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.saveMultipleLawyerObjections(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/forms/create-duplicate-data-for-frogs', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.createDuplicateDataForFrogs(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/forms/update-connected-forms-data', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.updateConnectedFormsData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/forms/remove-connected-form-set', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.removeConnectedFormSet(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/forms/remove-all-frogs-connected-form', authMiddleware, async (req, res) => {
    try {
        const responseData = await formsApi.RemoveAllFrogsConnectedForm(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Formotps */
app.get('/rest/forms/otps', authMiddleware, async (req, res) => {
    try {
        const responseData = await formotpApi.getAll(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/forms/get-all-questions-by-otp', async (req, res) => {
    try {
        const responseData = await formsApi.getFormDataByOtpWithUnansweredQuestion(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

/* OtherParties FormsOtps */
app.get('/rest/OtherParties/otps', authMiddleware, async (req, res) => {
    try {
        const responseData = await OtherPartiesFormsOtpsApi.getAll(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Plans */
app.post('/rest/plans', authMiddleware, async (req, res) => {
    try {
        const responseData = await plansApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/plans', authMiddleware, async (req, res) => {
    try {
        const responseData = await plansApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/signup/plans', async (req, res) => {
    try {
        const responseData = await plansApi.getAllSignup(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/plans/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await plansApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/plans', authMiddleware, async (req, res) => {
    try {
        const responseData = await plansApi.updateMany(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/plans/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await plansApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/plans/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await plansApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Subscriptions */
app.post('/rest/subscriptions', authMiddleware, async (req, res) => {
    try {
        const responseData = await subscriptionsApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/subscriptions', authMiddleware, async (req, res) => {
    try {
        const responseData = await subscriptionsApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/subscriptions/history', authMiddleware, async (req, res) => {
    try {
        const responseData = await subscriptionsApi.getAllHistory(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/subscriptions/admin/history', authMiddleware, async (req, res) => {
    try {
        const responseData = await subscriptionsApi.adminGetAllHistory(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/subscriptions/history/dashboard/admin', authMiddleware, async (req, res) => {
    try {
        const responseData = await subscriptionsApi.getAllActivationFeeAndFeeWaived(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/subscriptions/active/dashboard/admin', authMiddleware, async (req, res) => {
    try {
        const responseData = await subscriptionsApi.getAllActiveSubscriptions(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/subscriptions/vip/enable', authMiddleware, async (req, res) => {
    try {
        const responseData = await subscriptionsApi.createVIPSubscription(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/subscriptions/vip/disable', authMiddleware, async (req, res) => {
    try {
        const responseData = await subscriptionsApi.deleteVIPSubscription(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.put('/rest/subscriptions/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await subscriptionsApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
// Billing
app.post('/rest/stripe/webhook', async (req, res) => {
    try {
        const responseData = await billingApi.stripeWebHook(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/stripe/updateSubscription', async (req, res) => {
    try {
        const responseData = await billingApi.updateSubscriptionWebHookStatus(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/subscriptions/propounding', authMiddleware, async (req, res) => {
    try {
        const responseData = await subscriptionsApi.createPropoundingSubscription(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/subscriptions/cancel-propounding', authMiddleware, async (req, res) => {
    try {
        const responseData = await subscriptionsApi.cancelPropoundingSubscription(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Other Parties */
app.post('/rest/other-parties', authMiddleware, async (req, res) => {
    try {
        const responseData = await OtherPartiesApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/other-parties/', authMiddleware, async (req, res) => {
    try {
        const responseData = await OtherPartiesApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        console.log(err);
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/other-parties/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await OtherPartiesApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/other-parties/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await OtherPartiesApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/other-parties/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await OtherPartiesApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Legalforms */
app.post('/rest/legalforms/get-frogs-questions', authMiddleware, async (req, res) => {
    try {
        const responseData = await LegalformsApi.getQuestionsUsingLeagalFormID(req)
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/legalforms/shred', authMiddleware, async (req, res) => {
    try {
        const responseData = await LegalformsApi.shredForm(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/legalforms/update-filename/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await LegalformsApi.updateFilename(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/legalforms/quick-create-forms-create', authMiddleware, async (req, res) => {
    try {
        const responseData = await LegalformsApi.QuickCreateFormsCreate(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/legalforms/forms-responsedeadline-setdate', authMiddleware, async (req, res) => {
    try {
        const responseData = await LegalformsApi.updateFormsResponsedeadlineDate(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post("/rest/legalforms/send-email-forms", authMiddleware, async (req, res) => {
    try {
        const responseData = await LegalformsApi.sendEmailForms(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res.status(err.statusCode || 500).json({ error: err.message });
    }
}
);
app.put('/rest/legal-forms/save-s3-key/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await LegalformsApi.saveS3FileKeyForPdf(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/legalforms/email-document-download', async (req, res) => {
    try {
        const responseData = await LegalformsApi.emailDocumentDownload(req);
        if (responseData.statusCode === 200) {
            return res
                .status(responseData.statusCode)
                .redirect(responseData.body);
        } else {
            return res
                .status(responseData.statusCode)
                .json(JSON.parse(responseData.body));
        }
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Cases */
app.get('/rest/cases', authMiddleware, async (req, res) => {
    try {
        const responseData = await casesApi.getAll(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/cases/case-details/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await casesApi.getCaseDetails(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/cases/clone', authMiddleware, async (req, res) => {
    try {
        const responseData = await casesApi.cloningCases(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/cases/clone-client', authMiddleware, async (req, res) => {
    try {
        const responseData = await casesApi.DuplicateCaseClientCreate(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/integrations/update-client-and-case', authMiddleware, async (req, res) => {
    try {
        const responseData = await casesApi.UpdateClientAndCase(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/update-texas-idc', authMiddleware, async (req, res) => {
    try {
        const responseData = await casesApi.updateTexasIDC(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/cases/practice-case-transfer', async (req, res) => {
    try {
        const responseData = await casesApi.practiceCaseTransfer(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/cases/other-party-get-all', authMiddleware, async (req, res) => {
    try {
        const responseData = await casesApi.otherPartyCaseGetAll(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Practice-Template */
app.post('/rest/practice-template', authMiddleware, async (req, res) => {
    try {
        const responseData = await practiceTemplateApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/practice-template/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await practiceTemplateApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/practice-template/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await practiceTemplateApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/practice-template/', authMiddleware, async (req, res) => {
    try {
        const responseData = await practiceTemplateApi.getAll(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/practice-template/upload-url', authMiddleware, async (req, res) => {
    try {
        const responseData = await practiceTemplateApi.UploadTemplateFile(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/practice-template/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await practiceTemplateApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Invoice */
app.post('/rest/send-invoice', authMiddleware, async (req, res) => {
    try {
        const responseData = await invoiceApi.sendInvoice(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

/* States */
app.post('/rest/state', authMiddleware, async (req, res) => {
    try {
        const responseData = await stateApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/state', async (req, res) => {
    try {
        const responseData = await stateApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/state/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await stateApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/state/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await stateApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/state/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await stateApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.post('/rest/medical-document-properties', authMiddleware, async (req, res) => {
    try {
        const responseData = await medicalDocumentProperties.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/medical-document-properties/:clientId/:caseId', authMiddleware, async (req, res) => {
    try {
        const responseData = await medicalDocumentProperties.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/medical-document-properties/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await medicalDocumentProperties.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/medical-document-properties/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await medicalDocumentProperties.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/medical-document-properties/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await medicalDocumentProperties.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/get-duplicate-summery-public-url', authMiddleware, async (req, res) => {
    try {
        const responseData = await medicalDocumentProperties.getSecuredPublicUrlForSummeryPrivateFile(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Archive */
app.post('/rest/archive/cases', authMiddleware, async (req, res) => {
    try {
        const responseData = await archiveApi.caseArchive(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/archive/cases', authMiddleware, async (req, res) => {
    try {
        const responseData = await archiveApi.getAllCaseArchive(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.post('/rest/unarchive/cases', authMiddleware, async (req, res) => {
    try {
        const responseData = await archiveApi.casesUnarchive(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Fee Waiver */
app.post('/rest/fee-waiver', authMiddleware, async (req, res) => {
    try {
        const responseData = await feewaiverApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/fee-waiver', authMiddleware, async (req, res) => {
    try {
        const responseData = await feewaiverApi.getAll(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/fee-waiver/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await feewaiverApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/fee-waiver/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await feewaiverApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* migration-script */
/* Don't Remove Objction migration script */
app.post('/rest/migration-script/add-objection-state', authMiddleware, async (req, res) => {
    try {
        const responseData = await migrationScriptApi.stateNewObjection(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/migration-script/discount-amount', async (req, res) => {
    try {
        const responseData = await migrationScriptApi.updateDiscountAmount(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.post('/rest/migration-script/discount-amount/activation', async (req, res) => {
    try {
        const responseData = await migrationScriptApi.updateDiscountAmountActivation(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Propound Template */

app.post('/rest/propound', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/propound', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/propound/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/propound/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/propound/upload-template', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundApi.createPropoundTemplates(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/propound/save-template-questions', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundApi.savePropoundTemplatesData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/propound/template/:id/:document_type', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundApi.getPropoundTemplatesQuestions(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

/* Propound Help Request */
app.post('/rest/propound-support/', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundHelpRequestApi.sendPropoundHelpRequest(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/propound-support/', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundHelpRequestApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/propound-public-url/', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundHelpRequestApi.getSecuredPublicUrlForPropound(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/propound-support/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundHelpRequestApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/propound-support/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundHelpRequestApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/propound-support/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundHelpRequestApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* app.get('/rest/propound/document-type', authMiddleware , async (req, res) => {
  try {
    const responseData = await propoundApi.getAllByCaseType(req);
    return res
              .status(responseData.statusCode)
              .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
                  .status(err.statusCode || 500)
                  .json({ error: err.message });
  }
}); */

/* Propound Document-Upload-Progreess */

app.put('/rest/propound/document-upload-progreess/set-status', async (req, res) => {
    try {
        const responseData = await propoundDocumentUploadProgressApi.setStatus(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/propound/document-upload-progreess/get-status', async (req, res) => {
    try {
        const responseData = await propoundDocumentUploadProgressApi.getStatus(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

/* Propound Forms */

app.post('/rest/cases/propound', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundFormsApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/cases/propound', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundFormsApi.getAllQuestionByCaseId(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/cases/propoundforms/save-serving-attorney-details/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundFormsApi.saveServingAttorneyDetails(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/cases/propound/:id/:case_id/:document_type', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundFormsApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/cases/propound/generated-document-questions', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundFormsApi.generatedDocumentQuestions(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get("/rest/cases/propound/:id/:case_id/:document_type", authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundFormsApi.getQuestionByCaseIdAndDocumentType(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res.status(err.statusCode || 500).json({ error: err.message });
    }
}
);
app.delete("/rest/cases/propound/:id", authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundFormsApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res.status(err.statusCode || 500).json({ error: err.message });
    }
}
);
app.post("/rest/cases/propound/send-propound-template", authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundFormsApi.sendPropoundFormsToResponder(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res.status(err.statusCode || 500).json({ error: err.message });
    }
}
);
app.get('/rest/propoundforms/email-document-download', async (req, res) => {
    try {
        const responseData = await propoundFormsApi.emailDocumentDownload(req);
        if (responseData.statusCode === 200) {
            return res
                .status(responseData.statusCode)
                .redirect(responseData.body);
        } else {
            return res
                .status(responseData.statusCode)
                .json(JSON.parse(responseData.body));
        }
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/cases/propound/save-s3-key/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundFormsApi.saveS3FileKey(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/propound-forms/upload-file', async (req, res) => {
    try {
        const responseData = await propoundFormsApi.userUploadFile(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/propound-forms/update-filename', authMiddleware, async (req, res) => {
    try {
        const responseData = await propoundFormsApi.updateFilename(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Propound Responder Details */
app.post('/rest/propound-responder', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundResponderDetailsApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/propound-responder/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundResponderDetailsApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/propound-responder', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundResponderDetailsApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/propound-responder', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundResponderDetailsApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/propound-responder/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundResponderDetailsApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/propound-responder/details/:token_id', async (req, res) => {
    try {
        const responseData = await PropoundResponderDetailsApi.getResponderDataByTokenId(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/propound-responder/template', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundResponderDetailsApi.getRespondersTemplateDetailsByPracticeId(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/propound-responder/case', authMiddleware, async (req, res) => {
    try {
        const responseData = await PropoundResponderDetailsApi.createReponderCaseDetails(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Frogs Templates */

app.post('/rest/frogs-template', authMiddleware, async (req, res) => {
    try {
        const responseData = await frogsTemplateApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/frogs-template/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await frogsTemplateApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/frogs-template', authMiddleware, async (req, res) => {
    try {
        const responseData = await frogsTemplateApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/frogs-template', authMiddleware, async (req, res) => {
    try {
        const responseData = await frogsTemplateApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/frogs-template/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await frogsTemplateApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Discount */
app.post('/rest/discount-code', authMiddleware, async (req, res) => {
    try {
        const responseData = await discountApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/discount-code', authMiddleware, async (req, res) => {
    try {
        const responseData = await discountApi.getAll(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/discount-code/history', authMiddleware, async (req, res) => {
    try {
        const responseData = await discountApi.getAllHistory(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/validate/discount-code', authMiddleware, async (req, res) => {
    try {
        const responseData = await discountApi.validateDiscountCode(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/discount-code/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await discountApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/discount-code/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await discountApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/discount-code/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await discountApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
//stripe
app.post('/rest/webhook', async (req, res) => {
    try {
        const responseData = await stripeAPI.listallCharges(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/admin/billing-history', authMiddleware, async (req, res) => {
    try {
        const responseData = await stripeAPI.getBillingDetails(req);
        return res
            .status(responseData.statusCode).header(responseData.headers)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/admin/subscription-cancel', authMiddleware, async (req, res) => {
    try {
        const responseData = await stripeAPI.subscriptionCancel(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/admin/get-stripe-customer-details', async (req, res) => {
    try {
        const responseData = await stripeAPI.fetchActiveSubscriptionUsingCustomerID(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/admin/get-stripe-subscription-details', async (req, res) => {
    try {
        const responseData = await stripeAPI.fetchSubscriptionInfo(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.post('/rest/manual/subscription-history-create', async (req, res) => {
    try {
        const responseData = await pricingChangesApi.createSubscriptionHistoryManual(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.post('/rest/manual/check-active-canceled-subscription', async (req, res) => {
    try {
        const responseData = await pricingChangesApi.checkActiveCanceledSubscriptions(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Cron Task  */
// app.post('/rest/cron-task/', async (req, res) => {
//   try {
//     const responseData = await cronApi.caseReminderCron(req);
//     return res
//               .status(responseData.statusCode)
//               .json(JSON.parse(responseData.body));
//   } catch (err) {
//     return res
//                   .status(err.statusCode || 500)
//                   .json({ error: err.message });
//   }
// });
// app.get('/rest/cron-task/', async (req, res) => {
//   try {
//     const responseData = await pricingChangesApi.monthlySubscriptionRenewal(req);
//     return res
//               .status(responseData.statusCode)
//               .json(JSON.parse(responseData.body));
//   } catch (err) {
//     return res
//                   .status(err.statusCode || 500)
//                   .json({ error: err.message });
//   }
// });
app.get('/rest/subscription-yearly-renewal', async (req, res) => {
    try {
        const responseData = await pricingChangesApi.getAllYearlySubscriptions(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/subscription-yearly-renewal/:id', async (req, res) => {
    try {
        const responseData = await pricingChangesApi.getYearlyRenewalPractice(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/subscription-yearly-renewal', authMiddleware, async (req, res) => {
    try {
        const responseData = await pricingChangesApi.renewalYearlyPlan(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/subscription-monthly-renewal', async (req, res) => {
    try {
        const responseData = await pricingChangesApi.getAllMonthlySubscriptions(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* app.post('/rest/manual-subscription-monthly', async (req, res) => {
try {
    const responseData = await pricingChangesApi.manualSubscriptionForMonthly(req);
    return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
} catch (err) {
    return res
                .status(err.statusCode || 500)
                .json({ error: err.message });
}
});  */
// app.post('/rest/update-subscription-plans', async (req, res) => {
//     try {
//         const responseData = await pricingChangesApi.monthlySubscriptionStatusUpdate(req);
//         return res
//             .status(responseData.statusCode)
//             .json(JSON.parse(responseData.body));
//     } catch (err) {
//         return res
//             .status(err.statusCode || 500)
//             .json({ error: err.message });
//     }
// });
/* Document Extraction Progress */
app.post('/rest/document-extraction-progress/upload-private-file', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.uploadPrivateFile(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/document-extraction-progress/set-status', async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.setStatus(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/document-extraction-progress/get-status', async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.getStatus(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/document-extraction-progress/update-document-data/:id', async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.updateDocumentData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/document-extraction-progress/save-extraction-form', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.saveExtractionData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/document-extraction-progress', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/document-extraction-progress/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/document-extraction-progress/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.put('/rest/document-extraction-progress/:legalforms_id', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.updateExtractionFormsQuestions(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/document-extraction-support', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.getFailedDocuments(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/document-extraction-support/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/document-extraction-support', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.createClientCaseAndLegalforms(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/document-extraction-progress/help-request/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocumentExtractionProgressApi.quickCreateHelpRequest(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
//Google 
app.post('/rest/google/sign-in', async (req, res) => {
    try {
        const responseData = await GoogleApi.fetchSigninDetails(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/google/sign-up', async (req, res) => {
    try {
        const responseData = await GoogleApi.fetchSignUpDetails(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/google/authentication', async (req, res) => {
    try {
        const responseData = await GoogleApi.googleAuthentication(req);
        return res
            // .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

//Mircosoft
app.get('/rest/microsoft/callback', async (req, res) => {
    try {
        const responseData = await MicrosoftApi.microsoftCallback(req);
        return res
            .status(responseData.statusCode)
            .redirect(responseData.body);
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/microsoft/oauth-redirect', async (req, res) => {
    try {
        const responseData = await MicrosoftApi.microsoftGetRedirect(req);
        console.log(responseData);
        return res
            .status(responseData.statusCode)
            .redirect(responseData.body);
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/microsoft/authentication', async (req, res) => {
    try {
        const responseData = await MicrosoftApi.microsoftAuthentication(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/login/session-login', async (req, res) => {
    try {
        const responseData = await LoginApi.sessionLogin(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/login/email-validation', async (req, res) => {
    try {
        const responseData = await LoginApi.emailValidation(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/track-info', authMiddleware, async (req, res) => {
    try {
        const responseData = await LoginApi.updateDeviceId(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

/* Lawyer Objection changes */
app.post('/rest/lawyer-objections', authMiddleware, async (req, res) => {
    try {
        const responseData = await LawyerObjectionsApi.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/lawyer-objections/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await LawyerObjectionsApi.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/lawyer-objections/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await LawyerObjectionsApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/lawyer-objections', authMiddleware, async (req, res) => {
    try {
        const responseData = await LawyerObjectionsApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/lawyer-objections/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await LawyerObjectionsApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Forms Response History  */
app.get('/rest/forms-response-history/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await FormsResponseHistoryApi.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})
app.get('/rest/forms-response-history', authMiddleware, async (req, res) => {
    try {
        const responseData = await FormsResponseHistoryApi.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})
app.delete('/rest/forms-response-history/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await FormsResponseHistoryApi.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})
/* Scripts */
app.post('/rest/scripts/update-color-code', async (req, res) => {
    try {
        const responseData = await scriptApi.updateColorCode(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})
/* Filevine */
app.post('/rest/integrations/filevine/create-client-and-case', authMiddleware, async (req, res) => {
    try {
        const responseData = await filevineApi.createClientandCase(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/integrations/filevine-key-details', authMiddleware, async (req, res) => {
    try {
        const responseData = await filevineApi.fileVinekeydetails(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/integrations/filevine/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await filevineApi.destroyTokenData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/integrations/filevine/file-upload', authMiddleware, async (req, res) => {
    try {
        const responseData = await filevineApi.fileUpload(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/integrations/filevine/s3-file-size', authMiddleware, async (req, res) => {
    try {
        const responseData = await filevineApi.fileSizeUsingS3Key(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/filevine/update-client-and-case', authMiddleware, async (req, res) => {
    try {
        const responseData = await filevineApi.updateClientandCase(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/filevine/update-client-to-filevine', authMiddleware, async (req, res) => {
    try {
        const responseData = await filevineApi.updateClientToFilevine(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/filevine/update-case-to-filevine', authMiddleware, async (req, res) => {
    try {
        const responseData = await filevineApi.updateCaseToFilevine(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/common-update-filevine', authMiddleware, async (req, res) => {
    try {
        const responseData = await filevineApi.commonUpdateFilevine(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* My case */
app.post('/rest/integrations/mycase/get-auth-token', authMiddleware, async (req, res) => {
    try {
        const responseData = await mycaseApi.getMycaseAuthToken(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/integrations/mycase/create-client-and-case', authMiddleware, async (req, res) => {
    try {
        const responseData = await mycaseApi.createClientandCase(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/integrations/mycase/get-data', authMiddleware, async (req, res) => {
    try {
        const responseData = await mycaseApi.getMycaseData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/integrations/mycase-file-upload', authMiddleware, async (req, res) => {
    try {
        const responseData = await mycaseApi.mycaseFileUpload(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/mycase-update-case-client', authMiddleware, async (req, res) => {
    try {
        const responseData = await mycaseApi.UpdateClientandCase(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/update-case-to-mycase', authMiddleware, async (req, res) => {
    try {
        const responseData = await mycaseApi.updateCaseToMycase(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/update-client-to-mycase', authMiddleware, async (req, res) => {
    try {
        const responseData = await mycaseApi.updateClientToMycase(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/common-update-mycase', authMiddleware, async (req, res) => {
    try {
        const responseData = await mycaseApi.commonUpdateMycase(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/integrations/mycase/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await mycaseApi.destroyTokenData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

/* Clio Integration */
app.post('/rest/integrations/get-clio-authtoken', authMiddleware, async (req, res) => {
    try {
        const responseData = await ClioApi.getClioAuthToken(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/integrations/get-clio-data', authMiddleware, async (req, res) => {
    try {
        const responseData = await ClioApi.getClioData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/integrations/clio/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await ClioApi.destroyTokenData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/integrations/clio/create-client-and-case', authMiddleware, async (req, res) => {
    try {
        const responseData = await ClioApi.createClientandCase(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/clio-update-client-and-case', authMiddleware, async (req, res) => {
    try {
        const responseData = await ClioApi.UpdateClientandCase(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/update-case-to-clio', authMiddleware, async (req, res) => {
    try {
        const responseData = await ClioApi.updateCaseToClio(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/update-client-to-clio', authMiddleware, async (req, res) => {
    try {
        const responseData = await ClioApi.updateClientToClio(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/integrations/clio-file-upload', authMiddleware, async (req, res) => {
    try {
        const responseData = await ClioApi.clioFileUpload(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/common-update-clio', authMiddleware, async (req, res) => {
    try {
        const responseData = await ClioApi.commonUpdateClio(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/update-getone-clio-data', authMiddleware, async (req, res) => {
    try {
        const responseData = await ClioApi.updateGetOneClioData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

// litify
app.post('/rest/integrations/get-litify-data', authMiddleware, async (req, res) => {
    try {
        const responseData = await LitifyApi.getLitifyData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/update-client-to-litify', authMiddleware, async (req, res) => {
    try {
        const responseData = await LitifyApi.updateClientToLitify(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/update-case-to-litify', authMiddleware, async (req, res) => {
    try {
        const responseData = await LitifyApi.updateCaseToLitify(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/common-update-litify', authMiddleware, async (req, res) => {
    try {
        const responseData = await LitifyApi.commonUpdateLitify(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/integrations/litify-file-upload', authMiddleware, async (req, res) => {
    try {
        const responseData = await LitifyApi.litifyFileUpload(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/integrations/get-multiple-records-litify', authMiddleware, async (req, res) => {
    try {
        const responseData = await LitifyApi.getMultipleRecords(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/integrations/bulk-create-litify', authMiddleware, async (req, res) => {
    try {
        const responseData = await LitifyApi.bulkCreate(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/integrations/bulk-update-litify', authMiddleware, async (req, res) => {
    try {
        const responseData = await LitifyApi.bulkUpdate(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
//medical history summary
app.get('/rest/medical-history-summery-api/startMedicalSummarybackgroundProcess', async (req, res) => {
    try {
        const responseData = await OpenAiApi.startMedicalSummarybackgroundProcess(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/medical-history-summery/getSummaryDetails', async (req, res) => {
    try {
        const responseData = await OpenAiApi.getSummaryDetails(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/forms-automation', async (req, res) => {
    try {
        const responseData = await automation.getOpposingCounsilDetails(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/medical-history-summery-ai/retryGenerateMedicalSummary/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await OpenAiApi.retryGenerateMedicalSummary(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* website_authentication */
app.post('/rest/website-authentication', async (req, res) => {
    try {
        const responseData = await WebsiteAuthenticationApi.siteAuthentication(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* User Based Subscription */
app.post('/rest/users-license/subscription', authMiddleware, async (req, res) => {
    try {
        const responseData = await usersLicenseAPI.create(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/users-license/subscription/:id', authMiddleware, async (req, res) => {
    console.log('called put')
    try {
        const responseData = await usersLicenseAPI.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/users-license/subscription/propounding', authMiddleware, async (req, res) => {
    try {
        const responseData = await usersLicenseAPI.propoundingSubscription(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/users-license/subscription/cancel-propounding/:id', authMiddleware, async (req, res) => {
    try {
        const responseData = await usersLicenseAPI.cancelPropoundingSubscription(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/users-license/subscription-cancel', authMiddleware, async (req, res) => {
    try {
        const responseData = await usersLicenseAPI.subscriptionDelete(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/users-license/license-migration', async (req, res) => {
    try {
        const responseData = await usersLicenseAPI.totalLicenseCountMigration(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/users-license/billing', async (req, res) => {
    try {
        const responseData = await usersLicenseAPI.fetchUserLicenseBillingStatus(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/users-license/subscripton-billing', async (req, res) => {
    try {
        const responseData = await usersLicenseAPI.fetchUserLicenseSubscriptionBilling(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/users/license-update', async (req, res) => {
    try {
        const responseData = await UsersApi.licenseUpdate(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/user/update-notification-status', authMiddleware,async (req,res)=>{
    try {
        const responseData = await UsersApi.UpdateZenNotification(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Video Call */
app.post('/rest/video-calls/join', async (req, res) => {
    try {
        const responseData = await VideoCallsApi.join(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.post('/rest/video-calls/attendee', async (req, res) => {
    try {
        const responseData = await VideoCallsApi.attendee(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.post('/rest/video-calls/end', async (req, res) => {
    try {
        const responseData = await VideoCallsApi.end(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.post('/rest/video-calls/start_capture', async (req, res) => {
    try {
        const responseData = await VideoCallsApi.start_capture(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.put('/rest/video-calls/end_capture', async (req, res) => {
    try {
        const responseData = await VideoCallsApi.end_capture(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.put('/rest/video-calls/capture_concatenation', async (req, res) => {
    try {
        const responseData = await VideoCallsApi.capture_concatenation(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.post('/rest/video-calls/start_meetingTranscript', async (req, res) => {
    try {
        const responseData = await VideoCallsApi.start_meetingTranscript(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.post('/rest/video-calls/stop_meetingTranscript', async (req, res) => {
    try {
        const responseData = await VideoCallsApi.stop_meetingTranscript(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.get('/rest/video-calls/read_transcriptData', async (req, res) => {
    try {
        const responseData = await VideoCallsApi.read_transcriptData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});

app.get('/rest/video-calls/meeting_scheduler', async (req, res) => {
    try {
        const responseData = await VideoCallsApi.meeting_scheduler(req)
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body))
    } catch (err) {
        return res.status(err.statusCode || 500).json({ error: err.message })
    }
})

app.get('/rest/video-calls/google_oauth', async (req, res) => {
    try {
        const responseData = await VideoCallsApi.google_oauth(req)
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body))
    } catch (err) {
        return res.status(err.statusCode || 500).json({ error: err.message })
    }
})
/* doc_editor */
app.post('/rest/doc-editor/url-generator', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocEditorAPI.docUrlGenerator(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/doc-editor/public-url-generator', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocEditorAPI.docPublicUrlGenerator(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/legal-forms/save-doc-SFDT', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocEditorAPI.SaveDocSFDTkey(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/file-rename', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocEditorAPI.fileRename(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/doc-to-pdf-file-rename',authMiddleware,async (req,res)=>{
    try {
        const responseData = await DocEditorAPI.DocToPdfFileRename(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/test-email', authMiddleware, async (req, res) => {
    try {
        const responseData = await DocEditorAPI.testEmail(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.get('/rest/rename_db', async (req, res) => {
    try {
        const responseData = await DocEditorAPI.NameRenameapi(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* hashed files */
app.post('/rest/hash/file-hashing', authMiddleware, async (req, res) => {
    try {
        const responseData = await HashedFilesAPI.fileHashing(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.put('/rest/hash/update-question', authMiddleware, async (req, res) => {
    try {
        const responseData = await HashedFilesAPI.updateQuestion(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/hash/file-upload-url', authMiddleware, async (req, res) => {
    try {
        const responseData = await HashedFilesAPI.fileUploadURL(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.post('/rest/hash/delete-hash-data', authMiddleware, async (req, res) => {
    try {
        const responseData = await HashedFilesAPI.deleteHashData(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
app.delete('/rest/hash/delete-data-by-duration', authMiddleware, async (req, res) => {
    try {
        const responseData = await HashedFilesAPI.deleteDataByDuration(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
});
/* Stripe */
app.post('/rest/stripe/update-invoice-details', async (req, res) => {
    try {
        const responseData = await paymentGatewayAPI.updateSubscriptionTables(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})
app.post('/rest/stripe/sample/invoice', async (req, res) => {
    try {
        const responseData = await paymentGatewayAPI.getInvoiceDetails(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})
app.post('/rest/stripe/sample/charge', async (req, res) => {
    try {
        const responseData = await paymentGatewayAPI.getChargeDetails(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})
/* Document Versioning */
app.get('/rest/document-versioning/:id',authMiddleware, async (req, res) => {
    try {
        const responseData = await documentVersioningAPI.getOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})
app.get('/rest/document-versioning',authMiddleware,async (req, res) => {
    try {
        const responseData = await documentVersioningAPI.getAll(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})
app.put('/rest/document-versioning',authMiddleware,async (req, res) => {
    try {
        const responseData = await documentVersioningAPI.update(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})
app.delete('/rest/document-versioning',authMiddleware,async (req, res) => {
    try {
        const responseData = await documentVersioningAPI.destroy(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})
app.post('/rest/pdf-conversion',authMiddleware,async (req, res) => {
    try {
        const responseData = await documentVersioningAPI.pdfConversion(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})
app.get('/rest/pdf-status/:id/:type',authMiddleware,async (req, res) => {
    try {
        const responseData = await documentVersioningAPI.pdfStatusGetOne(req);
        return res
            .status(responseData.statusCode)
            .json(JSON.parse(responseData.body));
    } catch (err) {
        return res
            .status(err.statusCode || 500)
            .json({ error: err.message });
    }
})

module.exports.handler = serverless(app);
