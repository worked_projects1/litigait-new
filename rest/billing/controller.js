const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY, { apiVersion: '' });
const { sendEmail } = require('../../utils/mailModule');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const authMiddleware = require('../../auth');
const { documentTypes } = require('../../rest/helpers/documentType.helper');


const stripeWebHook = async (event) => {
    const input = typeof event === 'string' ? JSON.parse(event) : event;
    const { Subscriptions } = await connectToDatabase();
    if (input && input.data && input.data.object && input.data.object.object === 'subscription') {
        const existingSubscriptionDetails = await Subscriptions.findOne({
            where: { stripe_subscription_id: input.data.object.id }, order: [['createdAt', 'DESC']],
        });
        if (existingSubscriptionDetails) {
            const subscription = await stripe.subscriptions.retrieve(
                input.data.object.id
            );
            if (subscription) {
                existingSubscriptionDetails.stripe_subscription_data = JSON.stringify(subscription);
                await existingSubscriptionDetails.save();
            }
        }
    }
    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'text/plain',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body: JSON.stringify({
            status: 'ok',
        }),
    };
};
const fetchBillingStatus = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { case_id, document_type, document_generation_type, client_id, legalforms_id, propoundforms_id } = input;
        const practice_id = event.user.practice_id;
        const {
            Practices, Settings, Orders, MedicalHistory, MedicalHistorySummery, Op, Cases, Clients, PracticeSettings,
            Subscriptions, Users, Plans } = await connectToDatabase();
        const practiceObject = await Practices.findOne({ where: { id: practice_id } });
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${practice_id} was not found`);

        const usersObject = await Users.findOne({ where: { id: event.user.id } });

        const settingsObject = await Settings.findOne({ where: { key: 'global_settings' }, raw: true, });

        if (!settingsObject) throw new HTTPError(400, 'Settings data not found');
        let settings = JSON.parse(settingsObject.value);

        const practiceSettings = await PracticeSettings.findOne({ where: { practice_id, }, raw: true, logging: console.log });
        if (practiceSettings) {
            const practiceSettingsObject = JSON.parse(practiceSettings.value);
            settings = Object.assign(settings, practiceSettingsObject);
        }

        let validSubscriptionFeatures = [];
        let plan_category = document_generation_type == 'propound_doc' ? 'propounding' : 'responding';

        let subscriptionDetails = await Subscriptions.findOne({
            where: { practice_id, plan_category },
            order: [['createdAt', 'DESC']], logging: console.log
        });

        let subscriptionValidity, sub_plain = undefined;
        const subscriptionId = subscriptionDetails?.stripe_subscription_id;

        if (subscriptionId && subscriptionId.startsWith('sub_') && subscriptionDetails?.plan_id) {
            const subscription = await stripe.subscriptions.retrieve(subscriptionId);
            if (subscription) {
                subscriptionDetails.stripe_subscription_data = JSON.stringify(subscription);
                const updateubscriptionDetails = await subscriptionDetails.save();
                subscriptionDetails = updateubscriptionDetails.get({ plain: true });
                const stripeSubscriptionDataObject = JSON.parse(subscriptionDetails.stripe_subscription_data);
                subscriptionValidity = new Date(parseInt(stripeSubscriptionDataObject.current_period_end) * 1000);
            }
        } else if (subscriptionId && subscriptionId.startsWith('pi_')) {
            subscriptionDetails = subscriptionDetails.get({ plain: true });
            subscriptionValidity = new Date(subscriptionDetails.subscribed_valid_till);
        }
        let planDetails;
        const today = new Date();
        if ((subscriptionValidity > today) || ['responding_vip', 'propounding_vip'].includes(subscriptionDetails?.plan_id)) {
            let query = {};
            if (subscriptionDetails?.plan_id == 'responding_vip') query.plan_id = 'responding_vip';
            else if (subscriptionDetails?.plan_id == 'propounding_vip') query.plan_id = 'propounding_vip';
            else query.stripe_product_id = subscriptionDetails.stripe_product_id;
            planDetails = await Plans.findOne({ raw: true, where: query, logging: console.log });
            validSubscriptionFeatures = planDetails.features_included.split(',');
        }

        let free_tier_available = true;
        let medicalhistory_first_set = true;
        let credit_card_details_available = false;
        let custom_quote_needed = false;

        let stripe_payment_methods = [];

        let price = 0;
        let pagesCount = 0;
        let pricing_tier = {};
        let uneffectedPrice = 0;
        let minimumPrice = 0;
        let medicalHistory_previous_price = 0;
        let previous_uploaded_docs_pagesCount = 0;
        let total_uploaded_docs_pagesCount = 0;
        let medical_history_total_price = 0;

        const availableFormDocumentType = await documentTypes({ state: 'all', outputType: 'types' });
        if (document_type === 'TEKSIGN') {
            free_tier_available = false;
            const tekSignUsageCount = await Orders.count({
                where: {
                    status: 'completed', practice_id, client_id, legalforms_id, document_type: 'TEKSIGN',
                    document_generation_type: 'teksign'
                }, logging: console.log
            });
            if (validSubscriptionFeatures.includes('teksign') ||
                ((tekSignUsageCount !== 0) || (planDetails && planDetails.plan_id !== 'free_trial'))) {
                //tek-as-u-go user have to pay first tek-sign documents.
                free_tier_available = true;
            }
            else if (settings.teksign) {
                price = parseFloat(settings.teksign, 10);
            } else {
                price = 0;
            }
        } else if ((document_generation_type == 'pos' || document_generation_type == 'POS') && availableFormDocumentType.includes(document_type)) {
            const posFreeQuotaUseageCount = await Orders.count({
                where: {
                    practice_id, document_type: { [Op.in]: availableFormDocumentType }, amount_charged: 0,
                }
            });
            if (posFreeQuotaUseageCount >= parseInt(settings.no_of_docs_free_tier, 10)) {
                free_tier_available = false;
            }
            price = parseFloat(settings[document_generation_type], 10);
            if (validSubscriptionFeatures.includes('pos')) {
                price = 0;
                free_tier_available = true;
            }
        } else if (availableFormDocumentType.includes(document_type) && document_generation_type != 'pos' && document_generation_type != 'propound_doc') {

            const freeQuotaUseageCount = await Orders.count({
                where: {
                    practice_id: event.user.practice_id,
                    document_type: { [Op.in]: availableFormDocumentType },
                    amount_charged: 0,
                }
            });
            if (freeQuotaUseageCount >= parseInt(settings.no_of_docs_free_tier, 10)) {
                free_tier_available = false;
            }
            price = parseFloat(settings[`${document_generation_type}_generation_price`][document_type], 10);

            if ((input.document_generation_type == 'template' && validSubscriptionFeatures.includes('shell')) ||
                (input.document_generation_type == 'final_doc' && validSubscriptionFeatures.includes('discovery'))) {
                price = 0;
                free_tier_available = true;
            }

        } else if (document_type === 'custom_template') {
            free_tier_available = false;
            price = 300;
            if (settings.custom_template) {
                price = parseFloat(settings.custom_template, 10);
            }
        } else if (document_generation_type === 'propound_doc') {
            const propoundDocFreeQuotaUsageCount = await Orders.count({
                where: {
                    status: 'completed', practice_id, client_id, propoundforms_id,
                    document_type, document_generation_type: 'propound_doc'
                }, logging: console.log
            });
            if (propoundDocFreeQuotaUsageCount >= parseInt(settings.no_of_docs_free_tier, 10)) {
                free_tier_available = false;
            }
            if (validSubscriptionFeatures.includes('propound_doc')) {
                price = 0;
                free_tier_available = true;
            }
        } else if (document_type === 'MEDICAL_HISTORY') {
            free_tier_available = false;
            pagesCount = await MedicalHistory.sum('pages', { where: { case_id } });
            const MedicalSummeryRecords = await MedicalHistorySummery.findOne({
                where: { case_id: input.case_id }, order: [['createdAt', 'DESC']], raw: true
            });

            if (MedicalSummeryRecords) {
                medicalhistory_first_set = false;
                medicalHistory_previous_price = parseFloat(MedicalSummeryRecords.amount_charged);
                previous_uploaded_docs_pagesCount = MedicalSummeryRecords.pages;
            }
            if (settings?.medical_history_pricing_tier) {
                const pricingPlans = settings.medical_history_pricing_tier;
                for (let i = 0; i < pricingPlans.length; i += 1) {
                    if (pagesCount > pricingPlans[i].from && pricingPlans[i].custom_quote_needed) {
                        pricing_tier = pricingPlans[i];
                        custom_quote_needed = true;
                    } else if (pagesCount >= pricingPlans[i].from && pagesCount <= pricingPlans[i].to) {
                        pricing_tier = pricingPlans[i];
                        price = parseFloat(pricingPlans[i].price) * pagesCount;
                        uneffectedPrice = price;
                    }
                }
                if (settings?.medical_history_minimum_pricing) {
                    const minimumPricing = parseFloat(settings.medical_history_minimum_pricing);
                    if (price < minimumPricing) {
                        price = minimumPricing;
                        minimumPrice = price;
                    }
                }

                if (price) {
                    price = parseFloat(price.toFixed(2));
                }
            } else {
                return {
                    statusCode: 400,
                    headers: {
                        'Content-Type': 'text/plain',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Credentials': true
                    },
                    body: JSON.stringify({ status: 'error', message: 'Pricing tier data not available', }),
                };
            }
            if (!medicalhistory_first_set) {
                if (validSubscriptionFeatures.length && price) {
                    price *= 0.90; // 10% discount for Yearly Subscription
                }
                price = parseFloat(price.toFixed(2));
                medical_history_total_price = price;
                total_uploaded_docs_pagesCount = pagesCount;
                pagesCount -= previous_uploaded_docs_pagesCount;
                if (pagesCount < 0) {
                    pagesCount *= -1;
                } else if (pagesCount == 0) {
                    pagesCount = previous_uploaded_docs_pagesCount;
                }
                if (price > medicalHistory_previous_price) {
                    price -= medicalHistory_previous_price;
                    price = parseFloat(price.toFixed(2));
                } else {
                    price = 0;
                }
            } else {
                if (validSubscriptionFeatures.length && price) {
                    price *= 0.90; // 10% discount for Yearly Subscription
                }
                price = parseFloat(price.toFixed(2));
                medical_history_total_price = price;
                total_uploaded_docs_pagesCount = pagesCount;
            }
        }
        if (practiceObject && practiceObject.stripe_customer_id) {
            const paymentMethods = await stripe.paymentMethods.list({
                customer: practiceObject.stripe_customer_id,
                type: 'card',
            });
            stripe_payment_methods = paymentMethods.data || [];
            if (paymentMethods.data.length) {
                credit_card_details_available = true;
            }
        }
        if (custom_quote_needed === true) {
            const caseObject = await Cases.findOne({ where: { id: input.case_id } });
            const clientObject = await Clients.findOne({ where: { id: caseObject.client_id } });
            let template = 'Total pages exceeded';
            if (event?.user?.name) { template = template + ' by ' + event.user.name }
            await sendEmail(process.env.SUPPORT_EMAIL_RECEIPIENT, `${template}`, `
      User Email: ${event.user.email} <br/>Practice Name : ${practiceObject.name} <br/>
      Client Name : ${clientObject.name} <br/>Case Title : ${caseObject.case_title} <br/>
      Case Number : ${caseObject.case_number} <br/>Document Type: Medical History <br/>
      Total Pages: ${pagesCount} <br/>`);
        }
        if (price && price > 0 && price < 0.50) price = 0.50;

        medical_history_total_price = medical_history_total_price.toFixed(2);
        uneffectedPrice = uneffectedPrice.toFixed(2);
        medicalHistory_previous_price = medicalHistory_previous_price.toFixed(2);
        const data = {
            free_tier_available, credit_card_details_available, price, stripe_payment_methods,
            custom_quote_needed, pagesCount, pricing_tier, validSubscriptionFeatures, uneffectedPrice,
            minimumPrice, medicalHistory_previous_price, previous_uploaded_docs_pagesCount,
            total_uploaded_docs_pagesCount, medical_history_total_price,
            is_tekasyougo: usersObject?.notification_status ? usersObject.notification_status : false
        };
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(data),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch billing details.' }),
        };
    }
}
const setUpPaymentIntent = async (event) => {
    try {
        const { Practices } = await connectToDatabase();
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

        if (!practiceObject.stripe_customer_id) {
            const customer = await stripe.customers.create();
            console.log('Customer ID ' + customer.id);
            practiceObject.stripe_customer_id = customer.id;
        }

        const intent = await stripe.setupIntents.create({
            customer: practiceObject.stripe_customer_id,
        });

        await practiceObject.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                client_secret: intent.client_secret,
            }),
        };
    } catch (err) {
        console.log('Stripe error : payment intent');
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the practices.' }),
        };
    }
};
const testPayment = async (event) => {
    try {
        const { Practices } = await connectToDatabase();
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

        const paymentMethods = await stripe.paymentMethods.list({
            customer: practiceObject.stripe_customer_id,
            type: 'card',
        });
        if (!paymentMethods.data.length) {
            throw new HTTPError(400, 'Payment method not added');
        }

        const paymentMethod = paymentMethods.data[0];
        const paymentIntent = await stripe.paymentIntents.create({
            amount: 1099,
            currency: 'usd',
            customer: practiceObject.stripe_customer_id,
            payment_method: paymentMethod.id,
            off_session: true,
            confirm: true,
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                paymentIntent,
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the practices.' }),
        };
    }
};
const saveCardData = async (event) => {
    try {
        const input = JSON.parse(event.body);
        const { Practices } = await connectToDatabase();
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

        if (!input) {
            throw new HTTPError(400, 'fields missing');
        }
        if (!input.payment_method) {
            throw new HTTPError(400, 'Stripe Token missing');
        }
        if (practiceObject.stripe_customer_id) {
            const paymentMethods = await stripe.paymentMethods.list({
                customer: practiceObject.stripe_customer_id,
                type: 'card',
            });
            if (paymentMethods.data.length) {
                for (let i = 0; i < paymentMethods.data.length; i += 1) {
                    if (paymentMethods.data[i].id != input.payment_method) {
                        await stripe.paymentMethods.detach(paymentMethods.data[i].id);
                    }
                }
            }
            await stripe.paymentMethods.attach(
                input.payment_method,
                { customer: practiceObject.stripe_customer_id }
            );
            await stripe.customers.update(
                practiceObject.stripe_customer_id,
                {
                    invoice_settings: { default_payment_method: input.payment_method },
                    email: event.user.email,
                    name: practiceObject.name,
                    metadata: {
                        practice_id: event.user.practice_id,
                    },
                }
            );
        } else {
            const stripeCustomer = await stripe.customers.create(
                {
                    description: ' Added Stripe Data',
                    email: event.user.email,
                    name: practiceObject.name,
                    metadata: {
                        practice_id: event.user.practice_id,
                    },
                }
            );
            practiceObject.stripe_customer_id = stripeCustomer.id;
            await practiceObject.save();
            await stripe.paymentMethods.attach(
                input.payment_method,
                { customer: practiceObject.stripe_customer_id }
            );
            await stripe.customers.update(
                practiceObject.stripe_customer_id,
                { invoice_settings: { default_payment_method: input.payment_method } }
            );
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'card data saved',
            }),
        };
    } catch (err) {
        console.log('Stripe error : saving card data');
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the practices.' }),
        };
    }
};
const updateSubscriptionWebHookStatus = async (event) => {
    try {
        const { Op, Users, Plans, Practices, SubscriptionHistoryWebhook, SubscriptionHistory, Subscriptions, DiscountCode, DiscountHistory } = await connectToDatabase();
        let body = '';
        console.log(event.body);
        if (process.env.CODE_ENV == 'local' && event.body) {
            //body = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
            body = typeof event.body?.data === 'string' ? JSON.parse(event.body.data) : event.body.data;
        } else {
            body = typeof event.data === 'string' ? JSON.parse(event.data) : event.data;
        }
        const monthly_plans = ['monthly', 'responding_monthly_495', 'responding_monthly_349', 'propounding_monthly_199'];
        const yearly_plans = ['yearly', 'responding_yearly_5100', 'responding_yearly_3490', 'propounding_yearly_2199'];
        console.log(body);
        const customer_id = body.object.customer;
        const practicesobj = await Practices.findOne({ where: { stripe_customer_id: customer_id, is_deleted: { [Op.not]: true } }, raw: true });
        if (!practicesobj) throw new HTTPError(404, `Invalid stripe customer id ${customer_id}`);
        /* Find Initially created user */
        const usersobj = await Users.findOne({
            where: { practice_id: practicesobj.id, is_deleted: { [Op.not]: true }, role: { [Op.in]: ['lawyer', 'paralegal'] }, is_admin: true },
            order: [['createdAt', 'ASC']],
            raw: true
        });
        /* Find Plans details */
        const planObject = body.object.metadata.plan_id
            ? await Plans.findOne({ where: { plan_id: body.object.metadata.plan_id }, raw: true })
            : await Plans.findOne({ where: { stripe_product_id: body.object.plan.product }, raw: true });
        /* Find previous subscription history */
        const currentSubscriptionObj = await SubscriptionHistory.findOne({
            where: { practice_id: practicesobj.id, plan_category: planObject.plan_category },
            order: [['createdAt', 'DESC']],
            raw: true,
        });

        let subscriptionValidityStart = new Date(body.object.current_period_start * 1000);
        let subscriptionValidity = new Date(body.object.current_period_end * 1000);
        let latest_actvity, switchedPlan, esquiretek_activity_type, payment_type, discountCodeObj, discount_percentage, discount_price, discount_code, discounted_price, discount_amount, base_price;
        let status = 'Success';

        /* Find Payment type */
        if (event.type === 'customer.subscription.created') {
            payment_type = 'New';
        } else if (event.type === 'customer.subscription.updated') {
            if (
                currentSubscriptionObj?.plan_type &&
                ((monthly_plans.includes(currentSubscriptionObj.plan_type) && monthly_plans.includes(planObject.plan_type)) ||
                    (yearly_plans.includes(currentSubscriptionObj.plan_type) && yearly_plans.includes(planObject.plan_type)))
            ) {
                payment_type = 'Renewal';
            } else {
                payment_type = 'New';
            }
        } else if (event.type === 'customer.subscription.deleted') {
            payment_type = 'Subscription Canceled';
            subscriptionValidity = new Date();
        }
        /* Find latest_actvity,payment_type,switchedPlan,esquiretek_activity_type for canceled subscription*/
        if (body.object.cancel_at_period_end && body.object.canceled_at && body.object.cancel_at && ['active', 'trialing'].includes(body.object.status)) {
            latest_actvity = 'downgrade';
            payment_type = 'Subscription Canceled';
            switchedPlan = 'subscription_cancel';
            esquiretek_activity_type = 'DOWNGRADE';
        } else {
            if (body.object.metadata.plan_id && body.object.metadata.latest_actvity) {
                latest_actvity = body.object.metadata.latest_actvity;
            }
            switchedPlan = planObject.plan_type;
            const historyCount = await SubscriptionHistory.count({ where: { practice_id: practicesobj.id } });
            esquiretek_activity_type = historyCount <= 0 ? 'NEW_SUBSCRIPTION' : 'UPGRADED';
        }

        esquiretek_activity_type = !['active', 'trialing'].includes(body.object.status) ? undefined : esquiretek_activity_type;

        /* Find payment_type,esquiretek_activity_type for subscription create*/
        if (event.type === 'customer.subscription.created' && !['active', 'trialing'].includes(body.object.status)) {
            latest_actvity = 'Subscription Creation';
            payment_type = 'Failed';
            esquiretek_activity_type = 'UPGRADE';
            status = "Payment Failed";
        }

        /* Find payment_type,esquiretek_activity_type for subscription update*/
        if (event.type === 'customer.subscription.updated' && !['active', 'trialing'].includes(body.object.status)) {
            payment_type = 'Failed';
            esquiretek_activity_type = 'UPGRADE';
            status = "Subscription Canceled";
        }

        const cancel_at = body.object.cancel_at ? new Date(body.object.cancel_at * 1000) : null;
        const canceled_at = body.object.canceled_at ? new Date(body.object.canceled_at * 1000) : null;

        let price = planObject.price;

        /* Adding price details */
        if ((event.type === 'customer.subscription.created' || event.type === 'customer.subscription.updated') && body.object.discount?.coupon?.id && ['active', 'trialing'].includes(body.object.status)) {
            discount_code = body.object.discount.coupon.name;
            discountCodeObj = await DiscountCode.findOne({ where: { discount_code } });
            discount_percentage = discountCodeObj?.discount_percentage;
            const percentage = 1 - (discount_percentage / 100);
            discounted_price = base_price = price = (percentage * price).toFixed(2);
            discount_amount = planObject.price - parseFloat(discounted_price);
            discount_amount = discount_amount.toFixed(2)
            /*             const invoiceObj = await stripe.invoices.retrieve(body?.object?.latest_invoice);
                         discounted_price = invoiceObj?.total_discount_amounts[0]?.amount;
                         discounted_price = discounted_price / 100;
                         discount_amount = invoiceObj?.total / 100; */

        } else if (event.type === 'customer.subscription.deleted') {
            price = currentSubscriptionObj.price;
        }
        /* Create subscription history */
        const subscriptionHistoryData = {
            id: uuid.v4(),
            order_date: new Date(),
            practice_id: practicesobj.id,
            user_id: usersobj.id,
            subscribed_by: usersobj.name,
            price: price,
            plan_type: planObject.plan_type,
            subscribed_on: new Date(),
            plan_id: planObject.plan_id,
            stripe_subscription_id: body.object.id,
            subscription_valid_start: subscriptionValidityStart,
            subscription_valid_till: subscriptionValidity,
            stripe_subscription_data: JSON.stringify(body.object),
            cancel_at: cancel_at,
            cancel_at_period_end: body.object.cancel,
            canceled_at: canceled_at,
            esquiretek_activity_type: esquiretek_activity_type,
            payment_type: payment_type,
            switched_plan: switchedPlan,
            stripe_latest_activity: latest_actvity,
            stripe_product_id: planObject.stripe_product_id,
            status: status,
            event_type: event.type,
            plan_category: planObject.plan_category,
        };

        await SubscriptionHistory.create(subscriptionHistoryData);
        await SubscriptionHistoryWebhook.create(subscriptionHistoryData);

        /* Insert Discount History details. */
        if (body.object.discount?.coupon?.id && ['active', 'trialing'].includes(body.object.status) && !body.object.cancel_at_period_end) {
            const plan_type = planObject.plan_type.split('_').slice(0, planObject.plan_type.length - 1);
            const discount_for = plan_type.slice(0, plan_type.length - 1).join('_').toLowerCase();
            const subscriptionObj = await Subscriptions.findOne({
                where: { practice_id: practicesobj.id, plan_category: planObject.plan_category },
                order: [['createdAt', 'DESC']],
            });
            const discount_history = {
                id: uuid.v4(),
                practice_id: practicesobj.id,
                plan_type: planObject.plan_type,
                plan_category: planObject.plan_category,
                discount_for,
                promocode: discountCodeObj.name,
                discount_percentage,
                base_price: planObject.price,
                discounted_price,
                discount_amount
            };
            console.log(discount_history);
            await DiscountHistory.create(discount_history);
        }
        /* Subscribe Propounding Plan in stripe */
        const checkPropoundingSubscription = await Subscriptions.findOne({
            where: { practice_id: practicesobj.id, plan_category: 'propounding' },
            raw: true,
        });

        if (checkPropoundingSubscription && checkPropoundingSubscription.stripe_subscription_id.startsWith('pi_') && !practicesobj.is_propounding_canceled) {
            let propoundingPlan;
            if (planObject.plan_type && ['monthly', 'responding_monthly_349', 'responding_monthly_495'].includes(planObject.plan_type)) propoundingPlan = 'propounding_monthly_199';
            if (planObject.plan_type && ['yearly', 'responding_yearly_5100', 'responding_yearly_3490'].includes(planObject.plan_type)) propoundingPlan = 'propounding_yearly_2199';
            const propoundingPlanObj = await Plans.findOne({ where: { plan_type: propoundingPlan, active: true } });

            const subscriptionCreateObj = {
                customer: practicesobj.stripe_customer_id,
                items: [
                    { price: propoundingPlanObj.plan_id },
                ],
                metadata: {
                    practice_id: practicesobj.id,
                    user_email: usersobj.email,
                    plan_id: propoundingPlanObj.plan_id,
                    stripe_product_id: propoundingPlanObj.stripe_product_id,
                },
            };

            if (body.object.discount?.coupon?.name) {
                const propound_discountObj = await DiscountCode.findOne({ where: { discount_code: discount_code }, raw: true });
                if (propound_discountObj?.id) {
                    const is_eligible_for = propound_discountObj.plan_type.split(',');
                    if (is_eligible_for.includes(propoundingPlanObj.plan_type)) {
                        const stripe_couponcode_obj = JSON.parse(propound_discountObj.stripe_obj);
                        subscriptionCreateObj.coupon = stripe_couponcode_obj.id;
                    }
                }
            }

            const propoundingSubscription = await stripe.subscriptions.create(subscriptionCreateObj);

            if (propoundingSubscription?.status === 'incomplete') {
                await stripe.subscriptions.del(propoundingSubscription.id);
            }

            const subscriptionValidity = new Date(parseInt(propoundingSubscription.current_period_end) * 1000);
            /* Update propounding subscription in db */
            const updateColumn = {
                subscribed_on: new Date(),
                plan_id: propoundingPlanObj.plan_id,
                stripe_subscription_id: propoundingSubscription.id,
                subscribed_valid_till: subscriptionValidity,
                stripe_subscription_data: JSON.stringify(propoundingSubscription),
                stripe_product_id: propoundingPlanObj.stripe_product_id,
                plan_category: propoundingPlanObj.plan_category,
            };
            await Subscriptions.update(updateColumn, { where: { id: checkPropoundingSubscription.id } });
        }
        /*  */
        if (checkPropoundingSubscription && checkPropoundingSubscription.stripe_subscription_id.startsWith('pi_') && practicesobj.is_propounding_canceled) {
            await Practices.update({ is_propounding_canceled: null }, { where: { id: practicesobj.id } });
            await Subscriptions.destroy({ where: { practice_id: practicesobj.id, plan_category: 'propounding' } });
        }
        return;
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };
    }
}
module.exports.saveCardData = middy(saveCardData).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.fetchBillingStatus = middy(fetchBillingStatus).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.setUpPaymentIntent = middy(setUpPaymentIntent).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.testPayment = middy(testPayment).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.stripeWebHook = stripeWebHook;
module.exports.updateSubscriptionWebHookStatus = updateSubscriptionWebHookStatus;