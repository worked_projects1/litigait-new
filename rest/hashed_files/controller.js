const AWS = require('aws-sdk');
const s3 = new AWS.S3({ region: 'us-east-1' });
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const uuid = require('uuid');
const crypto = require('crypto');
const timeZone = require("../../utils/timeStamp");
const pdf = require('pdf-parse');

const {
  validateFileHashing, validateUpdateQuestion, validateFileHashingQuickCreate, validateDeleteHashData
} = require('./validation');

const fileHashing = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    console.log(input);
    const { s3_file_key, legalform_id, filename, template_id, uploaded_type, document_type, state } = input;
    validateFileHashing(input);
    if ((uploaded_type == 'Responding' || uploaded_type == 'Propounding') && !state) throw new HTTPError(400, 'State not provided');
    if ((uploaded_type == 'Responding' || uploaded_type == 'Propounding') && !state) throw new HTTPError(400, 'document_type not provided');
    const form_id = legalform_id ? legalform_id : template_id ? template_id : null;
    const form_type = template_id ? 'Propoundform' : 'Legalform';

    const { HashedFiles } = await connectToDatabase();
    const Bucket = template_id ? process.env.S3_BUCKET_FOR_PROPOUND_SETTINGS_DOCUMENTS : process.env.S3_BUCKET_FOR_FORMS;
    const s3params = {
      Bucket,
      Key: s3_file_key,
    };
    const s3File = await s3.getObject(s3params).promise();
    if (!s3File) throw new HTTPError(400, 'File not found in s3 Bucket');

    const ConversionData = await PdfHashConversion(s3File.Body);
    let hashData;
    if (uploaded_type != 'QuickCreate') {
      console.log("not quick create");
      hashData = await HashedFiles.findOne({
        where: {
          hash_code: ConversionData.hashValue,
          page_count: ConversionData.pageCount,
          document_type: document_type,
          state: state,
        }
      });
    }
    if (uploaded_type == 'QuickCreate') {
      console.log("quick create");
      hashData = await HashedFiles.findOne({
        where: {
          hash_code: ConversionData.hashValue,
          page_count: ConversionData.pageCount,
          uploaded_type: uploaded_type,
        }
      });
      console.log(hashData);
    }
    if (hashData) {
      let file_uploads = hashData.file_uploads;
      hashData.file_uploads = file_uploads + 1;
      hashData.recent_usage = new Date();
      await hashData.save();
      hashData.questions = JSON.parse(hashData.questions) || null;
      if (uploaded_type == 'QuickCreate' && hashData.document_data) {
        return fileHashingQuickCreate(input, hashData);
      }
    }
    if (!hashData) {
      hashData = await HashedFiles.create({
        id: uuid.v4(),
        practice_id: event.user.practice_id,
        form_id,
        form_type,
        filename: filename,
        s3_file_key: s3_file_key,
        hash_code: ConversionData.hashValue,
        uploaded_type: uploaded_type,
        page_count: ConversionData.pageCount,
        questions_available: 'false',
      });
      console.log("hashData create");
    }

    const hashResponse = Object.assign(
      {
        id: hashData.id,
        practice_id: event.user.practice_id,
        form_id: hashData.form_id || null,
        hash_code: hashData.hash_code,
        uploaded_type: hashData.uploaded_type,
        questions: hashData.questions || null,
        questions: hashData.questions || null,
        recent_usage: hashData.recent_usage || null,
        questions_available: hashData.questions_available || null,
        state: hashData.state || null,
      }
    );

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ hashResponse }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'hash not found' }),
    };
  }
}

const updateQuestion = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const { s3_file_key, questions } = input;
    validateUpdateQuestion(input);
    const { HashedFiles } = await connectToDatabase();
    const Bucket = process.env.S3_BUCKET_FOR_FORMS;
    const s3params = {
      Bucket,
      Key: s3_file_key,
    };
    const s3File = await s3.getObject(s3params).promise();
    if (!s3File) throw new HTTPError(400, 'File not found in s3 Bucket');
    const ConversionData = await PdfHashConversion(s3File.Body);
    const hashData = await HashedFiles.findOne({
      where: {
        hash_code: ConversionData.hashValue,
        page_count: ConversionData.pageCount
      }
    });

    if (!hashData) throw new HTTPError(400, `Hash Record not found`);
    hashData.questions = questions;
    hashData.questions_available = 'true';
    const hashResponse = await hashData.save();
    hashResponse.questions = JSON.parse(hashResponse.questions);

    if (hashResponse) {
      await s3.deleteObject(s3params).promise().catch((e) => console.error(e));
    }
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ message: 'Question successfully upload to the hash record.', hashResponse }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'hash not found.' }),
    };
  }
}


const fileHashingQuickCreate = async (input, hashData) => {
  try {
    const { id, practice_id, user_id } = input;
    const { document_data, document_type } = hashData;
    const { DocumentExtractionProgress, Op } = await connectToDatabase();

    validateFileHashingQuickCreate(input);
    const extractionObj = await DocumentExtractionProgress.findOne({ where: { id: id } });
    if (!extractionObj) throw new HTTPError(404, `Document extraction with id: ${params.id} was not found`);

    const DocumentData = JSON.parse(document_data);
    const set_number = DocumentData?.set_number
    const extractionData = await DocumentExtractionProgress.update({ document_data: document_data, set_number, document_type }, { where: { id, practice_id, user_id } });

    const hashResponse = Object.assign(
      {
        id: hashData.id,
        practice_id: practice_id,
        hash_code: hashData.hash_code,
        uploaded_type: hashData.uploaded_type,
        questions: hashData.questions || null,
        document_data: hashData.document_data || null,
        recent_usage: hashData.recent_usage || null,
        questions_available: hashData.questions_available || null,
        state: hashData.state || null,
        quick_create_id: extractionObj.id || null
      }
    );
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ hashResponse }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'hash not found.' }),
    };
  }
}

const deleteHashData = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const { s3_file_key } = input;
    validateDeleteHashData(input);
    const { HashedFiles } = await connectToDatabase();
    const Bucket = process.env.S3_BUCKET_FOR_FORMS;
    const s3params = {
      Bucket,
      Key: s3_file_key,
    };
    const s3File = await s3.getObject(s3params).promise();
    if (!s3File) throw new HTTPError(400, 'File not found in s3 Bucket');
    const ConversionData = await PdfHashConversion(s3File.Body);
    const deletedHash = await HashedFiles.destroy({
      where: {
        hash_code: ConversionData.hashValue,
        page_count: ConversionData.pageCount
      }
    });

    if (deletedHash) {
      await s3.deleteObject(s3params).promise().catch((e) => console.error(e));
    }

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ message: 'hash data deleted successfully' }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'failed to delete hash data' }),
    };
  }
}

const PdfHashConversion = async (input) => {
  const pdfBuffer = Buffer.from(input);
  const pdfData = await pdf(pdfBuffer);
  const pageCount = pdfData.numpages;
  const pdfText = pdfData.text;
  const hash = crypto.createHash('sha256');
  if (pdfText.trim().length == 0) {
    hash.update(pdfBuffer);
    console.log("Scanned file");
  } else {
    hash.update(pdfText);
    console.log("Text file");
  }
  const hashValue = hash.digest('hex');
  console.log(hashValue);
  if (!hashValue) throw new HTTPError(500, 'hash code not generated');
  hash.destroy();
  return { hashValue, pageCount };
};

const fileUploadURL = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

    const { filename } = input;
    const timeStamp = timeZone.isoTime();
    const s3_file_key = `hash/${process.env.CODE_ENV}/${timeStamp}/${filename}`
    const s3Params = {
      Bucket: process.env.S3_BUCKET_FOR_FORMS,
      Key: s3_file_key,
      ContentType: 'application/pdf',
      Expires: 60 * 60 * 1,
      ACL: 'private',
    };

    const uploadURL = s3.getSignedUrl('putObject', s3Params);

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ uploadURL, s3_file_key }),
    };
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'failed to generate upload url' }),
    };
  }
}

const deleteDataByDuration = async () => {
  try {

    const { HashedFiles, Op } = await connectToDatabase();

    const dataCount = await HashedFiles.count({
      where: {
        createdAt: {
          [Op.lt]: new Date(new Date() - 24 * 60 * 60 * 60 * 1000),
        }
      }
    });
    console.log(`Hash data count for last two months : ${dataCount}`);
    const removedData = await HashedFiles.destroy({
      where: {
        createdAt: {
          [Op.lt]: new Date(new Date() - 24 * 60 * 60 * 60 * 1000),
        }
      }
    });
    console.log(`Data Removed for last two months are listed below:`);
    console.log(`${removedData}`);
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        message: `Successfully deleted last two months hash records`,
        deleted_records_count: dataCount,
      }),
    };
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'failed to delete hash data' }),
    };
  }
}

module.exports.fileHashing = fileHashing;
module.exports.updateQuestion = updateQuestion;
module.exports.deleteHashData = deleteHashData;
module.exports.fileUploadURL = fileUploadURL;
module.exports.deleteDataByDuration = deleteDataByDuration;
