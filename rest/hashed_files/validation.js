const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');


exports.validateFileHashing = function (data) {
    const rules = {
        s3_file_key: 'required',
        uploaded_type: 'required',
        filename: 'required',
    };
    const validation = new Validator(data, rules);
    
    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateUpdateQuestion = function (data) {
    const rules = {
        s3_file_key: 'required',
        questions: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateFileHashingQuickCreate = function (data) {
    const rules = {
        id: 'required',
        user_id: 'required',
        practice_id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateDeleteHashData = function (data) {
    const rules = {
        s3_file_key: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};