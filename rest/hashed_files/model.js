module.exports = (sequelize, type) => sequelize.define('HashedFiles', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    form_id: type.STRING,
    form_type: type.STRING,
    filename: type.STRING,
    s3_file_key: type.STRING,
    hash_code: type.STRING,
    page_count: type.INTEGER,
    questions: type.TEXT('long'),
    document_data: type.TEXT('long'),
    state: type.STRING,
    document_type: type.STRING,
    uploaded_type: type.STRING, // QuickCreate, Responding, Propounding
    questions_available: {
        type:type.STRING,
        defaultValue: 'false',
        allowNull: false
    },                           // true, false, NoQuestions
    file_uploads:{
        type: type.INTEGER,
        defaultValue: 1,
        allowNull: false
    },
    recent_usage:type.DATE,
});