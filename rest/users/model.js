const bcrypt = require('bcryptjs');
const {generateColorCode} = require('../helpers/users.helper');

module.exports = (sequelize, type) => {
    const User = sequelize.define('Users', {
            id: {
                type: type.STRING,
                primaryKey: true,
            },
            name: type.STRING,
            email: type.STRING,
            password: type.STRING,
            temporary_password: type.BOOLEAN,
            practice_id: type.STRING,
            device_id: type.STRING,
            signature: type.TEXT('long'),
            role: type.STRING, // three types:
            // legal company  roles are lawyer, paralegal
            // litigait roles superAdmin , manager , operator , medicalExpert , QualityTechnician (only applicable for litigait)
            user_from: type.STRING,
            is_email_verified: type.BOOLEAN,
            is_admin: type.BOOLEAN,
            last_login_ts: type.DATE,
            archived: type.BOOLEAN,
            state_bar_number: type.STRING,
            privacy_policy_terms_of_use: type.BOOLEAN,
            privacy_policy: type.BOOLEAN,
            terms_of_use: type.BOOLEAN,
            login_attempts: type.INTEGER,
            reactivation_token: type.TEXT,
            reactivation_token_generation_date: type.DATE,
            twofactor_status: type.BOOLEAN,
            twofactor_otp: type.STRING,
            notification: type.BOOLEAN,
            color_code: {
                type: type.STRING,
                validate:{
                     getColorCodeForPracticeUsers: async () => {
                        let value = this?.practice_id ? await generateColorCode(this.practice_id, sequelize.models.Users) : null;
                    }
                }
            },
            created_by: type.STRING,
            is_free_user : {
                type: type.BOOLEAN,
                defaultValue : false
            },
            created_from: type.STRING,
            is_deleted: type.BOOLEAN,
            quick_create_notification: {
                type: type.BOOLEAN,
                defaultValue : true
            },
        }, {
            hooks: {
                async beforeCreate(user) {
                    const salt = await bcrypt.genSalt(); // whatever number you want
                    user.password = await bcrypt.hash(user.password, salt);
                },
            },
        },
    );
    User.afterCreate(async (user_details) => {
        await sequelize.models.Practices.increment('no_of_users', {
            by: 1,
            where: {id: user_details.practice_id, is_deleted:{[type.Op.not]:true}}
        });
    });
    User.prototype.validPassword = async function (password) {
        return await bcrypt.compare(password, this.password);
    };
    User.prototype.generateNewPassword = async function (password) {
        const salt = await bcrypt.genSalt(); // whatever number you want
        return await bcrypt.hash(password, salt);
    };
    User.prototype.checkIfLast6Password = async function (password, PasswordHistory) {
        // return false;
        // const { PasswordHistory } = await connectToDatabase();
        const query = {
            limit: 6,
            order: [
                ['createdAt', 'DESC'],
            ],
            where: {email: this.email},
        };
        const pastPasswords = await PasswordHistory.findAll(query);
        if (!pastPasswords.length) {
            return false;
        }
        let matchedAnyLast6Password = false;
        for (let i = 0; i < pastPasswords.length; i += 1) {
            const pastPasswordObject = pastPasswords[i];
            const passwordMatchResult = await bcrypt.compare(password, pastPasswordObject.password);
            if (passwordMatchResult) {
                matchedAnyLast6Password = true;
            }
        }
        return matchedAnyLast6Password;
    };
    return User;
};

