module.exports = (sequelize, type) => sequelize.define('PasswordHistory', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    user_id: type.STRING,
    email: type.STRING,
    password: type.STRING,
});
