const uuid = require("uuid");
const jwt = require("jsonwebtoken");
const moment = require('moment');
const middy = require("@middy/core");
const doNotWaitForEmptyEventLoop = require("@middy/do-not-wait-for-empty-event-loop");
const connectToDatabase = require("../../db");
const { HTTPError } = require("../../utils/httpResp");
const { commonDomains } = require("../../utils/emailDomainValidation");
const authMiddleware = require("../../auth");
const phoneUtil = require("google-libphonenumber").PhoneNumberUtil.getInstance();
const { customDateandTime } = require('../../utils/datetimeModule');
const { dateDiffInDays, unixTimeStamptoDateTime, unixTimeStamptoDate } = require('../../utils/timeStamp');
const {
    validateSignup,
    validateCreateUser,
    validateUpdateUser,
    validateReadUsers,
    validateForgetPassword,
    validateResetPassword,
    validateTwofactor,
    validateSignature
} = require("./validation");
const { sendEmail } = require("../../utils/mailModule");
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
const CryptoJS = require("crypto-js");
const { QueryTypes } = require("sequelize");
const { generateRandomString } = require('../../utils/randomStringGenerator');
const { calculateOneTimeFeeDiscount, getPlansByplanType,
    validateDiscountCode, createSubscriptionMetaObj, getPropoundingPlanType } = require('../helper_functions/controller.subscription');
const { respondingPlans, respondingYearlyPlans, respondingMonthlyPlans, propoundingMonthlyPlans,
    propoundingPlans, propoundingYearlyPlans
} = require('../helpers/plans.helper');

const signUp = async (event) => {
    try {
        const { Practices, Users, Op, AdminObjections, CustomerObjections, PasswordHistory,
            Feewaiver, Subscriptions, PropoundResponder, Plans } = await connectToDatabase();
        let id;
        if (process.env.NODE_ENV === "test" && event.body.id) id = event.body.id;
        const dataObject = Object.assign(JSON.parse(event.body), { id: id || uuid.v4(), is_free_user: false, last_login_ts: new Date() });
        validateSignup(dataObject);
        if (!dataObject.practice_name) throw new HTTPError(400, "practice name missing");

        const userObject = await Users.findOne({ where: { email: dataObject.email, is_deleted: { [Op.not]: true }, } });

        /* if (userObject && userObject.is_deleted) {
          await userObject.destroy();
        } else  */
        if (userObject) throw new HTTPError(400, `User with id: ${dataObject.email} already exist`);
        let findEmail = dataObject.email;
        const domain = findEmail.split("@");
        findEmail = "@" + domain[1];
        const is_commonDomain = commonDomains.includes(findEmail);
        if (!is_commonDomain) {
            const checkThisDomainExist = await Users.findOne({
                where: {
                    email: { [Op.like]: "%" + findEmail }, is_deleted: { [Op.not]: true },
                    role: { [Op.in]: ['lawyer', 'paralegal'] }
                },
                order: [["createdAt", "ASC"]], raw: true, logging: console.log,
            });
            if (checkThisDomainExist) {
                throw new HTTPError(400, `This organization already has an account. Please contact your admin (${checkThisDomainExist?.email}) to create an additional user.`);
            }
        }
        const feeWaiverobj = await Feewaiver.findOne({
            where: { email: dataObject.email, expiry_date: { [Op.gte]: new Date() } },
        });
        if (feeWaiverobj) {
            dataObject.one_time_activation_fee = false;
        } else {
            dataObject.one_time_activation_fee = true;
        }

        let practice_obj = {
            name: dataObject.practice_name,
            id: uuid.v4(),
            one_time_activation_fee: dataObject.one_time_activation_fee
        };

        const practice = await Practices.create(practice_obj);
        dataObject.practice_id = practice.id;

        dataObject.is_admin = true;
        const user = await Users.create(dataObject);
        await PasswordHistory.create({
            email: dataObject.email, password: user.password, id: uuid.v4(),
        });
        const subscriptionObj = await Subscriptions.create({
            practice_id: practice.id, subscribed_by: user.id, subscribed_on: new Date(), plan_id: "free_trial", id: uuid.v4(),
            plan_category: 'responding'
        });

        let plansdetails;
        if (subscriptionObj && subscriptionObj.plan_id) {
            plansdetails = await Plans.findOne({ where: { plan_id: subscriptionObj.plan_id, active: true }, raw: true });
        }


        let createdDate = moment(practice.createdAt).format('MM-DD-YYYY HH:mm:ss');
        let template = `New Firm Account Created For ${practice.name}`
        await sendEmail(process.env.PRACTICE_EMAIL_RECEIPIENT, template, `
            Practice Name: ${practice.name}<br/>
            Plan Type: ${plansdetails?.name}<br/>
            User Email: ${user?.email}<br/>
            Created Date: ${createdDate}
            `);


        const tokenUser = { id: user.id, role: user.role };

        const token = jwt.sign(tokenUser, process.env.JWT_SECRET, { expiresIn: process.env.JWT_EXPIRATION_TIME });
        const allAdminObjections = await AdminObjections.findAll({ raw: true });
        const processedObjections = [];
        for (let i = 0; i < allAdminObjections.length; i += 1) {
            const objectionItem = allAdminObjections[i];
            objectionItem.practice_id = practice.id;
            delete objectionItem.created_by_admin_id;
            objectionItem.adminobjection_id = objectionItem.id;
            objectionItem.id = uuid.v4();
            processedObjections.push(objectionItem);
        }
        if (processedObjections.length) {
            await CustomerObjections.bulkCreate(processedObjections);
        }
        /* Check Propound responder token id contains in dataObject */
        if (dataObject && dataObject.token_id) {
            const propoundResponderObj = await PropoundResponder.findOne({ where: { id: dataObject.token_id } });
            if (!propoundResponderObj) throw new HTTPError(400, ' Invalid token id exist please check');
            propoundResponderObj.responder_practice_id = practice.id;
            propoundResponderObj.is_new_user = false;
            await propoundResponderObj.save();
        }
        /* update recent login timestamp to practice table. */
        await Practices.update({ recent_login_ts: new Date() }, { where: { id: practice.id } });

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                authToken: `JWT ${token}`,
                onetime_activation: practice.one_time_activation_fee,
                subscription_id: subscriptionObj.id,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not create the users.",
            }),
        };
    }
};
const create = async (event) => {
    try {
        const input = event.body;
        const { email, role, notification, userCreatedIn } = input;
        const id = process.env.NODE_ENV === "test" ? input?.id : undefined;

        const litigaitRoles = ["superAdmin", "manager", "operator", "medicalExpert", "QualityTechnician"];
        let userExists = input?.user_password_exist;

        const practice_id = !litigaitRoles.includes(event.user.role) ? event.user.practice_id : undefined;
        const tmp_password = !userExists;

        const { Users, Practices, PasswordHistory, Op, Subscriptions, Feewaiver, Settings, Plans, PracticeSettings } = await connectToDatabase();

        const userObjs = await Users.findAll({
            where: {
                email, is_deleted: { [Op.not]: true },
                role: { [Op.not]: true },
                created_from: { [Op.not]: 'super_admin' }
            },
            is_free_user: false,
            order: [['createdAt', 'ASC']],
        });

        const adminUser = await Users.findOne({ where: { email, role: { [Op.in]: litigaitRoles }, is_deleted: { [Op.not]: true } } });

        if (adminUser) { throw new HTTPError(500, 'This email is already registered to the EsquireTek user account') };

        let user_password_exist = false;
        let i = 0;
        if (userObjs.length > 0) {
            input.password = userObjs[i].password;
            user_password_exist = true;
        }

        if (role === "superAdmin" && !litigaitRoles.includes(event.user.role)) {
            throw new HTTPError(400, `You Don't have Permission to create this user.`);
        }

        if (!litigaitRoles.includes(event.user.role) && practice_id) input.practice_id = practice_id;
        const dataObject = Object.assign(input, { id: id || uuid.v4(), temporary_password: tmp_password, created_from: userCreatedIn, created_by: event.user.id });
        if (!dataObject?.practice_id) dataObject.is_free_user = input?.is_free_user || true;
        if (dataObject?.practice_id) dataObject.is_free_user = input?.is_free_user || false;
        validateCreateUser(dataObject);

        const userObject = await Users.findOne({
            where: {
                email: dataObject?.email,
                practice_id: dataObject?.practice_id || null,
                role: { [Op.not]: litigaitRoles },
                is_deleted: { [Op.not]: true }
            },
        });

        if (userObject) throw new HTTPError(400, `User with email: ${dataObject.email} already exists.`);

        const settingsObject = await Settings.findOne({ where: { key: "global_settings" } });
        const plainSettingsObject = settingsObject.get({ plain: true });
        const settings = JSON.parse(plainSettingsObject.value);
        const monthly_user_limit = parseInt(settings.monthly_user_limit, 10);
        const yearly_user_limit = parseInt(settings.yearly_user_limit, 10);
        const monthly_plan_type = respondingMonthlyPlans;
        monthly_plan_type.push('free_trial');
        const yearly_plan_type = respondingYearlyPlans;
        let free_user_limit = settings?.free_user_limit

        /* Getting free user limit from Practice settings */
        if (practice_id) {
            const practice_settings_obj = await PracticeSettings.findOne({ where: { practice_id }, raw: true });
            if (practice_settings_obj) {
                let practice_setting_response = practice_settings_obj?.value ? JSON.parse(practice_settings_obj.value) : {};
                free_user_limit = practice_setting_response?.free_user_limit;
            }
        }

        let existingUserCount = 0;
        let userModel;
        if (dataObject?.practice_id) {
            const practiceObject = await Practices.findOne({ where: { id: dataObject.practice_id } });
            existingUserCount = await Users.count({
                where: { practice_id: dataObject.practice_id, is_deleted: { [Op.not]: true }, is_free_user: false },
                raw: true,
            });
        if (dataObject?.practice_id == 'f6e8e69c-cb3a-48fc-b88a-12264def1cab' && existingUserCount >= free_user_limit && input?.is_free_user == false)throw new HTTPError(400, `User limit exceeded.`);
            if (new Date(practiceObject.createdAt).getTime() < new Date(settings.new_pricings_from).getTime()) {
                dataObject.notification_status = true;
            }

            const existingSubscriptionObj = await Subscriptions.findOne({
                where: { practice_id: dataObject.practice_id, plan_category: 'responding' }, order: [["createdAt", "DESC"]],
                logging: console.log
            });
            console.log(practiceObject?.billing_type);
            if (existingSubscriptionObj && existingSubscriptionObj.plan_id) {
                const plansObj = await Plans.findOne({ where: { plan_id: existingSubscriptionObj.plan_id }, raw: true });
                if (practiceObject?.billing_type == 'unlimited_users_billing') {
                    if ((monthly_plan_type.includes(plansObj.plan_type) && existingUserCount >= monthly_user_limit) ||
                        (yearly_plan_type.includes(plansObj.plan_type) && existingUserCount >= yearly_user_limit)) {
                        throw new HTTPError(400, `Your account has reached the maximum number of users allowed. If you have inactive users, you can delete them to create new user accounts.`);
                    }
                } else if (practiceObject?.billing_type == 'limited_users_billing' && existingSubscriptionObj?.plan_id && existingSubscriptionObj?.plan_id != 'free_trial' && !dataObject?.is_free_user) {
                    if (existingUserCount >= practiceObject?.license_count && (respondingMonthlyPlans.includes(plansObj?.plan_type) || respondingYearlyPlans.includes(plansObj?.plan_type))) {
                        throw new HTTPError(400, `Your account has reached the maximum number of users allowed. Please purchase license for new users.`);
                    }
                }
            } else if (practiceObject?.billing_type == 'unlimited_users_billing') {
                if (existingUserCount >= monthly_user_limit) {
                    throw new HTTPError(400, `Your account has reached the maximum number of users allowed. If you have inactive users, you can delete them to create new user accounts.`);
                }
            }

            userModel = await Users.create(dataObject);

            if (existingSubscriptionObj?.plan_id == 'free_trial' && existingUserCount >= free_user_limit && practiceObject?.billing_type == 'limited_users_billing' && (userCreatedIn == 'customer_admin' || !input?.is_free_user)) {
                await Practices.increment('license_count', { by: 1, where: { id: practiceObject.id } });
            }
        } else {
            userModel = await Users.create(dataObject);
        }


        const user = userModel.get({ plain: true });
        await PasswordHistory.create({ email: dataObject.email, password: userModel.password, id: uuid.v4(), });
        user.user_password_exist = user_password_exist;

        if (user?.practice_id) {
            const practiceData = await Practices.findOne({ where: { id: user.practice_id } });

            if (practiceData) user.practice_name = practiceData.name;
            const checkSubscription = await Subscriptions.findOne({ where: { practice_id: dataObject.practice_id, plan_category: 'responding' }, raw: true });
            if (!litigaitRoles.includes(event.user.role) && dataObject.practice_id && existingUserCount === 0 && !checkSubscription?.id) {
                const feeWaiverobj = await Feewaiver.findOne({ where: { email: dataObject.email, expiry_date: { [Op.gte]: new Date() } } });
                if (!feeWaiverobj) {
                    await Practices.update({ one_time_activation_fee: true }, { where: { id: user.practice_id } });
                } else {
                    await Practices.update({ one_time_activation_fee: false }, { where: { id: user.practice_id } });
                }
            }
        }

        if (notification) {

            let emailTemplate = "";

            if (userExists == false) {
                if (userCreatedIn == "super_admin") {
                    emailTemplate = "Hi " + input.name + ",<br/><br/>EsquireTek has created an account for you to access the site. Here is the site url with login credentials for initial login. On the first login, you must change your password.<br/><br/> <a href=\"https://www.esquiretek.com/\">https://www.esquiretek.com/</a> <br/><br/>" + "Email&nbsp;:&nbsp;" + input.email + "<br/>Password&nbsp;:&nbsp;" + input.password;
                } else if (userCreatedIn == "customer_admin") {
                    emailTemplate = "Hi " + input.name + ",<br/><br/>" + user.practice_name + " has created an account for you to access the EsquireTek site. Here is the site url with login credentials for initial login. On the first login, you must change your password.<br/><br/> <a href=\"https://www.esquiretek.com/\">https://www.esquiretek.com/</a> <br/><br/>" + "Email&nbsp;:&nbsp;" + input.email + "<br/>Password&nbsp;:&nbsp;" + input.password;
                }
                await sendEmail(input.email, "Welcome To EsquireTek", emailTemplate, user.practice_name);
            }
            const supportMail = "<a href='#' style='text-decoration: none; mailto:color:#1a1110'>support@esquiretek.com</a>";
            if (userExists == true) {
                if (userCreatedIn == "super_admin") {
                    emailTemplate = "Hi " + input.name + ",<br/><br/>EsquireTek has granted you access to their firm. To access it, please follow these steps:" + "<br/><br/>1. Login to EsquireTek with your credentials.<br/>2. Click on ‘Switch Practice’ at the top right of the screen." + "<br/><br/>If you have any questions, please contact us at " + supportMail;

                    await sendEmail(input.email, `EsquireTek Granted Access To ${user.practice_name}`, emailTemplate, user.practice_name);

                } else if (userCreatedIn == "customer_admin") {
                    emailTemplate = "Hi " + input.name + ",<br/><br/>" + user.practice_name + " has granted you access to their firm. To access it, please follow these steps:" + "<br/><br/>1. Login to EsquireTek with your credentials.<br/>2. Click on ‘Switch Practice’ at the top right of the screen." + "<br/><br/>If you have any questions, please contact us at " + supportMail;

                    await sendEmail(input.email, `Access Granted To ${user.practice_name}`, emailTemplate, user.practice_name);

                }
            }
        }
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(user),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not create the users.",
            }),
        };
    }
};

const create_new = async (event) => {
    try {
        let id;
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        if (process.env.NODE_ENV === "test" && input.id) id = input.id;
        const litigaitAdminRoles = ["superAdmin", "manager", "operator"];
        const { body, user } = event;
        const { notification, userCreatedIn, role } = input;
        if (role == "superAdmin" && !litigaitAdminRoles.includes(role)) {
            throw new HTTPError(400, `You Don't have Permission to create this user.`);
        }
        if (!litigaitAdminRoles.includes(user?.role) && user?.practice_id) {
            input.practice_id = user.practice_id;
        }
        const temporary_password = !!notification;
        const dataObject = { ...body, id: id || uuid.v4(), temporary_password, created_by: event.user.id, created_from: 'customer_admin' };

        validateCreateUser(dataObject);
        const { Users, Practices, PasswordHistory, Op, Subscriptions, Feewaiver, Settings, Plans } = await connectToDatabase();
        const userObject = await Users.findOne({
            where: { email: dataObject.email, is_deleted: { [Op.not]: true }, },
        });
        if (userObject) throw new HTTPError(400, `User with id: ${dataObject.email} already exist`);
        const userRoles = ["superAdmin", "manager", "operator", "medicalExpert", "QualityTechnician"];
        if (dataObject?.practice_id) {
            const practiceObject = await Practices.findOne({ where: { id: dataObject.practice_id }, raw: true });
            const settingsObject = await Settings.findOne({ where: { key: "global_settings", } });

            const plainSettingsObject = settingsObject.get({ plain: true });
            const settings = JSON.parse(plainSettingsObject.value);

            if (new Date(practiceObject.createdAt).getTime() < new Date(settings.new_pricings_from).getTime()) {
                dataObject.notification_status = true;
            }

            const monthly_user_limit = parseInt(settings.monthly_user_limit, 10);
            const yearly_user_limit = parseInt(settings.yearly_user_limit, 10);
            const monthly_plan_type = respondingMonthlyPlans;
            monthly_plan_type.push('free_trial');
            const yearly_plan_type = respondingYearlyPlans;

            const existingSubscription = await Subscriptions.findOne({
                where: { practice_id: dataObject.practice_id, plan_category: 'responding' },
                order: [["createdAt", "DESC"]],
            });

            if (existingSubscription?.plan_id) {
                const plansObj = await Plans.findOne({ where: { plan_id: existingSubscription.plan_id }, raw: true, });

                if ((monthly_plan_type.includes(plansObj.plan_type) && existingUserCount >= monthly_user_limit) ||
                    (yearly_plan_type.includes(plansObj.plan_type) && existingUserCount >= yearly_user_limit)) {
                    throw new HTTPError(400, `Your account has reached the maximum number of users allowed. If you have inactive users, you can delete them in order to create new user accounts.`);
                }
            } else { //pay_as_you_go
                if (existingUserCount >= monthly_user_limit) {
                    throw new HTTPError(400, `Your account has reached the maximum number of users allowed. If you have inactive users, you can delete them in order to create new user accounts.`);
                }
            }
        }
        const newUser = await Users.create(dataObject);

        await PasswordHistory.create({ email: dataObject.email, password: newUser.password, id: uuid.v4() });

        const userPlainText = newUser.get({ plain: true });

        if (userPlainText?.practice_id) {
            const practiceData = await Practices.findOne({
                where: { id: userPlainText.practice_id },
            });
            if (practiceData) {
                userPlainText.practice_name = practiceData.name;
            }

            const checkSubscription = await Subscriptions.findOne({ where: { practice_id: dataObject.practice_id, plan_category: 'responding' }, raw: true });
            if (!userRoles.includes(event.userPlainText.role) && dataObject.practice_id && existingUserCount == 0 && !checkSubscription?.id) {
                const subscriptionObj = await Subscriptions.create({
                    practice_id: dataObject.practice_id, subscribed_by: userPlainText.id,
                    subscribed_on: new Date(), plan_id: "free_trial", id: uuid.v4(),
                });
                const feeWaiverobj = await Feewaiver.findOne({ where: { email: dataObject.email, expiry_date: { [Op.gte]: new Date() } } });
                if (!feeWaiverobj) {
                    await Practices.update({ one_time_activation_fee: true }, { where: { id: userPlainText.practice_id } });
                } else {
                    await Practices.update({ one_time_activation_fee: false }, { where: { id: userPlainText.practice_id } });
                }
            }
        }

        let emailTemplate = "";
        if (userCreatedIn == "super_admin") {
            emailTemplate = "Hi " + input.name + ",<br/><br/> EsquireTek has created an account for you to access the site. Here is the site url with login credentials for initial login. On the first login, you must change your password." + "<br/><br/>" + "https://www.esquiretek.com/" + "<br/><br/>" + "Email&nbsp;:&nbsp;" + input.email + "<br/>Password&nbsp;:&nbsp;" + input.password;
        } else if (userCreatedIn == "customer_admin") {
            emailTemplate = "Hi " + input.name + ",<br/><br/> " + userPlainText.practice_name + " has created an account for you to access the EsquireTek site. Here is the site url with login credentials for initial login. On the first login, you must change your password." + "<br/><br/>" + "https://www.esquiretek.com/" + "<br/><br/>" + "Email&nbsp;:&nbsp;" + input.email + "<br/>Password&nbsp;:&nbsp;" + input.password;
        }
        if (notification) {
            await sendEmail(input.email, "Welcome To EsquireTek", emailTemplate, userPlainText.practice_name);
        }

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not create the Users.",
            }),
        };
    }
}

const create_practice_users = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { Users, Practices, PasswordHistory, Op, Subscriptions, Feewaiver, Settings, Plans } = await connectToDatabase();
        const { body, user } = event;
        const { notification, userCreatedIn, role } = input;
        const temporary_password = !!notification;
        const dataObject = { ...body, id: id || uuid.v4(), temporary_password, created_by: event.user.id, created_from: 'customer_admin' };
        const userObject = await Users.findOne({ where: { email: dataObject.email, is_deleted: { [Op.not]: true }, } });
        let existingUserCount = 0;
        if (userObject) throw new HTTPError(400, `User with id: ${dataObject.email} already exist`);
        if (dataObject?.practice_id) {
            const practiceObject = await Practices.findOne({ where: { id: dataObject.practice_id }, raw: true });
            existingUserCount = practiceObject?.license_count || 0;
            const settingsObject = await Settings.findOne({ where: { key: "global_settings", }, raw: true });

            const settings = JSON.parse(settingsObject.value);

            const monthly_user_limit = parseInt(settings.monthly_user_limit, 10);
            const yearly_user_limit = parseInt(settings.yearly_user_limit, 10);

            const monthly_plan_type = respondingMonthlyPlans;
            monthly_plan_type.push('free_trial');
            const yearly_plan_type = respondingYearlyPlans;

            const existingSubscription = await Subscriptions.findOne({
                where: { practice_id: dataObject.practice_id, plan_category: 'responding' },
                order: [["createdAt", "DESC"]],
            });

            if (existingSubscription?.plan_id) {
                const plansObj = await Plans.findOne({ where: { plan_id: existingSubscription.plan_id }, raw: true, });

                if ((monthly_plan_type.includes(plansObj.plan_type) && existingUserCount >= monthly_user_limit) ||
                    (yearly_plan_type.includes(plansObj.plan_type) && existingUserCount >= yearly_user_limit)) {
                    throw new HTTPError(400, `Your account has reached the maximum number of users allowed. If you have inactive users, you can delete them in order to create new user accounts.`);
                }
            } else { //pay_as_you_go
                if (existingUserCount >= monthly_user_limit) {
                    throw new HTTPError(400, `Your account has reached the maximum number of users allowed. If you have inactive users, you can delete them in order to create new user accounts.`);
                }
            }
        }
        /* Users Based Subscription for New Practice */



        const newUser = await Users.create(dataObject);

        const userPlainText = newUser.get({ plain: true });

        if (userPlainText?.practice_id) {
            const practiceData = await Practices.findOne({ where: { id: userPlainText.practice_id } });
            if (practiceData?.id) userPlainText.practice_name = practiceData.name;

            const checkSubscription = await Subscriptions.findOne({ where: { practice_id: dataObject.practice_id, plan_category: 'responding' }, raw: true });
            if (dataObject.practice_id && existingUserCount == 0 && !checkSubscription?.id) {
                const subscriptionObj = await Subscriptions.create({
                    practice_id: dataObject.practice_id, subscribed_by: userPlainText.id,
                    subscribed_on: new Date(), plan_id: "free_trial", id: uuid.v4(),
                });
                const feeWaiverobj = await Feewaiver.findOne({ where: { email: dataObject.email, expiry_date: { [Op.gte]: new Date() } } });
                if (!feeWaiverobj) {
                    await Practices.update({ one_time_activation_fee: true }, { where: { id: userPlainText.practice_id } });
                } else {
                    await Practices.update({ one_time_activation_fee: false }, { where: { id: userPlainText.practice_id } });
                }
            }
        }

        let emailTemplate = "";
        if (userCreatedIn == "super_admin") {
            emailTemplate = "Hi " + input.name + ",<br/><br/> EsquireTek has created an account for you to access the site. Here is the site url with login credentials for initial login. On the first login, you must change your password." + "<br/><br/>" + "https://www.esquiretek.com/" + "<br/><br/>" + "Email&nbsp;:&nbsp;" + input.email + "<br/>Password&nbsp;:&nbsp;" + input.password;
        } else if (userCreatedIn == "customer_admin") {
            emailTemplate = "Hi " + input.name + ",<br/><br/> " + userPlainText.practice_name + " has created an account for you to access the EsquireTek site. Here is the site url with login credentials for initial login. On the first login, you must change your password." + "<br/><br/>" + "https://www.esquiretek.com/" + "<br/><br/>" + "Email&nbsp;:&nbsp;" + input.email + "<br/>Password&nbsp;:&nbsp;" + input.password;
        }
        if (notification) {
            await sendEmail(input.email, "Welcome To EsquireTek", emailTemplate, userPlainText.practice_name);
        }

        await PasswordHistory.create({ email: dataObject.email, password: newUser.password, id: uuid.v4() });
    } catch (err) {
        console.log(err);
    }
}

const usersLicencePurchase = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { Users, Practices, PasswordHistory, Op, Subscriptions, DiscountCode, PracticeSettings, Feewaiver, Settings, Plans, SubscriptionHistory } = await connectToDatabase();
        const { practice_id, license_count } = input;
        if (!license_count) throw new HTTPError(400, 'License count not found.');
        if (!input?.responding) throw new HTTPError(400, 'Responding license price not found.');

        /* Validate Practice Details */
        const practiceObject = await Practices.findOne({ where: { id: practice_id, is_deleted: { [Op.not]: true }, billing_type: 'limited_users_billing' } });
        if (!practiceObject?.id) throw new HTTPError(400, `Invalid Practice Please check.`);
        if (!practiceObject?.stripe_customer_id) {
            throw new HTTPError(400, 'Please provide the credit card information on Credit Card Info settings and try again.');
        }
        /* get Payment Methods */
        const paymentMethods = await stripe.paymentMethods.list({
            customer: practiceObject.stripe_customer_id,
            type: 'card',
        });
        if (paymentMethods.data.length == 0) throw new HTTPError(400, 'This customer has no attached payment source or default payment method.');

        input.paymentMethod_id = paymentMethods.data[0];

        const users_count = await Users.count({ where: { practice_id, is_deleted: { [Op.not]: true }, role: { [Op.in]: ['lawyer', 'paralegal'] }, is_free_user: false } });

        /* Find Subscriptions */
        const subscriptionObj = await Subscriptions.findAll({
            where: { practice_id, plan_id: { [Op.not]: null }, stripe_subscription_id: { [Op.not]: null } },
            order: [['createdAt', 'ASC']],
            raw: true
        });
        if (subscriptionObj.length == 0) throw new HTTPError(400, 'Invalid Subscription plans.please check.');

        /* Getting free user limit from Global settings */
        const global_settings_Obj = await Settings.findOne({ where: { key: 'global_settings' }, raw: true });
        const settings_value = JSON.parse(global_settings_Obj.value);
        if (!settings_value?.free_user_limit) throw new HTTPError(400, 'Users limit not found.please contact support.');

        /* Getting free user limit from Practice settings */
        const practice_settings_obj = await PracticeSettings.findOne({ where: { practice_id: event.user.practice_id }, raw: true });
        const Practice_settings_value = practice_settings_obj?.value ? JSON.parse(practice_settings_obj.value) : {};
        const free_user_limit = Practice_settings_value?.free_user_limit ? Practice_settings_value?.free_user_limit : settings_value?.free_user_limit;


        let is_eligible_to_couponcode = false;
        const additional_users_count = parseInt(input?.license_count);

        for (let i = 0; i < subscriptionObj.length; i++) {
            const subscription_details = subscriptionObj[i];
            const stripe_subscription_data = JSON.parse(subscription_details.stripe_subscription_data);
            const coupon_code = stripe_subscription_data?.discount?.coupon?.name || null;
            let discount_obj;
            let percentage = 1; //default percentage;
            let stripe_couponcode_obj, discount_percentage, stripe_subscription_create_obj;

            const subscriped_root_plan = await Plans.findOne({ where: { plan_id: subscription_details.plan_id, active: true }, raw: true });

            if (!subscriped_root_plan?.id) throw new HTTPError(400, 'Invalid plan details.please check.');

            if (coupon_code) {
                discount_obj = await DiscountCode.findOne({ where: { name: { [Op.like]: coupon_code } }, raw: true });
            }

            /* validate discount code */
            if (discount_obj?.name) {
                const discountcode_obj = await validateDiscountCode(discount_obj?.name);
                const is_eligible_for = discountcode_obj.plan_type.split(',');
                if (is_eligible_for.includes(subscriped_root_plan.plan_type)) is_eligible_to_couponcode = true;
                stripe_couponcode_obj = JSON.parse(discountcode_obj.stripe_obj);
                /* Calculate Percentage */
                discount_percentage = discountcode_obj.discount_percentage;
                percentage = 1 - (discountcode_obj.discount_percentage / 100);
            }

            /* Get Addition plans */
            const additional_subscriptions_plan_obj = await getPlansByplanType('standard_plans', subscriped_root_plan?.plan_category, subscriped_root_plan?.plan_type);
            const additional_subscriptions_stripe_price_id = additional_subscriptions_plan_obj?.stripe_price_id;
            const { validity, price } = additional_subscriptions_plan_obj;

            /* Create meta data */
            const total_users = parseInt(practiceObject?.license_count) + additional_users_count;
            const additional_users = (parseInt(practiceObject?.license_count) - free_user_limit) + additional_users_count;
            const metadata = {
                practice_id: event.user.practice_id,
                user_email: event.user.email,
                plan_id: subscriped_root_plan.plan_id,
                stripe_product_id: subscriped_root_plan.stripe_product_id,
                plan_name: subscriped_root_plan.name,
                billing_type: 'limited_users_billing',
                total_users,
                additional_users,
                free_users: free_user_limit,
                plan_type: subscriped_root_plan?.plan_category,
                date: new Date()
            };

            let subscription_price = input[additional_subscriptions_plan_obj.plan_category].license_total_price;

            let { current_invoice_price, upcoming_invoice_price } = subscription_details;

            let prorated_subscription = {
                amount: parseInt(subscription_price * 100), currency: 'usd', customer: practiceObject.stripe_customer_id,
                payment_method: input.paymentMethod_id.id, off_session: true, confirm: true, metadata
            }

            current_invoice_price = parseFloat(current_invoice_price) + parseFloat(subscription_price);
            current_invoice_price = parseFloat(current_invoice_price).toFixed(2);

            if (subscription_price != 0) {
                stripe_subscription_create_obj = await stripe.paymentIntents.create(prorated_subscription);
            } else if (subscription_price == 0) {
                stripe_subscription_create_obj = Object.assign(prorated_subscription, { id: 'pi_one_hundred_per_cent_discount', status: 'succeeded' });
            }

            const ProratedAdditionalUserSubscriptionObj = createSubscriptionMetaObj(additional_subscriptions_stripe_price_id, additional_users_count, metadata, 'additional_user_subscription', false);

            const subscriptionHistoryData = {
                id: uuid.v4(), order_date: new Date(), practice_id: event.user.practice_id, user_id: event.user.id,
                subscribed_by: event.user.name, price: parseFloat(subscription_price), subscribed_on: new Date(),
                plan_id: additional_subscriptions_plan_obj.plan_id, plan_type: additional_subscriptions_plan_obj.plan_type,
                stripe_subscription_id: stripe_subscription_create_obj.id, subscription_valid_start: new Date(),
                subscription_valid_till: subscription_details.subscribed_valid_till,
                stripe_subscription_data: JSON.stringify(stripe_subscription_create_obj), stripe_product_id: additional_subscriptions_plan_obj.stripe_product_id,
                switched_plan: additional_subscriptions_plan_obj.plan_type,
                license_count: additional_users_count, plan_category: additional_subscriptions_plan_obj.plan_category, billing_type: additional_subscriptions_plan_obj?.billing_type
            };

            if (stripe_subscription_create_obj?.status == 'succeeded') {
                subscriptionHistoryData.status = 'Success';
                subscriptionHistoryData.payment_type = 'New';
                subscriptionHistoryData.esquiretek_activity_type = 'UPGRADE',
                    subscriptionHistoryData.switched_plan = additional_subscriptions_plan_obj.plan_type;
                await SubscriptionHistory.create(subscriptionHistoryData);
                await Subscriptions.update({ current_invoice_price: current_invoice_price }, { where: { id: subscription_details.id } });
            } else {
                subscriptionHistoryData.status = 'Failed';
                subscriptionHistoryData.payment_type = 'Subscription Canceled';
                subscriptionHistoryData.switched_plan = additional_subscriptions_plan_obj.plan_type;
                await SubscriptionHistory.create(subscriptionHistoryData);
                const paymentIntent = await stripe.paymentIntents.cancel(paymentIntent.id);
                throw new HTTPError(400, 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
            }

            if (stripe_subscription_data?.id.startsWith('sub_')) {
                /* Additional Users */
                const subscription_history = stripe_subscription_data?.items?.data || [];
                console.log(subscription_history);
                if (subscription_history.length >= 3 && subscription_history[2]?.id) { // already subscription have additional license.
                    let additional_license_subscription = await stripe.subscriptions.update(stripe_subscription_data.id, {
                        items: [{
                            id: subscription_history[2].id,
                            quantity: (parseInt(practiceObject?.license_count) - free_user_limit) + additional_users_count, // Set the new quantity here
                        }],
                        proration_behavior: 'none',
                        metadata
                    });

                    if (additional_license_subscription.status !== 'active') {
                        throw new HTTPError(400, 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
                    }
                } else {// Newly create additional license.
                    let additional_license_subscription = await stripe.subscriptions.update(stripe_subscription_data.id, ProratedAdditionalUserSubscriptionObj);

                    if (additional_license_subscription.status !== 'active') {
                        throw new HTTPError(400, 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
                    }
                }
            }
        }
        practiceObject.license_count = parseInt(practiceObject?.license_count + additional_users_count);
        await practiceObject.save();
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({ status: 'Ok', message: 'License purchased successfully.' }),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not create the users.",
            }),
        };
    }
}

const getMe = async (event, context) => {
    try {
        const { Practices, PracticesTemplates, VersionHistory, PasswordHistory, Settings, Subscriptions, Mycase, Orders, PracticeSettings, Clio,
            Plans, Users, Cases, Clients, Filevine, Op, sequelize, DiscountHistory, DiscountCode } = await connectToDatabase();

        const user = event.user;

        user.user_name = event.user.name;
        const practice_data = await PracticesTemplates.findAll({ where: { practice_id: user.practice_id }, raw: true });

        const userData = await Users.findOne({ where:{ id: user.id, is_deleted: { [Op.not]: true }}});

        if (!userData) throw new HTTPError(400, 'User not found');

        user.practiceDetails = practice_data;

        const cases = await Cases.findAll({ where: { practice_id: event.user.practice_id, is_deleted: { [Op.not]: true } } });
        user.cases_available = cases.length > 0 ? true : false;
        const client = await Clients.findAll({ where: { practice_id: event.user.practice_id, is_deleted: { [Op.not]: true } } });
        user.clients_available = client.length > 0 ? true : false;

        if (user.practice_id) {
            const practiceDetails = await Practices.findOne({ where: { id: event.user.practice_id } });
            if (practiceDetails.is_deleted) throw new HTTPError(400, `Could not login this practice. please check with admin.`);
            user.purchased_license_count = practiceDetails?.license_count;
            if (practiceDetails) {
                // PRACTICE_CREATED_BEFORE
                const getcurrentSubscription = await Subscriptions.findAll({
                    where: { practice_id: event.user.practice_id, plan_id: { [Op.not]: null } },
                    order: [["createdAt", "DESC"]], raw: true
                });
                for (const activesub of getcurrentSubscription) {

                    if (activesub && activesub.stripe_subscription_id && activesub.stripe_subscription_id.startsWith('sub_')) {
                        const activeSubscription = await Subscriptions.findOne({ where: { id: activesub.id }, raw: true });
                        let subscription, getActivePlan;
                        /* Update latest invoice and charge ID details here */
                        let invoiceObj, chargeObj;
                        try {
                            subscription = await stripe.subscriptions.retrieve(activesub.stripe_subscription_id);
                            if (subscription?.latest_invoice) {
                                invoiceObj = await stripe.invoices.retrieve(subscription.latest_invoice);
                                if (invoiceObj?.charge) {
                                    chargeObj = await stripe.charges.retrieve(invoiceObj.charge);
                                }
                            }
                            getActivePlan = await Plans.findOne({ where: { stripe_product_id: subscription?.items?.data[0]?.plan?.product }, raw: true });
                        } catch (err) {
                            invoiceObj = chargeObj = undefined;
                        }
                        console.log(subscription);
                        const updateColumn = {};

                        updateColumn.subscribed_on = subscription?.current_period_start ? new Date(parseInt(subscription.current_period_start) * 1000) : null;
                        updateColumn.plan_id = subscription?.items?.data[0]?.plan?.id || null;
                        updateColumn.stripe_subscription_id = subscription?.id || null;
                        updateColumn.subscribed_valid_till = subscription?.current_period_end ? new Date(parseInt(subscription.current_period_end) * 1000) : null;
                        if (subscription?.id) updateColumn.stripe_subscription_data = JSON.stringify(subscription); // don't set null to stripe subscription data
                        updateColumn.stripe_product_id = subscription?.items?.data[0]?.plan?.product || null;
                        if (getActivePlan?.plan_category) updateColumn.plan_category = getActivePlan.plan_category;
                        updateColumn.invoice_id = invoiceObj?.id || activeSubscription?.invoice_id;
                        updateColumn.payment_attempt_count = invoiceObj?.attempt_count || activeSubscription?.payment_attempt_count;
                        updateColumn.charge_id = invoiceObj?.charge || activeSubscription?.charge_id;
                        updateColumn.charge_response = chargeObj?.outcome ? JSON.stringify(chargeObj?.outcome) : activeSubscription?.charge_response;

                        let cancel_at = subscription?.cancel_at || null;
                        let current_period_end = subscription?.current_period_end || null;
                        if (subscription?.status && subscription.status == 'active' && (!cancel_at && !current_period_end)) {
                            const upcoming_invoice = await stripe.invoices.retrieveUpcoming({ subscription: subscription.id });
                            updateColumn.upcoming_invoice_price = parseFloat(upcoming_invoice?.total / 100);
                        }
                        console.log(updateColumn);
                        await Subscriptions.update(updateColumn, { where: { id: activesub.id } });
                    }
                }
                console.log('after subscription updates');
                const practicePlainText = practiceDetails.get({ plain: true });
                if (practicePlainText && practicePlainText.phone) {
                    const phnRawText = phoneUtil.parseAndKeepRawInput(practicePlainText.phone, "US");
                    const phoneNumber = phoneUtil.formatInOriginalFormat(phnRawText, "US");
                    practicePlainText.phone = phoneNumber;
                }
                user.practiceDetails = practicePlainText;
                let practice_createdAt = new Date(practicePlainText.createdAt);
                let practice_created_year = practice_createdAt.getFullYear();
                let practice_created_after = parseInt(process.env.PRACTICE_CREATED_AFTER);
                user.is_new_practice = practice_created_year >= practice_created_after ? true : false;

                if (!user.practiceDetails.objections) {
                    user.practiceDetails.objections = false;
                }
            }
        }

        const versionHistoryObject = await VersionHistory.findOne({ where: {}, });
        user.version = versionHistoryObject.id ? versionHistoryObject.version : 1;

        const lastPasswordChangeObject = await PasswordHistory.findOne({
            where: { email: user.email },
            order: [["createdAt", "DESC"]],
        });

        if (lastPasswordChangeObject) {
            user.lastPasswordChangeDate = new Date(lastPasswordChangeObject.createdAt);
        } else {
            const days30DaysBack = new Date();
            days30DaysBack.setDate(days30DaysBack.getDate() - 90);
            user.lastPasswordChangeDate = days30DaysBack;
        }

        const settingsObject = await Settings.findOne({ where: { key: "global_settings", } });
        if (user.practice_id) {
            let existingUserCount = await Users.count({
                where: { practice_id: user.practice_id, is_deleted: { [Op.not]: true }, is_free_user: false },
                raw: true,
            });
            user.total_no_of_user_count = existingUserCount;
        }
        if (settingsObject) {
            const plainSettingsObject = settingsObject.get({ plain: true });
            const settings = JSON.parse(plainSettingsObject.value);
            user.session_timeout = settings.session_timeout;
            user.monthly_user_limit = settings.monthly_user_limit || 0;
            user.yearly_user_limit = settings.yearly_user_limit || 0;
            user.propound_template_practices = settings.propound_template_practices || false;
            user.custom_template_limit = settings.custom_template_limit || 0;
            user.tekasyougo_practices = settings.tekasyougo_practices || false;
            user.old_pricing_till = settings.old_pricing_till || false;
            user.new_pricings_from = settings.new_pricings_from || false;
            user.new_responding_feature_from = settings.new_responding_feature_from || false;
            user.response_history_limit = settings.response_history_limit || false;
            user.show_document_editor_ids = settings.show_document_editor_ids || null;
        }

        let plan_type = { responding: 'tek_as_you_go', propounding: '' };
        let subscription_details = { responding: {}, propounding: {} }
        let activeSubscriptionObj = await Subscriptions.findAll({
            where: { practice_id: event.user.practice_id,
                    plan_id: { [Op.not]: null }
                    },
            order: [["createdAt", "DESC"]], raw: true,
        });
        
        if (activeSubscriptionObj.length > 2) throw new HTTPError(400, `Please check active subscription details.`);
        const inactiveSubscriptionObj = await Subscriptions.findAll({
            where: { practice_id: event.user.practice_id },
            raw: true,
        });

        for (let x = 0; x < inactiveSubscriptionObj.length; x++) {
            subscription_details[inactiveSubscriptionObj[x].plan_category] = {
                payment_attempt_count: inactiveSubscriptionObj[x]?.payment_attempt_count || null,
                charge_response: inactiveSubscriptionObj[x]?.charge_response ? JSON.parse(inactiveSubscriptionObj[x]?.charge_response) : null,
            }
        }
        user.subscription_details = subscription_details;
        for (let i = 0; i < activeSubscriptionObj.length; i++) {
            let subscriptionObj = activeSubscriptionObj[i];
            if (subscriptionObj && subscriptionObj.plan_id && !["free_trial", "responding_vip", "propounding_vip"].includes(subscriptionObj.plan_id)) {
                let plansObj;
                if (event.user.email == 'siva@ospitek.com') {
                    plansObj = await Plans.findOne({ where: { plan_id: subscriptionObj.plan_id }, raw: true });
                } else {
                    plansObj = await Plans.findOne({ where: { plan_id: subscriptionObj.plan_id, active: true }, raw: true });
                }
                if (!plansObj) throw new HTTPError(400, `Invalid suscription plan id.`);
                plan_type[plansObj.plan_category] = plansObj.plan_type;
                subscription_details[plansObj.plan_category] = {
                    payment_attempt_count: subscriptionObj?.payment_attempt_count || null,
                    charge_response: subscriptionObj?.charge_response ? JSON.parse(subscriptionObj?.charge_response) : null,
                }
                if (subscriptionObj.stripe_subscription_id.startsWith('sub_')) {
                    let stripeObj = JSON.parse(subscriptionObj.stripe_subscription_data);
                    let sub_status = stripeObj.status;
                    let sub_cancel_at = stripeObj.cancel_at ? unixTimetoDateconvert(stripeObj.current_period_end) : undefined;
                    let sub_current_period_end = unixTimetoDateconvert(stripeObj.current_period_end);
                    let current_date = new Date();
                    let invoiceObj;
                    let chargeObj;
                    if (stripeObj?.latest_invoice) invoiceObj = await stripe.invoices.retrieve(stripeObj?.latest_invoice);
                    if (invoiceObj?.charge) chargeObj = await stripe.charges.retrieve(invoiceObj?.charge);

                    if ((sub_status && !['active'].includes(sub_status)) || (sub_current_period_end && current_date && sub_current_period_end <= current_date && sub_cancel_at && sub_cancel_at <= current_date)) {
                        if (['incomplete_expired', 'canceled'].includes(sub_status) || (['past_due', 'incomplete', 'unpaid'].includes(sub_status) && (subscriptionObj?.payment_attempt_count >= 4 && (chargeObj?.status == 'failed' && invoiceObj?.status == 'open')))) {
                            if (!['incomplete_expired', 'canceled'].includes(sub_status)) await stripe.subscriptions.del(stripeObj.id);
                            const updateColumn = {
                                stripe_product_id: null, stripe_subscription_id: null,
                                subscribed_on: null, plan_id: null, subscribed_valid_till: null,
                                invoice_id: null, payment_attempt_count: null, charge_id: null, charge_response: null
                            };
                            await Subscriptions.update(updateColumn, { where: { practice_id: user.practice_id, id: subscriptionObj.id, } });
                        }
                        if (['past_due', 'incomplete', 'unpaid'].includes(sub_status) && subscriptionObj?.payment_attempt_count < 4) {
                            plan_type[plansObj.plan_category] = plansObj.plan_type;
                        } else {
                            plan_type[plansObj.plan_category] = '';
                        }
                        subscriptionObj = await Subscriptions.findOne({
                            where: { practice_id: user.practice_id, id: subscriptionObj.id, },
                            raw: true, order: [["createdAt", "DESC"]],
                        });
                    }
                }
            } else if (subscriptionObj && !subscriptionObj.plan_id && subscriptionObj.plan_category == 'responding') {
                plan_type.responding = '';
            } else if (subscriptionObj && !subscriptionObj.plan_id && subscriptionObj.plan_category == 'propounding') {
                plan_type.propounding = '';
            }
            if (subscriptionObj?.plan_id == "free_trial") {
                plan_type.responding = 'free_trial';
            }
            if (subscriptionObj?.plan_id == "responding_vip") {
                plan_type.responding = 'responding_vip';
            }
            if (subscriptionObj?.plan_id == "propounding_vip") {
                plan_type.propounding = 'propounding_vip';
            }
        }
        user.plan_type = plan_type;

        if (event?.user?.practice_id) {
            let settingsObj;
            const practiceSettings = await PracticeSettings.findOne({ where: { practice_id: event.user.practice_id, }, raw: true });
            if (practiceSettings) {
                settingsObj = JSON.parse(practiceSettings.value);
            } else {
                settingsObj = settingsObject.get({ plain: true });
                settingsObj = JSON.parse(settingsObj.value);
            }
            user.free_user_limit = settingsObj?.free_user_limit;
            const ordersCount = await Orders.count({ where: { practice_id: event.user.practice_id } });

            if (ordersCount >= parseInt(settingsObj.no_of_docs_free_tier, 10)) {
                user.free_tier_count = 0;
            } else {
                user.free_tier_count = (parseInt(settingsObj.no_of_docs_free_tier, 10) - ordersCount);
            }
        }
        const fileVinedetails = await Filevine.findOne({
            where: { practice_id: event.user.practice_id },
            attributes:['id','filevine_user_id','filevine_org_id','filevine_key','filevine_secret','filevine_refresh_token','filevine_hash','filevine_timeStamp','filevine_baseurl','createdAt'],
            order: [["createdAt", "DESC"]] });
        const tokenData = await Mycase.findOne({
            where: { practice_id: event.user.practice_id, access_token: { [Op.not]: null } },
            attributes:['id','auth_code','access_token','refresh_token','createdAt'] ,
            order: [["createdAt", "DESC"]] });
        const clioDetails = await Clio.findOne({
            where: { practice_id: event.user.practice_id, access_token: { [Op.not]: null } },
            attributes:['id','auth_code','access_token','access_token_timestamp','refresh_token','createdAt'],
            order: [["createdAt", "DESC"]] });
        const users_role = ['lawyer', 'paralegal'];
        if (event.user.role && users_role.includes(event.user.role)) {
            if (tokenData?.id) user.mycaseDetails = tokenData.get({ plain: true });
            if (fileVinedetails?.id) user.filevineDetails = fileVinedetails.get({ plain: true });
            if (clioDetails?.id) user.clioDetails = clioDetails.get({ plain: true });
        }
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(user),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": false,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the Users.",
            }),
        };
    }
};
const getSample = async (event) => ({
    statusCode: 200, headers: {
        "Content-Type": "text/plain", "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Credentials": true,
    }, body: JSON.stringify({
        sample: true,
    }),
});
const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const { Users, Practices, Op } = await connectToDatabase();
        const user = await Users.findOne({
            where: { id: event.params.id, is_deleted: { [Op.not]: true } },
        });
        if (!user) throw new HTTPError(404, `User with id: ${params.id} was not found`);
        const userObj = await user.get({ plain: true });
        const practiceData = await Practices.findOne({
            where: { id: userObj.practice_id },
        });
        if (practiceData && "name" in practiceData) userObj.practice_name = practiceData.name;
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(userObj),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the Users.",
            }),
        };
    }
};
const getAll_new_pagination = async (event) => {
    try {
        const { Op, Users, Practices, PracticesTemplates } = await connectToDatabase();
        const urlParams = event.query || {};
        if (urlParams && urlParams.type) {
            if (urlParams.type == "admin") {
                return getAllPaginationAdmin(event);
            } else if (urlParams.type == "practice") {
                return getAllPaginationPractice(event);
            }
        }

        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.raw = true;
        query.where.is_deleted = { [Op.not]: true };
        const litigaitRoles = ["superAdmin", "manager", "operator", "medicalExpert", "QualityTechnician",];

        if (!litigaitRoles.includes(event.user.role)) {
            query.where.practice_id = event.user.practice_id;
            query.where.role = { [Op.notIn]: litigaitRoles };
        } else if (query.list_type == "practice_users") {
            query.where.role = { [Op.notIn]: litigaitRoles };
            if (query.practice_id) {
                query.where.practice_id = query.practice_id;
            }
            if (query.is_admin) {
                query.where.is_admin = query.is_admin;
            }
        } else {
            // query.where.role = { [Op.in]: litigaitRoles };
        }

        query.where.id = { [Op.ne]: event.user.id };

        if (event.user.role == "medicalExpert") {
            query.where.role = "medicalExpert";
            delete query.where.id;
        }

        if (event.user.role == "QualityTechnician") {
            query.where.role = "QualityTechnician";
            delete query.where.id;
        }
        query.order = [["createdAt", "DESC"]];
        query.is_deleted = { [Op.not]: true };
        query.attributes = ["id", "name", "email", "practice_id", "role", "is_email_verified", "is_admin", "last_login_ts", "archived", "state_bar_number",];
        validateReadUsers(query);
        const users = await Users.findAll(query);
        const usersCount = await Users.count({ where: { practice_id: event.user.practice_id } });
        // if (query.list_type == 'practice_users') {
        for (let i = 0; i < users.length; i += 1) {
            if (users[i].practice_id) {
                const practiceData = await Practices.findOne({
                    where: { id: users[i].practice_id },
                });
                const practiceTemplateData = await PracticesTemplates.findOne({
                    where: { practice_id: users[i].practice_id },
                });
                if (practiceTemplateData) {
                    users[i].practice_template_id = practiceTemplateData.id;
                    users[i].practice_template_custom_url = practiceTemplateData.custom_template;
                    users[i].practice_template_status = practiceTemplateData.status;
                }
                if (practiceData) {
                    users[i].practice_name = practiceData.name;
                }
            }
        }
        // }
        let totalPages, currentPage;
        if (query.limit && (users.length > 0)) {
            totalPages = Math.ceil(usersCount / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const usersDetails = {
            total_items: usersCount,
            response: users,
            total_pages: totalPages,
            current_page: currentPage
        };
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(usersDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the users.",
            }),
        };
    }
};
const getAllPaginationAdmin_new_pagination = async (event) => {
    try {
        const { Op, sequelize, Practices, PracticesTemplates } = await connectToDatabase();
        const urlParams = event.query || {};
        let searchKey = "";
        const filterKey = {};
        const sortKey = {};
        let filterQuery = "";
        let sortQuery = "";
        let searchQuery = "";
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let codeSnip = "";
        let codeSnip2 = "";
        let where = `WHERE Users.is_deleted IS NOT true `;

        let role = "'superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'";
        const litigaitRoles = ["superAdmin", "manager", "operator", "medicalExpert", "QualityTechnician",];
        if (urlParams.type == "admin") {
            if (litigaitRoles.includes(event.user.role)) {
                where += "AND Users.role IN (" + role + ")";
            } else if (query.list_type == "practice_users") {
                query.where.role = { [Op.notIn]: litigaitRoles };
                where += "AND Users.role IS NOT IN (" + role + ")";
                if (query.practice_id) {
                    let tempid = "'" + event.practice_id + "'";
                    where += "AND Users.practice_id = " + tempid;
                }
                if (query.is_admin) {
                    where += "AND Users.is_admin = " + query.is_admin;
                }
            }
        }
        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' AND Users.createdAt >= "' + event?.query?.from_date + '" AND Users.createdAt <= "' + event?.query?.to_date + '"';
        }
        if (event.user.role == "medicalExpert") {
            query.where.role = "medicalExpert";
            where += " AND Users.role = " + "'medicalExpert'";
        } else if (event.user.role == "QualityTechnician") {
            query.where.role = "QualityTechnician";
            where += " AND Users.role = " + "'QualityTechnician'";
        } else {
            let tempid = "'" + event.user.id + "'";
            where += " AND Users.id != " + tempid;
        }

        /**Sort**/
        if (query.sort == "false") {
            sortQuery += ` ORDER BY Users.createdAt DESC`;
        } else if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != "all" && sortKey.column != "" && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != "all" && sortKey.type != "" && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != "false" && searchKey.length >= 1 && searchKey != "") {
            searchQuery = ` Users.name LIKE '%${searchKey}%' OR Users.email LIKE '%${searchKey}%' OR Users.role LIKE '%${searchKey}%' `;
        }

        if (searchQuery != "" && sortQuery != "") {
            codeSnip = where + " AND (" + searchQuery + ")" + sortQuery;
            codeSnip2 = where + " AND (" + searchQuery + ")" + sortQuery;
        } else if (searchQuery == "" && sortQuery != "") {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != "" && sortQuery == "") {
            codeSnip = where + " AND (" + searchQuery + ")";
            codeSnip2 = where + " AND (" + searchQuery + ")";
        } else if (searchQuery == "" && sortQuery == "") {
            codeSnip = where;
            codeSnip2 = where;
        }

        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }
        let sqlQuery = "SELECT * FROM Users " + codeSnip;

        let sqlQueryCount = "SELECT * FROM Users " + codeSnip2;

        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT,
        });

        for (let i = 0; i < serverData.length; i += 1) {
            if (serverData[i].practice_id) {
                const practiceData = await Practices.findOne({
                    where: { id: serverData[i].practice_id }, raw: true,
                });
                const practiceTemplateData = await PracticesTemplates.findOne({
                    where: { practice_id: serverData[i].practice_id }, raw: true,
                });
                if (practiceTemplateData) {
                    serverData[i].practice_template_id = practiceTemplateData.id;
                    serverData[i].practice_template_custom_url = practiceTemplateData.custom_template;
                    serverData[i].practice_template_status = practiceTemplateData.status;
                }
                if (practiceData) {
                    serverData[i].practice_name = practiceData.name;
                }
            }
        }

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT,
        });

        let totalPages, currentPage;
        if (query.limit && (serverData.length > 0)) {
            totalPages = Math.ceil(TableDataCount.length / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const serverDetails = {
            total_items: TableDataCount.length,
            response: serverData,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Expose-Headers": "totalPageCount",
                "totalPageCount": TableDataCount.length,
            }, body: JSON.stringify(serverDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the Users.",
            }),
        };
    }
};
const getAllPaginationPractice_new_pagination = async (event) => {
    try {
        const { Op, sequelize, Practices, PracticesTemplates } = await connectToDatabase();
        const urlParams = event.query || {};
        let searchKey = "";
        const filterKey = {};
        const sortKey = {};
        let filterQuery = "";
        let sortQuery = "";
        let searchQuery = "";
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let codeSnip = "";
        let codeSnip2 = "";
        let where = `WHERE Users.is_deleted IS NOT true `;

        let role = `'superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'`;
        const litigaitRoles = ["superAdmin", "manager", "operator", "medicalExpert", "QualityTechnician",];
        if (urlParams.type == "practice") {
            if (litigaitRoles.includes(event.user.role)) {
                where += `AND Users.role NOT IN (${role})`;
            } else if (query.list_type == "practice_users") {
                where += ` AND Users.role NOT IN (${role})`;
                if (query.practice_id) {
                    where += ` AND Users.practice_id = '${event.practice_id}'`;
                }
                if (query.is_admin) {
                    where += " AND Users.is_admin = " + query.is_admin;
                }
            }
        }
        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' AND Users.createdAt >= "' + event?.query?.from_date + '" AND Users.createdAt <= "' + event?.query?.to_date + '"';
        }
        if (event.user.role == "medicalExpert") {
            query.where.role = "medicalExpert";
            where += " AND Users.role = " + "'medicalExpert'";
        } else if (event.user.role == "QualityTechnician") {
            query.where.role = "QualityTechnician";
            where += " AND Users.role = " + "'QualityTechnician'";
        } else {
            where += ` AND Users.id !='${event.user.id}'`;
        }

        /**Sort**/
        if (query.sort == "false") {
            sortQuery += ` ORDER BY Users.createdAt DESC`;
        } else if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != "all" && sortKey.column != "" && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != "all" && sortKey.type != "" && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != "false" && searchKey.length >= 1 && searchKey != "") {
            searchQuery = ` Users.name LIKE '%${searchKey}%' OR Users.email LIKE '%${searchKey}%' OR Users.role LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%'`;
        }

        if (searchQuery != "" && sortQuery != "") {
            codeSnip = where + " AND (" + searchQuery + ")" + sortQuery;
            codeSnip2 = where + " AND (" + searchQuery + ")" + sortQuery;
        } else if (searchQuery == "" && sortQuery != "") {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != "" && sortQuery == "") {
            codeSnip = where + " AND (" + searchQuery + ")";
            codeSnip2 = where + " AND (" + searchQuery + ")";
        } else if (searchQuery == "" && sortQuery == "") {
            codeSnip = where;
            codeSnip2 = where;
        }

        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let sqlQuery = "select Users.id, Users.name, Users.email, Users.practice_id, Users.role, Users.createdAt, Users.is_email_verified, " + "Users.is_admin, Users.last_login_ts, " + "Users.archived, Users.state_bar_number, Practices.name AS practice_name from Users LEFT JOIN Practices ON Users.practice_id = Practices.id " + codeSnip;
        let sqlQueryCount = "select Users.id, Users.name, Users.email, Users.practice_id, Users.role, Users.createdAt, Users.is_email_verified, " + "Users.is_admin, Users.last_login_ts, " + "Users.archived, Users.state_bar_number, Practices.name AS practice_name from Users LEFT JOIN Practices ON Users.practice_id = Practices.id " + codeSnip2;

        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT,
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT,
        });

        let totalPages, currentPage;
        if (query.limit && (serverData.length > 0)) {
            totalPages = Math.ceil(TableDataCount.length / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }
        const serverDetails = {
            total_items: TableDataCount.length,
            response: serverData,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Expose-Headers": "totalPageCount",
                "totalPageCount": TableDataCount.length,
            }, body: JSON.stringify(serverDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the Users.",
            }),
        };
    }
};

const getAll = async (event) => {
    try {
        const { Op, Users, Practices, PracticesTemplates } = await connectToDatabase();
        const urlParams = event.query || {};
        console.log(urlParams);
        console.log(urlParams.type);
        if (urlParams && urlParams.type) {
            if (urlParams.type == "admin") {
                return getAllPaginationAdmin(event);
            } else if (urlParams.type == "practice") {
                return getAllPaginationPractice(event);
            }
        }
        let header = event.headers;
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.raw = true;
        query.where.is_deleted = { [Op.not]: true };
        const litigaitRoles = ["superAdmin", "manager", "operator", "medicalExpert", "QualityTechnician",];

        if (!litigaitRoles.includes(event.user.role)) {
            query.where.practice_id = event.user.practice_id;
            query.where.role = { [Op.notIn]: litigaitRoles };
        } else if (query.list_type == "practice_users") {
            query.where.role = { [Op.notIn]: litigaitRoles };
            if (query.practice_id) {
                query.where.practice_id = query.practice_id;
            }
            if (query.is_admin) {
                query.where.is_admin = query.is_admin;
            }
        } else {
            // query.where.role = { [Op.in]: litigaitRoles };
        }

        query.where.id = { [Op.ne]: event.user.id };

        if (event.user.role == "medicalExpert") {
            query.where.role = "medicalExpert";
            delete query.where.id;
        }

        if (event.user.role == "QualityTechnician") {
            query.where.role = "QualityTechnician";
            delete query.where.id;
        }
        query.order = [["createdAt", "DESC"]];
        query.is_deleted = { [Op.not]: true };
        query.attributes = ["id", "name", "email", "practice_id", "role", "is_email_verified", "is_admin", "last_login_ts", "archived", "state_bar_number", "signature", "color_code"];
        validateReadUsers(query);
        const users = await Users.findAll(query);
        // if (query.list_type == 'practice_users') {
        for (let i = 0; i < users.length; i += 1) {
            if (users[i].practice_id) {
                const practiceData = await Practices.findOne({
                    where: { id: users[i].practice_id },
                });
                const practiceTemplateData = await PracticesTemplates.findOne({
                    where: { practice_id: users[i].practice_id },
                });
                if (practiceTemplateData) {
                    users[i].practice_template_id = practiceTemplateData.id;
                    users[i].practice_template_custom_url = practiceTemplateData.custom_template;
                    users[i].practice_template_status = practiceTemplateData.status;
                }
                if (practiceData) {
                    users[i].practice_name = practiceData.name;
                }
            }
        }
        // }
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(users),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the users.",
            }),
        };
    }
};
const getAllPaginationAdmin = async (event) => {
    try {
        const { Op, sequelize, Practices, PracticesTemplates } = await connectToDatabase();
        const urlParams = event.query || {};
        let searchKey = "";
        const filterKey = {};
        const sortKey = {};
        let filterQuery = "";
        let sortQuery = "";
        let searchQuery = "";
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let codeSnip = "";
        let codeSnip2 = "";
        let where = `WHERE Users.is_deleted IS NOT true `;

        let role = "'superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'";
        const litigaitRoles = ["superAdmin", "manager", "operator", "medicalExpert", "QualityTechnician",];
        if (urlParams.type == "admin") {
            if (litigaitRoles.includes(event.user.role)) {
                where += "AND Users.role IN (" + role + ")";
            } else if (query.list_type == "practice_users") {
                query.where.role = { [Op.notIn]: litigaitRoles };
                where += "AND Users.role IS NOT IN (" + role + ")";
                if (query.practice_id) {
                    let tempid = "'" + event.practice_id + "'";
                    where += "AND Users.practice_id = " + tempid;
                }
                if (query.is_admin) {
                    where += "AND Users.is_admin = " + query.is_admin;
                }
            }
        }
        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' AND Users.createdAt >= "' + event?.query?.from_date + '" AND Users.createdAt <= "' + event?.query?.to_date + '"';
        }
        if (event.user.role == "medicalExpert") {
            query.where.role = "medicalExpert";
            where += " AND Users.role = " + "'medicalExpert'";
        } else if (event.user.role == "QualityTechnician") {
            query.where.role = "QualityTechnician";
            where += " AND Users.role = " + "'QualityTechnician'";
        } else {
            let tempid = "'" + event.user.id + "'";
            where += " AND Users.id != " + tempid;
        }

        /**Sort**/
        if (query.sort == "false") {
            sortQuery += ` ORDER BY Users.createdAt DESC`;
        } else if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != "all" && sortKey.column != "" && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != "all" && sortKey.type != "" && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != "false" && searchKey.length >= 1 && searchKey != "") {
            searchQuery = ` Users.name LIKE '%${searchKey}%' OR Users.email LIKE '%${searchKey}%' OR Users.role LIKE '%${searchKey}%' `;
        }

        if (searchQuery != "" && sortQuery != "") {
            codeSnip = where + " AND (" + searchQuery + ")" + sortQuery;
            codeSnip2 = where + " AND (" + searchQuery + ")" + sortQuery;
        } else if (searchQuery == "" && sortQuery != "") {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != "" && sortQuery == "") {
            codeSnip = where + " AND (" + searchQuery + ")";
            codeSnip2 = where + " AND (" + searchQuery + ")";
        } else if (searchQuery == "" && sortQuery == "") {
            codeSnip = where;
            codeSnip2 = where;
        }

        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }
        let sqlQuery = "SELECT * FROM Users " + codeSnip;

        let sqlQueryCount = "SELECT * FROM Users " + codeSnip2;
        console.log(sqlQuery);
        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT,
        });

        for (let i = 0; i < serverData.length; i += 1) {
            if (serverData[i].practice_id) {
                const practiceData = await Practices.findOne({
                    where: { id: serverData[i].practice_id }, raw: true,
                });
                const practiceTemplateData = await PracticesTemplates.findOne({
                    where: { practice_id: serverData[i].practice_id }, raw: true,
                });
                if (practiceTemplateData) {
                    serverData[i].practice_template_id = practiceTemplateData.id;
                    serverData[i].practice_template_custom_url = practiceTemplateData.custom_template;
                    serverData[i].practice_template_status = practiceTemplateData.status;
                }
                if (practiceData) {
                    serverData[i].practice_name = practiceData.name;
                }
            }
        }

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT,
        });

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Expose-Headers": "totalPageCount",
                "totalPageCount": TableDataCount.length,
            }, body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the Users.",
            }),
        };
    }
};
const getAllPaginationPractice = async (event) => {
    try {
        const { Op, sequelize, Practices, PracticesTemplates } = await connectToDatabase();
        const urlParams = event.query || {};
        let searchKey = "";
        const filterKey = {};
        const sortKey = {};
        let filterQuery = "";
        let sortQuery = "";
        let searchQuery = "";
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let codeSnip = "";
        let codeSnip2 = "";
        let where = `WHERE Users.is_deleted IS NOT true `;

        let role = `'superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'`;
        const litigaitRoles = ["superAdmin", "manager", "operator", "medicalExpert", "QualityTechnician",];
        if (urlParams.type == "practice") {
            if (litigaitRoles.includes(event.user.role)) {
                where += `AND Users.role NOT IN (${role})`;
            } else if (query.list_type == "practice_users") {
                where += ` AND Users.role NOT IN (${role})`;
                if (query.practice_id) {
                    where += ` AND Users.practice_id = '${event.practice_id}'`;
                }
                if (query.is_admin) {
                    where += " AND Users.is_admin = " + query.is_admin;
                }
            }
        }
        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' AND Users.createdAt >= "' + event?.query?.from_date + '" AND Users.createdAt <= "' + event?.query?.to_date + '"';
        }
        if (event.user.role == "medicalExpert") {
            query.where.role = "medicalExpert";
            where += " AND Users.role = " + "'medicalExpert'";
        } else if (event.user.role == "QualityTechnician") {
            query.where.role = "QualityTechnician";
            where += " AND Users.role = " + "'QualityTechnician'";
        } else {
            where += ` AND Users.id !='${event.user.id}'`;
        }

        /**Sort**/
        if (query.sort == "false") {
            sortQuery += ` ORDER BY Users.createdAt DESC`;
        } else if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != "all" && sortKey.column != "" && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != "all" && sortKey.type != "" && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != "false" && searchKey.length >= 1 && searchKey != "") {
            searchQuery = ` Users.name LIKE '%${searchKey}%' OR Users.email LIKE '%${searchKey}%' OR Users.role LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%'`;
        }

        if (searchQuery != "" && sortQuery != "") {
            codeSnip = where + " AND (" + searchQuery + ")" + sortQuery;
            codeSnip2 = where + " AND (" + searchQuery + ")" + sortQuery;
        } else if (searchQuery == "" && sortQuery != "") {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != "" && sortQuery == "") {
            codeSnip = where + " AND (" + searchQuery + ")";
            codeSnip2 = where + " AND (" + searchQuery + ")";
        } else if (searchQuery == "" && sortQuery == "") {
            codeSnip = where;
            codeSnip2 = where;
        }

        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let sqlQuery = "select Users.id, Users.name, Users.email, Users.is_free_user, Users.color_code, Users.practice_id, Users.role, Users.createdAt, Users.is_email_verified, " + "Users.is_admin, Users.last_login_ts, " + "Users.archived, Users.state_bar_number, Practices.name AS practice_name from Users LEFT JOIN Practices ON Users.practice_id = Practices.id " + codeSnip;
        console.log(sqlQuery);
        let sqlQueryCount = "select Users.id, Users.name, Users.email, Users.is_free_user, Users.color_code, Users.practice_id, Users.role, Users.createdAt, Users.is_email_verified, " + "Users.is_admin, Users.last_login_ts, " + "Users.archived, Users.state_bar_number, Practices.name AS practice_name from Users LEFT JOIN Practices ON Users.practice_id = Practices.id " + codeSnip2;

        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT,
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT,
        });

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Expose-Headers": "totalPageCount",
                "totalPageCount": TableDataCount.length,
            }, body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the Users.",
            }),
        };
    }
};
const getAllPaginationPractice_new = async (event) => {
    try {
        const { Op, sequelize, Practices, PracticesTemplates } = await connectToDatabase();
        const urlParams = event.query || {};
        let searchKey = "";
        const filterKey = {};
        const sortKey = {};
        let filterQuery = "";
        let sortQuery = "";
        let searchQuery = "";
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let codeSnip = "";
        let codeSnip2 = "";
        let where = `WHERE is_deleted IS NOT true `;

        let role = `'superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'`;
        const litigaitRoles = ["superAdmin", "manager", "operator", "medicalExpert", "QualityTechnician",];
        if (urlParams.type == "practice") {
            if (litigaitRoles.includes(event.user.role)) {
                where += `AND role NOT IN (${role})`;
            } else if (query.list_type == "practice_users") {
                where += ` AND role NOT IN (${role})`;
                if (query.practice_id) {
                    where += ` AND practice_id = '${event.practice_id}'`;
                }
                if (query.is_admin) {
                    where += " AND is_admin = " + query.is_admin;
                }
            }
        }

        if (event.user.role == "medicalExpert") {
            query.where.role = "medicalExpert";
            where += " AND role = " + "'medicalExpert'";
        } else if (event.user.role == "QualityTechnician") {
            query.where.role = "QualityTechnician";
            where += " AND role = " + "'QualityTechnician'";
        } else {
            where += ` AND id !='${event.user.id}'`;
        }
        /**Filter**/
        if (query.filter != 'false') {
            query.filter = JSON.parse(query.filter);
            filterKey.type = query.filter.plan_type;
            if (filterKey.type != 'all' && filterKey.type != '' && filterKey.type) {
                filterQuery += ` AND plan_type LIKE '%${filterKey.type}%'`;
            }
        }

        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` group by id ORDER BY createdAt DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` group by id ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' AND createdAt >= "' + event?.query?.from_date + '" AND createdAt <= "' + event?.query?.to_date + '"';
        }

        /**Search**/
        if (searchKey != "false" && searchKey.length >= 1 && searchKey != "") {
            searchQuery = ` name LIKE '%${searchKey}%' OR email LIKE '%${searchKey}%' OR role LIKE '%${searchKey}%' OR practice_name LIKE '%${searchKey}%' OR plan_name LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + sortQuery;
            codeSnip2 = where + filterQuery + sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')';
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery != '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')';
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery;
            codeSnip2 = where + filterQuery;
        }

        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let get_active_subscription_query = `select Users.id as id, Users.name as name, Users.email as email, Users.role as role, Users.is_admin as is_admin, Users.createdAt as createdAt,
        Practices.name as practice_name, group_concat(Plans.name separator ', ') AS plan_name, group_concat(Plans.plan_type separator ', ') as plan_type,
        Users.last_login_ts as last_login_ts, Users.is_deleted as is_deleted from Users
        inner join Practices on Users.practice_id = Practices.id
        inner join Subscriptions ON Users.practice_id = Subscriptions.practice_id 
        inner join Plans on Subscriptions.plan_id = Plans.plan_id 
        where Users.is_deleted IS NOT true AND Practices.is_deleted IS NOT TRUE AND Users.practice_id IS NOT NULL group by id `;

        let get_tek_as_you_go_query = ` select Users.id as id, Users.name as name, Users.email as email, Users.role as role, Users.is_admin as is_admin, Users.createdAt as createdAt,
        Practices.name as practice_name, 'TEK AS-YOU-GO' as plan_name, 'pay_as_you_go' AS plan_type, Users.last_login_ts as last_login_ts, Users.is_deleted as is_deleted from Users
        inner join Practices on Users.practice_id = Practices.id
        where Users.is_deleted IS NOT true AND Practices.is_deleted IS NOT TRUE AND Users.practice_id IS NOT NULL and Users.practice_id not in (select practice_id from Subscriptions) group by id `;

        let get_canceled_subscription_query = ` select Users.id as id, Users.name as name, Users.email as email, Users.role as role, Users.is_admin as is_admin, Users.createdAt as createdAt,
        Practices.name as practice_name, 'Subscription Canceled' as plan_name, 'canceled_subscription' AS plan_type, Users.last_login_ts as last_login_ts, Users.is_deleted as is_deleted from Users
        inner join Practices on Users.practice_id = Practices.id
        where Users.is_deleted IS NOT true AND Practices.is_deleted IS NOT TRUE AND Users.practice_id IS NOT NULL and Users.practice_id in (select practice_id from Subscriptions where stripe_subscription_id is null and plan_id is null) group by id `;

        let combine_query;

        if (query?.filter == 'false' || !query?.filter?.plan_type || query?.filter?.plan_type == 'all' || query?.filter?.plan_type == '') {
            combine_query = get_active_subscription_query + 'union' + get_tek_as_you_go_query + 'union' + get_canceled_subscription_query;
        } else if (query?.filter != 'false' && query?.filter?.plan_type != '' && !['all', 'pay_as_you_go', 'canceled_subscription'].includes(query?.filter?.plan_type)) {
            combine_query = get_active_subscription_query;
        } else if (query?.filter != 'false' && query?.filter?.plan_type == 'pay_as_you_go') {
            combine_query = get_tek_as_you_go_query;
        } else if (query?.filter != 'false' && query?.filter?.plan_type == 'canceled_subscription') {
            combine_query = get_canceled_subscription_query;
        }


        let sqlQuery = 'select id,name,email,role,is_admin,createdAt,practice_name,plan_name, plan_type, ' +
            'last_login_ts from (' + combine_query + ') as user_details ' + codeSnip;
        console.log(sqlQuery);
        let sqlQueryCount = 'select id,name,email,role,is_admin,createdAt,practice_name,plan_name, plan_type, ' +
            'last_login_ts from (' + combine_query + ') as user_details ' + codeSnip;

        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT,
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT,
        });

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Expose-Headers": "totalPageCount",
                "totalPageCount": TableDataCount.length,
            }, body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the Users.",
            }),
        };
    }
};
const update = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        validateUpdateUser(input);
        const { Users, PasswordHistory, Op, Practices, Subscriptions, Plans } = await connectToDatabase();

        const userObject = await Users.findOne({
            where: { id: params.id, is_deleted: { [Op.not]: true } },
        });
        if (!userObject) throw new HTTPError(404, `User with id: ${params.id} was not found`);

        if (userObject?.practice_id) {
            const practiceObj = await Practices.findOne({ where: { id: userObject.practice_id }, raw: true });
            if (practiceObj?.billing_type == 'limited_users_billing') {
                const users_count = await Users.count({
                    where: { practice_id: userObject.practice_id, is_free_user: false, is_deleted: { [Op.not]: true } }
                });
                const subscriptionObj = await Subscriptions.findOne({
                    where: { practice_id: userObject.practice_id, plan_category: 'responding' },
                    order: [["createdAt", "DESC"]],
                });
                const plansObj = await Plans.findOne({ where: { plan_id: subscriptionObj.plan_id }, raw: true });
                if (subscriptionObj?.plan_id && subscriptionObj.plan_id != 'free_trial' && !input?.is_free_user) {
                    if (users_count == practiceObj?.license_count &&
                        (respondingMonthlyPlans.includes(plansObj?.plan_type) || respondingYearlyPlans.includes(plansObj?.plan_type)) &&
                        (userObject?.is_free_user != input.is_free_user)) {
                        throw new HTTPError(400, 'Your account has reached the maximum number of allowed users. Please purchase licenses for new users.');
                    }
                } else if (subscriptionObj?.plan_id === 'free_trial' && !input?.is_free_user) {
                    await Practices.increment('license_count', { by: 1, where: { id: userObject.practice_id } });
                }
            }
        }

        // let is_password_changed = input.change_password;
        if (input.password) {
            if (await userObject.checkIfLast6Password(input.password, PasswordHistory)) throw new HTTPError(401, "Can not used any last 6 past password");
            input.password = await userObject.generateNewPassword(input.password);
        } else {
            input.password = userObject.password;
        }

        const updatedModel = Object.assign(userObject, input);
        const updateData = await updatedModel.save();
        const updatedDetails = updateData.get({ plain: true });
        const userObjs = await Users.findAll({
            where: { id: { [Op.not]: params.id }, email: userObject.email, is_deleted: { [Op.not]: true }, }, raw: true
        });

        updatedDetails.user_password_exist = false;
        for (let i = 0; i < userObjs.length; i++) {
            const userData = await Users.findOne({
                where: { id: userObjs[i].id, practice_id: userObjs[i].practice_id, email: userObject.email, is_deleted: { [Op.not]: true }, }
            });

            if (!userData) throw new HTTPError(400, 'user not found for a practice');
            updatedDetails.user_password_exist = true;
            userData.password = userObject.password;
            await userData.save();
        }

        await PasswordHistory.create({
            email: updatedModel.email, password: updatedModel.password, id: uuid.v4(),
        });
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not update the Users.",
            }),
        };
    }
};

const updateAll = async (event) => {
    try {
        const { Users, Op } = await connectToDatabase();
        const condition = event.body.condition || {};
        const updateParams = event.body.updateParams;
        condition.is_deleted = { [Op.not]: true };
        const result = Users.update(updateParams, { where: condition });
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(result),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the users.",
            }),
        };
    }
};
const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const { Users, PasswordHistory, Practices, Op, Subscriptions, PracticeSettings, Settings } = await connectToDatabase();
        const users = await Users.findOne({ where: { id: params.id } });
        if (!users) throw new HTTPError(404, `User with id: ${params.id} was not found`);
        const practice_obj = await Practices.findOne({ where: { id: users.practice_id } });

        /* Getting free user limit from Global settings */
        const global_settings_Obj = await Settings.findOne({ where: { key: 'global_settings' }, raw: true });
        const settings_value = JSON.parse(global_settings_Obj.value);
        if (!settings_value?.free_user_limit) throw new HTTPError(400, 'Users limit not found.please contact support.');
        /* Getting free user limit from Practice settings */
        if (practice_obj) {
            const practice_settings_obj = await PracticeSettings.findOne({ where: { practice_id: practice_obj.id }, raw: true });
            const Practice_settings_value = practice_settings_obj?.value ? JSON.parse(practice_settings_obj.value) : {};
            const free_user_limit = Practice_settings_value?.free_user_limit ? Practice_settings_value?.free_user_limit : settings_value?.free_user_limit;


            if (users.created_from == 'customer_admin' && practice_obj?.billing_type == 'limited_users_billing') {
                /* Get Active Responding Subscription */
                const subscriptionObj = await Subscriptions.findOne({
                    where: {
                        practice_id: users.practice_id, plan_category: 'responding', stripe_product_id: { [Op.not]: null },
                        plan_id: { [Op.not]: null }, stripe_subscription_id: { [Op.not]: null }
                    },
                    raw: true
                });
                if (subscriptionObj?.stripe_subscription_data) {
                    const stripe_subscription_data = JSON.parse(subscriptionObj.stripe_subscription_data);
                    const subscription_history = stripe_subscription_data?.items?.data || [];
                    if (subscription_history[2]?.quantity > 0) await stripe.subscriptions.update(subscriptionObj.stripe_subscription_id, {
                        items: [{
                            id: subscription_history[2].id,
                            quantity: subscription_history[2].quantity - 1, // Set the new quantity here
                        }],
                    });
                }
                /* Get propounding Subscription */
                const subscriptionPropoundingObj = await Subscriptions.findOne({
                    where: {
                        practice_id: users.practice_id, plan_category: 'propounding', stripe_product_id: { [Op.not]: null },
                        plan_id: { [Op.not]: null }, stripe_subscription_id: { [Op.not]: null }
                    },
                    raw: true
                });
                const propoundingStripeObj = subscriptionPropoundingObj?.stripe_subscription_data ? JSON.parse(subscriptionPropoundingObj.stripe_subscription_data) : {};
                if (propoundingStripeObj?.id && propoundingStripeObj?.id.startsWith('sub_')) {
                    const propounding_subscription_history = propoundingStripeObj?.items?.data || [];
                    if (propounding_subscription_history[2]?.quantity > 0) await stripe.subscriptions.update(subscriptionPropoundingObj.stripe_subscription_id, {
                        items: [{
                            id: propounding_subscription_history[2].id,
                            quantity: propounding_subscription_history[2].quantity - 1, // Set the new quantity here
                        }],
                    });
                }
            }
            // if (practice_obj.license_count > free_user_limit) {
            //     practice_obj.license_count = parseInt(practice_obj.license_count) - 1;
            //     await practice_obj.save();
            // }
        }
        users.is_deleted = true;
        await users.save();
        await Practices.decrement('no_of_users', {
            by: 1,
            where: { id: practice_obj.id, is_deleted: { [Op.not]: true } }
        });

        await PasswordHistory.destroy({ where: { user_id: users.id } });
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(users),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could destroy fetch the Users.",
            }),
        };
    }
};
const forgetPassword = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateForgetPassword(input);

        const email = input.email;

        const { Users, Op } = await connectToDatabase();

        const userObject = await Users.findOne({
            where: {
                email, is_deleted: { [Op.not]: true },
            },
        });

        if (!userObject) throw new HTTPError(404, "Couldn't find your EsquireTek account");

        const tokenUser = {
            id: userObject.id, role: userObject.role,
        };
        const token = jwt.sign(tokenUser, process.env.JWT_SECRET, {
            expiresIn: 60 * 90,
        });
        let link = `<a href="${process.env.APP_URL}/reset-password/?email=${userObject.email}&token=${token}&expireIn=90">link</a>`;
        await sendEmail(userObject.email, "Important: Reset your password", `This email is in response to your request to reset password. If you have not requested to reset password, please ignore this email. <br/><br/>
    Reset your password by clicking on the ${link}.`, "EsquireTek");
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                status: "ok", message: "Password reset instructions sent to your email.",
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not create the users.",
            }),
        };
    }
};
const resetPassword = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateResetPassword(input);
        const { Users, PasswordHistory, Op } = await connectToDatabase();
        /* const userObject = await Users.findOne({where:{id:event.user.id); }}*/
        const userObject = await Users.findOne({
            where: { is_deleted: { [Op.not]: true }, id: event.user.id },
        });
        if (!userObject) throw new HTTPError(400, "Users not found");
        if (await userObject.checkIfLast6Password(input.new_password, PasswordHistory)) throw new HTTPError(401, "Can not use any last 6 past password");
        userObject.password = await userObject.generateNewPassword(input.new_password);
        userObject.login_attempts = 0;
        userObject.reactivation_token = null;
        await userObject.save();

        const userObjs = await Users.findAll({
            where: {
                id: { [Op.not]: event.user.id },
                email: userObject.email,
                is_deleted: { [Op.not]: true },
            }, raw: true
        });
        let user_password_exist = false;
        for (let i = 0; i < userObjs.length; i++) {
            const userData = await Users.findOne({
                where: {
                    id: userObjs[i].id,
                    practice_id: userObjs[i].practice_id,
                    email: userObject.email,
                    is_deleted: { [Op.not]: true },
                }
            });

            if (!userData) throw new HTTPError(400, 'user not found for a practice');
            user_password_exist = true;
            userData.password = userObject.password;
            await userData.save();
        }

        await PasswordHistory.create({
            email: userObject.email, password: userObject.password, id: uuid.v4(),
        });

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                status: "ok",
                message: "Password Changed",
                user_password_exist
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not update the Users.",
            }),
        };
    }
};
const resetPasswordOld = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateResetPassword(input);
        const { Users, PasswordHistory, Op } = await connectToDatabase();
        /* const userObject = await Users.findOne({where:{id:event.user.id); }}*/
        const userObject = await Users.findOne({
            where: { is_deleted: { [Op.not]: true }, id: event.user.id },
        });
        if (!userObject) throw new HTTPError(400, "Users not found");
        if (await userObject.checkIfLast6Password(input.new_password, PasswordHistory)) throw new HTTPError(401, "Can not use any last 6 past password");
        userObject.password = await userObject.generateNewPassword(input.new_password);
        userObject.login_attempts = 0;
        userObject.reactivation_token = null;
        await userObject.save();
        await PasswordHistory.create({
            email: userObject.email, password: userObject.password, id: uuid.v4(),
        });

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                status: "ok", message: "Password Changed",
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not update the Users.",
            }),
        };
    }
};
const changePassword = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        validateResetPassword(input);
        const { Users, PasswordHistory, Op } = await connectToDatabase();
        /* const userObject = await Users.findOne({where:{id:event.user.id); }}*/
        const userObject = await Users.findOne({
            where: { is_deleted: { [Op.not]: true }, id: event.user.id },
        });
        if (!userObject) throw new HTTPError(400, "Users not found");
        if (await userObject.checkIfLast6Password(input.new_password, PasswordHistory)) throw new HTTPError(401, "Can not use any last 6 past password");
        userObject.password = await userObject.generateNewPassword(input.new_password);
        userObject.temporary_password = false;
        await userObject.save();

        let user_password_exist = false;
        const userObjs = await Users.findAll({
            where: {
                id: { [Op.not]: userObject.id },
                email: userObject.email,
                is_deleted: { [Op.not]: true },
            }, raw: true
        });
        for (let i = 0; i < userObjs.length; i++) {
            const userData = await Users.findOne({
                where: {
                    id: userObjs[i].id,
                    practice_id: userObjs[i].practice_id,
                    email: userObject.email,
                    is_deleted: { [Op.not]: true },
                }
            });


            if (!userData) throw new HTTPError(400, 'user not found for a practice');
            user_password_exist = true;
            userData.password = userObject.password;
            await userData.save();
            console.log(userData);
        }

        await PasswordHistory.create({
            email: userObject.email, password: userObject.password, id: uuid.v4(),
        });
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                status: "ok",
                message: "Password Changed",
                user_password_exist
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not update the Users.",
            }),
        };
    }
};
const changePasswordOld = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        validateResetPassword(input);
        const { Users, PasswordHistory, Op } = await connectToDatabase();
        /* const userObject = await Users.findOne({where:{id:event.user.id); }}*/
        const userObject = await Users.findOne({
            where: { is_deleted: { [Op.not]: true }, id: event.user.id },
        });
        if (!userObject) throw new HTTPError(400, "Users not found");
        if (await userObject.checkIfLast6Password(input.new_password, PasswordHistory)) throw new HTTPError(401, "Can not use any last 6 past password");
        userObject.password = await userObject.generateNewPassword(input.new_password);
        userObject.temporary_password = false;
        await userObject.save();
        await PasswordHistory.create({
            email: userObject.email, password: userObject.password, id: uuid.v4(),
        });
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                status: "ok", message: "Password Changed",
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not update the Users.",
            }),
        };
    }
};
const resetLoginAttempt = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { Users, Op } = await connectToDatabase();
        const userObject = await Users.findOne({
            where: {
                email: input.email, is_deleted: { [Op.not]: true },
            },
        });
        if (!userObject) throw new HTTPError(400, "Users not found");
        userObject.login_attempts = 0;
        await userObject.save();
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                status: "ok", message: "Login Attempt reset",
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not update the Users.",
            }),
        };
    }
};
const twofactorSetting = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        validateTwofactor(input);
        const { Users, Op } = await connectToDatabase();
        const id = event.user.id;
        const userObject = await Users.findOne({
            where: { id: id, is_deleted: { [Op.not]: true } },
        });
        if (userObject) {
            if (userObject.is_deleted === null || userObject.is_deleted === 0) {
                let field = {
                    twofactor_status: input.twofactor,
                };

                const result = await Users.update(field, {
                    where: { id: id, is_deleted: { [Op.not]: true } },
                });
                let res;
                if (input.twofactor == true) {
                    userObject.twofactor_status = true;
                } else {
                    userObject.twofactor_status = false;
                }
                await userObject.save();
                if (result[0] == 1) {
                    res = true;
                } else {
                    res = false;
                }
                return {
                    statusCode: 200, headers: {
                        "Content-Type": "text/plain",
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true,
                    }, body: JSON.stringify({
                        message: "Two Factor Authentication Settings Updated",
                    }),
                };
            }
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not update the Users.",
            }),
        };
    }
};
const checkSubscriptionValidation = async (event) => {
    try {
        const params = event.user;
        const body = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { Subscriptions } = await connectToDatabase();
        const query = {};
        query.raw = true;
        if (!query.where) {
            query.where = {};
        }

        query.where.practice_id = params.practice_id;
        query.order = [["createdAt", "DESC"]];
        query.logging = console.log;
        //  subscriptionModelCheck = await Subscriptions.findOne(query);
        const subscriptionObj = await Subscriptions.findOne(query);
        if (subscriptionObj && subscriptionObj.plan_id !== "free_trial") {
            const stripe_subscription_data = JSON.parse(subscriptionObj.stripe_subscription_data);
            const current_date = new Date();
            const current_period_end = unixTimetoDateconvert(stripe_subscription_data.current_period_end);
            if ((current_period_end <= current_date && stripe_subscription_data.canceled_at && stripe_subscription_data.status === "active" && (stripe_subscription_data.cancel_at_period_end || !stripe_subscription_data.cancel_at_period_end)) || (stripe_subscription_data.canceled_at && stripe_subscription_data.status === "canceled")) {
                let updateColumn = { stripe_product_id: null, stripe_subscription_id: null, subscribed_on: null, plan_id: null, subscribed_valid_till: null }
                await Subscriptions.update(updateColumn, { where: { practice_id: user.practice_id, id: subscriptionObj.id } });

                return {
                    statusCode: 200, headers: {
                        "Content-Type": "text/plain",
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true,
                    }, body: JSON.stringify({
                        status: "ok", message: "Previous subscription expired.removed existing subscriptions.",
                    }),
                };
            }
        }

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                status: "ok", message: "Successfully processed the request",
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not check Supscription details.",
            }),
        };
    }
};
const unixTimetoDateconvert = (timestamp) => {
    const unixTimestamp = timestamp;
    const date = new Date(unixTimestamp * 1000);
    return date;
};
const sessionTokenGenerate = async (event) => {
    try {
        const { Users, Settings } = await connectToDatabase();
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const userId = input.id;
        const userObject = await Users.findOne({ where: { id: userId } });
        const userPlain = userObject.get({ plain: true });

        const tokenUser = {
            id: userPlain.id, role: userPlain.role,
        };
        const token = jwt.sign(tokenUser, process.env.JWT_SECRET, {
            expiresIn: process.env.JWT_EXPIRATION_TIME,
        });
        const authToken = `JWT ${token}`;
        const ciphertext = CryptoJS.AES.encrypt(authToken, userId).toString();
        const responseData = {
            user: {
                name: userPlain.name, role: userPlain.role,
            },
        };
        responseData.authToken = ciphertext;
        const settingsObject = await Settings.findOne({
            where: { key: "global_settings" },
        });
        if (settingsObject) {
            const plainSettingsObject = settingsObject.get({ plain: true });
            const settings = JSON.parse(plainSettingsObject.value);
            responseData.session_timeout = settings.session_timeout;
        }

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(responseData),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Cannot Generate session Token.",
            }),
        };
    }
};
const updateSignature = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        // validateSignature(input);
        const { Users, Op } = await connectToDatabase();
        const userObject = await Users.findOne({
            where: { is_deleted: { [Op.not]: true }, id: event.user.id },
        });
        if (!userObject) throw new HTTPError(400, "Users not found");

        userObject.signature = input.signature;
        await userObject.save();
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                status: "ok", message: "Signature Updated",
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not update the Users.",
            }),
        };
    }
};
const updateQCNotificationStatus = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const { Users, Op } = await connectToDatabase();
        const usersDetails = await Users.findOne({ id: params.id });
        if (!usersDetails) { throw new HTTPError(400, "Details not found for this id") };
        await Users.update({ quick_create_notification: false }, { where: { id: params.id } });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Notification status updated successfully',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not notification details' }),
        };
    }
}
const getAllUsersForDashboard_new_pagination = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();

        let sortQuery = '';
        let searchQuery = '';
        let codeSnip = '';
        let codeSnip2 = '';

        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        let where = `WHERE Users.is_deleted IS NOT true AND Users.practice_id IS NOT NULL `;

        // let last_login_ts = event?.query?.filter_type == 'active' ? customDateandTime(event.query.filter):'';

        let last_login_ts;
        if (event?.query?.filter) {
            last_login_ts = customDateandTime(event.query.filter);
        } else if (!event?.query?.filter && event?.query?.filter_type == 'active' && event?.query?.from_date && event?.query?.to_date) {
            last_login_ts = {};
            last_login_ts.from_date = event.query.from_date;
            last_login_ts.to_date = event.query.to_date;
        }
        if (event?.query?.filter) {
            let dates = customDateandTime(event.query.filter);
            where += ' AND Users.createdAt >= "' + dates.from_date + '" AND Users.createdAt <= "' + dates.to_date + '"';
        }
        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' AND Users.createdAt >= "' + event?.query?.from_date + '" AND Users.createdAt <= "' + event?.query?.to_date + '"';
        }
        if (event?.query?.filter_type == 'active') {
            where += ' AND Users.last_login_ts >= "' + last_login_ts.from_date + '" AND Users.last_login_ts <= "' + last_login_ts.to_date + '"';
        }



        /**Sort**/
        if (query.sort == "false") {
            sortQuery += ` ORDER BY Users.createdAt DESC`;
        } else if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            if (query?.sort?.column != "all" && query?.sort?.column) {
                sortQuery += ` ORDER BY ${query?.sort?.column}`;
            }
            if (query?.sort?.type != "all" && query?.sort?.type) {
                sortQuery += ` ${query?.sort?.type}`;
            }
        }

        /**Search**/
        if (query?.search != "false" && query?.search) {
            searchQuery = ` Users.name LIKE '%${query?.search}%' OR Practices.name LIKE '%${query?.search}%' OR Users.email LIKE '%${query?.search}%' OR Users.role LIKE '%${query?.search}%' `;
        }

        if (searchQuery != "" && sortQuery != "") {
            codeSnip = where + " AND (" + searchQuery + ")" + sortQuery;
            codeSnip2 = where + " AND (" + searchQuery + ")" + sortQuery;
        } else if (searchQuery == "" && sortQuery != "") {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != "" && sortQuery == "") {
            codeSnip = where + " AND (" + searchQuery + ")";
            codeSnip2 = where + " AND (" + searchQuery + ")";
        }

        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let Query = "SELECT Practices.name as practice_name ," +
            "Users.* FROM Users INNER JOIN Practices ON Users.practice_id = Practices.id ";
        let sqlQuery = Query + codeSnip;
        let sqlQueryCount = Query + codeSnip2;

        const serverData = await sequelize.query(sqlQuery, { type: QueryTypes.SELECT, });

        const TableDataCount = await sequelize.query(sqlQueryCount, { type: QueryTypes.SELECT, });

        let totalPages, currentPage;
        if (query.limit && (serverData.length > 0)) {
            totalPages = Math.ceil(TableDataCount.length / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const serverDetails = {
            total_items: TableDataCount.length,
            response: serverData,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Expose-Headers": "totalPageCount",
                "totalPageCount": TableDataCount.length,
            }, body: JSON.stringify(serverDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the Users.",
            }),
        };
    }
};

const getAllUsersForDashboard = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();

        let sortQuery = '';
        let searchQuery = '';
        let codeSnip = '';
        let codeSnip2 = '';

        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        let where = `WHERE Users.is_deleted IS NOT true AND Users.practice_id IS NOT NULL `;

        // let last_login_ts = event?.query?.filter_type == 'active' ? customDateandTime(event.query.filter):'';

        let last_login_ts;
        if (event?.query?.filter) {
            last_login_ts = customDateandTime(event.query.filter);
        } else if (!event?.query?.filter && event?.query?.filter_type == 'active' && event?.query?.from_date && event?.query?.to_date) {
            last_login_ts = {};
            last_login_ts.from_date = event.query.from_date;
            last_login_ts.to_date = event.query.to_date;
        }
        if (event?.query?.filter) {
            let dates = customDateandTime(event.query.filter);
            where += ' AND Users.createdAt >= "' + dates.from_date + '" AND Users.createdAt <= "' + dates.to_date + '"';
        }
        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' AND Users.createdAt >= "' + event?.query?.from_date + '" AND Users.createdAt <= "' + event?.query?.to_date + '"';
        }
        if (event?.query?.filter_type == 'active') {
            where += ' AND Users.last_login_ts >= "' + last_login_ts.from_date + '" AND Users.last_login_ts <= "' + last_login_ts.to_date + '"';
        }



        /**Sort**/
        if (query.sort == "false") {
            sortQuery += ` ORDER BY Users.createdAt DESC`;
        } else if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            if (query?.sort?.column != "all" && query?.sort?.column) {
                sortQuery += ` ORDER BY ${query?.sort?.column}`;
            }
            if (query?.sort?.type != "all" && query?.sort?.type) {
                sortQuery += ` ${query?.sort?.type}`;
            }
        }

        /**Search**/
        if (query?.search != "false" && query?.search) {
            searchQuery = ` Users.name LIKE '%${query?.search}%' OR Practices.name LIKE '%${query?.search}%' OR Users.email LIKE '%${query?.search}%' OR Users.role LIKE '%${query?.search}%' `;
        }

        if (searchQuery != "" && sortQuery != "") {
            codeSnip = where + " AND (" + searchQuery + ")" + sortQuery;
            codeSnip2 = where + " AND (" + searchQuery + ")" + sortQuery;
        } else if (searchQuery == "" && sortQuery != "") {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != "" && sortQuery == "") {
            codeSnip = where + " AND (" + searchQuery + ")";
            codeSnip2 = where + " AND (" + searchQuery + ")";
        }

        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let Query = "SELECT Practices.name as practice_name ," +
            "Users.* FROM Users INNER JOIN Practices ON Users.practice_id = Practices.id ";
        let sqlQuery = Query + codeSnip;
        let sqlQueryCount = Query + codeSnip2;

        console.log(sqlQuery);

        const serverData = await sequelize.query(sqlQuery, { type: QueryTypes.SELECT, });

        const TableDataCount = await sequelize.query(sqlQueryCount, { type: QueryTypes.SELECT, });

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Expose-Headers": "totalPageCount",
                "totalPageCount": TableDataCount.length,
            }, body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the Users.",
            }),
        };
    }
};
const getAllUsersForDashboard_new = async (event) => {
    try {
        console.log('********* Inside dashboard **********');
        const { sequelize } = await connectToDatabase();
        let searchKey = '';
        let sortQuery = '';
        let searchQuery = '';
        let codeSnip = '';
        let codeSnip2 = '';
        let users_where = ' ';
        let filterQuery = '';

        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let where = `WHERE Users.is_deleted IS NOT true`;

        // let last_login_ts = event?.query?.filter_type == 'active' ? customDateandTime(event.query.filter):'';

        let last_login_ts;
        if (event?.query?.filter) {
            last_login_ts = customDateandTime(event.query.filter);
        } else if (!event?.query?.filter && event?.query?.filter_type == 'active' && event?.query?.from_date && event?.query?.to_date) {
            last_login_ts = {};
            last_login_ts.from_date = event.query.from_date;
            last_login_ts.to_date = event.query.to_date;
        }
        if (event?.query?.filter) {
            let dates = customDateandTime(event.query.filter);
            where += ' AND createdAt >= "' + dates.from_date + '" AND createdAt <= "' + dates.to_date + '"';
        }
        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' AND createdAt >= "' + event?.query?.from_date + '" AND createdAt <= "' + event?.query?.to_date + '"';
        }
        if (event?.query?.filter_type == 'active' && last_login_ts?.from_date && last_login_ts?.to_date) {
            users_where += ' AND Users.last_login_ts >= "' + last_login_ts.from_date + '" AND Users.last_login_ts <= "' + last_login_ts.to_date + '" ';
        }



        /**Sort**/
        if (query.sort == "false") {
            sortQuery += ` group by id ORDER BY DESC`;
        } else if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            if (query?.sort?.column != "all" && query?.sort?.column) {
                sortQuery += ` group by id ORDER BY ${query?.sort?.column}`;
            }
            if (query?.sort?.type != "all" && query?.sort?.type) {
                sortQuery += ` ${query?.sort?.type}`;
            }
        }

        /**Search**/
        if (searchKey != "false" && searchKey.length >= 1 && searchKey != "") {
            searchQuery = ` name LIKE '%${searchKey}%' OR email LIKE '%${searchKey}%' OR role LIKE '%${searchKey}%' OR practice_name LIKE '%${searchKey}%' OR plan_name LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + sortQuery;
            codeSnip2 = where + filterQuery + sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')';
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery != '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')';
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery;
            codeSnip2 = where + filterQuery;
        }

        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let get_active_subscription_query = `select Users.id as id, Users.name as name, Users.email as email, Users.role as role, Users.is_admin as is_admin, Users.createdAt as createdAt, ` +
            `Practices.name as practice_name, group_concat(Plans.name separator ', ') AS plan_name, group_concat(Plans.plan_type separator ', ') as plan_type, ` +
            `Users.last_login_ts as last_login_ts, Users.is_deleted as is_deleted from Users ` +
            `inner join Practices on Users.practice_id = Practices.id ` +
            `inner join Subscriptions ON Users.practice_id = Subscriptions.practice_id ` +
            `inner join Plans on Subscriptions.plan_id = Plans.plan_id ` +
            `where Users.is_deleted IS NOT true AND Practices.is_deleted IS NOT TRUE${users_where}AND Users.practice_id IS NOT NULL group by id `;

        let get_tek_as_you_go_query = ` select Users.id as id, Users.name as name, Users.email as email, Users.role as role, Users.is_admin as is_admin, Users.createdAt as createdAt, ` +
            `Practices.name as practice_name, 'TEK AS-YOU-GO' as plan_name, 'pay_as_you_go' AS plan_type, Users.last_login_ts as last_login_ts, Users.is_deleted as is_deleted from Users ` +
            `inner join Practices on Users.practice_id = Practices.id ` +
            `where Users.is_deleted IS NOT true AND Practices.is_deleted IS NOT TRUE${users_where}AND Users.practice_id IS NOT NULL and Users.practice_id not in (select practice_id from Subscriptions) group by id `;

        let get_canceled_subscription_query = ` select Users.id as id, Users.name as name, Users.email as email, Users.role as role, Users.is_admin as is_admin, Users.createdAt as createdAt, ` +
            `Practices.name as practice_name, 'Subscription Canceled' as plan_name, 'canceled_subscription' AS plan_type, Users.last_login_ts as last_login_ts, Users.is_deleted as is_deleted from Users ` +
            `inner join Practices on Users.practice_id = Practices.id ` +
            `where Users.is_deleted IS NOT true AND Practices.is_deleted IS NOT TRUE${users_where}AND Users.practice_id IS NOT NULL and Users.practice_id in (select practice_id from Subscriptions where stripe_subscription_id is null and plan_id is null) group by id `;

        let combine_query;

        if (query?.filter == 'false' || !query?.filter?.plan_type || query?.filter?.plan_type == 'all' || query?.filter?.plan_type == '') {
            combine_query = get_active_subscription_query + 'union' + get_tek_as_you_go_query + 'union' + get_canceled_subscription_query;
        } else if (query?.filter != 'false' && query?.filter?.plan_type != '' && !['all', 'pay_as_you_go', 'canceled_subscription'].includes(query?.filter?.plan_type)) {
            combine_query = get_active_subscription_query;
        } else if (query?.filter != 'false' && query?.filter?.plan_type == 'pay_as_you_go') {
            combine_query = get_tek_as_you_go_query;
        } else if (query?.filter != 'false' && query?.filter?.plan_type == 'canceled_subscription') {
            combine_query = get_canceled_subscription_query;
        }

        let Query = 'select id,name,email,role,is_admin,createdAt,practice_name,plan_name, plan_type, ' +
            'last_login_ts from (' + combine_query + ') as user_details ' + codeSnip;
        let sqlQuery = Query + codeSnip;
        let sqlQueryCount = Query + codeSnip2;

        const serverData = await sequelize.query(sqlQuery, { type: QueryTypes.SELECT, });
        console.log(sqlQueryCount);
        const TableDataCount = await sequelize.query(sqlQueryCount, { type: QueryTypes.SELECT, });

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Expose-Headers": "totalPageCount",
                "totalPageCount": TableDataCount.length,
            }, body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the Users.",
            }),
        };
    }
};
const updateForOtherSignUps = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;

        let responseData;
        const email = input.email;

        if (input.name && input.email) {

            const MailDomain = `@${email.split('@')[1]}`;
            const is_commonDomain = commonDomains.includes(MailDomain);
            if (!is_commonDomain) {
                const checkThisDomainExist = await Users.findOne({
                    where: {
                        email: { [Op.like]: '%' + MailDomain },
                        is_deleted: { [Op.not]: true },
                        role: { [Op.in]: ['lawyer', 'paralegal'] }
                    },
                    order: [['createdAt', 'ASC']],
                    raw: true
                });
                if (checkThisDomainExist) throw new HTTPError(400, `This organization already has an account. Please contact your admin (${checkThisDomainExist?.email}) to create an additional user.`);
            }

            const { Practices, Users, Op, AdminObjections, CustomerObjections, PasswordHistory,
                Feewaiver, Subscriptions, PropoundResponder, Plans } = await connectToDatabase();

            const name = input.name;
            const password = input.password;
            const user_from = input.user_from;
            const practice_name = input.practice_name;

            const dataObject = Object.assign({}, { id: uuid.v4(), });

            if (!password) { dataObject.password = generateRandomString(8) };

            dataObject.one_time_activation_fee = true;
            dataObject.email = email;
            dataObject.name = name;
            dataObject.role = "lawyer";
            dataObject.user_from = user_from;
            dataObject.created_from = 'customer_admin';
            dataObject.is_free_user = false;
            dataObject.privacy_policy_terms_of_use = input.privacy_policy_terms_of_use;

            const feeWaiverobj = await Feewaiver.findOne({ where: { email: email, expiry_date: { [Op.gte]: new Date() } } });
            if (feeWaiverobj) {
                dataObject.one_time_activation_fee = false;
            }

            let practice_obj = { id: uuid.v4(), name: practice_name, one_time_activation_fee: dataObject.one_time_activation_fee };

            const practice = await Practices.create(practice_obj);
            dataObject.practice_id = practice.id;
            dataObject.is_admin = true;
            dataObject.device_id = input.device_id;
            const user = await Users.create(dataObject);

            await PasswordHistory.create({
                email: dataObject.email, password: user.password, id: uuid.v4(),
            });

            const subscriptionObj = await Subscriptions.create({
                practice_id: practice.id, subscribed_by: user.id, subscribed_on: new Date(), plan_id: "free_trial", id: uuid.v4(),
            });
            let plansdetails;
            if (subscriptionObj && subscriptionObj.plan_id) {
                plansdetails = await Plans.findOne({ where: { plan_id: subscriptionObj.plan_id, active: true }, raw: true });
            }
            let createdDate = moment(practice.createdAt).format('MM-DD-YYYY HH:mm:ss');
            let template = `New Firm Account Created For ${practice.name}`
            await sendEmail(process.env.PRACTICE_EMAIL_RECEIPIENT, template, `
                Practice Name: ${practice.name}<br/>
                Plan Type: ${plansdetails?.name}<br/>
                User Email: ${user?.email}<br/>
                Created Date: ${createdDate}
                `);

            const tokenUser = { id: user.id, role: user.role };

            const token = jwt.sign(tokenUser, process.env.JWT_SECRET, { expiresIn: process.env.JWT_EXPIRATION_TIME });
            const allAdminObjections = await AdminObjections.findAll({ raw: true });
            const processedObjections = [];
            for (let i = 0; i < allAdminObjections.length; i += 1) {
                const objectionItem = allAdminObjections[i];
                objectionItem.practice_id = practice.id;
                delete objectionItem.created_by_admin_id;
                objectionItem.adminobjection_id = objectionItem.id;
                objectionItem.id = uuid.v4();
                processedObjections.push(objectionItem);
            }
            if (processedObjections.length) {
                await CustomerObjections.bulkCreate(processedObjections);
            }
            const newUser = true;
            const signingMethod = 'signup';

            /* Check Propound responder token id contains in dataObject */
            if (dataObject && dataObject.token_id) {
                const propoundResponderObj = await PropoundResponder.findOne({ where: { id: dataObject.token_id } });
                if (!propoundResponderObj) throw new HTTPError(400, ' Invalid token id exist please check');
                propoundResponderObj.responder_practice_id = practice.id;
                propoundResponderObj.is_new_user = false;
                await propoundResponderObj.save();
            }

            responseData = {
                authToken: `JWT ${token}`,
                user: {
                    onetime_activation: practice.one_time_activation_fee,
                    subscription_id: subscriptionObj.id,
                    user_from: user_from,
                    newUserLogin: newUser,
                    signingMethod: signingMethod,
                }
            }
        } else {
            throw new HTTPError(400, "username or email not found");
        }


        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(responseData),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not update the User details.",
            }),
        };
    }
}
const updateNameAndStateBarNumber = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const userId = event.user.id;
        const practice_id = event.user.practice_id;
        const caseId = input.case_id;

        const { Users, Cases, Op } = await connectToDatabase();

        const userData = await Users.findOne({
            where: {
                id: userId,
                practice_id: practice_id,
                is_deleted: { [Op.not]: true }
            }
        })

        if (!userData) { throw new HTTPError(400, 'user not found'); }

        if (input?.user_name) { userData.name = input.user_name };
        if (input?.state_bar_number) { userData.state_bar_number = input.state_bar_number };
        await userData.save();
        if (input?.case_id) {
            const caseData = await Cases.findOne({
                where: {
                    id: caseId,
                    practice_id: practice_id,
                    is_deleted: { [Op.not]: true }
                }
            })
            if (!caseData) { throw new HTTPError(400, 'case data not found'); }
            caseData.attorneys = input.attorneys;
            await caseData.save();
        }
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(userData),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not update the User details.",
            }),
        };
    }
}

const userExists = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { email, practice_id } = input;
        const { Users, Op } = await connectToDatabase();
        const userRoles = ["superAdmin", "manager", "operator", "medicalExpert", "QualityTechnician"];

        const userData = await Users.findAll({
            where: {
                email: email,
                role: { [Op.not]: userRoles },
                is_deleted: { [Op.not]: true }
            }
        })

        const userExists = await Users.findOne({
            where: {
                email: email,
                practice_id: practice_id,
                role: { [Op.not]: userRoles },
                is_deleted: { [Op.not]: true }
            }
        })

        let user_password_exist = null;

        if (userData.length > 0 && !userExists) {
            user_password_exist = true;
        } else {
            user_password_exist = false;
        }

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({ user_password_exist }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "can not verify this email.",
            }),
        };
    }
}

const licenseUpdate = async (event) => {
    try {
        /*         const { Users, Practices, Op } = await connectToDatabase();
                const practiceObj = await Practices.findAll({ where: { is_deleted: { [Op.not]: true } }, raw: true });
                for (let i = 0; i < practiceObj.length; i++) {
                    const users_count = await Users.count({ where: { practice_id: practiceObj[i].id, is_deleted: { [Op.not]: true } } });
                    await Practices.update({ license_count: users_count }, { where: { id: practiceObj[i].id } });
                } */
        let temp = await stripe.subscriptions.update('sub_1NdtRoABxGGVMf06Wg5uahXQ', { metadata: { 'test': 'tested' } });
        let res = await stripe.subscriptions.retrieve('sub_1NdtRoABxGGVMf06Wg5uahXQ');
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(res),
        };

    } catch (err) {
        console.log(err);
    }
}

const UpdateZenNotification = async (event) => {
    try {
        const  user = event.user;
        const { id, practice_id } = user;
        const { Users, Op } = await connectToDatabase();
        // const userRoles = ["superAdmin", "manager", "operator", "medicalExpert", "QualityTechnician"];

        const userData = await Users.findOne({
            where: {
                id: id,
                practice_id: practice_id,
                // role: { [Op.not]: userRoles },
                is_deleted: { [Op.not]: true }
            }
        });

        if(!userData)throw new HTTPError(403, "User Not Found");
        userData.notification = false;
        await userData.save();

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({message: "User notification status successfully updated."}),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Failed to update user notification status",
            }),
        };
    }
}

module.exports.signUp = signUp;
module.exports.create = create;
module.exports.getMe = middy(getMe).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.updateAll = middy(updateAll).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = destroy;
module.exports.getSample = middy(getSample).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.forgetPassword = forgetPassword;
module.exports.resetPassword = middy(resetPassword).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.changePassword = changePassword;
module.exports.resetLoginAttempt = resetLoginAttempt;
module.exports.twofactorSetting = middy(twofactorSetting).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.checkSubscriptionValidation = checkSubscriptionValidation;
module.exports.sessionTokenGenerate = sessionTokenGenerate;
module.exports.updateSignature = updateSignature;
module.exports.updateQCNotificationStatus = updateQCNotificationStatus;
module.exports.getAllUsersForDashboard = getAllUsersForDashboard;
module.exports.updateForOtherSignUps = updateForOtherSignUps;
module.exports.updateNameAndStateBarNumber = updateNameAndStateBarNumber;
module.exports.usersLicencePurchase = usersLicencePurchase;
module.exports.userExists = userExists;
module.exports.licenseUpdate = licenseUpdate;
module.exports.UpdateZenNotification = UpdateZenNotification;

/* 
    sub_status      sub_status_cancel_at_period_end      sub_cancel_at     sub_current_period_end                 reason
->   active                  true                          same_date           same_date              going to cancel at the end of the date.
->   active                  false                         same_date           same_date              going to cancel at the custom date (before end of the date).
->   cancelled               false                         null                end_of_date            subscription cancelled 
->   active                  false                         null                end_of_date            active plan. not cancelled 
*/