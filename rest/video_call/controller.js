const uuid = require('uuid')
const connectToDatabase = require('../../db')
const { HTTPError } = require('../../utils/httpResp')
const AWS = require('aws-sdk')
const chime = new AWS.Chime({
  region: 'us-west-2'
})
chime.endpoint = new AWS.Endpoint(
  'https://service.chime.aws.amazon.com/console'
)
const util = require('util')
const sts = new AWS.STS({
  region: 'us-west-2'
})
const s3 = new AWS.S3()

//https://docs.aws.amazon.com/chime-sdk/latest/dg/migrate-pipelines.html
const chimeSDKMediaPipelines = new AWS.ChimeSDKMediaPipelines({
  region: 'us-west-2'
})
const chimeSDKMediaPipelinesEndpoint =
  'https://media-pipelines-chime.us-west-2.amazonaws.com'
chimeSDKMediaPipelines.endpoint = new AWS.Endpoint(
  chimeSDKMediaPipelinesEndpoint
)

// Optional features like Echo Reduction is only available on Regional Meetings API
// https://docs.aws.amazon.com/chime-sdk/latest/dg/meeting-namespace-migration.html
const chimeSDKMeetings = new AWS.ChimeSDKMeetings({
  region: 'us-west-2'
})
const chimeSDKMeetingsEndpoint =
  'https://meetings-chime.us-west-2.amazonaws.com'
chimeSDKMeetings.endpoint = new AWS.Endpoint(chimeSDKMeetingsEndpoint)

const CAPTURE_S3_DESTINATION_PREFIX = 'esquiretek-twillio-recording' //bucket name of video recording

// const schedule = require('node-schedule')
const { google } = require('googleapis')

// return regional API just for Echo Reduction for now.
function getClientForMeeting(meeting, echoReduction = 'false') {
  if (
    echoReduction === 'true' ||
    (meeting &&
      meeting.Meeting &&
      meeting.Meeting.MeetingFeatures &&
      meeting.Meeting.MeetingFeatures.Audio &&
      meeting.Meeting.MeetingFeatures.Audio.EchoReduction === 'AVAILABLE')
  ) {
    return chimeRegional
  }
  return chime
}

const createMeeting = async event => {
  const meetingCache = {}
  const attendeeCache = {}

  try {
    const input =
      typeof event.body === 'string' ? JSON.parse(event.body) : event.body

    const title = input.title
    const name = input.name
    const region = input.region || 'us-east-1'

    const client = chimeSDKMeetings
    if (!meetingCache[title]) {
      let request = {
        ClientRequestToken: uuidv4(),
        MediaRegion: region,
        ExternalMeetingId: title.substring(0, 64)
      }
      if (input.ns_es === 'true') {
        request.MeetingFeatures = {
          Audio: {
            // The EchoReduction parameter helps the user enable and use Amazon Echo Reduction.
            EchoReduction: 'AVAILABLE'
          }
        }
      }

      meetingCache[title] = await client.createMeeting(request).promise()
      attendeeCache[title] = {}
    }
    const joinInfo = {
      JoinInfo: {
        Title: title,
        Meeting: meetingCache[title].Meeting,
        Attendee: (
          await client
            .createAttendee({
              MeetingId: meetingCache[title].Meeting.MeetingId,
              ExternalUserId: uuidv4()
            })
            .promise()
        ).Attendee
      }
    }
    attendeeCache[title][joinInfo.JoinInfo.Attendee.AttendeeId] = name

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(joinInfo)
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        error: err.message || 'Could not create the State Deatails.'
      })
    }
  }
}

const getMeeting = async meetingTitle => {
  const { Meetings } = await connectToDatabase()
  const meetingObject = await Meetings.findOne({
    where: {
      title: meetingTitle
    },
    attributes: ['meeting_data']
  })

  return meetingObject ? meetingObject.meeting_data : null
}

const putCaptureData = async (meeting_title, capture_data) => {
  const { Meetings } = await connectToDatabase()
  let meetingInfo = await Meetings.update(
    {
      capture_data: JSON.stringify(capture_data)
    },
    {
      where: {
        title: meeting_title
      }
    }
  )

  return meetingInfo ? meetingInfo.capture_data : null
}

const getCaptureData = async meetingTitle => {
  try {
    const { Meetings } = await connectToDatabase()
    const meetingObject = await Meetings.findOne({
      where: {
        title: meetingTitle
      },
      attributes: ['capture_data']
    })

    return meetingObject ? meetingObject.capture_data : null
  } catch (err) {
    console.log(err)
    throw err
  }
}

const checkIfMeetingIsPresent = async (meetingInfo, ns_es) => {
  const client = chimeSDKMeetings
  const meetingId = meetingInfo.Meeting.MeetingId

  const params = {
    MeetingId: meetingId
  }

  try {
    // Convert the callback-based function to a promise-based function
    const getMeetingPromise = util.promisify(client.getMeeting.bind(client))
    const data = await getMeetingPromise(params)

    // If the meeting exists, return true
    return true
  } catch (error) {
    // If the meeting doesn't exist (404 error), return false
    if (error.statusCode === 404) {
      return false
    }
    // Rethrow other errors
    throw error
  }
}

const join = async event => {
  try {
    const input =
      typeof event.body === 'string' ? JSON.parse(event.body) : event.body
    const { Meetings, MeetingAttendees } = await connectToDatabase()

    const title = input.title
    const name = input.name
    const region = 'us-west-2'

    let meetingInfo = await getMeeting(title)
    const client = chimeSDKMeetings
    meetingInfo =
      typeof meetingInfo === 'string' ? JSON.parse(meetingInfo) : meetingInfo

    if (!meetingInfo) {
      const request = {
        ClientRequestToken: uuid.v4(),
        MediaRegion: region,
        ExternalMeetingId: title.substring(0, 64)
      }

      if (input.ns_es && input.ns_es === 'true') {
        request.MeetingFeatures = {
          Audio: {
            // The EchoReduction parameter helps the user enable and use Amazon Echo Reduction.
            EchoReduction: 'AVAILABLE'
          }
        }
      }
      console.info(
        'Creating new meeting before joining: ' + JSON.stringify(request)
      )
      meetingInfo = await client.createMeeting(request).promise()

      const data = {
        title: title,
        meeting_data: JSON.stringify(meetingInfo)
      }

      const dataObject = Object.assign(data, {
        id: uuid.v4()
      })

      await Meetings.create(dataObject)
    } else {
      let result = await checkIfMeetingIsPresent(meetingInfo, input.ns_es)

      if (!result) {
        //Need to add & update the status node here
        await Meetings.destroy({
          where: {
            title: title
          }
        })
        await MeetingAttendees.destroy({
          where: {
            meeting_title: title
          }
        })

        const request = {
          ClientRequestToken: uuid.v4(),
          MediaRegion: region,
          ExternalMeetingId: title.substring(0, 64)
        }

        if (input.ns_es && input.ns_es === 'true') {
          request.MeetingFeatures = {
            Audio: {
              // The EchoReduction parameter helps the user enable and use Amazon Echo Reduction.
              EchoReduction: 'AVAILABLE'
            }
          }
        }
        console.info(
          'Creating new meeting before joining: ' + JSON.stringify(request)
        )
        meetingInfo = await client.createMeeting(request).promise()

        const data = {
          title: title,
          meeting_data: JSON.stringify(meetingInfo)
        }

        const dataObject = Object.assign(data, {
          id: uuid.v4()
        })

        await Meetings.create(dataObject)
      }
    }

    //     console.info('Adding new attendee');
    const attendeeInfo = await client
      .createAttendee({
        MeetingId: meetingInfo.Meeting.MeetingId,
        ExternalUserId: uuid.v4()
      })
      .promise()
    //     // putAttendee(title, attendeeInfo.Attendee.AttendeeId, name);

    const dataObject = Object.assign(
      {
        meeting_id: meetingInfo.Meeting.MeetingId,
        meeting_title: title,
        attendee_id: attendeeInfo.Attendee.AttendeeId,
        attendee_name: name,
        attendee_external_user_id: attendeeInfo.Attendee.ExternalUserId,
        attendee_join_token: attendeeInfo.Attendee.JoinToken,
        attendee_data: JSON.stringify(attendeeInfo)
      },
      {
        id: uuid.v4()
      }
    )

    await MeetingAttendees.create(dataObject)

    const joinInfo = {
      JoinInfo: {
        Title: title,
        Meeting: meetingInfo.Meeting,
        Attendee: attendeeInfo.Attendee
      }
    }

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(joinInfo)
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        error: err.message || 'Could not create the State Deatails.'
      })
    }
  }
}

const getAttendee = async (title, attendeeId) => {
  const { MeetingAttendees } = await connectToDatabase()
  const attendeeObject = await MeetingAttendees.findOne({
    where: {
      attendee_id: attendeeId
    },
    attributes: ['attendee_name']
  })
  if (!attendeeObject) {
    return 'recording'
  }

  let attendeeData = {}
  if (attendeeObject && attendeeObject.attendee_name) {
    attendeeData = attendeeObject.attendee_name
  }

  if (attendeeObject && !attendeeObject.attendee_name) {
    return 'Unknown'
  }
  return attendeeData
}

const attendee = async event => {
  try {
    const input =
      typeof event.body === 'string' ? JSON.parse(event.body) : event.body

    const title = input.title
    const attendeeId = input.attendee
    const attendeeInfo = {
      AttendeeInfo: {
        AttendeeId: attendeeId,
        Name: await getAttendee(title, attendeeId)
      }
    }
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(attendeeInfo)
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        error: err.message || 'Could not fetch the attendee Details.'
      })
    }
  }
}

const end = async event => {
  try {
    const input =
      typeof event.body === 'string' ? JSON.parse(event.body) : event.body

    const title = input.title
    let meetingInfo = await getMeeting(title)
    meetingInfo =
      typeof meetingInfo === 'string' ? JSON.parse(meetingInfo) : meetingInfo
    const client = chimeSDKMeetings
    await client
      .deleteMeeting({
        MeetingId: meetingInfo.Meeting.MeetingId
      })
      .promise()
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        message: 'Meeting Ended'
      })
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        error: err.message || 'Could not delete the meeting record.'
      })
    }
  }
}

const start_capture = async event => {
  const input =
    typeof event.body === 'string' ? JSON.parse(event.body) : event.body

  try {
    const title = input.title
    let meeting = await getMeeting(title)
    meeting = typeof meeting === 'string' ? JSON.parse(meeting) : meeting

    const callerInfo = await sts.getCallerIdentity().promise()
    let meetingRegion = meeting.Meeting.MediaRegion

    //presenterOnly condition
    // const chimeSdkMeetingConfiguration = {
    //   ArtifactsConfiguration: {
    //     Audio: {
    //       MuxType: "AudioWithCompositedVideo", // Change to "AudioWithCompositedVideo" if you want both audio and video.
    //     },
    //     Video: {
    //       State: "Disabled", // Enable video recording
    //       MuxType: "VideoOnly",
    //     },
    //     Content: {
    //       State: "Disabled", // Enable recording of content sharing
    //       MuxType: "ContentOnly", // Change to "VideoOnly" if you want only video content to be recorded.
    //     },
    //     CompositedVideo: {
    //       Layout: "GridView",
    //       Resolution: "FHD",
    //       GridViewConfiguration: {
    //         ContentShareLayout: "PresenterOnly",
    //         PresenterOnlyConfiguration: {
    //           PresenterPosition: "TopRight",
    //         },
    //       },
    //     },
    //   },
    // };

    //Horizontal or Vertical viewcondition
    const chimeSdkMeetingConfiguration = {
      ArtifactsConfiguration: {
        Audio: {
          MuxType: 'AudioWithCompositedVideo'
        },
        Video: {
          State: 'Disabled',
          MuxType: 'VideoOnly'
        },
        Content: {
          State: 'Disabled', // Enable recording of content sharing
          MuxType: 'ContentOnly'
        },
        CompositedVideo: {
          Layout: 'GridView',
          Resolution: 'FHD',
          GridViewConfiguration: {
            ContentShareLayout: 'Vertical'
          }
        }
      }
    }

    let captureS3Destination = `arn:aws:s3:::${CAPTURE_S3_DESTINATION_PREFIX}/${meeting.Meeting.MeetingId}/`
    const request = {
      SourceType: 'ChimeSdkMeeting',
      SourceArn: `arn:aws:chime::${callerInfo.Account}:meeting:${meeting.Meeting.MeetingId}`,
      SinkType: 'S3Bucket',
      SinkArn: captureS3Destination,
      ChimeSdkMeetingConfiguration: chimeSdkMeetingConfiguration
    }

    let pipelineInfo = await chimeSDKMediaPipelines
      .createMediaCapturePipeline(request)
      .promise()

    await putCaptureData(title, pipelineInfo)

    //return response(201, 'application/json', JSON.stringify(pipelineInfo));

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(pipelineInfo)
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        error: err.message || 'Could not capture the meeting.'
      })
    }
  }
}

const end_capture = async event => {
  try {
    const input =
      typeof event.body === 'string' ? JSON.parse(event.body) : event.body
    const title = input.title

    let pipelineInfo = await getCaptureData(title)
    pipelineInfo =
      typeof pipelineInfo === 'string' ? JSON.parse(pipelineInfo) : pipelineInfo

    if (pipelineInfo) {
      await chimeSDKMediaPipelines
        .deleteMediaCapturePipeline({
          MediaPipelineId: pipelineInfo.MediaCapturePipeline.MediaPipelineId
        })
        .promise()
      return {
        statusCode: 200,
        headers: {
          'Content-Type': 'text/plain',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true
        },
        body: JSON.stringify('Capture Ended')
      }
    } else {
      return {
        statusCode: 500,
        headers: {
          'Content-Type': 'text/plain',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true
        },
        body: JSON.stringify({
          msg: 'No pipeline to stop for this meeting'
        })
      }
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        error: err.message || 'Could not capture the meeting.'
      })
    }
  }
}

const capture_concatenation = async event => {
  const input =
    typeof event.body === 'string' ? JSON.parse(event.body) : event.body

  try {
    const media_pipeline_arn = input.media_pipeline_arn

    let captureS3Destination =
      'arn:aws:s3:::esquiretek-twillio-recording/concat1'

    const request = {
      Sources: [
        {
          Type: 'MediaCapturePipeline',
          MediaCapturePipelineSourceConfiguration: {
            MediaPipelineArn: `${media_pipeline_arn}`, //must be <30 days old
            ChimeSdkMeetingConfiguration: {
              ArtifactsConfiguration: {
                Audio: {
                  State: 'Enabled'
                },
                Video: {
                  State: 'Enabled'
                },
                Content: {
                  State: 'Enabled'
                },
                DataChannel: {
                  State: 'Enabled'
                },
                TranscriptionMessages: {
                  State: 'Enabled'
                },
                MeetingEvents: {
                  State: 'Enabled'
                },
                CompositedVideo: {
                  State: 'Enabled'
                }
              }
            }
          }
        }
      ],
      Sinks: [
        {
          Type: 'S3Bucket',
          S3BucketSinkConfiguration: {
            Destination: `${captureS3Destination}`
          }
        }
      ]
    }

    let pipelineInfo = await chimeSDKMediaPipelines
      .createMediaConcatenationPipeline(request)
      .promise()

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(pipelineInfo)
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        error: err.message || 'Could not capture the meeting.'
      })
    }
  }
}

const start_meetingTranscript = async event => {
  try {
    const input =
      typeof event.body === 'string' ? JSON.parse(event.body) : event.body

    const meetingId = input.meeting_id
    const region = 'us-west-2'

    var request = {
      MeetingId: meetingId,
      TranscriptionConfiguration: {
        EngineTranscribeSettings: {
          ContentIdentificationType: 'PII',
          // ContentRedactionType: 'PII',
          LanguageCode: 'en-US',
          PartialResultsStability: 'high',
          Region: region
        }
      }
    }

    let resultInfo = await chimeSDKMeetings
      .startMeetingTranscription(request)
      .promise()

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(resultInfo)
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        error: err.message || 'Could not start the meeting transcription.'
      })
    }
  }
}

const stop_meetingTranscript = async event => {
  try {
    const input =
      typeof event.body === 'string' ? JSON.parse(event.body) : event.body

    const title = input.title
    let meetingInfo = await getMeeting(title)
    meetingInfo =
      typeof meetingInfo === 'string' ? JSON.parse(meetingInfo) : meetingInfo

    await chimeSDKMeetings
      .stopMeetingTranscription({
        MeetingId: meetingInfo.Meeting.MeetingId
      })
      .promise()

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({})
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        error: err.message || 'Could not stop the meeting transcription.'
      })
    }
  }
}

const read_transcriptData = async event => {
  try {
    const params = {
      Bucket: 'esquiretek-twillio-recording',
      Key:
        'concat1/transcription-messages/f7e3714b-3af7-4763-8092-d2c2b1304e6d.txt'
    }

    const s3Object = await s3.getObject(params).promise()
    const lines = s3Object.Body.toString().split('\n')

    const resultsArray = []

    lines.forEach((line, i) => {
      if (i !== 0 && i > 10 && line !== '') {
        const result = JSON.parse(line)
        const result_json = result['transcript']['results'][0]

        if (result_json['isPartial'] === false) {
          const speakerExternalUserId =
            result_json['alternatives'][0]['items'][0]['speakerExternalUserId']
          const transcript = result_json['alternatives'][0]['transcript']

          const speakerId = speakerExternalUserId.split('#')[1] // Extract the part after '#'

          resultsArray.push({
            [speakerId]: transcript
          })
        }
      }
    })

    // Sending email with the results
    // try {
    //     const transporter = nodemailer.createTransport({
    //         service: 'Gmail', // Replace with your email service provider
    //         auth: {
    //             user: 'your-email@gmail.com', // Replace with your email address
    //             pass: 'your-email-password', // Replace with your email password or app password
    //         },
    //     });

    //     let emailContent = '';

    //     resultsArray.forEach((result) => {
    //         const speaker = Object.keys(result)[0];
    //         const transcript = result[speaker];
    //         emailContent += `${speaker}: ${transcript}\n`;
    //     });

    //     const mailOptions = {
    //         from: 'your-email@gmail.com', // Replace with your email address
    //         to: 'recipient@example.com', // Replace with the recipient's email address
    //         subject: 'Transcript Results',
    //         text: emailContent,
    //     };

    //     const info = await transporter.sendMail(mailOptions);

    //     console.log('Email sent: ' + info.response);

    //     return {
    //         statusCode: 200,
    //         headers: {
    //             'Content-Type': 'text/plain',
    //             'Access-Control-Allow-Origin': '*',
    //             'Access-Control-Allow-Credentials': true,
    //         },
    //         body: JSON.stringify(resultsArray),
    //     };
    // } catch (err) {
    //     console.log(err);
    //     return {
    //         statusCode: err.statusCode || 500,
    //         headers: {
    //             'Content-Type': 'text/plain',
    //             'Access-Control-Allow-Origin': '*',
    //             'Access-Control-Allow-Credentials': true,
    //         },
    //         body: JSON.stringify({ error: err.message || 'Could not stop the meeting transcription.' }),
    //     };
    // }

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(resultsArray)
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        error: err.message || 'Could not stop the meeting transcription.'
      })
    }
  }
}

const meeting_scheduler = async event => {
  let input = {
    title: 'Sample Meeting',
    startTime: '2023-08-03T14:30:00',
    duration: '1 hour',
    topic: 'Sample Topic',
    region: 'us-west-2'
  }

  try {
    const date = new Date(2023, 7, 3, 18, 31, 0)
    // const currentDate = new Date()
    // const currentHours = currentDate.getHours()
    // console.log(currentHours);

    const job = schedule.scheduleJob(date, function () {
      console.log('The world is going to end today.')
    })

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({})
    }
  } catch (error) {
    console.log(error)
    return {
      statusCode: error.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        error: error.message || 'Could not stop the meeting transcription.'
      })
    }
  }
}

const google_oauth = async event => {
  const redirect_uri = `${process.env.APP_URL}/google-oauth2`

  // CalendarTest project:
  // const oauth2Client = new google.auth.OAuth2(
  //   '417316033534-bv6hgrac6uqaqa7k56u0sttmkpm1k7cq.apps.googleusercontent.com',
  //   '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',
  //   'http://localhost:3000/google/redirect'
  // )

  //EsquireTek local
  const oauth2Client = new google.auth.OAuth2(
    process.env.GOOGLE_CLIENT_ID,
    process.env.GOOGLE_CLIENT_SECRET,
    redirect_uri
  )
  console.log(oauth2Client)

  try {
    // generate a url that asks permissions for Blogger and Google Calendar scopes
    const scopes = ['https://www.googleapis.com/authcalendar/']

    const url = oauth2Client.generateAuthUrl({
      // 'online' (default) or 'offline' (gets refresh_token)
      access_type: 'offline',

      // If you only need one scope you can pass it as a string
      scope: scopes
    })

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(url)
    }
  } catch (error) {
    console.log(error)
    return {
      statusCode: error.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        error: error.message || 'Could not stop the meeting transcription.'
      })
    }
  }
}

module.exports.start_meetingTranscript = start_meetingTranscript
module.exports.join = join
module.exports.attendee = attendee
module.exports.end = end
module.exports.checkIfMeetingIsPresent = checkIfMeetingIsPresent
module.exports.start_capture = start_capture
module.exports.end_capture = end_capture
module.exports.capture_concatenation = capture_concatenation
module.exports.stop_meetingTranscript = stop_meetingTranscript
module.exports.read_transcriptData = read_transcriptData
module.exports.meeting_scheduler = meeting_scheduler
module.exports.google_oauth = google_oauth
