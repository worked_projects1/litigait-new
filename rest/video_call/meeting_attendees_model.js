module.exports = (sequelize, type) => sequelize.define('MeetingAttendees', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    meeting_id: type.STRING,
    meeting_title: type.STRING,
    attendee_id: type.STRING,
    attendee_name: type.STRING,
    attendee_external_user_id: type.STRING,
    attendee_join_token: type.STRING,
    attendee_data: type.TEXT('long')
});
