module.exports = (sequelize, type) => sequelize.define('Meetings', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    title: type.STRING,
    meeting_data: type.TEXT('long'),
    capture_data: type.TEXT('long'),
    external_meeting_id: type.STRING,
    audio_host_url: type.STRING,
    audio_fallback_url: type.STRING,
    screen_data_url: type.STRING,
    screen_sharing_url: type.STRING,
    screen_viewing_url: type.STRING,
    signaling_url: type.STRING,
    turn_control_url: type.STRING,
    event_ingestion_url: type.STRING,
    media_region: type.STRING
});
