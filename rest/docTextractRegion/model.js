module.exports = (sequelize, type) => sequelize.define('DocTextractRegion', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    case_id: type.STRING,
    legalforms_id: type.STRING,
    propound_template_id: type.STRING,
    document_extraction_id: type.STRING,
    user_id: type.STRING,
    doc_textract_region:type.STRING,
    region_sent_timestamp: type.DATE,
    response_from_region_timestamp: type.DATE,
    uploaded_type: type.STRING, // Responding, Propounding & QuickCreate
});