module.exports = (sequelize, type) => sequelize.define('LawyerObjections', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    adminobjection_id: type.STRING,
    practice_id: type.STRING,
    user_id: type.STRING,
    objection_title: type.STRING,
    objection_text: type.TEXT,
    state: type.STRING,
    objection_view: type.BOOLEAN,
});
