const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const authMiddleware = require('../../auth');
const { sendEmail } = require('../../utils/mailModule');
const { commonDomains } = require("../../utils/emailDomainValidation");

const create = async (event) => {
    try {
        const { Feewaiver, Users, Op } = await connectToDatabase();

        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        if (!input.email) throw new HTTPError(400, `Fee waiver Email ID was not found.`);
        const checkExistEmail = await Users.findAll({
            where: { email: input.email, is_deleted: { [Op.not]: true } },
            raw: true
        });
        const checkExistEmailFeeWaiver = await Feewaiver.findAll({ where: { email: input.email }, raw: true });
        if (checkExistEmail.length != 0 || checkExistEmailFeeWaiver.length != 0) throw new HTTPError(400, `Email ID already exist`);

        let findEmail = input.email;
        const domain = findEmail.split('@');
        findEmail = '@' + domain[1];
        const is_commonDomain = commonDomains.includes(findEmail);
        if (!is_commonDomain) {
            const checkThisDomainExist = await Users.findOne({
                where: {
                    email: { [Op.like]: '%' + findEmail },
                    is_deleted: { [Op.not]: true },
                    role: { [Op.in]: ['lawyer', 'paralegal'] }
                },
                order: [['createdAt', 'ASC']],
                raw: true
            });
            if (checkThisDomainExist) throw new HTTPError(400, `This organization already has an account. Please contact your admin (${checkThisDomainExist?.email}) to create an additional user.`);
            const feewaiverDomain = await Feewaiver.findOne({ where: { email: { [Op.like]: '%' + findEmail } } });
            if (feewaiverDomain) throw new HTTPError(400, `Domain  ${domain[1]} already exist in Fee waiver`);
            const feeWaiverobj = await Feewaiver.findOne({ where: { email: input.email } });
            if (feeWaiverobj) throw new HTTPError(400, `Email ${findEmail} already exist`);
        }


        let TimeStamp = new Date();
        TimeStamp.setDate(TimeStamp.getDate() + 7);
        let cDate = TimeStamp.getDate();
        let Year = TimeStamp.getFullYear();
        let Month = TimeStamp.getMonth() + 1;
        if (Month < 10) Month = '0' + Month;
        if (cDate < 10) cDate = '0' + cDate;
        let subscriptionEndDate = Year + '-' + Month + '-' + cDate + ' ' + '23:59:00';

        const dataObject = Object.assign(input, { id: uuid.v4() });
        dataObject.expiry_date = subscriptionEndDate;
        const saveObj = await Feewaiver.create(dataObject);
        const plaintext = saveObj.get({ plain: true });

        subscriptionEndDate = Month + '/' + cDate + '/' + Year;

        const subject = 'EsquireTek - Activation fee waived till ' + subscriptionEndDate;

        const body = 'Hello,' + '<br/><br/>' + 'Thank you for your interest in joining EsquireTek. If you sign up using this email address before ' + subscriptionEndDate + ', we will waive the one time activation fee that is normally charged for new customers.' +
            '<br/><br/>' + 'Welcome to EsquireTek.' + '<br/><br/>' + 'Signed,<br/>' + 'Gerry Espinoza<br/>' + 'Operations Manager<br/>';

        await sendEmail(plaintext.email, subject, body, 'EsquireTek');

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plaintext),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the fee waiver.' }),
        };
    }
}
const getAll_new_pagination = async (event) => {
    try {
        const { Feewaiver } = await connectToDatabase();

        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        query.order = [['createdAt', 'DESC']];
        const feeWaiverDeatails = await Feewaiver.findAll(query);
        const feeWaiverDeatailsCount = await Feewaiver.count();
        let totalPages, currentPage;
        if (query.limit && (feeWaiverDeatails.length > 0)) {
            totalPages = Math.ceil(feeWaiverDeatailsCount / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const feeWaiverDetails = {
            total_items: feeWaiverDeatailsCount,
            response: feeWaiverDeatails,
            total_pages: totalPages,
            current_page: currentPage
        };
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': feeWaiverDeatailsCount

            },
            body: JSON.stringify(feeWaiverDetails),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch fee waiver.' }),
        };
    }
}
const getAll = async (event) => {
    try {
        const { Feewaiver } = await connectToDatabase();

        const headers = event.headers;
        const query = {};
        if (headers.offset) query.offset = parseInt(headers.offset, 10);
        if (headers.limit) query.limit = parseInt(headers.limit, 10);
        query.order = [['createdAt', 'DESC']];
        const feeWaiverDeatails = await Feewaiver.findAll(query);
        const feeWaiverDeatailsCount = await Feewaiver.count();

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': feeWaiverDeatailsCount

            },
            body: JSON.stringify(feeWaiverDeatails),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch fee waiver.' }),
        };
    }
}
const getOne = async (event) => {
    try {
        const { Feewaiver, Users } = await connectToDatabase();
        const params = event.params || event.pathParameters;
        const feewaiverDeatails = await Feewaiver.findOne({ where: { id: params.id }, raw: true });
        if (!feewaiverDeatails) throw new HTTPError(400, `Fee waiver Email ID was not found.`);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(feewaiverDeatails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch fee waiver.' }),
        };
    }
}
const destroy = async (event) => {
    try {
        const { Feewaiver, Users } = await connectToDatabase();
        const params = event.params || event.pathParameters;
        const checkExistEmail = await Feewaiver.findOne({ where: { id: params.id } });
        if (!checkExistEmail) throw new HTTPError(400, `Fee waiver Email ID was not found.`);
        await checkExistEmail.destroy();

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ message: "Fee waiver destroyed" }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ message: "could not destroy Fee Waiver." }),
        };
    }
}
module.exports.create = create;
module.exports.getAll = getAll;
module.exports.destroy = destroy;
module.exports.getOne = getOne;