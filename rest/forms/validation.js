const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');

exports.validateCreateForms = function (data) {
    const rules = {
        case_id: 'required|min:4|max:64',
        document_type: 'required|min:3|max:64',
        question_number: 'numeric',
        question_text: 'min:4',
        lawyer_response_text: 'min:4',
        lawyer_response_status: 'min:4|max:64',
        client_response_text: 'min:4|max:64',
        client_response_status: 'min:4|max:64',
        last_updated_by_lawyer: 'date',
        last_updated_by_client: 'date',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateGetOneForm = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateUpdateForms = function (data) {
    const rules = {
        case_id: 'min:4|max:64',
        document_type: 'min:3|max:64',
        question_number: 'numeric',
        question_text: 'min:4',
        lawyer_response_text: 'min:4',
        lawyer_response_status: 'min:4|max:64',
        client_response_text: 'min:4|max:64',
        client_response_status: 'min:4|max:64',
        last_updated_by_lawyer: 'date',
        last_updated_by_client: 'date',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateDeleteForm = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateReadForms = function (data) {
    const rules = {
        offset: 'numeric',
        limit: 'numeric',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateSentQuestionToClient = function (data) {
    const rules = {
        case_id: 'required|min:4|max:64',
        document_type: 'required|min:3|max:64',
        //client_id: 'required|min:4|max:64',
        sending_type: 'required|min:3|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateVerificationRequestToClient = function (data) {
    const rules = {
        case_id: 'required|min:4|max:64',
        document_type: 'required|min:3|max:64',
        client_id: 'required|min:4|max:64',
        legalforms_id: 'required|min:4|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateClientQuestionAnswering = function (data) {
    const rules = {
        case_id: 'required',
        practice_id: 'required',
        document_type: 'required',
        legalforms_id: 'required',
        otp_code: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

