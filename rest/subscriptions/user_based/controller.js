const uuid = require('uuid');
const connectToDatabase = require('../../../db');
const { HTTPError } = require('../../../utils/httpResp');
const { dateDiffInDays, unixTimeStamptoDateTime, unixTimeStamptoDate } = require('../../../utils/timeStamp');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const { calculateOneTimeFeeDiscount, getPlansByplanType,
    validateDiscountCode, createSubscriptionMetaObj, getPropoundingPlanType } = require('../../helper_functions/controller.subscription');
const { sendEmail } = require('../../../utils/mailModule');

/* Update is_yearly_free_trial_available is false. */

const create = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const dataObject = Object.assign(input, { id: uuid.v4(), practice_id: event.user.practice_id });
        const { Practices, Plans, Subscriptions, Op, PracticeSettings, Settings, DiscountCode, DiscountHistory } = await connectToDatabase();
        let is_eligible_to_couponcode = false;
        let stripe_couponcode_obj;
        const practiceObject = await Practices.findOne({
            where: {
                id: event.user.practice_id, is_deleted: { [Op.not]: true },
                billing_type: 'limited_users_billing'
            },
            logging: console.log
        });

        if (!practiceObject?.id) throw new HTTPError(400, `Invalid Practice Please check.`);
        if (!practiceObject?.stripe_customer_id) {
            throw new HTTPError(400, 'Please provide the credit card information on Credit Card Info settings and try again.');
        }
        const paymentMethods = await stripe.paymentMethods.list({
            customer: practiceObject.stripe_customer_id,
            type: 'card',
        });
        if (paymentMethods.data.length == 0) throw new HTTPError(400, 'This customer has no attached payment source or default payment method.');

        event.body.paymentMethod_id = paymentMethods.data[0];

        const planObject = await Plans.findOne({ where: { plan_id: dataObject.plan_id, active: true }, raw: true });
        if (!planObject?.id) throw new HTTPError(400, 'Invalid plan details.please check.');

        /* validate discount code */
        if (dataObject?.discount_code) {
            const discountcode_obj = await validateDiscountCode(dataObject?.discount_code);
            const is_eligible_for = discountcode_obj.plan_type.split(',');
            if (is_eligible_for.includes(planObject.plan_type)) is_eligible_to_couponcode = true;
            stripe_couponcode_obj = JSON.parse(discountcode_obj.stripe_obj);
        }

        /* Getting free user limit from Global settings */
        const global_settings_Obj = await Settings.findOne({ where: { key: 'global_settings' }, raw: true });
        const settings_value = JSON.parse(global_settings_Obj.value);
        if (!settings_value?.free_user_limit) throw new HTTPError(400, 'Users limit not found.please contact support.');
        /* Getting free user limit from Practice settings */
        const practice_settings_obj = await PracticeSettings.findOne({ where: { practice_id: event.user.practice_id }, raw: true });
        const Practice_settings_value = practice_settings_obj?.value ? JSON.parse(practice_settings_obj.value) : {};
        const free_user_limit = Practice_settings_value?.free_user_limit ? Practice_settings_value?.free_user_limit : settings_value?.free_user_limit;

        //find additional users exist.
        const additional_users_count = practiceObject?.license_count > free_user_limit ? parseInt(practiceObject?.license_count) - free_user_limit : 0;

        /* Get Free Plans */
        const free_subscriptions_plan_obj = await getPlansByplanType('complimentary_plans', planObject?.plan_category, planObject?.plan_type);
        const free_subscriptions_stripe_price_id = free_subscriptions_plan_obj?.stripe_price_id;

        /* Get Addition plans */
        const additional_subscriptions_plan_obj = await getPlansByplanType('standard_plans', planObject?.plan_category, planObject?.plan_type);
        const additional_subscriptions_stripe_price_id = additional_subscriptions_plan_obj?.stripe_price_id;


        const metadata = {
            practice_id: event.user.practice_id,
            user_email: event.user.email,
            plan_id: dataObject.plan_id,
            stripe_product_id: planObject.stripe_product_id,
            plan_name: planObject.name,
            billing_type: 'limited_users_billing',
            total_users: practiceObject?.license_count,
            additional_users: additional_users_count,
            free_users: free_user_limit,
            plan_type: 'responding',
            date: new Date()
        };


        const rootUserSubscriptionObj = { customer: practiceObject?.stripe_customer_id, ...createSubscriptionMetaObj(dataObject.plan_id, 1, metadata, 'root_user_subscription') };
        const freeUserSubscriptionObj = createSubscriptionMetaObj(free_subscriptions_stripe_price_id, free_user_limit - 1, metadata, 'free_user_subscription');
        const additionalUserSubscriptionObj = createSubscriptionMetaObj(additional_subscriptions_stripe_price_id, additional_users_count, metadata, 'additional_user_subscription');

        /* Update discount code */
        if (is_eligible_to_couponcode && dataObject?.discount_code && stripe_couponcode_obj?.id) {
            rootUserSubscriptionObj.coupon = stripe_couponcode_obj.id;
            additionalUserSubscriptionObj.coupon = stripe_couponcode_obj.id;
        }

        /* One Time Fees charges Here */
        if (practiceObject.one_time_activation_fee) {
            event.body.plan_type = planObject.plan_type;
            let response = await oneTimeActivationCharge(event);
            if (!response?.status) throw new HTTPError(400, response.message);
        }

        /* Create Root customer Subscription with amount */
        const subscription = await stripe.subscriptions.create(rootUserSubscriptionObj);
        const invoiceObj = subscription?.latest_invoice ? await stripe.invoices.retrieve(subscription?.latest_invoice) : undefined;
        const chargeObj = invoiceObj?.charge ? await stripe.charges.retrieve(invoiceObj?.charge) : undefined;;

        if (subscription.status !== 'active') {
            await stripe.subscriptions.del(subscription.id);
            throw new HTTPError(400, 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
        }

        /* Create Free license users */
        const free_license_subscription = await stripe.subscriptions.update(subscription.id, freeUserSubscriptionObj);

        /* Additional Users */
        if (additional_users_count) {
            const additional_license_subscription = await stripe.subscriptions.update(subscription.id, additionalUserSubscriptionObj);
        }

        /* Delete Free trial subscription */
        await Subscriptions.destroy({ where: { practice_id: dataObject.practice_id, plan_id: { [Op.or]: [null, 'free_trial'] } }, logging: console.log });

        /* Retrieve Subscription details */
        const updatedSubscriptionObj = await stripe.subscriptions.retrieve(subscription.id);

        /* Get future invoice */
        const upcoming_invoice = await stripe.invoices.retrieveUpcoming({ subscription: subscription.id });
        const upcoming_invoice_price = parseFloat(upcoming_invoice?.total / 100);
        current_invoice_price = upcoming_invoice_price;

        const subscriptionValidity = new Date(parseInt(subscription.current_period_end) * 1000);
        await Subscriptions.create({
            id: dataObject.id, practice_id: dataObject.practice_id,
            subscribed_by: event.user.id, subscribed_on: new Date(), plan_id: dataObject.plan_id, stripe_subscription_id: subscription.id,
            stripe_subscription_data: JSON.stringify(updatedSubscriptionObj), stripe_product_id: planObject.stripe_product_id,
            subscribed_valid_till: subscriptionValidity, plan_category: planObject.plan_category, current_invoice_price, upcoming_invoice_price: upcoming_invoice_price,
            invoice_id: invoiceObj?.id || null, payment_attempt_count: invoiceObj?.attempt_count || null,
            charge_id: invoiceObj?.charge || null, charge_response: chargeObj?.outcome ? JSON.stringify(chargeObj?.outcome) : null
        });

        /* This if condition only work's in local level. */
        if (subscription?.discount?.coupon?.id && subscription.status == 'active' && process.env.NODE_ENV === 'test') {

            let discountCodeObj, discount_percentage, discounted_price, discount_amount;

            const discount_code = subscription.discount.coupon.name;
            discountCodeObj = await DiscountCode.findOne({ where: { discount_code: discount_code } });
            discount_percentage = discountCodeObj.discount_percentage;
            const percentage = 1 - (discount_percentage / 100);
            discounted_price = (percentage * planObject.price).toFixed(2);

            let plan_type = planObject.plan_type.split('_').slice(0, planObject.plan_type.length - 1);
            let discount_for = plan_type.slice(0, plan_type.length - 1).join('_').toLowerCase();
            discount_amount = planObject.price - parseFloat(discounted_price);

            let discount_history = {
                id: uuid.v4(), practice_id: dataObject.practice_id, plan_category: planObject.plan_category,
                discount_for, promocode: discountCodeObj.name, discount_percentage, plan_type: planObject.plan_type, discounted_price,
                discount_amount, base_price: planObject.price
            }
            console.log(discount_history);
            await DiscountHistory.create(discount_history);
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the subscriptionModelObject.' }),
        };
    }
}

const subscriptionDelete = async (event) => {
    try {
        const { Subscriptions, Plans, DiscountHistory, Users, DiscountCode, Practices, SubscriptionHistory, Op, PracticeSettings, Settings } = await connectToDatabase();
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id, is_deleted: { [Op.not]: true }, billing_type: 'limited_users_billing' } });

        /* Get Active Subscription */
        const subscriptionObj = await Subscriptions.findOne({ where: { practice_id: event.user.practice_id, plan_category: 'responding' } });
        const stripe_subscription_data = JSON.parse(subscriptionObj.stripe_subscription_data);
        if (subscriptionObj?.stripe_subscription_id && subscriptionObj?.stripe_subscription_id.startsWith('sub_')) {
            await stripe.subscriptions.del(stripe_subscription_data.id);
        }

        const propoundingSubscription = await Subscriptions.findOne({
            where: { practice_id: subscriptionObj.practice_id, plan_category: 'propounding' },
        });

        /* Remove Existing Subscription */
        if (propoundingSubscription?.stripe_subscription_id && propoundingSubscription.stripe_subscription_id.startsWith('sub_') && !practiceObject?.is_propounding_canceled) {
            await stripe.subscriptions.del(propoundingSubscription.stripe_subscription_id);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Subscriptions.' }),
        };
    }
}

const update = async (event) => {
    try {
        const { Subscriptions, Plans, DiscountHistory, Users, DiscountCode, Practices, SubscriptionHistory, Op, PracticeSettings, Settings } = await connectToDatabase();
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id, is_deleted: { [Op.not]: true }, billing_type: 'limited_users_billing' } });

        /* Get New Subscription Plan */
        const newSubscriptionPlan = await Plans.findOne({ where: { plan_id: input.plan_id, plan_category: 'responding', active: true } });
        if (!newSubscriptionPlan?.id) throw new HTTPError(400, 'Invalid plan details.please check.');

        /* Getting free user limit from Global settings */
        const global_settings_Obj = await Settings.findOne({ where: { key: 'global_settings' }, raw: true });
        const settings_value = JSON.parse(global_settings_Obj.value);
        if (!settings_value?.free_user_limit) throw new HTTPError(400, 'Users limit not found.please contact support.');

        /* Getting free user limit from Practice settings */
        const practice_settings_obj = await PracticeSettings.findOne({ where: { practice_id: event.user.practice_id }, raw: true });
        const Practice_settings_value = practice_settings_obj?.value ? JSON.parse(practice_settings_obj.value) : {};
        const free_user_limit = Practice_settings_value?.free_user_limit ? Practice_settings_value?.free_user_limit : settings_value?.free_user_limit;

        // find additional users exist.
        const additional_users_count = practiceObject?.license_count > free_user_limit ? parseInt(practiceObject?.license_count) - free_user_limit : 0;

        /* Get Active Subscription Details */
        const activeSubscriptionsObj = await Subscriptions.findAll({
            where: { practice_id: event.user.practice_id, plan_id: { [Op.not]: null } },
            order: [["createdAt", "ASC"]], raw: true
        });

        /* One Time Fees charges Here */
        if (practiceObject.one_time_activation_fee) {
            let response = await oneTimeActivationCharge(event);
            if (!response?.status) throw new HTTPError(400, response.message);
        }

        for (let i = 0; i < activeSubscriptionsObj.length; i++) {
            let planObject;
            let is_eligible_to_couponcode = false;
            let stripe_couponcode_obj;
            const subscriptionObj = activeSubscriptionsObj[i];
            const json_data = JSON.parse(subscriptionObj?.stripe_subscription_data);
            if (json_data?.cancel_at_period_end && subscriptionObj?.plan_category == 'propounding') continue;
            if (subscriptionObj?.plan_category == 'responding') {
                planObject = await Plans.findOne({ where: { plan_id: input.plan_id, plan_category: 'responding', active: true }, raw: true });
            } else {
                const propounding_plan = getPropoundingPlanType(newSubscriptionPlan?.plan_type);
                planObject = await Plans.findOne({ where: { plan_type: propounding_plan, active: true }, raw: true });
            }

            /* validate discount code */
            if (input?.discount_code) {
                const discountcode_obj = await validateDiscountCode(input?.discount_code);
                const is_eligible_for = discountcode_obj.plan_type.split(',');
                if (is_eligible_for.includes(planObject.plan_type)) is_eligible_to_couponcode = true;
                stripe_couponcode_obj = JSON.parse(discountcode_obj.stripe_obj);
            }

            /* Get Free Plans */
            const free_subscriptions_plan_obj = await getPlansByplanType('complimentary_plans', planObject?.plan_category, planObject?.plan_type);
            const free_subscriptions_stripe_price_id = free_subscriptions_plan_obj?.stripe_price_id;

            /* Get Addition plans */
            const additional_subscriptions_plan_obj = await getPlansByplanType('standard_plans', planObject?.plan_category, planObject?.plan_type);
            const additional_subscriptions_stripe_price_id = additional_subscriptions_plan_obj?.stripe_price_id;

            const metadata = {
                practice_id: event.user.practice_id,
                user_email: event.user.email,
                plan_id: planObject.plan_id,
                stripe_product_id: planObject.stripe_product_id,
                plan_name: planObject.name,
                billing_type: 'limited_users_billing',
                total_users: practiceObject?.license_count,
                additional_users: additional_users_count,
                free_users: free_user_limit,
                plan_type: subscriptionObj?.plan_category,
                date: new Date()
            };

            const rootUserSubscriptionObj = { customer: practiceObject?.stripe_customer_id, ...createSubscriptionMetaObj(planObject.plan_id, 1, metadata, 'root_user_subscription') };
            const freeUserSubscriptionObj = createSubscriptionMetaObj(free_subscriptions_stripe_price_id, free_user_limit - 1, metadata, 'free_user_subscription');
            const additionalUserSubscriptionObj = createSubscriptionMetaObj(additional_subscriptions_stripe_price_id, additional_users_count, metadata, 'additional_user_subscription');

            // await stripe.subscriptions.del(subscriptionObj.stripe_subscription_id);

            /* Update discount code */
            if (input?.discount_code && stripe_couponcode_obj?.id && new Date(practiceObject.createdAt).getTime() >= new Date(global_settings_Obj.new_pricings_from).getTime() && is_eligible_to_couponcode) {
                rootUserSubscriptionObj.coupon = stripe_couponcode_obj.id;
                additionalUserSubscriptionObj.coupon = stripe_couponcode_obj.id;
            }

            const subscription_response = await subscripeNewPlans(rootUserSubscriptionObj, freeUserSubscriptionObj, additionalUserSubscriptionObj, additional_users_count);
            const updatedSubscriptionObj = await stripe.subscriptions.retrieve(subscription_response.id);
            const invoiceObj = updatedSubscriptionObj?.latest_invoice ? await stripe.invoices.retrieve(updatedSubscriptionObj?.latest_invoice) : undefined;
            const chargeObj = invoiceObj?.charge ? await stripe.charges.retrieve(invoiceObj?.charge) : undefined;

            const upcoming_invoice = await stripe.invoices.retrieveUpcoming({ subscription: subscription_response.id });
            const upcoming_invoice_price = parseFloat(upcoming_invoice?.total / 100);
            let current_invoice_price = upcoming_invoice_price;

            const subscriptionValidity = new Date(parseInt(subscription_response.current_period_end) * 1000);
            await Subscriptions.create({
                id: uuid.v4(), practice_id: event.user.practice_id,
                subscribed_by: event.user.id, subscribed_on: new Date(), plan_id: input.plan_id, stripe_subscription_id: subscription_response.id,
                stripe_subscription_data: JSON.stringify(updatedSubscriptionObj), stripe_product_id: planObject.stripe_product_id,
                subscribed_valid_till: subscriptionValidity, plan_category: planObject.plan_category, current_invoice_price, upcoming_invoice_price: upcoming_invoice_price,
                invoice_id: invoiceObj?.id || null, payment_attempt_count: invoiceObj?.attempt_count || null,
                charge_id: invoiceObj?.charge || null, charge_response: chargeObj?.outcome ? JSON.stringify(chargeObj?.outcome) : null
            });
            await Subscriptions.destroy({ where: { id: subscriptionObj.id } });
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Successfully processed the request',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Subscriptions.' }),
        };
    }
}

const subscripeNewPlans = async (rootUserSubscriptionObj, freeUserSubscriptionObj, additionalUserSubscriptionObj, additional_users_count) => {
    try {

        return new Promise((resolve) => {
            setTimeout(async () => {
                /* Create Root customer Subscription with amount */
                const subscription = await stripe.subscriptions.create(rootUserSubscriptionObj);
                if (subscription?.status !== 'active') {
                    await stripe.subscriptions.del(subscription.id);
                    throw new Error('Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
                }
                /* Create Free license users */
                await stripe.subscriptions.update(subscription.id, freeUserSubscriptionObj);

                /* Additional Users */
                if (additional_users_count) {
                    let additional_license_subscription = await stripe.subscriptions.update(subscription.id, additionalUserSubscriptionObj);
                    if (additional_license_subscription?.status !== 'active') {
                        await stripe.subscriptions.del(subscription.id);
                        throw new Error('Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
                    }
                }
                resolve(subscription);
            }, 2000);
        });
    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
}

const propoundingSubscription = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { Practices, Plans, Subscriptions, SubscriptionHistory, Op, PracticeSettings, Settings, Users, DiscountCode, DiscountHistory } = await connectToDatabase();
        /* Getting Practice Info */
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id, is_deleted: { [Op.not]: true }, billing_type: 'limited_users_billing' } });
        if (!practiceObject?.id) throw new HTTPError(400, `Invalid Practice Please check.`);
        if (!practiceObject?.stripe_customer_id) {
            throw new HTTPError(400, 'Please provide the credit card information on Credit Card Info settings and try again.');
        }

        const paymentMethods = await stripe.paymentMethods.list({
            customer: practiceObject.stripe_customer_id,
            type: 'card',
        });
        if (paymentMethods.data.length == 0) throw new HTTPError(400, 'This customer has no attached payment source or default payment method.');

        const dataObject = Object.assign(input, { id: uuid.v4(), practice_id: event.user.practice_id, paymentMethod_id: paymentMethods.data[0] });

        let is_eligible_to_couponcode = false;
        let stripe_couponcode_obj, discount_percentage, stripe_subscription_create_obj;
        let percentage = 1; //default percentage;

        /* Getting User Info */
        const usersobj = await Users.findOne({
            where: {
                practice_id: practiceObject.id, is_deleted: { [Op.not]: true }, role: 'lawyer', is_admin: true
            }, order: [['createdAt', 'ASC']], raw: true
        });
        /* Getting Propounding Plans */
        const propounding_plan_obj = await Plans.findOne({ where: { plan_id: dataObject.plan_id, active: true }, raw: true });
        if (!propounding_plan_obj?.id) throw new HTTPError(400, 'Invalid plan details.please check.');

        const { validity, price } = propounding_plan_obj;
        /* Getting Responding Subscription */
        const responding_subscription = await Subscriptions.findOne({
            where: { practice_id: event.user.practice_id, plan_category: 'responding' },
            order: [['createdAt', 'DESC']], logging: console.log, raw: true
        });
        const responding_subscription_stripeObj = JSON.parse(responding_subscription.stripe_subscription_data);
        /* Metadata */
        const metadata = {
            practice_id: event.user.practice_id,
            user_email: event.user.email,
            plan_id: dataObject.plan_id,
            stripe_product_id: propounding_plan_obj.stripe_product_id,
            plan_name: propounding_plan_obj.name,
            billing_type: 'limited_users_billing',
            plan_type: 'propounding',
            intent_for: 'propounding_subscription',
            date: new Date()
        };

        /* validate discount code */
        if (dataObject?.discount_code) {
            const discountcode_obj = await validateDiscountCode(dataObject?.discount_code);
            const is_eligible_for = discountcode_obj.plan_type.split(',');
            if (is_eligible_for.includes(propounding_plan_obj.plan_type)) {
                is_eligible_to_couponcode = true;
                stripe_couponcode_obj = JSON.parse(discountcode_obj.stripe_obj);
                /* Calculate Percentage */
                discount_percentage = discountcode_obj.discount_percentage;
                percentage = 1 - (discountcode_obj.discount_percentage / 100);
                metadata.discount_code_id = stripe_couponcode_obj?.id;
                metadata.discount_code_name = stripe_couponcode_obj?.name;
                metadata.discount_percentage_off = stripe_couponcode_obj?.percent_off;
            }
        }

        /* Getting free user limit from Global settings */
        const global_settings_Obj = await Settings.findOne({ where: { key: 'global_settings' }, raw: true });
        const settings_value = JSON.parse(global_settings_Obj.value);
        if (!settings_value?.free_user_limit) throw new HTTPError(400, 'Users limit not found.please contact support.');
        /* Getting free user limit from Practice settings */
        const practice_settings_obj = await PracticeSettings.findOne({ where: { practice_id: event.user.practice_id }, raw: true });
        const Practice_settings_value = practice_settings_obj?.value ? JSON.parse(practice_settings_obj.value) : {};
        const free_user_limit = Practice_settings_value?.free_user_limit ? Practice_settings_value?.free_user_limit : settings_value?.free_user_limit;

        //find additional users exist.
        const additional_users_count = practiceObject?.license_count > free_user_limit ? parseInt(practiceObject?.license_count) - free_user_limit : 0;

        /* Calculate Prorated days */
        const current_date = new Date();
        const responding_start_date = new Date(responding_subscription.subscribed_on);
        let days_difference = dateDiffInDays(current_date, responding_start_date);

        /* Get Free Plans */
        const free_subscriptions_plan_obj = await getPlansByplanType('complimentary_plans', propounding_plan_obj?.plan_category, propounding_plan_obj?.plan_type);
        const free_subscriptions_stripe_price_id = free_subscriptions_plan_obj?.stripe_price_id;

        /* Get Addition plans */
        const additional_subscriptions_plan_obj = await getPlansByplanType('standard_plans', propounding_plan_obj?.plan_category, propounding_plan_obj?.plan_type);
        const additional_subscriptions_price = additional_subscriptions_plan_obj?.price;
        const additional_subscriptions_stripe_price_id = additional_subscriptions_plan_obj?.stripe_price_id;


        let subscription_price = 0;

        if (days_difference > 0 && ['users_propounding_monthly_license_199', 'users_propounding_yearly_license_2149_20'].includes(propounding_plan_obj.plan_type)) {
            days_difference = validity - days_difference;
            //Root user discount
            base_price = Math.ceil((price / validity) * days_difference);
            discounted_price = subscription_price = Math.ceil(base_price * percentage);
            //additional users discount
            if (additional_users_count) {
                let additional_user_base_price = Math.ceil((additional_subscriptions_price / validity) * days_difference);
                let additional_user_subscription_price = Math.ceil(additional_user_base_price * percentage);
                discounted_price = subscription_price = Math.ceil(additional_user_subscription_price + subscription_price);
            }
        }

        metadata.total_users = practiceObject?.license_count;
        metadata.additional_users = additional_users_count;
        metadata.free_users = free_user_limit;
        let upcoming_invoice_price = 0;
        let current_invoice_price = 0;
        let invoiceObj;
        let chargeObj;
        console.log(metadata);
        if (days_difference == 0 && ['users_propounding_monthly_license_199', 'users_propounding_yearly_license_2149_20'].includes(propounding_plan_obj.plan_type)) {

            const rootUserSubscriptionObj = { customer: practiceObject?.stripe_customer_id, ...createSubscriptionMetaObj(dataObject.plan_id, 1, metadata, 'root_user_subscription') };
            const freeUserSubscriptionObj = createSubscriptionMetaObj(free_subscriptions_stripe_price_id, free_user_limit - 1, metadata, 'free_user_subscription');
            const additionalUserSubscriptionObj = createSubscriptionMetaObj(additional_subscriptions_stripe_price_id, additional_users_count, metadata, 'additional_user_subscription');

            /* Update discount code */
            if (is_eligible_to_couponcode && dataObject?.discount_code && stripe_couponcode_obj?.id) {
                rootUserSubscriptionObj.coupon = stripe_couponcode_obj.id;
                additionalUserSubscriptionObj.coupon = stripe_couponcode_obj.id;
            }

            /* Create Root customer Subscription with amount */
            const subscription = await stripe.subscriptions.create(rootUserSubscriptionObj);

            if (subscription.status !== 'active') {
                await stripe.subscriptions.del(subscription.id);
                throw new HTTPError(400, 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
            }

            /* Update Free license users */
            const free_license_subscription = await stripe.subscriptions.update(subscription.id, freeUserSubscriptionObj);

            /* Additional Users */
            if (additional_users_count) {
                await stripe.subscriptions.update(subscription.id, additionalUserSubscriptionObj);
            }
            /* If responding subscription is cancelled. have to cancel propounding subscription. */
            if (responding_subscription_stripeObj?.cancel_at_period_end) {
                await stripe.subscriptions.update(subscription.id, { cancel_at_period_end: true });
            }

            /* Getting latest subscription info */
            stripe_subscription_create_obj = subscription?.id ? await stripe.subscriptions.retrieve(subscription.id) : undefined;
            invoiceObj = subscription?.latest_invoice ? await stripe.invoices.retrieve(subscription?.latest_invoice) : undefined;
            chargeObj = invoiceObj?.charge ? await stripe.charges.retrieve(invoiceObj?.charge) : undefined;

            const upcoming_invoice = await stripe.invoices.retrieveUpcoming({ subscription: subscription.id });
            upcoming_invoice_price = parseFloat(upcoming_invoice?.total / 100);
            current_invoice_price = upcoming_invoice_price;
        } else {
            // free_user_limit,
            // additional_users_count
            upcoming_invoice_price = subscription_price;
            current_invoice_price = upcoming_invoice_price;
            let propounding_subscription_obj = {
                amount: parseInt(subscription_price * 100), currency: 'usd', customer: practiceObject.stripe_customer_id,
                payment_method: dataObject.paymentMethod_id.id, off_session: true, confirm: true, metadata
            }
            if (subscription_price != 0) {
                stripe_subscription_create_obj = await stripe.paymentIntents.create(propounding_subscription_obj);
            } else if (subscription_price == 0) {
                stripe_subscription_create_obj = Object.assign(propounding_subscription_obj, { id: 'pi_one_hundred_per_cent_discount', status: 'succeeded' });
            }
            if (stripe_subscription_create_obj && stripe_subscription_create_obj.status && stripe_subscription_create_obj.status === 'incomplete') {
                const subscriptionHistoryData = {
                    id: uuid.v4(), order_date: new Date(), practice_id: event.user.practice_id, user_id: usersobj.id,
                    subscribed_by: usersobj.name, price: parseInt(subscription_price),
                    plan_type: propounding_plan_obj.plan_type, subscribed_on: new Date(), plan_id: propounding_plan_obj.plan_id,
                    stripe_subscription_id: stripe_subscription_create_obj.id, subscription_valid_start: new Date(),
                    subscription_valid_till: responding_subscription.subscribed_valid_till, stripe_subscription_data: JSON.stringify(stripe_subscription_create_obj),
                    esquiretek_activity_type: 'NEW_SUBSCRIPTION', payment_type: 'New', stripe_product_id: propounding_plan_obj.stripe_product_id,
                    status: 'Payment failure', switched_plan: propounding_plan_obj.plan_type, latest_actvity: 'Subscription Creation',
                    payment_type: 'failure', esquiretek_activity_type: 'UPGRADE', plan_category: 'propounding'
                };
                await SubscriptionHistory.create(subscriptionHistoryData);
                const paymentIntent = await stripe.paymentIntents.cancel(paymentIntent.id);
                throw new HTTPError(400, 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
            }
        }
        /* Create Subscription Entry */
        let new_subscription_obj;
        if (stripe_subscription_create_obj?.id) {
            await Subscriptions.destroy({ where: { practice_id: event.user.practice_id, plan_id: null, stripe_subscription_id: null, plan_category: propounding_plan_obj.plan_category } });
            new_subscription_obj = await Subscriptions.create({
                id: uuid.v4(),
                practice_id: event.user.practice_id,
                subscribed_by: event.user.id,
                subscribed_on: new Date(),
                plan_id: input.plan_id,
                stripe_subscription_id: stripe_subscription_create_obj.id,
                stripe_subscription_data: JSON.stringify(stripe_subscription_create_obj),
                subscription_meta_data: '',
                stripe_product_id: propounding_plan_obj.stripe_product_id,
                subscribed_valid_till: responding_subscription.subscribed_valid_till,
                plan_category: propounding_plan_obj.plan_category,
                upcoming_invoice_price: upcoming_invoice_price,
                current_invoice_price: current_invoice_price,
                invoice_id: invoiceObj?.id || null,
                payment_attempt_count: invoiceObj?.attempt_count || null,
                charge_id: invoiceObj?.charge || null,
                charge_response: chargeObj?.outcome ? JSON.stringify(chargeObj?.outcome) : null
            });
            practiceObject.is_propounding_canceled = responding_subscription_stripeObj?.cancel_at_period_end ? true : false;
            await practiceObject.save();

            if (stripe_subscription_create_obj?.id.startsWith('pi_')) {
                const subscription_history_data = {
                    id: uuid.v4(), order_date: new Date(), practice_id: event.user.practice_id, user_id: usersobj.id,
                    subscribed_by: usersobj.name, price: parseInt(subscription_price),
                    plan_type: propounding_plan_obj.plan_type, subscribed_on: new Date(), plan_id: propounding_plan_obj.plan_id,
                    stripe_subscription_id: stripe_subscription_create_obj.id, subscription_valid_start: new Date(),
                    subscription_valid_till: responding_subscription.subscribed_valid_till, stripe_subscription_data: JSON.stringify(stripe_subscription_create_obj),
                    esquiretek_activity_type: 'NEW_SUBSCRIPTION', payment_type: 'New', stripe_product_id: propounding_plan_obj.stripe_product_id,
                    status: 'Success', switched_plan: propounding_plan_obj.plan_type, latest_actvity: 'Subscription Creation',
                    payment_type: 'New', esquiretek_activity_type: 'UPGRADE', plan_category: 'propounding'
                };

                let subscription_history_obj = await SubscriptionHistory.create(subscription_history_data);
                subscription_history_obj = subscription_history_obj.get({ plain: true });
                if (subscription_history_obj?.id && input.discount_code && is_eligible_to_couponcode) {
                    const plan_type = propounding_plan_obj.plan_type.split('_').slice(0, propounding_plan_obj.plan_type.length - 1);
                    const discount_for = plan_type.slice(0, plan_type.length - 1).join('_').toLowerCase();
                    const discount_amount = parseInt(base_price) - parseInt(discounted_price);
                    const discount_history = {
                        id: uuid.v4(), practice_id: event.user.practice_id, plan_category: 'propounding',
                        discount_for, promocode: input.discount_code, discount_percentage, base_price,
                        discounted_price, discount_amount, plan_type: propounding_plan_obj.plan_type
                    };
                    console.log(discount_history);
                    await DiscountHistory.create(discount_history);
                }
            }
            /* Email Template */
            let template = 'Hi';
            if (event?.user?.name) template = template + ' ' + event.user.name;
            let emailBody = template + ',<br><br>' +
                practiceObject.name.charAt(0).toUpperCase() + practiceObject.name.slice(1) + ' now has access to our propounding features. ' +
                'Thanks for signing up! We are excited to help speed up your propounding process.' + '<br><br>' +
                'If you have questions, we are here to help. Email us any time at support@esquiretek.com.<br><br>The EsquireTek Team.';

            let subject = 'Propounding Features Unlocked';
            await sendEmail(event.user.email, subject, emailBody, 'EsquireTek');
        }
        new_subscription_obj.stripe_subscription_data = JSON.parse(new_subscription_obj.stripe_subscription_data);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(new_subscription_obj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create propounding subscription.' }),
        };
    }
}

const cancelPropoundingSubscription = async (event) => {
    try {
        const { Practices, Op, Subscriptions, Users, Settings, PracticeSettings, Plans } = await connectToDatabase();
        const { practice_id } = event?.user;
        const params = event.params || event.pathParameters;
        const practice_object = await Practices.findOne({ where: { id: practice_id } });
        const users_object = await Users.findOne({
            where: { practice_id: practice_id, is_deleted: { [Op.not]: true } },
            order: [['createdAt', 'ASC']],
            logging: console.log
        });
        const propounding_subscription = await Subscriptions.findOne({
            where: { practice_id: practice_id, plan_category: 'propounding', id: params.id },
            raw: true
        });
        const plansObj = await Plans.findOne({ where: { plan_id: propounding_subscription.plan_id } });

        /* Getting free user limit from Global settings */
        const global_settings_Obj = await Settings.findOne({ where: { key: 'global_settings' }, raw: true });
        const settings_value = JSON.parse(global_settings_Obj.value);
        if (!settings_value?.free_user_limit) throw new HTTPError(400, 'Users limit not found.please contact support.');

        /* Getting free user limit from Practice settings */
        const practice_settings_obj = await PracticeSettings.findOne({ where: { practice_id: practice_object.id }, raw: true });
        const Practice_settings_value = practice_settings_obj?.value ? JSON.parse(practice_settings_obj.value) : {};
        const free_user_limit = Practice_settings_value?.free_user_limit ? Practice_settings_value?.free_user_limit : settings_value?.free_user_limit;

        //find additional users exist.
        const additional_users_count = practice_object?.license_count > free_user_limit ? parseInt(practice_object?.license_count) - free_user_limit : 0;

        const metadata = {
            total_users: practice_object?.license_count,
            plan_id: plansObj.plan_id,
            stripe_product_id: plansObj.stripe_product_id,
            plan_name: plansObj.name,
            billing_type: practice_object.billing_type,
            total_users: practice_object?.license_count,
            plan_type: plansObj?.plan_category,
            subscription_for: 'subscription_cancel',
            date: new Date()
        };

        if (event?.user?.practice_id && event?.user?.email) {
            metadata.practice_id = event.user.practice_id;
            metadata.user_email = event.user.email;
        }
        if (practice_object.billing_type == 'limited_users_billing') {
            metadata.additional_users = additional_users_count;
        }

        let endDate;
        if (propounding_subscription.stripe_subscription_id.startsWith('sub_')) {
            const subscription = await stripe.subscriptions.update(propounding_subscription.stripe_subscription_id, { cancel_at_period_end: true, metadata });
            endDate = unixTimeStamptoDate(subscription.cancel_at);
        } else {
            const subscription = new Date(propounding_subscription.subscribed_valid_till).getTime();
            endDate = unixTimeStamptoDate(subscription);
        }
        practice_object.is_propounding_canceled = true;
        await practice_object.save();
        let template = 'Hi';
        if (users_object?.name) { template = template + ' ' + users_object.name };
        const subject = `Your Propounding feature will be active till ${endDate}`;
        const message_body = template + ',<br><br>' +
            'We have canceled the auto-renewal for propounding feature based on your request. ' +
            'You will be able to use propounding feature till the end of the current subscription period ' + endDate + '.<br><br>' +
            'Thank you for trying out the propounding feature. We are always looking to improve EsquireTek ' +
            'if you have any feedback for us on how to improve our platform, or if this cancellation was in error, ' +
            'please email (support@esquiretek.com) with any comments or concerns.';
        await sendEmail(users_object.email, subject, message_body, 'EsquireTek');

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', message: 'Propounding subscriptions created successfully.' }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create propounding subscription.' }),
        };
    }
}

const totalLicenseCountMigration = async (event) => {
    try {
        const { Users, Practices, Op, Subscriptions, PracticeSettings, Settings } = await connectToDatabase();
        const practices = await Practices.findAll({
            where: { is_deleted: { [Op.not]: true } }
        });

        const promises = await practices.map(async (practice) => {
            const usersCount = await Users.count({
                where: { practice_id: practice.id, is_deleted: { [Op.not]: true } }
            });
            await Practices.update({ license_count: usersCount, billing_type: 'unlimited_users_billing' }, { where: { id: practice.id } })
        });
        await Promise.all(promises);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: "Ok" }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the subscriptionModelObject.' }),
        };
    }
}

const oneTimeActivationCharge = async (event) => {
    try {
        const { Practices, SubscriptionHistory, DiscountCode, Plans, DiscountHistory, Op } = await connectToDatabase();
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        if (!input.paymentMethod_id?.id) throw new Error('Payment method id not found.');

        let discount_history = {
            id: uuid.v4(), practice_id: event.user.practice_id, subscription_id: '', plan_category: 'responding',
            discount_for: 'activationFee', promocode: input.discount_code, discount_percentage: '', base_price: 249, discounted_price: '', discount_amount: ''
        }
        let discounted_price = 249;
        let is_eligible_for;
        /* Validate Discount code */
        const metadata = {
            practice_id: event.user.practice_id,
            user_id: event.user.practice_id,
            email_id: event.user.email,
            intent_for: 'activation_fee'
        }

        if (input.discount_code) {
            const discount_obj = await calculateOneTimeFeeDiscount(input.discount_code);
            discount_history.discounted_price = discount_obj?.discounted_price;
            discounted_price = discount_obj?.discounted_price;
            discount_history.discount_amount = discount_obj?.discount_amount;
            is_eligible_for = discount_obj?.is_eligible_for;
            /* Meta data updating */
            if (discount_obj?.id) metadata.discount_code_id = discount_obj?.id;
            if (discount_obj?.name) metadata.discount_code_name = discount_obj?.name;
            if (discount_obj?.percent_off) metadata.discount_percentage_off = discount_obj?.percent_off;

        }


        /* Create Payment Intent.If discount code is greater than zero. */
        if (discounted_price != 0) {
            let onetime_obj = {
                amount: parseInt(discounted_price * 100), currency: 'usd', customer: practiceObject.stripe_customer_id,
                payment_method: input.paymentMethod_id.id, off_session: true, confirm: true, metadata
            }
            console.log(onetime_obj);
            const paymentIntent = await stripe.paymentIntents.create(onetime_obj);

            if (paymentIntent && paymentIntent.status && paymentIntent.status === 'incomplete') {
                const paymentIntent = await stripe.paymentIntents.cancel(paymentIntent.id);
                await SubscriptionHistory.create({
                    id: uuid.v4(), order_date: new Date(), practice_id: event.user.practice_id,
                    user_id: event.user.id, subscribed_by: event.user.name, price: discounted_price, subscribed_on: new Date(),
                    plan_type: 'activationFee', payment_type: 'New', status: 'Failure', plan_category: 'responding'
                });
                throw new Error('Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
            }
        }
        const subscriptionHistoryData = {
            id: uuid.v4(), order_date: new Date(), practice_id: event.user.practice_id, user_id: event.user.id,
            subscribed_by: event.user.name, price: discounted_price, subscribed_on: new Date(), plan_type: 'activationFee', payment_type: 'New',
            status: 'Success', plan_category: 'responding'
        };
        /* Create Discount History */
        const subscriptionHistoryObj = await SubscriptionHistory.create(subscriptionHistoryData);
        if (input.discount_code && is_eligible_for.includes('activation_fee')) {
            await DiscountHistory.create(discount_history);
        }
        /* Update Practice Details */
        if (subscriptionHistoryObj) {
            practiceObject.one_time_activation_fee = false;
            practiceObject.discount_amount = 0;
            await practiceObject.save();
        }
        return { status: true, message: 'success' };
    } catch (err) {
        console.log(err);
        throw new Error('Could not subscribe one time charge. ' + err);
    }
};

const fetchUserLicenseBillingStatus = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { practice_id, license_count } = input;
        if (!license_count) throw new HTTPError(400, `Invalid license count.`);
        const { Practices, Users, Subscriptions, DiscountCode, Op, Settings, PracticeSettings, Plans } = await connectToDatabase();
        const practiceObject = await Practices.findOne({ where: { id: practice_id } });
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${practice_id} was not found`);
        if (practiceObject?.billing_type == 'unlimited_users_billing') throw new HTTPError(401, `Unauthorized.`);
        const active_subscriptions = await Subscriptions.findAll({
            where: { practice_id, plan_id: { [Op.not]: null }, stripe_subscription_id: { [Op.not]: null } },
            order: [['createdAt', 'ASC']],
            raw: true,
            logging: console.log
        });

        /* Getting free user limit from Global settings */
        const global_settings_Obj = await Settings.findOne({ where: { key: 'global_settings' }, raw: true });
        const settings_value = JSON.parse(global_settings_Obj.value);
        if (!settings_value?.free_user_limit) throw new HTTPError(400, 'Users limit not found.please contact support.');
        /* Getting free user limit from Practice settings */
        const practice_settings_obj = await PracticeSettings.findOne({ where: { practice_id }, raw: true });
        const Practice_settings_value = practice_settings_obj?.value ? JSON.parse(practice_settings_obj.value) : {};

        const free_user_limit = Practice_settings_value?.free_user_limit ? Practice_settings_value?.free_user_limit : settings_value?.free_user_limit;
        if (practiceObject?.license_count < free_user_limit) {
            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify({ status: 'Ok', message: 'free license available.' }),
            };
        }

        const discount_details = {};
        console.log(active_subscriptions.length);
        for (let i = 0; i < active_subscriptions.length; i++) {
            const subscription_obj = active_subscriptions[i];
            const subscription_stripe_obj = JSON.parse(subscription_obj.stripe_subscription_data)
            const planObject = await Plans.findOne({ where: { plan_id: subscription_obj.plan_id }, raw: true });
            const additional_subscriptions_plan_obj = await getPlansByplanType('standard_plans', planObject?.plan_category, planObject?.plan_type);

            let license_price = discount_license_price = additional_subscriptions_plan_obj.price;
            let discount_percentage = 0;
            let percent_off = 0;
            let prorated_status = false;
            let days_difference = 0;
            let prorated_license_price = 0;
            let prorated_discount_license_price = 0;
            let prorated_discount_license_total_price = 0;
            if (planObject?.plan_category == 'propounding') {
                const getRespondingSubscription = await Subscriptions.findOne({
                    where: { practice_id, plan_id: { [Op.not]: null }, stripe_subscription_id: { [Op.not]: null }, plan_category: 'responding', }
                });
                const current_date = new Date();
                const responding_start_date = new Date(getRespondingSubscription?.subscribed_on);
                days_difference = dateDiffInDays(current_date, responding_start_date);
            } else {
                const current_date = new Date();
                const responding_start_date = new Date(subscription_obj?.subscribed_on);
                days_difference = dateDiffInDays(current_date, responding_start_date);
            }

            let invoiceId;
            let invoice;
            let invoice_total;
            // console.log(JSON.stringify(subscription_stripe_obj));
            console.log(subscription_stripe_obj?.id);
            if (subscription_stripe_obj?.id.startsWith('sub_')) {
                if (subscription_stripe_obj?.discount?.coupon?.percent_off) percent_off = subscription_stripe_obj?.discount?.coupon?.percent_off;
                invoiceId = subscription_stripe_obj.latest_invoice;
                invoice = await stripe.invoices.retrieve(invoiceId);
                invoice_total = (parseFloat(invoice?.total) / 100).toFixed(2);
                invoice_total = parseFloat(invoice_total);
            } else if (subscription_stripe_obj?.id.startsWith('pi_')) {
                if (subscription_stripe_obj?.metadata?.discount_percentage_off) percent_off = subscription_stripe_obj?.metadata?.discount_percentage_off;
                invoice_total = (parseFloat(subscription_stripe_obj?.amount_received) / 100).toFixed(2);
                invoice_total = parseFloat(invoice_total);
            }


            let total_license_price = discount_license_price * license_count;
            if (percent_off) {
                discount_percentage = 1 - (percent_off / 100);
                discount_license_price = (parseFloat(additional_subscriptions_plan_obj.price) * discount_percentage).toFixed(2);
                discount_license_price = parseFloat(discount_license_price);
                total_license_price = discount_license_price * license_count;
            }
            let grand_total = parseFloat(invoice_total + total_license_price).toFixed(2);
            grand_total = parseFloat(grand_total);
            discount_details[subscription_obj.plan_category] = {
                previous_subscription_price: invoice_total,
                license_price,
                percent_off,
                license_count,
                discount_license_price,
                total_license_price,
                grand_total,
                previous_license_count: practiceObject.license_count
            }

            if (days_difference > 0) {
                prorated_discount_license_price = prorated_license_price = Math.ceil((parseInt(additional_subscriptions_plan_obj?.price) / parseInt(additional_subscriptions_plan_obj?.validity)) * (parseInt(additional_subscriptions_plan_obj?.validity) - days_difference));
                if (discount_percentage) prorated_discount_license_price = additional_subscriptions_plan_obj?.price * discount_percentage;
                prorated_discount_license_total_price = prorated_discount_license_price * license_count;
            }
            if (days_difference > 0) {
                discount_details[subscription_obj.plan_category].prorated_status = true;
                discount_details[subscription_obj.plan_category].prorated = {};
                discount_details[subscription_obj.plan_category].prorated.license_price = prorated_license_price;
                discount_details[subscription_obj.plan_category].prorated.day_difference = (parseInt(additional_subscriptions_plan_obj?.validity) - days_difference);
                discount_details[subscription_obj.plan_category].prorated.discount_license_price = prorated_discount_license_price;
                discount_details[subscription_obj.plan_category].prorated.discount_license_total_price = prorated_discount_license_total_price;
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(discount_details),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch user billing details.' }),
        };
    }
}

const fetchUserLicenseSubscriptionBilling = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { practice_id, plan_id, plan_category } = input;
        let discount_code = input?.discount_code;
        let is_first_subscription = false;
        let invoice_total = 0;
        let days_difference = 0;
        let discount_percentage = 0;
        const { Practices, Users, Subscriptions, DiscountCode, Op, Settings, PracticeSettings, Plans } = await connectToDatabase();
        const practiceObject = await Practices.findOne({ where: { id: practice_id } });
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${practice_id} was not found`);
        if (practiceObject?.billing_type == 'unlimited_users_billing') throw new HTTPError(401, `Unauthorized.`);

        /* Getting free user limit from Global settings */
        const global_settings_Obj = await Settings.findOne({ where: { key: 'global_settings' }, raw: true });
        const settings_value = JSON.parse(global_settings_Obj.value);
        if (!settings_value?.free_user_limit) throw new HTTPError(400, 'Users limit not found.please contact support.');
        /* Getting free user limit from Practice settings */
        const practice_settings_obj = await PracticeSettings.findOne({ where: { practice_id }, raw: true });
        const Practice_settings_value = practice_settings_obj?.value ? JSON.parse(practice_settings_obj.value) : {};
        const plansArr = [];
        /* Get Plans details */
        const planObject = await Plans.findOne({ where: { plan_id }, raw: true });
        plansArr.push(planObject.plan_type);
        console.log(plansArr);

        if (planObject.plan_type == 'users_responding_monthly_license_495') plansArr.push('users_propounding_monthly_license_199');
        if (planObject.plan_type == 'users_propounding_monthly_license_199') plansArr.push('users_responding_monthly_license_495');
        if (planObject.plan_type == 'users_responding_yearly_license_5100') plansArr.push('users_propounding_yearly_license_2149_20');
        if (planObject.plan_type == 'users_propounding_yearly_license_2149_20') plansArr.push('users_responding_yearly_license_5100');

        console.log(plansArr);

        let is_eligible_for = [];
        const billing_details = [];
        let responding_discount_code;
        let responding_discount_code_obj;
        if (plan_category == 'propounding') {
            const responding_subscription = await Subscriptions.findOne({
                where: { practice_id, plan_id: { [Op.not]: null }, stripe_subscription_id: { [Op.not]: null }, plan_category: 'responding', }
            });
            let subscription_stripe_Obj = responding_subscription;
            const subscriptionStripeObj = JSON.parse(subscription_stripe_Obj.stripe_subscription_data);
            console.log(subscriptionStripeObj?.discount?.coupon);
            responding_discount_code = subscriptionStripeObj?.discount?.coupon?.name || null;
            responding_discount_code_obj = responding_discount_code ? await validateDiscountCode(responding_discount_code) : {};
        }

        /* Get Discount Details */
        // let discount_percentage = 1;
        const discount_obj = discount_code ? await validateDiscountCode(discount_code) : {};
        if (discount_obj?.plan_type) {
            is_eligible_for = discount_obj.plan_type.split(',');
            let eligible_status = false;
            if (is_eligible_for.includes('activation_fee')) eligible_status = true;
            if (is_eligible_for.includes(planObject.plan_type)) eligible_status = true
            if (!eligible_status) throw new HTTPError(400, 'Invalid Discount code.');
        }
        let percent_off = 1;
        if (discount_obj?.discount_percentage && (is_eligible_for.includes(planObject.plan_type) || is_eligible_for.includes('activation_fee'))) {
            discount_percentage = discount_obj?.discount_percentage;
            percent_off = 1 - (discount_obj?.discount_percentage / 100);
        }
        /* Validate Activation Fee */
        if (is_eligible_for.includes('activation_fee')) {
            let actual_price = 249;
            let type = 'activation_fee';
            const discount_price = Math.ceil((percent_off * actual_price).toFixed(2)).toString();
            let discount_info = Object.assign({}, {
                type, actual_price, discount_price, percent_off: discount_percentage, actual_discount_price: discount_price
            });
            billing_details.push(discount_info);
        }
        for (let i = 0; i < plansArr.length; i++) {
            const response = {};

            let previous_invoice_total = 0;
            let isFirstSubscription = 0;
            let days_difference = 0;
            let discount_percentage = 0;

            /* Get Plans details */
            const planObject = await Plans.findOne({ where: { plan_type: plansArr[i] }, raw: true, logging: console.log });
            if (!planObject) continue;
            if (is_eligible_for.includes(planObject?.plan_type)) {
                percent_off = 1 - (discount_obj?.discount_percentage / 100)
                discount_percentage = discount_obj?.discount_percentage;
            } else {
                percent_off = 1;
            }
            /* Check Active Subscription Details */
            const active_subscriptions = await Subscriptions.findOne({
                where: { practice_id, plan_id: { [Op.not]: null }, stripe_subscription_id: { [Op.not]: null }, plan_category: planObject?.plan_category },
                order: [['createdAt', 'ASC']],
                raw: true,
            });

            if (percent_off == 1 && planObject?.plan_category == 'responding' && responding_discount_code_obj?.id) {
                percent_off = 1 - (responding_discount_code_obj?.discount_percentage / 100);
                discount_percentage = responding_discount_code_obj?.discount_percentage;
            }

            if (planObject?.plan_category == 'propounding') {
                const getRespondingSubscription = await Subscriptions.findOne({
                    where: { practice_id, plan_id: { [Op.not]: null }, stripe_subscription_id: { [Op.not]: null }, plan_category: 'responding', }
                });
                const current_date = new Date();
                const responding_start_date = new Date(getRespondingSubscription?.subscribed_on);
                days_difference = dateDiffInDays(current_date, responding_start_date);
            }

            if (active_subscriptions) {
                previous_invoice_total = await calculateInvoiceTotal(active_subscriptions);
            } else {
                isFirstSubscription = true;
            }


            let additional_user_status = false;
            let license_price = planObject.price;
            let additional_license_price = 0;
            let license_discount_price = planObject.price;
            let additional_license_discount_price = 0;
            let license_total_price = 0;
            let additional_license_total_price = 0;
            let prorated_status = false;
            let prorated_license_price = 0;
            let prorated_license_discount_price = 0;
            let prorated_additional_license_price = 0;
            let prorated_additional_license_discount_price = 0;
            let prorated_additional_license_total_price = 0;
            let plan_category = planObject?.plan_category;
            let additional_license_actual_price = 0;
            const plan_validity = planObject?.validity;

            if (isFirstSubscription) {
                previous_invoice_total = parseFloat(planObject?.price).toFixed(2);
                previous_invoice_total = parseFloat(previous_invoice_total);
            }
            /* Find additional license */
            const free_user_limit = Practice_settings_value?.free_user_limit || settings_value?.free_user_limit;
            const additional_license_count = parseInt(practiceObject?.license_count) - free_user_limit;

            license_discount_price = license_price * percent_off;
            license_total_price = license_discount_price; //root user license price. 

            response.previous_invoice_total = previous_invoice_total;
            response.license_price = license_price;
            response.percent_off = discount_percentage;
            response.plan_category = plan_category;
            response.plan_type = plansArr[i];
            response.plan_validity = plan_validity;
            response.license_discount_price = license_discount_price;
            response.license_total_price = license_total_price;
            response.free_license_count = free_user_limit;
            response.additional_license_count = additional_license_count;
            response.total_licese_count = practiceObject?.license_count;

            /* Calculate Additional license count */
            if (additional_license_count) {
                additional_user_status = true;
                const additional_license_plan_obj = await getPlansByplanType('standard_plans', planObject?.plan_category, planObject?.plan_type);
                additional_license_price = parseFloat(additional_license_plan_obj?.price).toFixed(2);
                additional_license_price = parseFloat(additional_license_price);
                additional_license_actual_price = additional_license_price * additional_license_count;
                additional_license_discount_price = additional_license_price * percent_off; //single liceense discount amount.
                additional_license_total_price = additional_license_discount_price * additional_license_count;
            }
            response.additional_user_status = additional_user_status;
            if (additional_user_status) {
                response.additional_user = {};
                response.additional_user.additional_license_price = additional_license_price;
                response.additional_user.additional_license_count = additional_license_count;
                response.additional_user.additional_license_actual_price = additional_license_actual_price;
                response.additional_user.additional_license_discount_price = additional_license_discount_price;
                response.additional_user.additional_license_total_price = additional_license_total_price;
            }

            // prorated only calculate for propounding subscriptions.
            /* Prorate License Price */
            if (days_difference > 0 && planObject?.plan_category === 'propounding') {
                prorated_status = true;
                prorated_license_price = Math.ceil((license_price / plan_validity) * (plan_validity - days_difference));
                prorated_license_discount_price = prorated_license_price * percent_off;
                if (additional_license_count) { // calculate prorate amount for additional license.
                    const additional_license_plan_obj = await getPlansByplanType('standard_plans', planObject?.plan_category, planObject?.plan_type);
                    prorated_additional_license_price = Math.ceil((additional_license_price / additional_license_plan_obj?.validity) * (additional_license_plan_obj?.validity - days_difference));
                    prorated_additional_license_discount_price = prorated_additional_license_price * percent_off; //single liceense discount amount.
                    prorated_additional_license_total_price = parseFloat(prorated_additional_license_discount_price * additional_license_count).toFixed(2);
                    prorated_additional_license_total_price = parseFloat(prorated_additional_license_total_price);
                }
            }

            response.prorated_status = prorated_status;
            if (prorated_status) {
                response.prorated = {};
                response.prorated.prorated_license_price = prorated_license_price;
                response.prorated.prorated_license_discount_price = prorated_license_discount_price;
                response.prorated.days_difference = days_difference;
                response.prorated.additional_license_count = additional_license_count;
                response.prorated.prorated_additional_license_price = prorated_additional_license_price;
                response.prorated.prorated_additional_license_discount_price = prorated_additional_license_discount_price;
                response.prorated.prorated_additional_license_total_price = prorated_additional_license_total_price;
            }

            let next_invoice = license_total_price + additional_license_total_price;
            next_invoice = next_invoice && parseFloat(next_invoice).toFixed(2) || next_invoice;
            next_invoice = parseFloat(next_invoice);
            let current_invoice = prorated_status ? prorated_license_discount_price + prorated_additional_license_total_price : license_total_price + additional_license_total_price;
            current_invoice = current_invoice && parseFloat(current_invoice).toFixed(2) || current_invoice;
            current_invoice = parseFloat(current_invoice);
            response.actual_price = parseFloat(license_price + additional_license_actual_price).toFixed(2);
            response.next_invoice = next_invoice;
            response.current_invoice = current_invoice;
            billing_details.push(response);
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(billing_details),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch user billing details.' }),
        };
    }
}

const calculateInvoiceTotal = async (subscription) => {
    try {
        const subscriptionData = JSON.parse(subscription.stripe_subscription_data || '{}');
        if (subscriptionData.id) {
            if (subscriptionData.id.startsWith('sub_')) {
                if (subscription.plan_id === subscriptionData.items.data[0].plan.id) {
                    const invoiceId = subscriptionData.latest_invoice;
                    const invoice = await stripe.invoices.retrieve(invoiceId);
                    return invoice.total / 100;
                }
            } else if (subscriptionData.id.startsWith('pi_')) {
                return subscriptionData.amount_received / 100;
            }
        }
        return null;
    } catch (err) {
        console.log(err);
        throw new Error('Could Not calculate previous invoice amount ' + err);
    }
}
module.exports.create = create;
module.exports.update = update;
module.exports.propoundingSubscription = propoundingSubscription;
module.exports.cancelPropoundingSubscription = cancelPropoundingSubscription;
module.exports.totalLicenseCountMigration = totalLicenseCountMigration;
module.exports.fetchUserLicenseBillingStatus = fetchUserLicenseBillingStatus;
module.exports.fetchUserLicenseSubscriptionBilling = fetchUserLicenseSubscriptionBilling;
module.exports.subscriptionDelete = subscriptionDelete;