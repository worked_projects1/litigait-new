const {STRING} = require("sequelize");

module.exports = (sequelize, type) => sequelize.define('SubscriptionHistory', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    order_date: type.DATE,
    practice_id: type.STRING,
    user_id: type.STRING,
    subscribed_by: type.STRING,
    price: type.STRING,
    subscribed_on: type.DATE,
    plan_id: type.STRING,
    plan_type: type.STRING, // New Subscription Plan
    stripe_subscription_id: type.STRING,
    subscription_valid_start: type.DATE,
    subscription_valid_till: type.DATE,
    cancel_at: type.DATE,
    cancel_at_period_end: type.BOOLEAN,
    canceled_at: type.DATE,
    esquiretek_activity_type: type.STRING,
    stripe_subscription_data: type.TEXT,
    stripe_product_id: type.STRING,
    status: type.STRING,
    payment_type: type.STRING,
    /* event_type: type.STRING, */
    switched_plan: type.STRING, // New Subscription Plan
    /* activity_type: type.STRING, */
    stripe_latest_activity: type.STRING,
    plan_category: type.STRING,
    is_reminder_send: type.BOOLEAN,
    license_count : type.INTEGER, // purchased license count
    active_users_count : type.INTEGER,
    billing_type : type.STRING
});
