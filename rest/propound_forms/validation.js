// propoundforms
const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');


exports.validateCreate = function (data) {

    const rules = {
        case_id: 'required|min:4|max:64',
        propound_template_id: 'min:4|max:64',
        file_name: 'required',
        document_type: 'required|min:3|max:64',
        questions: 'required'
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validategetAllQuestionByCaseId = function (data) {

    const rules = {
        case_id: 'required|min:4|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateUpdate = function (data) {

    const rules = {
        id: 'required|min:4|max:64',
        document_type: 'required',
        case_id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};
exports.validateDelete = function (data) {

    const rules = {
        id: 'required|min:4|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validategetQuestionByCaseIdAndDocumentType = function (data) {

    const rules = {
        id: 'required|min:4|max:64',
        case_id: 'required|min:4|max:64',
        document_type: 'required|min:3|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};