const uuid = require("uuid");
const phoneUtil =
  require("google-libphonenumber").PhoneNumberUtil.getInstance();
const connectToDatabase = require("../../db");
const { HTTPError } = require("../../utils/httpResp");
const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.REGION || "us-east-1" });
const s3 = new AWS.S3();
const { sendEmail, sendEmailwithCC } = require("../../utils/mailModule");
const { generateRandomString } = require('../../utils/randomStringGenerator');
const { documentTypes } = require('../../rest/helpers/documentType.helper');

const {
  validateCreate,
  validategetAllQuestionByCaseId,
  validateUpdate,
  validategetQuestionByCaseIdAndDocumentType,
  validateDelete,
} = require("./validation");
const { respondingPlans, respondingYearlyPlans, respondingMonthlyPlans, propoundingMonthlyPlans,
  propoundingPlans, propoundingYearlyPlans
} = require('../helpers/plans.helper');

const create = async (event) => {
  try {
    const { PropoundForms, Op } = await connectToDatabase();
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    // validateCreate(input);
    const questions = input.questions;
    const questions_count = questions.length;
    if (!questions || questions_count == 0)
      throw new HTTPError(404, `Questions not found`);

    for (i = 0; i < questions_count; i++) {
      questions[i].question_number_text = questions[i].question_number;
    }

    const dataObject = Object.assign(input, {
      id: uuid.v4(),
      questions: JSON.stringify(questions),
      number_of_questions: questions.length,
    });

    await PropoundForms.create(dataObject);
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        message: "Propound forms details saved successfully.",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "could not save propound forms details.",
      }),
    };
  }
};
const getAllQuestionByCaseId = async (event) => {
  try {
    const {
      PropoundTemplates,
      PropoundForms,
      Op,
      Cases,
      Practices,
      PracticesTemplates,
      FeeTerms,
      HipaaTerms,
      Clients,
      LegalForms,
    } = await connectToDatabase();

    const params = event.pathParameters || event.query;
    validategetAllQuestionByCaseId(params);
    const case_id = params.case_id;
    const practice_id = event.user.practice_id;

    const cases = await Cases.findOne({
      where: { id: case_id, practice_id, is_deleted: { [Op.not]: true } },
    });
    if (!cases)
      throw new HTTPError(404, `Cases with id: ${params.id} was not found`);
    const plainCase = cases.get({ plain: true });

    /*     const practiceTemplate = await PracticesTemplates.findOne({
          where: { practice_id: plainCase.practice_id },
          raw: true,
        });
    
        if (practiceTemplate) {
          if (practiceTemplate.modified_template_s3_file_key_frogs != null) {
            plainCase.modified_template_s3_file_key_frogs =
              practiceTemplate.modified_template_s3_file_key_frogs;
          }
          if (practiceTemplate.modified_template_s3_file_key_sprogs != null) {
            plainCase.modified_template_s3_file_key_sprogs =
              practiceTemplate.modified_template_s3_file_key_sprogs;
          }
          if (practiceTemplate.modified_template_s3_file_key_rfpd != null) {
            plainCase.modified_template_s3_file_key_rfpd =
              practiceTemplate.modified_template_s3_file_key_rfpd;
          }
          if (practiceTemplate.modified_template_s3_file_key_rfa != null) {
            plainCase.modified_template_s3_file_key_rfa =
              practiceTemplate.modified_template_s3_file_key_rfa;
          }
          plainCase.modified_template_status = practiceTemplate.status;
        } */
    const practiceDetails = await Practices.findOne({
      where: {
        id: plainCase.practice_id,
      },
    });
    if (!plainCase.fee_terms) {
      const feetermsObject = await FeeTerms.findOne({
        where: {
          practice_id: plainCase.practice_id,
        },
      });
      if (feetermsObject && feetermsObject.terms_text) {
        plainCase.fee_terms = feetermsObject.terms_text;
      }
    }
    plainCase.hipaa_terms = "";
    if (plainCase.practice_id) {
      const hipaatermsObject = await HipaaTerms.findOne({
        where: {
          practice_id: plainCase.practice_id,
        },
      });
      if (hipaatermsObject) {
        plainCase.hipaa_terms = hipaatermsObject.terms_text;
      }
    }

    plainCase.phone = "";
    if (plainCase.client_id) {
      const clientDetails = await Clients.findOne({
        where: {
          id: plainCase.client_id,
        },
      });
      if (clientDetails) {
        plainCase.client_phone = clientDetails.phone;
        plainCase.client_name = clientDetails.name;
      }
    }

    if (plainCase.opposing_counsel) {
      plainCase.opposing_counsel = JSON.parse(plainCase.opposing_counsel);
    }

    if (plainCase.propounder_opposing_counsel) {
      plainCase.propounder_opposing_counsel = JSON.parse(
        plainCase.propounder_opposing_counsel
      );
    }

    const propoundFormsObj = await PropoundForms.findAll({
      where: { case_id, practice_id, is_deleted: { [Op.not]: true }, },
      raw: true,
      logging: console.log,
    });
    const propoundTemplateId = await PropoundTemplates.findAll({
      where: {
        practice_id,
        is_deleted: { [Op.not]: true },
        questions: { [Op.not]: null },
        state: plainCase.state,
      },
      order: [["createdAt", "DESC"]],
      raw: true,
    });
    const s3BucketParams = { Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS, Expires: 60 * 60 * 1 };

    const FromsArr = await documentTypes({ state: cases.state, outputType: 'array', letterCase: 'small' });
    const FromsDocTypes = await documentTypes({ state: cases.state, outputType: 'types' });

    for (let i = 0; i < propoundFormsObj.length; i++) {
      const propoundRow = propoundFormsObj[i];
      propoundRow.attach_documents = JSON.parse(propoundRow.attach_documents);
      propoundRow.public_url = "";
      if (propoundRow.s3_file_key) {
        s3BucketParams.Key = propoundRow.s3_file_key;
        propoundRow.public_url = await s3.getSignedUrlPromise("getObject", {
          Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
          Expires: 60 * 60 * 1,
          Key: propoundRow.s3_file_key,
        });
      }
      if (propoundRow.pdf_s3_file_key) {
        s3BucketParams.Key = propoundRow.pdf_s3_file_key;
        propoundRow.pdf_file_url = await s3.getSignedUrlPromise("getObject", s3BucketParams);
      }
      propoundRow.questions = JSON.parse(propoundRow.questions);
      propoundRow.email_sent_to = JSON.parse(propoundRow.email_sent_to);

      for (let j = 0; j < FromsDocTypes.length; j++) {
        if (propoundRow.document_type == FromsDocTypes[j]) {
          FromsArr[FromsDocTypes[j].toLowerCase()].push(propoundRow);
        }
      }
    }
    const dataObject = Object.assign(plainCase, {
      propounding_form_details: FromsArr,
      practice_name: practiceDetails.name,
    });
    const FromsTemplateArr = await documentTypes({ state: cases.state, outputType: 'array', letterCase: 'small' });


    for (let i = 0; i < propoundTemplateId.length; i++) {
      const propoundRow = propoundTemplateId[i];
      propoundRow.questions = JSON.parse(propoundRow.questions);
      for (let j = 0; j < FromsDocTypes.length; j++) {
        if (propoundRow.document_type == FromsDocTypes[j]) {
          FromsTemplateArr[FromsDocTypes[j].toLowerCase()].push(propoundRow);
        }
      }
    }
    const finalObject = Object.assign(dataObject, {
      propound_template_details: FromsTemplateArr,
    });
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(finalObject),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "could not fetch propound details.",
      }),
    };
  }
};
const update = async (event) => {
  try {
    const { PropoundForms, Op } = await connectToDatabase();
    console.log(event);
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const questions = input.questions;
    const queryParams = event.pathParameters || event.params;
    const questions_count = questions.length;
    if (!questions || questions_count == 0)
      throw new HTTPError(404, `Questions not found`);

    for (i = 0; i < questions_count; i++) {
      questions[i].question_number_text = questions[i].question_number;
    }
    // validateUpdate(queryParams);
    const propoundFormsObj = await PropoundForms.findOne({
      where: {
        id: queryParams.id,
        practice_id: event.user.practice_id,
        document_type: queryParams.document_type,
        case_id: queryParams.case_id,
        is_deleted: { [Op.not]: true },
      },
    });
    if (!propoundFormsObj)
      throw new HTTPError(
        404,
        `Propound forms id: ${queryParams.id} was not found`
      );
    propoundFormsObj.is_deleted = true;
    await propoundFormsObj.save();

    const dataObject = Object.assign(input, {
      id: uuid.v4(),
      questions: JSON.stringify(questions),
      number_of_questions: questions.length,
    });

    const newPropoundFormsDetails = await PropoundForms.create(dataObject);
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(newPropoundFormsDetails),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "could not fetch propound forms details.",
      }),
    };
  }
};
const getQuestionByCaseIdAndDocumentType = async (event) => {
  try {
    const { PropoundForms, Op } = await connectToDatabase();
    const params = event.params || event.pathParameters;
    const practice_id = event.user.practice_id;
    // validategetQuestionByCaseIdAndDocumentType(params);
    const propoundFormsObj = await PropoundForms.findOne({
      where: {
        id: params.id,
        practice_id,
        case_id: params.case_id,
        document_type: params.document_type,
      },
      raw: true,
    });
    propoundFormsObj.questions = JSON.parse(propoundFormsObj.questions);
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(propoundFormsObj),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "could not save propound forms details.",
      }),
    };
  }
};
const destroy = async (event) => {
  try {
    const { PropoundForms, Op } = await connectToDatabase();
    console.log(event);
    const queryParams = event.pathParameters || event.params;
    // validateDelete(queryParams);
    const propoundFormsObj = await PropoundForms.findOne({
      where: { id: queryParams.id, practice_id: event.user.practice_id },
    });
    if (!propoundFormsObj)
      throw new HTTPError(
        404,
        `Propound forms id: ${queryParams.id} was not found`
      );
    await propoundFormsObj.destroy();
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "Ok",
        message: "Propound form details deleted successfully.",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "could not delete propound forms details.",
      }),
    };
  }
};

const sendPropoundFormsToResponderOld = async (event) => {
  try {
    const { PropoundForms, PropoundResponder, Practices, Users, Cases, Op } =
      await connectToDatabase();
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    /* 1.check Email ids exists in other practice or new one */
    const propounder_opposing_counsel_arr = input.propounder_opposing_counsel;
    let roles = ["manager", "medicalExpert", "superAdmin", "QualityTechnician"];
    for (let i = 0; i < propounder_opposing_counsel_arr.length; i++) {
      let opposing_counsel_email_str =
        propounder_opposing_counsel_arr[i].opposing_counsel_email;
      let opposing_counsel_email_arr = opposing_counsel_email_str.split(",");
      if (opposing_counsel_email_arr.length === 0)
        throw new HTTPError(400, "No email details found");
      const check_emailIdsExists = await Users.findAll({
        where: {
          email: { [Op.in]: opposing_counsel_email_arr },
          is_deleted: { [Op.not]: true },
        },
        logging: console.log,
        raw: true,
      });
      /* if email ids are present in exist practice have to check all the email ids are belongs to the same practice or different practice.
          check given email ids are not a superadmin users
      */
      if (check_emailIdsExists.length > 0) {
        check_emailIdsExists.map((row) => {
          if (roles.includes(row.role)) {
            throw new HTTPError(500, `Responder Email id is invalid.`);
          } else {
            return row.role;
          }
        });
        let hasExistInOtherPractice = false;
        check_emailIdsExists
          .map((row) => row.practice_id)
          .sort()
          .sort((a, b) => {
            if (a != b) hasExistInOtherPractice = true;
          });
        const practiceObject = await Practices.findOne({
          where: {
            id: check_emailIdsExists[0].practice_id,
            is_deleted: { [Op.not]: true },
          },
        });
        propounder_opposing_counsel_arr[i].opposing_counsel_practice_id =
          check_emailIdsExists[0].practice_id;
        propounder_opposing_counsel_arr[i].opposing_counsel_office_name =
          practiceObject.name;
        propounder_opposing_counsel_arr[i].is_new_user = false;
      } else {
        propounder_opposing_counsel_arr[i].is_new_user = true;
      }

      const propound_forms_obj = await PropoundForms.findOne({
        where: {
          id: input.propound_form_id,
          practice_id: input.propounder_practice_id,
          case_id: input.propounder_case_id,
        },
      });

      const token_id = uuid.v4();
      const questions = JSON.parse(
        propound_forms_obj.generated_document_questions
      );

      const dataObject = Object.assign(
        {},
        {
          id: uuid.v4(),
          propound_form_id: input.propound_form_id,
          responder_email:
            propounder_opposing_counsel_arr[i].opposing_counsel_email,
          responder_user_name:
            propounder_opposing_counsel_arr[i].opposing_counsel_attorney_name,
          document_type: input.document_type,
          questions: JSON.stringify(questions),
          number_of_questions_sent: questions.length,
          token_id: token_id,
          propounder_practice_id: input.propounder_practice_id,
          propounder_case_id: input.propounder_case_id,
          is_new_user: propounder_opposing_counsel_arr[i].is_new_user,
          is_template_used_by_responder: false,
        }
      );
      if (propounder_opposing_counsel_arr[i].opposing_counsel_practice_id)
        dataObject.responder_practice_id =
          propounder_opposing_counsel_arr[i].opposing_counsel_practice_id;
      if (propounder_opposing_counsel_arr[i].opposing_counsel_office_name)
        dataObject.responder_practice_name =
          propounder_opposing_counsel_arr[i].opposing_counsel_office_name;

      // if(!propounder_opposing_counsel_arr[i].is_new_user && propounder_opposing_counsel_arr[i].opposing_counsel_practice_id){

      //     const checkRespondersHaveCaseID = await PropoundResponder.findOne({
      //         where: {
      //             propounder_practice_id: dataObject.propounder_practice_id,
      //             propounder_case_id: dataObject.propounder_case_id,
      //             responder_practice_id: propounder_opposing_counsel_arr[i].opposing_counsel_practice_id,
      //             is_template_used_by_responder : true
      //         }, raw: true,
      //         });
      //         if(checkRespondersHaveCaseID && checkRespondersHaveCaseID.responder_case_id) dataObject.responder_case_id = checkRespondersHaveCaseID.responder_case_id;

      // }

      const propoundResponderObj = await PropoundResponder.create(dataObject);
      const propoundResponderPlainText = propoundResponderObj.get({
        plain: true,
      });

      if (propound_forms_obj && !propound_forms_obj.s3_file_key) {
        await PropoundResponder.destroy({
          where: { id: propoundResponderPlainText.id },
        });
        throw new HTTPError(400, "Documet details not found.");
      }
      propound_forms_obj.number_of_questions_sent = questions.length;
      propound_forms_obj.email_sent_date = new Date();
      await propound_forms_obj.save();

      const public_url = await s3.getSignedUrlPromise("getObject", {
        Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
        Expires: 60 * 60 * 1,
        Key: propound_forms_obj.s3_file_key,
      });

      const propounder_practice = await Practices.findOne({
        where: {
          is_deleted: { [Op.not]: true },
          id: input.propounder_practice_id,
        },
        raw: true,
      });

      const attachments = [
        {
          filename: "Propounding Template.docx",
          href: public_url, // URL of document save in the cloud.
          contentType:
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        },
      ];
      const propound_user_details = await Users.findOne({
        where: { id: event.user.id, is_deleted: { [Op.not]: true } },
        raw: true,
      });
      const propounder_casesObj = await Cases.findOne({
        where: { id: input.propounder_case_id },
        is_deleted: { [Op.not]: true },
        logging: console.log,
        raw: true,
      });

      let questionsUrl = `<a href="${process.env.APP_URL}/respond/?token_id=${token_id}">EsquireTek</a>`;

      let cc_email = undefined;
      if (input.cc_propounder_email) cc_email = input.cc_propounder_email;

      let to_email_string = opposing_counsel_email_arr;

      let practice_address = propounder_practice.name + ",";
      if (propounder_practice.address)
        practice_address += "<br>" + propounder_practice.address;
      if (propounder_practice.street)
        practice_address += "<br>" + propounder_practice.street;
      if (propounder_practice.city)
        practice_address += "<br>" + propounder_practice.city + ",";
      if (propounder_practice.state)
        practice_address += "&nbsp;" + propounder_practice.state;
      if (propounder_practice.zip_code)
        practice_address += "&nbsp;" + propounder_practice.zip_code;
      if (propounder_practice.phone)
        practice_address += "<br>" + "Telephone: " + propounder_practice.phone;
      if (propounder_practice.fax)
        practice_address += "<br>" + "Fax: " + propounder_practice.fax;
      if (propound_user_details.email)
        practice_address += "<br>" + "Email: " + propound_user_details.email;

      if (cc_email) {
        // / `Hello ${input.responder_user_name},<br>&nbsp;&nbsp;&nbsp;&nbsp;${propound_user_details.name} has sent you the propounding document for the case ${propounder_casesObj.case_title}. Please click this ${questionsUrl} to create a case and attach the questions from this document.<br><br>Notification from EsquireTek.`, /
        await sendEmailwithCC(
          to_email_string,
          cc_email,
          `Propounding doc for ${propounder_casesObj.case_title}`,
          `Hello,<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Please find attached for electronic service in the above referenced matter the below listed document: <ul><li>${propound_forms_obj.file_name}</li></ul>Please do not hesitate to contact me should you have any questions, comments or concerns.<br><br>Thank you.<br>${practice_address}`,
          propounder_practice.name,
          attachments
        );
      } else {
        // `Hello ${input.responder_user_name},<br>&nbsp;&nbsp;&nbsp;&nbsp;${propound_user_details.name} has sent you the propounding document for the case ${propounder_casesObj.case_title}. Please click this ${questionsUrl} to create a case and attach the questions from this document.<br><br>Notification from EsquireTek.`, /
        await sendEmail(
          to_email_string,
          `Propounding doc for ${propounder_casesObj.case_title}`,
          `Hello,<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Please find attached for electronic service in the above referenced matter the below listed document: <ul><li>${propound_forms_obj.file_name}</li></ul>Please do not hesitate to contact me should you have any questions, comments or concerns.<br><br>Thank you.<br>${practice_address}`,
          propounder_practice.name,
          attachments
        );
      }
    }

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "Ok",
        message: "Email send successfully.",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error:
          err.message || "could not send propound forms details to responder.",
      }),
    };
  }
};
const saveS3FileKey = async (event) => {
  try {
    const { PropoundForms, Op } = await connectToDatabase();
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const queryParams = event.pathParameters || event.params;
    // validateUpdate(queryParams);
    const propoundFormsObj = await PropoundForms.findOne({
      where: { id: queryParams.id },
    });
    if (!propoundFormsObj)
      throw new HTTPError(
        404,
        `Propound forms id: ${queryParams.id} was not found`
      );
    propoundFormsObj.s3_file_key = input.s3_file_key;
    const pdfS3FileKey = input?.pdf_s3_file_key;
    if (pdfS3FileKey) {
      propoundFormsObj.pdf_s3_file_key = pdfS3FileKey;
      propoundFormsObj.pdf_discovery_status = 'Completed';
      const pdfName = pdfS3FileKey.split('.pdf').join('').split('/').pop();
      propoundFormsObj.pdf_filename = pdfName;
    }
    const propoundSaveObj = await propoundFormsObj.save();
    propoundFormsObj.questions = JSON.parse(propoundFormsObj.questions);
    const plaintext = propoundSaveObj.get({ plain: true });
    plaintext.public_url = await s3.getSignedUrlPromise("getObject", {
      Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
      Expires: 60 * 60 * 1,
      Key: plaintext.s3_file_key,
    });
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(plaintext),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "could not fetch propound forms details.",
      }),
    };
  }
};

const sendPropoundFormsToResponder = async (event) => {
  try {
    const { PropoundForms, PropoundResponder, Practices, Users, Cases, Op } = await connectToDatabase();
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    /* 1.check Email ids exists in other practice or new one */
    const propounder_opposing_counsel_arr = input.propounder_opposing_counsel;
    let roles = ["manager", "medicalExpert", "superAdmin", "QualityTechnician", "operator"];

    const attach_documents = input.attach_documents;

    const propounder_casesObj = await Cases.findOne({
      where: { id: input.propounder_case_id }, is_deleted: { [Op.not]: true }, logging: console.log, raw: true,
    });


    if (attach_documents.length == 0) {
      await PropoundForms.update({ attach_documents: null }, {
        where: { id: input.attach_document_id, practice_id: input.propounder_practice_id, case_id: input.propounder_case_id, },
      });
    }

    if (attach_documents.length > 0) {
      await PropoundForms.update({ attach_documents: JSON.stringify(input.attach_documents) }, {
        where: { id: input.attach_document_id, practice_id: input.propounder_practice_id, case_id: input.propounder_case_id, },
      });
      for (let q = 0; q < attach_documents.length; q++) {
        let uploadfile = Object.assign({}, { filename: attach_documents[q].filename, href: attach_documents[q].public_url, contentType: attach_documents[q].contentType, })
        attachments.push(uploadfile);
      }
    }
    const email_verification_id = generateRandomString(12);
    const emailSentTo = [];
    for (let i = 0; i < propounder_opposing_counsel_arr.length; i++) {
      let opposing_counsel_email_str = propounder_opposing_counsel_arr[i].opposing_counsel_email;
      let opposing_counsel_email_arr = opposing_counsel_email_str.split(",");
      for (let j = 0; j < opposing_counsel_email_arr.length; j++) {
        emailSentTo.push(opposing_counsel_email_arr[j]);
      }
    }

    const emailSent = {
      emails: emailSentTo,
      cc_email: null,
    }
    let cc_email = undefined;
    if (input.cc_propounder_email) cc_email = emailSent.cc_email = input.cc_propounder_email;

    for (let i = 0; i < propounder_opposing_counsel_arr.length; i++) {
      let opposing_counsel_email_str = propounder_opposing_counsel_arr[i].opposing_counsel_email;
      let opposing_counsel_email_arr = opposing_counsel_email_str.split(",");
      if (opposing_counsel_email_arr.length === 0)
        throw new HTTPError(400, "No email details found");
      const check_emailIdsExists = await Users.findAll({
        where: { email: { [Op.in]: opposing_counsel_email_arr }, is_deleted: { [Op.not]: true } },
        logging: console.log, raw: true,
      });

      /* if email ids are present in exist practice have to check all the email ids are belongs to the same practice or different practice.*/
      if (check_emailIdsExists.length > 0) {
        check_emailIdsExists.map((row) => {
          if (roles.includes(row.role)) throw new HTTPError(500, `Responder Email id is invalid.`);
          else return row.role;
        });

        console.log(check_emailIdsExists);
        let hasExistInOtherPractice = false;

        check_emailIdsExists.map((row) => row.practice_id).sort().sort((a, b) => {
          if (a != b) hasExistInOtherPractice = true;
        });
        const practiceObject = await Practices.findOne({
          where: { id: check_emailIdsExists[0].practice_id, is_deleted: { [Op.not]: true }, }, logging: console.log
        });

        propounder_opposing_counsel_arr[i].opposing_counsel_practice_id =
          check_emailIdsExists[0].practice_id;
        propounder_opposing_counsel_arr[i].opposing_counsel_office_name =
          practiceObject.name;
        propounder_opposing_counsel_arr[i].is_new_user = false;
      } else {
        propounder_opposing_counsel_arr[i].is_new_user = true;
      }

      const token_id = uuid.v4();

      let email_template = "<ul>";
      if (input.selected_forms.length == 0) throw new HTTPError(400, "Propound forms id not provided");
      for (let j = 0; j < input.selected_forms.length; j++) {
        const propound_forms_obj = await PropoundForms.findOne({
          where: { id: input.selected_forms[j], practice_id: input.propounder_practice_id, case_id: input.propounder_case_id }, logging: console.log
        });
        if (!propound_forms_obj) throw new HTTPError(404, `Propound form with id: ${input.selected_forms[j]} was not found`);
        const questions = JSON.parse(propound_forms_obj.generated_document_questions);
        if (!questions) throw new HTTPError(404, "Questions not found");

        const dataObject = Object.assign({}, {
          id: uuid.v4(),
          propound_form_id: input.selected_forms[j],
          responder_email: propounder_opposing_counsel_arr[i].opposing_counsel_email,
          responder_user_name: propounder_opposing_counsel_arr[i].opposing_counsel_attorney_name,
          document_type: propound_forms_obj.document_type,
          questions: JSON.stringify(questions),
          number_of_questions_sent: questions.length,
          token_id: token_id,
          propounder_practice_id: input.propounder_practice_id,
          propounder_case_id: input.propounder_case_id,
          is_new_user: propounder_opposing_counsel_arr[i].is_new_user,
          is_template_used_by_responder: false,
        });

        if (propounder_opposing_counsel_arr[i].opposing_counsel_practice_id)
          dataObject.responder_practice_id = propounder_opposing_counsel_arr[i].opposing_counsel_practice_id;
        if (propounder_opposing_counsel_arr[i].opposing_counsel_office_name)
          dataObject.responder_practice_name = propounder_opposing_counsel_arr[i].opposing_counsel_office_name;

        const propoundResponderObj = await PropoundResponder.create(dataObject);
        const propoundResponderPlainText = propoundResponderObj.get({
          plain: true,
        });
        if (propound_forms_obj && !propound_forms_obj.s3_file_key) {
          await PropoundResponder.destroy({ where: { id: propoundResponderPlainText.id } });
          throw new HTTPError(400, "Document details not found.");
        }

        const Download_url = `${process.env.API_URL}/rest/propoundforms/email-document-download?id=${input.selected_forms[j]}&verification=${email_verification_id}`;
        console.log(Download_url);
        if (propound_forms_obj?.email_viewed_date) { propound_forms_obj.email_viewed_date = null; }
        propound_forms_obj.email_verification_id = email_verification_id;
        propound_forms_obj.number_of_questions_sent = questions.length;
        propound_forms_obj.email_sent_date = new Date();
        propound_forms_obj.email_sent_to = JSON.stringify(emailSent);
        const test = await propound_forms_obj.save();
        if (!propound_forms_obj?.pdf_filename) throw new HTTPError(400, "Pdf filename not found");

        email_template += `<li><a href=${Download_url}>${propound_forms_obj.pdf_filename}</a></li>`;
      }

      if (attach_documents?.length > 0) {
        email_template += "<br>Other Documents:<br><br>";
      }

      for (let q = 0; q < attach_documents.length; q++) {
        email_template += `<li><a href=${attach_documents[q]?.public_url}>${attach_documents[q]?.filename}</a></li>`;
      }

      email_template += "</ul>";

      const propounder_practice = await Practices.findOne({ where: { is_deleted: { [Op.not]: true }, id: input.propounder_practice_id, }, raw: true, });

      const propound_user_details = await Users.findOne({ where: { id: event.user.id, is_deleted: { [Op.not]: true } }, raw: true, });

      let to_email_string = opposing_counsel_email_arr;

      let practice_address = propounder_practice.name + ",";
      if (propounder_practice.street) practice_address += "<br>" + propounder_practice.street;
      if (propounder_practice.city) practice_address += "<br>" + propounder_practice.city + ",";
      if (propounder_practice.state) practice_address += "&nbsp;" + propounder_practice.state;
      if (propounder_practice.zip_code) practice_address += "&nbsp;" + propounder_practice.zip_code;
      if (propounder_practice.phone) {
        const Mobile = phoneUtil.parseAndKeepRawInput(propounder_practice.phone, "US");
        const MobileNumber = phoneUtil.formatInOriginalFormat(Mobile, "US");
        practice_address += "<br>" + "Telephone: " + MobileNumber;
      }
      if (propounder_practice.fax) {
        const Fax = phoneUtil.parseAndKeepRawInput(propounder_practice.fax, "US");
        const FaxNumber = phoneUtil.formatInOriginalFormat(Fax, "US");
        practice_address += "<br>" + "Fax: " + FaxNumber;
      }
      if (propound_user_details.email) practice_address += "<br>" + "Email: " + propound_user_details.email;

      let nameArr = propounder_opposing_counsel_arr[i].opposing_counsel_attorney_name.split(',');
      let opposingPractice = propounder_opposing_counsel_arr[i].opposing_counsel_office_name;

      console.log('ready to send email');
      const formsLength = input.selected_forms.length;

      if (cc_email) {
        await sendEmailwithCC(to_email_string, cc_email, `ELECTRONIC SERVICE - Propounding ${formsLength.length > 1 ? "docs" : "doc"} for ${propounder_casesObj.case_title}`,
          `Dear ${nameArr.length == 1 ? nameArr[0] : opposingPractice},<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Please click the link below to download the electronic service in the above referenced matter: ${email_template}Should you have any questions, please don't hesitate to contact me at the number listed below.<br><br>Sincerely,<br>${practice_address}`,
          propounder_practice.name
        );
      } else {
        await sendEmail(to_email_string, `ELECTRONIC SERVICE - Propounding ${formsLength.length > 1 ? "docs" : "doc"} for ${propounder_casesObj.case_title}`,
          `Dear ${nameArr.length == 1 ? nameArr[0] : opposingPractice},<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Please click the link below to download the electronic service in the above referenced matter: ${email_template}Should you have any questions, please don't hesitate to contact me at the number listed below.<br><br>Sincerely,<br>${practice_address}`,
          propounder_practice.name
        );
      }

    }



    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "Ok",
        message: "Email send successfully.",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error:
          err.message || "could not send propound forms details to responder.",
      }),
    };
  }
};

const saveServingAttorneyDetails = async (event) => {
  try {
    const { PropoundForms, Op } = await connectToDatabase();
    console.log(event);
    console.log("event--------------propound issue testing process");
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const queryParams = event.queryStringParameters || event.pathParameters || event.params;
    console.log(queryParams);
    const id = queryParams.id;
    if(!id) throw new HTTPError(404,"Propound forms id not provided");

    const propoundFormsObj = await PropoundForms.findOne({
      where: { id: id },
    });
    if (!propoundFormsObj) throw new HTTPError(404, `Propound forms id: ${id} was not found`);

    const dataObject = Object.assign({
      propounder_serving_attorney_name: input.propounder_serving_attorney_name,
      propounder_serving_attorney_street:
        input.propounder_serving_attorney_street,
      propounder_serving_attorney_city: input.propounder_serving_attorney_city,
      propounder_serving_attorney_state:
        input.propounder_serving_attorney_state,
      propounder_serving_attorney_zip_code:
        input.propounder_serving_attorney_zip_code,
      propounder_serving_attorney_email:
        input.propounder_serving_attorney_email,
      propounder_serving_date: input.propounder_serving_date,
    });

    await PropoundForms.update(dataObject, { where: { id: id } });

    const propoundFormsData = await PropoundForms.findOne({
      where: { id: id },
    });

    propoundFormsData.questions = JSON.parse(propoundFormsData.questions);

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(propoundFormsData),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "could not fetch propound forms details.",
      }),
    };
  }
};

const generatedDocumentQuestions = async (event) => {
  try {
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const { PropoundForms, Orders, Settings, Subscriptions, Plans, PracticeSettings, Clients, Practices } = await connectToDatabase();
    const generated_document_questions = input.generated_document_questions;
    const generated_document_questions_count = generated_document_questions.length;


    let plan_type;
    if (input.plan_type == 'free_trial') {
      plan_type = 'FREE TRIAL';
    } else if (input.plan_type == 'pay_as_you_go' || input.plan_type == 'tek_as_you_go') {
      plan_type = 'TEK AS-YOU-GO';
    } else if (respondingMonthlyPlans.includes(input.plan_type)) {
      plan_type = 'MONTHLY';
    } else if (respondingYearlyPlans.includes(input.plan_type)) {
      plan_type = 'YEARLY';
    } else if (propoundingMonthlyPlans.includes(input.plan_type)) {
      plan_type = 'MONTHLY PROPOUNDING';
    } else if (propoundingYearlyPlans.includes(input.plan_type)) {
      plan_type = 'YEARLY PROPOUNDING';
    } else if (['responding_vip', 'propounding_vip'].includes(input.plan_type)) {
      plan_type = 'VIP';
    }
    if (!plan_type) throw new HTTPError(400, `Plan Type not found.`);

    let planDetails = undefined;
    let validSubscriptionFeatures = [];
    const existingSubscriptionDetails = await Subscriptions.findOne({
      where: { practice_id: event.user.practice_id, plan_category: 'propounding' },
      order: [['createdAt', 'DESC']],
      raw: true
    });
    if (existingSubscriptionDetails && existingSubscriptionDetails.stripe_subscription_data && existingSubscriptionDetails.plan_id) {
      const stripeSubscriptionDataObject = JSON.parse(existingSubscriptionDetails.stripe_subscription_data);
      const subscriptionValidity = new Date(parseInt(stripeSubscriptionDataObject.current_period_end) * 1000);
      const today = new Date();
      if (subscriptionValidity > today) {
        planDetails = await Plans.findOne({
          raw: true,
          where: { stripe_product_id: existingSubscriptionDetails.stripe_product_id },
        });
        validSubscriptionFeatures = planDetails.features_included.split(',');
      }
    }

    const propoundFormsObj = await PropoundForms.findOne({
      where: {
        id: input.id,
        practice_id: event.user.practice_id,
        document_type: input.document_type,
        case_id: input.case_id,
      },
    });
    if (!propoundFormsObj)
      throw new HTTPError(404, `Propound forms id: ${input.id} was not found`);

    const ClientDetails = await Clients.findOne({
      where: { id: input.client_id },
    });
    const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
    if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

    const orderData = {
      id: uuid.v4(),
      order_date: new Date(),
      status: 'completed', // completed, pending
      practice_id: event.user.practice_id,
      user_id: event.user.id,
      case_id: input.case_id,
      client_id: input.client_id,
      document_type: input.document_type, // (FROGS, SPROGS, RFPD, RFA)
      document_generation_type: 'propound_doc', // template or final_doc
      amount_charged: 0,
      charge_id: '',
      case_title: input.case_title,
      client_name: ClientDetails.name,
      propoundforms_id: input.propoundforms_id,
      filename: propoundFormsObj.file_name,
      plan_type: plan_type,
      billing_type: practiceObject.billing_type || null
    };
    const settingsObject = await Settings.findOne({
      where: { key: 'global_settings' },
      raw: true,
    });

    if (!settingsObject) throw new HTTPError(400, 'Settings data not found');
    let settings = JSON.parse(settingsObject.value);

    const practiceSettings = await PracticeSettings.findOne({
      where: {
        practice_id: event.user.practice_id,
      },
      raw: true,
    });
    if (practiceSettings) {
      const practiceSettingsObject = JSON.parse(practiceSettings.value);
      settings = Object.assign(settings, practiceSettingsObject);
    }
    let price = 0;

    if (settings && settings.propound_doc) {
      price = settings.propound_doc;
    }
    if (validSubscriptionFeatures.includes('propound_doc')) {
      price = 0;
    }
    if (price <= 0) {
      const orderDataModel = await Orders.create(orderData);
      if (price < 0) {
        price = 0;
      }
      console.log('Order Data');
      console.log(orderDataModel);
    } else {
      if (!practiceDetails.stripe_customer_id) {
        throw new HTTPError(400, 'Free tire exceeded but payment method is not added yet');
      }
      const paymentMethods = await stripe.paymentMethods.list({
        customer: practiceDetails.stripe_customer_id,
        type: 'card',
      });
      if (!paymentMethods.data.length) {
        throw new HTTPError(400, 'Free tire exceeded but payment method not added');
      }

      if (price && price > 0 && price < 0.50) {
        price = 0.50;
      }
      const existOrderModel = await Orders.findAll({
        where: {
          status: 'completed',
          practice_id: orderData.practice_id,
          client_id: orderData.client_id,
          propoundforms_id: input.propoundforms_id,
          document_type: input.document_type,
          document_generation_type: 'propound_doc'
        }
      });

      if (existOrderModel.length != 0 || (planDetails && planDetails.plan_id != 'free_trial')) {
        const orderDataModel = await Orders.create(orderData);
      } else {
        const paymentMethod = paymentMethods.data[0];
        const paymentIntent = await stripe.paymentIntents.create({
          amount: parseInt(price * 100),
          currency: 'usd',
          customer: practiceDetails.stripe_customer_id,
          payment_method: paymentMethod.id,
          off_session: true,
          confirm: true,
        });
        orderData.charge_id = paymentIntent.id;
        orderData.amount_charged = price;
        const orderDataModel = await Orders.create(orderData);
        console.log('Order New Propound doc or other Data');
        console.log(orderData);
      }

    }

    if (input && generated_document_questions) {
      if (
        !generated_document_questions ||
        generated_document_questions_count == 0
      )
        throw new HTTPError(404, `Questions not found`);

      for (i = 0; i < generated_document_questions_count; i++) {
        generated_document_questions[i].question_number_text =
          generated_document_questions[i].question_number;
      }
      const dataObject = Object.assign({
        generated_document_questions: JSON.stringify(
          generated_document_questions
        ),
      });

      await PropoundForms.update(dataObject, {
        where: {
          id: input.id,
          practice_id: event.user.practice_id,
          document_type: input.document_type,
          case_id: input.case_id,
        },
      });
    }

    const propoundFormsObject = await PropoundForms.findOne({
      where: {
        id: input.id,
        practice_id: event.user.practice_id,
        document_type: input.document_type,
        case_id: input.case_id,
      },
      raw: true,
    });

    if (propoundFormsObject && propoundFormsObject.questions) {
      propoundFormsObject.questions = JSON.parse(propoundFormsObject.questions);
    }
    propoundFormsObj.pdf_discovery_status = 'new';
    await propoundFormsObj.save();
    
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(propoundFormsObject),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error:
          err.message ||
          "Could not fetch propound forms generated document questions details.",
      }),
    };
  }
};

const getUploadURL = async (input) => {

  let fileExtention = '';
  if (input.file_name) {
    fileExtention = `.${input.file_name.split('.').pop()}`;
  }

  const s3Params = {
    Bucket: process.env.S3_BUCKET_FOR_PROPOUND_ATTACHMENTS,
    Key: `${input.file_name}.${uuid.v4()}.${fileExtention}`,
    ContentType: input.content_type,
    ACL: 'public-read',
  };

  return new Promise((resolve, reject) => {
    const uploadURL = s3.getSignedUrl('putObject', s3Params);
    resolve({
      uploadURL,
      s3_file_key: s3Params.Key,
      public_url: `https://${s3Params.Bucket}.s3.amazonaws.com/${s3Params.Key}`,
    });
  });
};
const userUploadFile = async (event) => {
  try {
    const input = event.body;
    const uploadURLObject = await getUploadURL(input);
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        uploadURL: uploadURLObject.uploadURL,
        s3_file_key: uploadURLObject.s3_file_key,
        public_url: uploadURLObject.public_url,
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
    };
  }
};

const emailDocumentDownload = async (event) => {
  try {
    const params = event.query || event.params;
    const { PropoundForms } = await connectToDatabase();
    const email_verification_id = params?.verification;

    const PropoundFormsData = await PropoundForms.findOne({ where: { id: params?.id }, logging: console.log });

    if (!PropoundFormsData) throw new HTTPError(400, "Propoundforms not found");
    if (!PropoundFormsData?.pdf_s3_file_key) throw new HTTPError(400, "Pdf file not found");
    if (!PropoundFormsData?.email_viewed_date) PropoundFormsData.email_viewed_date = new Date();
    if (!PropoundFormsData?.email_verification_id) throw new HTTPError(400, "Propoundforms authorization id not found");
    if (PropoundFormsData?.email_verification_id != email_verification_id) throw new HTTPError(400, "Pdf Download url Expired");
    await PropoundFormsData.save();
    const public_url = await s3.getSignedUrlPromise("getObject", {
      Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
      Key: PropoundFormsData?.pdf_s3_file_key,
    });
    if (!public_url) throw new HTTPError(400, "Public URL not found");
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: public_url,
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ error: err.message || "unauthorised access to this file" }),
    };
  }

};

const updateFilename = async (event) => {
  try {

    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const { id, file_name, case_id, document_type } = input;
    const { PropoundForms } = await connectToDatabase();


    if (!id) throw new HTTPError(400, 'Propoundform id not provided');
    if (!file_name) throw new HTTPError(400, 'Propoundform id not provided');
    if (!document_type) throw new HTTPError(400, 'Propoundform id not provided');
    if (!case_id) throw new HTTPError(400, 'Propoundform id not provided');

    const existingPropoundForms = await PropoundForms.findAll({ where: { id, file_name, practice_id: event.user.practice_id, document_type, case_id } });

    if (existingPropoundForms.length) throw new HTTPError(404, `File Name  ${file_name} has been already existing in this Doument Type `);

    await PropoundForms.update({ file_name }, { where: { id } });

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ message: 'Filename has been updated successfully' }),
    };
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ error: err.message || "Filename update failed" }),
    };
  }
}
module.exports.create = create;
module.exports.getAllQuestionByCaseId = getAllQuestionByCaseId;
module.exports.update = update;
module.exports.destroy = destroy;
module.exports.getQuestionByCaseIdAndDocumentType =
  getQuestionByCaseIdAndDocumentType;
module.exports.sendPropoundFormsToResponder = sendPropoundFormsToResponder;
module.exports.saveS3FileKey = saveS3FileKey;
module.exports.saveServingAttorneyDetails = saveServingAttorneyDetails;
module.exports.generatedDocumentQuestions = generatedDocumentQuestions;
module.exports.userUploadFile = userUploadFile;
module.exports.emailDocumentDownload = emailDocumentDownload;
module.exports.updateFilename = updateFilename;
