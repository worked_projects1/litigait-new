const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const authMiddleware = require('../../auth');
const { validateArchiveCases, validateUnarchiveCases } = require('./validation');
const { QueryTypes } = require('sequelize');
const { documentTypes } = require('../../rest/helpers/documentType.helper');

const caseArchive = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        input.practice_id = event.user.practice_id;

        validateArchiveCases(input);
        const { Cases, Clients, Forms, LegalForms, Users, Op, CaseArchive, OtherPartiesForms } = await connectToDatabase();
        const casesObj = await Cases.findOne({
            where: { id: input.id, practice_id: input.practice_id, is_deleted: { [Op.not]: true }, is_archived: { [Op.not]: true } }
        });
        if (!casesObj) throw new HTTPError(404, `Could not archive this case.`);

        const casesArchiveObj = await CaseArchive.findOne({
            where: { case_id: casesObj.id, practice_id: input.practice_id, client_id: casesObj.client_id, is_deleted: { [Op.not]: true } }
        });
        if (casesArchiveObj) throw new HTTPError(400, `This case has already been archived.`);

        const legalFormsObj = await LegalForms.findAll({
            where: { case_id: casesObj.id, practice_id: casesObj.practice_id, client_id: casesObj.client_id },
            logging: console.log,
            order: [['createdAt', 'ASC']],
            raw: true
        });
        if (!legalFormsObj) throw new HTTPError(404, `Could not find legal form details for this case.`);

        const formsByType = await documentTypes({ state: casesObj.state, outputType: 'array', letterCase: 'small' });
        const otherPartiesFormsByType = await documentTypes({ state: casesObj.state, outputType: 'array', letterCase: 'small' });
        const stateDocumentTypes = await documentTypes({ state: casesObj.state, outputType: 'types', letterCase: 'small' });

        const responding_legalforms_id = [];
        const other_parties_legalforms_id = [];
        for (const legalForm of legalFormsObj) {
            const { practice_id, case_id, document_type, client_id } = legalForm;
            const otherPartiesFormsObj = await OtherPartiesForms.findAll({
                where: { practice_id: practice_id, case_id: case_id, legalforms_id: legalForm.id, document_type: document_type },
                order: [['createdAt', 'ASC']],
                raw: true
            });
            if (otherPartiesFormsObj.length !== 0) {
                const otherPartiesSortedQuestions = await questionsSort(otherPartiesFormsObj);
                otherPartiesFormsByType[legalForm.document_type.toLowerCase()].push({
                    legalforms_id: legalForm.id,
                    forms_questions: otherPartiesSortedQuestions
                });
                other_parties_legalforms_id.push(legalForm.id);
                // await OtherPartiesForms.destroy({
                //     where: { practice_id: casesObj.practice_id, case_id: casesObj.id, legalforms_id: legalForm.id }
                // });
            }
            const formsObj = await Forms.findAll({
                where: { practice_id, client_id, case_id, legalforms_id: legalForm.id, document_type },
                order: [['createdAt', 'ASC']],
                raw: true
            });
            if (formsObj.length !== 0) {
                const sortedQuestions = await questionsSort(formsObj);
                formsByType[legalForm.document_type.toLowerCase()].push({
                    legalforms_id: legalForm.id,
                    forms_questions: sortedQuestions
                });
                responding_legalforms_id.push(legalForm.id);
                // await Forms.destroy({
                //     where: { practice_id: casesObj.practice_id, client_id: casesObj.client_id, case_id: casesObj.id, legalforms_id: legalForm.id }
                // });
            }
        }


        const dataObject = {
            json_version: 'v1',
            case_id: casesObj.id,
            client_id: casesObj.client_id,
            practice_id: casesObj.practice_id,
            client_forms: formsByType,
            otherParties_forms: otherPartiesFormsByType
        };

        const archiveData = {
            id: uuid.v4(),
            practice_id: casesObj.practice_id,
            client_id: casesObj.client_id,
            case_id: casesObj.id,
            case_title: casesObj.case_title,
            case_number: casesObj.case_number,
            form_data: JSON.stringify(dataObject),
            state: casesObj.state
        };
        let is_arr_empty = false;
        if (responding_legalforms_id.length != 0) {
            let sumOfLength = 0;
            for (let i = 0; i < stateDocumentTypes.length; i++) {
                const formsLength = formsByType[stateDocumentTypes[i]].length;
                sumOfLength += sumOfLength + formsLength;
            }
            if (sumOfLength == 0) is_arr_empty = true;
        }
        otherPartiesFormsByType
        if (other_parties_legalforms_id.length != 0) {
            let sumOfLength = 0;
            for (let i = 0; i < stateDocumentTypes.length; i++) {
                const otherPartyformsLength = otherPartiesFormsByType[stateDocumentTypes[i]].length;
                sumOfLength += sumOfLength + otherPartyformsLength;
            }
            if (sumOfLength == 0) is_arr_empty = true;
        }

        const caseArchiveObj = await CaseArchive.findAll({
            where: { practice_id: casesObj.practice_id, client_id: casesObj.client_id, case_id: casesObj.id }
        });

        if (caseArchiveObj.length !== 0) {
            await CaseArchive.destroy({
                where: { practice_id: casesObj.practice_id, client_id: casesObj.client_id, case_id: casesObj.id }
            });
        }
        /* Destroy Forms Data */
        if (!is_arr_empty) {
            if (responding_legalforms_id.length) {
                await Forms.destroy({
                    where: { practice_id: casesObj.practice_id, client_id: casesObj.client_id, case_id: casesObj.id, legalforms_id: { [Op.in]: responding_legalforms_id } }
                });
            }

            if (other_parties_legalforms_id.length) {
                await OtherPartiesForms.destroy({
                    where: { practice_id: casesObj.practice_id, case_id: casesObj.id, legalforms_id: { [Op.in]: responding_legalforms_id } }
                });
            }
        }

        await CaseArchive.create(archiveData);
        casesObj.is_archived = true;
        await casesObj.save();



        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ message: "Case Archived Successfully.", status: "ok" }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not archive this case.' }),
        };
    }
};

const caseArchive_two = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        input.practice_id = event.user.practice_id;

        validateArchiveCases(input);
        const {
            Cases,
            Clients,
            Forms,
            LegalForms,
            Users,
            Op,
            CaseArchive,
            OtherPartiesForms
        } = await connectToDatabase();
        const casesObj = await Cases.findOne({
            where: {
                id: input.id,
                practice_id: input.practice_id,
                is_deleted: { [Op.not]: true },
                is_archived: { [Op.not]: true }
            }
        });
        if (!casesObj) throw new HTTPError(404, `could not archive this case.`);

        const casesArchiveObj = await CaseArchive.findOne({
            where: {
                case_id: casesObj.id,
                practice_id: input.practice_id,
                client_id: casesObj.client_id,
                is_deleted: { [Op.not]: true }
            }
        });
        if (casesArchiveObj) throw new HTTPError(400, `Already this Case was Archived.`);

        const legalFormsObj = await LegalForms.findAll({
            where: { case_id: casesObj.id, practice_id: casesObj.practice_id, client_id: casesObj.client_id },
            logging: console.log,
            order: [
                ['createdAt', 'ASC'],
            ],
            raw: true
        });
        if (!legalFormsObj) throw new HTTPError(404, `Count not find legalform deatails for this case.`);
        const FrogsArr = [];
        const SprogsArr = [];
        const RfpdArr = [];
        const RfaArr = [];
        const OtherPartiesFrogsArr = [];
        const OtherPartiesSprogsArr = [];
        const OtherPartiesRfpdArr = [];
        const OtherPartiesRfaArr = [];

        let formsObj = undefined;
        let otherPartiesFormsObj = undefined;

        for (let legalFormIndex = 0; legalFormIndex < legalFormsObj.length; legalFormIndex++) {
            /* OtherParties */
            /* Dont change the Other parties Forms and Forms position. */
            otherPartiesFormsObj = await OtherPartiesForms.findAll({
                where: {
                    practice_id: legalFormsObj[legalFormIndex].practice_id,
                    case_id: legalFormsObj[legalFormIndex].case_id,
                    legalforms_id: legalFormsObj[legalFormIndex].id,
                    document_type: legalFormsObj[legalFormIndex].document_type
                }, order: [
                    ['createdAt', 'ASC'],
                ], raw: true
            });
            if (otherPartiesFormsObj.length != 0) {
                const otherPartiesSortedQuestions = await questionsSort(otherPartiesFormsObj);
                const otherPartiesQuestionsArr = [];
                for (let OtherPartiesFormIndex = 0; OtherPartiesFormIndex < otherPartiesSortedQuestions.length; OtherPartiesFormIndex++) {
                    otherPartiesQuestionsArr[OtherPartiesFormIndex] = {
                        id: otherPartiesSortedQuestions[OtherPartiesFormIndex].id,
                        case_id: otherPartiesSortedQuestions[OtherPartiesFormIndex].case_id,
                        practice_id: otherPartiesSortedQuestions[OtherPartiesFormIndex].practice_id,
                        party_id: otherPartiesSortedQuestions[OtherPartiesFormIndex].party_id,
                        form_id: otherPartiesSortedQuestions[OtherPartiesFormIndex].form_id,
                        legalforms_id: otherPartiesSortedQuestions[OtherPartiesFormIndex].legalforms_id,
                        document_type: otherPartiesSortedQuestions[OtherPartiesFormIndex].document_type,
                        subgroup: otherPartiesSortedQuestions[OtherPartiesFormIndex].subgroup,
                        is_the_client_response_edited: otherPartiesSortedQuestions[OtherPartiesFormIndex].is_the_client_response_edited,
                        is_consultation_set: otherPartiesSortedQuestions[OtherPartiesFormIndex].is_consultation_set,
                        consultation_set_no: otherPartiesSortedQuestions[OtherPartiesFormIndex].consultation_set_no,
                        consultation_createdby: otherPartiesSortedQuestions[OtherPartiesFormIndex].consultation_createdby,

                        question_type: otherPartiesSortedQuestions[OtherPartiesFormIndex].question_type,
                        question_id: otherPartiesSortedQuestions[OtherPartiesFormIndex].question_id,
                        question_number_text: otherPartiesSortedQuestions[OtherPartiesFormIndex].question_number_text,
                        question_number: otherPartiesSortedQuestions[OtherPartiesFormIndex].question_number,
                        question_text: otherPartiesSortedQuestions[OtherPartiesFormIndex].question_text,
                        question_section_id: otherPartiesSortedQuestions[OtherPartiesFormIndex].question_section_id,
                        question_section: otherPartiesSortedQuestions[OtherPartiesFormIndex].question_section,
                        question_section_text: otherPartiesSortedQuestions[OtherPartiesFormIndex].question_section_text,
                        question_options: otherPartiesSortedQuestions[OtherPartiesFormIndex].question_options,

                        client_response_text: otherPartiesSortedQuestions[OtherPartiesFormIndex].client_response_text,
                        client_response_status: otherPartiesSortedQuestions[OtherPartiesFormIndex].client_response_status,

                        share_attorney_response: otherPartiesSortedQuestions[OtherPartiesFormIndex].share_attorney_response,
                        client_modified_response: otherPartiesSortedQuestions[OtherPartiesFormIndex].client_modified_response,
                        is_the_client_response_edited: otherPartiesSortedQuestions[OtherPartiesFormIndex].is_the_client_response_edited,

                        file_upload_status: otherPartiesSortedQuestions[OtherPartiesFormIndex].file_upload_status,
                        uploaded_documents: otherPartiesSortedQuestions[OtherPartiesFormIndex].uploaded_documents,
                        last_sent_to_client: otherPartiesSortedQuestions[OtherPartiesFormIndex].last_sent_to_client,
                        last_updated_by_client: otherPartiesSortedQuestions[OtherPartiesFormIndex].last_updated_by_client,

                        TargetLanguageCode: otherPartiesSortedQuestions[OtherPartiesFormIndex].TargetLanguageCode,
                        createdAt: otherPartiesSortedQuestions[OtherPartiesFormIndex].createdAt,
                        updatedAt: otherPartiesSortedQuestions[OtherPartiesFormIndex].updatedAt,
                    }
                }
                if (legalFormsObj[legalFormIndex].document_type == 'FROGS' || legalFormsObj[legalFormIndex].document_type == 'frogs') {
                    OtherPartiesFrogsArr[OtherPartiesFrogsArr.length] = {
                        legalforms_id: legalFormsObj[legalFormIndex].id,
                        forms_questions: otherPartiesQuestionsArr
                    }
                } else if (legalFormsObj[legalFormIndex].document_type == 'SPROGS' || legalFormsObj[legalFormIndex].document_type == 'sprogs') {
                    OtherPartiesSprogsArr[OtherPartiesSprogsArr.length] = {
                        legalforms_id: legalFormsObj[legalFormIndex].id,
                        forms_questions: otherPartiesQuestionsArr
                    }
                } else if (legalFormsObj[legalFormIndex].document_type == 'RFPD' || legalFormsObj[legalFormIndex].document_type == 'rfpd') {
                    OtherPartiesRfpdArr[OtherPartiesRfpdArr.length] = {
                        legalforms_id: legalFormsObj[legalFormIndex].id,
                        forms_questions: otherPartiesQuestionsArr
                    }
                } else if (legalFormsObj[legalFormIndex].document_type == 'RFA' || legalFormsObj[legalFormIndex].document_type == 'rfa') {
                    OtherPartiesRfaArr[OtherPartiesRfaArr.length] = {
                        legalforms_id: legalFormsObj[legalFormIndex].id,
                        forms_questions: otherPartiesQuestionsArr
                    }
                }
                await OtherPartiesForms.destroy({
                    where: {
                        practice_id: casesObj.practice_id,
                        case_id: casesObj.id,
                        legalforms_id: legalFormsObj[legalFormIndex].id
                    }
                });
            }

            /* Forms */
            formsObj = await Forms.findAll({
                where: {
                    practice_id: legalFormsObj[legalFormIndex].practice_id,
                    client_id: legalFormsObj[legalFormIndex].client_id,
                    case_id: legalFormsObj[legalFormIndex].case_id,
                    legalforms_id: legalFormsObj[legalFormIndex].id,
                    document_type: legalFormsObj[legalFormIndex].document_type
                }, order: [
                    ['createdAt', 'ASC'],
                ], raw: true
            });
            if (formsObj.length != 0) {
                const sortedQuestions = await questionsSort(formsObj);
                const questionsArr = [];
                for (let formsIndex = 0; formsIndex < sortedQuestions.length; formsIndex++) {
                    questionsArr[formsIndex] = {
                        id: sortedQuestions[formsIndex].id,
                        case_id: sortedQuestions[formsIndex].case_id,
                        practice_id: sortedQuestions[formsIndex].practice_id,
                        client_id: sortedQuestions[formsIndex].client_id,
                        legalforms_id: sortedQuestions[formsIndex].legalforms_id,
                        document_type: sortedQuestions[formsIndex].document_type,
                        subgroup: sortedQuestions[formsIndex].subgroup,
                        question_type: sortedQuestions[formsIndex].question_type,
                        question_id: sortedQuestions[formsIndex].question_id,
                        question_category: sortedQuestions[formsIndex].question_category,
                        question_category_id: sortedQuestions[formsIndex].question_category_id,
                        question_number_text: sortedQuestions[formsIndex].question_number_text,
                        question_number: sortedQuestions[formsIndex].question_number,
                        question_text: sortedQuestions[formsIndex].question_text,
                        question_section_id: sortedQuestions[formsIndex].question_section_id,
                        question_section: sortedQuestions[formsIndex].question_section,
                        question_section_text: sortedQuestions[formsIndex].question_section_text,
                        question_options: sortedQuestions[formsIndex].question_options,
                        duplicate_set_no: sortedQuestions[formsIndex].duplicate_set_no,
                        is_duplicate: sortedQuestions[formsIndex].is_duplicate,
                        is_consultation_set: sortedQuestions[formsIndex].is_consultation_set,
                        consultation_set_no: sortedQuestions[formsIndex].consultation_set_no,
                        consultation_createdby: sortedQuestions[formsIndex].consultation_createdby,
                        lawyer_response_text: sortedQuestions[formsIndex].lawyer_response_text,
                        lawyer_objection_text: sortedQuestions[formsIndex].lawyer_objection_text,
                        lawyer_response_status: sortedQuestions[formsIndex].lawyer_response_status,
                        lawyer_objection_status: sortedQuestions[formsIndex].lawyer_objection_status,
                        client_response_text: sortedQuestions[formsIndex].client_response_text,
                        client_response_status: sortedQuestions[formsIndex].client_response_status,
                        share_attorney_response: sortedQuestions[formsIndex].share_attorney_response,
                        client_modified_response: sortedQuestions[formsIndex].client_modified_response,
                        is_the_client_response_edited: sortedQuestions[formsIndex].is_the_client_response_edited,
                        file_upload_status: sortedQuestions[formsIndex].file_upload_status,
                        uploaded_documents: sortedQuestions[formsIndex].uploaded_documents,
                        last_sent_to_client: sortedQuestions[formsIndex].last_sent_to_client,
                        last_updated_by_lawyer: sortedQuestions[formsIndex].last_updated_by_lawyer,
                        last_updated_by_client: sortedQuestions[formsIndex].last_updated_by_client,
                        TargetLanguageCode: sortedQuestions[formsIndex].TargetLanguageCode,
                        createdAt: sortedQuestions[formsIndex].createdAt,
                        updatedAt: sortedQuestions[formsIndex].updatedAt,
                    }
                }
                if (legalFormsObj[legalFormIndex].document_type == 'FROGS' || legalFormsObj[legalFormIndex].document_type == 'frogs') {
                    FrogsArr[FrogsArr.length] = {
                        legalforms_id: legalFormsObj[legalFormIndex].id,
                        forms_questions: questionsArr
                    }
                } else if (legalFormsObj[legalFormIndex].document_type == 'SPROGS' || legalFormsObj[legalFormIndex].document_type == 'sprogs') {
                    SprogsArr[SprogsArr.length] = {
                        legalforms_id: legalFormsObj[legalFormIndex].id,
                        forms_questions: questionsArr
                    }
                } else if (legalFormsObj[legalFormIndex].document_type == 'RFPD' || legalFormsObj[legalFormIndex].document_type == 'rfpd') {
                    RfpdArr[RfpdArr.length] = {
                        legalforms_id: legalFormsObj[legalFormIndex].id,
                        forms_questions: questionsArr
                    }
                } else if (legalFormsObj[legalFormIndex].document_type == 'RFA' || legalFormsObj[legalFormIndex].document_type == 'rfa') {
                    RfaArr[RfaArr.length] = {
                        legalforms_id: legalFormsObj[legalFormIndex].id,
                        forms_questions: questionsArr
                    }
                }
                await Forms.destroy({
                    where: {
                        practice_id: casesObj.practice_id,
                        client_id: casesObj.client_id,
                        case_id: casesObj.id,
                        legalforms_id: legalFormsObj[legalFormIndex].id
                    }
                });
            }

        }
        let plainCase = {};
        const dataObject = Object.assign(plainCase, {
            json_version: 'v1',
            case_id: casesObj.id,
            client_id: casesObj.client_id,
            practice_id: casesObj.practice_id,
            client_forms: {
                frogs: FrogsArr,
                sprogs: SprogsArr,
                rfpd: RfpdArr,
                rfa: RfaArr,
            },
            otherParties_forms: {
                frogs: OtherPartiesFrogsArr,
                sprogs: OtherPartiesSprogsArr,
                rfpd: OtherPartiesRfpdArr,
                rfa: OtherPartiesRfaArr,
            }
        });

        const archiveData = {
            id: uuid.v4(),
            practice_id: casesObj.practice_id,
            client_id: casesObj.client_id,
            case_id: casesObj.id,
            case_title: casesObj.case_title,
            case_number: casesObj.case_number,
            form_data: JSON.stringify(dataObject),
            state: casesObj.state,
        };

        const caseArchiveObj = await CaseArchive.findAll({
            where: {
                practice_id: casesObj.practice_id,
                client_id: casesObj.client_id,
                case_id: casesObj.id
            }
        });
        if (caseArchiveObj.length != 0) {
            await CaseArchive.destroy({
                where: {
                    practice_id: casesObj.practice_id,
                    client_id: casesObj.client_id,
                    case_id: casesObj.id
                }
            });
        }

        await CaseArchive.create(archiveData);
        casesObj.is_archived = true;
        await casesObj.save();
        /*     if (formsObj.length!=0) {
                await Forms.destroy({where:{practice_id:casesObj.practice_id,client_id:casesObj.client_id,case_id:casesObj.id}});
            }
            if(otherPartiesFormsObj.length!=0){
                await OtherPartiesForms.destroy({where:{practice_id:casesObj.practice_id,case_id:casesObj.id}});
            }  */

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ message: "Case Archived Successfully.", status: "ok" }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not archive this case.' }),
        };
    }
};

const getAllCaseArchive_pagination_new = async (event) => {
    try {
        const { sequelize, LegalForms, Op, Users, CaseArchive } = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let filterQuery = '';
        let sortQuery = '';
        let searchQuery = '';
        const query = event.queryStringParameters || event.query || {};

        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let practice_id = event.user.practice_id;
        if (!practice_id) throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
        let codeSnip = '';
        let codeSnip2 = '';
        let where = `WHERE CaseArchives.practice_id = '${practice_id}' AND CaseArchives.is_unarchived IS NOT true AND CaseArchives.is_deleted IS NOT true`;

        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY CaseArchives.createdAt DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` CaseArchives.case_title LIKE '%${searchKey}%' OR Cases.claim_number LIKE '%${searchKey}%' OR Cases.matter_id LIKE '%${searchKey}%' OR Cases.date_of_loss LIKE '%${searchKey}%' OR CaseArchives.case_number LIKE '%${searchKey}%' OR CaseArchives.state LIKE '%${searchKey}%' OR Clients.name LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && sortQuery == '') {
            codeSnip = where + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where;
        }


        let sqlQuery = 'select CaseArchives.practice_id, CaseArchives.id, CaseArchives.client_id, CaseArchives.case_id, CaseArchives.case_title, CaseArchives.case_number, CaseArchives.state,' +
            '(SELECT MIN(response_deadline_enddate) FROM LegalForms WHERE case_id = CaseArchives.case_id AND date(response_deadline_enddate) >= date(now())) AS due_date, ' +
            'CaseArchives.createdAt, Clients.name as client_name , Practices.name as practice_name, Cases.claim_number, Cases.matter_id, Cases.attorneys, Cases.date_of_loss from CaseArchives ' +
            'INNER JOIN Practices ON CaseArchives.practice_id = Practices.id INNER JOIN Clients ON CaseArchives.client_id = Clients.id INNER JOIN Cases ON CaseArchives.case_id = Cases.id ' + codeSnip;

        let sqlQueryCount = 'select CaseArchives.practice_id, CaseArchives.id, CaseArchives.client_id, CaseArchives.case_id, CaseArchives.case_title, CaseArchives.case_number, CaseArchives.state,' +
            '(SELECT MIN(response_deadline_enddate) FROM LegalForms WHERE case_id = CaseArchives.case_id AND date(response_deadline_enddate) >= date(now())) AS due_date, ' +
            'CaseArchives.createdAt, Clients.name as client_name , Practices.name as practice_name, Cases.claim_number, Cases.matter_id, Cases.attorneys, Cases.date_of_loss from CaseArchives ' +
            'INNER JOIN Practices ON CaseArchives.practice_id = Practices.id INNER JOIN Clients ON CaseArchives.client_id = Clients.id INNER JOIN Cases ON CaseArchives.case_id = Cases.id ' + codeSnip2;

        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });

        for (let i = 0; i < serverData.length; i += 1) {
            if (serverData[i].attorneys != null && serverData[i].attorneys) {
                let attorneysIds = serverData[i].attorneys.split(",");
                let attorneyName = [];
                for (let l = 0; l < attorneysIds.length; l++) {
                    if (attorneysIds[l]) {
                        const UsersObj = await Users.findAll({
                            where: { id: attorneysIds[l] },
                            raw: true,
                            is_deleted: { [Op.not]: true }
                        });
                        if (UsersObj[0]?.name) attorneyName.push(UsersObj[0].name);
                    }
                }
                serverData[i].attorney_name = attorneyName.toString();
            } else {
                serverData[i].attorney_name = '';
            }
        }

        let totalPages, currentPage;
        if (query.limit && (serverData.length > 0)) {
            totalPages = Math.ceil(TableDataCount.length / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const serverDetails = {
            total_items: TableDataCount.length,
            response: serverData,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
}

const getAllCaseArchive = async (event) => {
    try {
        const { sequelize, LegalForms, Op, Users, CaseArchive } = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let filterQuery = '';
        let sortQuery = '';
        let searchQuery = '';
        const query = event.headers;

        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let practice_id = event.user.practice_id;
        if (!practice_id) throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
        let codeSnip = '';
        let codeSnip2 = '';
        let where = `WHERE CaseArchives.practice_id = '${practice_id}' AND CaseArchives.is_unarchived IS NOT true AND CaseArchives.is_deleted IS NOT true`;

        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY CaseArchives.createdAt DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` CaseArchives.case_title LIKE '%${searchKey}%' OR Cases.claim_number LIKE '%${searchKey}%' OR Cases.matter_id LIKE '%${searchKey}%' OR Cases.date_of_loss LIKE '%${searchKey}%' OR CaseArchives.case_number LIKE '%${searchKey}%' OR CaseArchives.state LIKE '%${searchKey}%' OR Clients.name LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && sortQuery == '') {
            codeSnip = where + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where;
        }


        let sqlQuery = 'select CaseArchives.practice_id, CaseArchives.id, CaseArchives.client_id, CaseArchives.case_id, CaseArchives.case_title, CaseArchives.case_number, CaseArchives.state,' +
            '(SELECT MIN(response_deadline_enddate) FROM LegalForms WHERE case_id = CaseArchives.case_id AND date(response_deadline_enddate) >= date(now())) AS due_date, ' +
            'CaseArchives.createdAt, Clients.name as client_name , Practices.name as practice_name, Cases.claim_number, Cases.matter_id, Cases.attorneys, Cases.date_of_loss from CaseArchives ' +
            'INNER JOIN Practices ON CaseArchives.practice_id = Practices.id INNER JOIN Clients ON CaseArchives.client_id = Clients.id INNER JOIN Cases ON CaseArchives.case_id = Cases.id ' + codeSnip;
        
        let sqlQueryCount = 'select CaseArchives.practice_id, CaseArchives.id, CaseArchives.client_id, CaseArchives.case_id, CaseArchives.case_title, CaseArchives.case_number, CaseArchives.state,' +
            '(SELECT MIN(response_deadline_enddate) FROM LegalForms WHERE case_id = CaseArchives.case_id AND date(response_deadline_enddate) >= date(now())) AS due_date, ' +
            'CaseArchives.createdAt, Clients.name as client_name , Practices.name as practice_name, Cases.claim_number, Cases.matter_id, Cases.attorneys, Cases.date_of_loss from CaseArchives ' +
            'INNER JOIN Practices ON CaseArchives.practice_id = Practices.id INNER JOIN Clients ON CaseArchives.client_id = Clients.id INNER JOIN Cases ON CaseArchives.case_id = Cases.id ' + codeSnip2;

        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });

        for (let i = 0; i < serverData.length; i += 1) {
            if (serverData[i].attorneys != null && serverData[i].attorneys) {
                let attorneysIds = serverData[i].attorneys.split(",");
                let attorneyName = [];
                for (let l = 0; l < attorneysIds.length; l++) {
                    if (attorneysIds[l]) {
                        const UsersObj = await Users.findAll({
                            where: { id: attorneysIds[l] },
                            raw: true,
                            is_deleted: { [Op.not]: true }
                        });
                        if (UsersObj[0]?.name) attorneyName.push(UsersObj[0].name);
                    }
                }
                serverData[i].attorney_name = attorneyName.toString();
            } else {
                serverData[i].attorney_name = '';
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
}

const questionsSort = async (formsObj) => {
    try {
        const compare = (x1, x2) => {
            let a = x1.question_number_text;
            let b = x2.question_number_text;
            let section_id_a = parseInt(x1.question_section_id);
            let section_id_b = parseInt(x2.question_section_id);
            if (a === b) {
                if (section_id_a < section_id_b) {
                    return -1;
                }
                if (section_id_a > section_id_b) {
                    return 1;
                }

                return 0
            }
            ;
            const aArr = a.split("."), bArr = b.split(".");
            for (let i = 0; i < Math.min(aArr.length, bArr.length); i++) {
                if (parseInt(aArr[i]) < parseInt(bArr[i])) {
                    return -1
                }
                ;
                if (parseInt(aArr[i]) > parseInt(bArr[i])) {
                    return 1
                }
                ;
            }
            if (aArr.length < bArr.length) {
                return -1
            }
            ;
            if (aArr.length > bArr.length) {
                return 1
            }
            ;
            return 0;
        };
        await formsObj.sort(compare);
        return formsObj;
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not sort the questions.' }),
        };
    }
}

const casesUnarchive = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        input.practice_id = event.user.practice_id;
        validateUnarchiveCases(input);
        const { Cases, Forms, CaseArchive, OtherPartiesForms } = await connectToDatabase();
        const casesObj = await Cases.findOne({
            where: {
                practice_id: input.practice_id,
                client_id: input.client_id,
                id: input.case_id,
                is_archived: true
            }
        });
        if (!casesObj) throw new HTTPError(404, `Count not find case deatails for this case Archive.`);
        const caseArchiveObj = await CaseArchive.findOne({
            where: {
                practice_id: input.practice_id,
                client_id: input.client_id,
                case_id: input.case_id,
                id: input.id,
            }
        });

        const formsData = typeof caseArchiveObj.form_data === 'string' ? JSON.parse(caseArchiveObj.form_data) : caseArchiveObj.form_data;

        let formsByType = await documentTypes({ state: casesObj.state, outputType: 'types', letterCase: 'small' });
        let otherPartiesFormsByType = await documentTypes({ state: casesObj.state, outputType: 'types', letterCase: 'small' });

        /* Forms */
        for (let i = 0; i < formsByType.length; i++) {
            let allForms = formsData?.client_forms[formsByType[i]] || [];
            for (let j = 0; j < allForms.length; j++) {
                const questions = allForms[j]?.forms_questions || [];
                if (questions.length > 0) {
                    await Forms.bulkCreate(questions);
                }
            }
        }

        // /* Other Parties */
        for (let i = 0; i < otherPartiesFormsByType.length; i++) {
            let allForms = formsData?.otherParties_forms[otherPartiesFormsByType[i]] || [];
            for (let j = 0; j < allForms.length; j++) {
                const questions = allForms[j]?.forms_questions || [];
                if (questions.length > 0) {
                    await Forms.bulkCreate(questions);
                }
            }
        }

        casesObj.is_archived = false;
        await casesObj.save();
        caseArchiveObj.is_deleted = true;
        await caseArchiveObj.save();
        // await caseArchiveObj.destroy();

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ status: "ok", message: "Cases unarchived successfully." }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could Unarchive this case.' }),
        };
    }
}
module.exports.caseArchive = caseArchive;
module.exports.getAllCaseArchive = getAllCaseArchive;
module.exports.casesUnarchive = casesUnarchive;