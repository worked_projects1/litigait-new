/* Cases Archive */
/* 
    Tables:
        Cases
        Legalforms,
        Forms,
        Formotp,
        OtherPartiesForms,
        OtherPartiesOtp

Scenarios:
    * Update archive status in Cases Table.
    * Get Forms Deatails Using Case id, Legalforms id,practice id, client id, document type.
    {Get Forms otp Deatails Using Case id, Legalforms id,practice id, client id, document type.}
    * Construct the JSON formate using Forms data.
*/

/*
{
  "case_id": "",
  "client_id": "",
  "practice_id": "",
  "frogs": [
    {
      "legalforms_id": "",
      "forms_questions": [
        {
          "id": "",
          "subgroup": "",
          "question_type": "",
          "question_id": "",
          "question_number_text": "",
          "question_number": "",
          "question_text": "",
          "question_section_id": "",
          "question_section": "",
          "question_section_text": "",
          "question_options": "",
          "lawyer_response_text": "",
          "lawyer_response_status": "",
          "client_response_text": "",
          "client_response_status": "",
          "file_upload_status": "",
          "uploaded_documents": "",
          "last_sent_to_client": "",
          "last_updated_by_lawyer": "",
          "last_updated_by_client": "",
          "TargetLanguageCode": ""
        }
      ]
    }
  ],
  "sprogs": [
    {
      "legalforms_id": "",
      "forms_questions": [
        {
          "id": "",
          "subgroup": "",
          "question_type": "",
          "question_id": "",
          "question_number_text": "",
          "question_number": "",
          "question_text": "",
          "question_section_id": "",
          "question_section": "",
          "question_section_text": "",
          "question_options": "",
          "lawyer_response_text": "",
          "lawyer_response_status": "",
          "client_response_text": "",
          "client_response_status": "",
          "file_upload_status": "",
          "uploaded_documents": "",
          "last_sent_to_client": "",
          "last_updated_by_lawyer": "",
          "last_updated_by_client": "",
          "TargetLanguageCode": ""
        }
      ]
    }
  ],
  "rfa": [
    {
      "legalforms_id": "",
      "forms_questions": [
        {
          "id": "",
          "subgroup": "",
          "question_type": "",
          "question_id": "",
          "question_number_text": "",
          "question_number": "",
          "question_text": "",
          "question_section_id": "",
          "question_section": "",
          "question_section_text": "",
          "question_options": "",
          "lawyer_response_text": "",
          "lawyer_response_status": "",
          "client_response_text": "",
          "client_response_status": "",
          "file_upload_status": "",
          "uploaded_documents": "",
          "last_sent_to_client": "",
          "last_updated_by_lawyer": "",
          "last_updated_by_client": "",
          "TargetLanguageCode": ""
        }
      ]
    }
  ],
  "rfpd": [
    {
      "legalforms_id": "",
      "forms_questions": [
        {
          "id": "",
          "subgroup": "",
          "question_type": "",
          "question_id": "",
          "question_number_text": "",
          "question_number": "",
          "question_text": "",
          "question_section_id": "",
          "question_section": "",
          "question_section_text": "",
          "question_options": "",
          "lawyer_response_text": "",
          "lawyer_response_status": "",
          "client_response_text": "",
          "client_response_status": "",
          "file_upload_status": "",
          "uploaded_documents": "",
          "last_sent_to_client": "",
          "last_updated_by_lawyer": "",
          "last_updated_by_client": "",
          "TargetLanguageCode": ""
        }
      ]
    }
  ]
}
*/

