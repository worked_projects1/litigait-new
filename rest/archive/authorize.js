const {HTTPError} = require('../../utils/httpResp');

const adminInternalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const internalRoles = ['superAdmin', 'manager', 'operator'];
const litigaitRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician', 'lawyer', 'paralegal'];

exports.authorizeCaseArchive = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetAllCaseArchive = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeCasesUnarchive = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};