module.exports = (sequelize, type) => sequelize.define('CaseArchive', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    client_id: type.STRING,
    case_id: type.STRING,
    case_title: type.STRING,
    case_number: type.STRING,
    form_data: type.TEXT('long'),
    is_deleted: type.BOOLEAN,
    is_unarchived: type.BOOLEAN,
    state: type.STRING
},{
    indexes: [
        {
            name: 'practice_id',
            fields: ['practice_id']
        },
        {
            name: 'client_id',
            fields: ['client_id']
        },
        {
            name: 'case_id',
            fields: ['case_id']
        },
        {
            name: 'practice_client_id',
            fields: ['practice_id','client_id']
        },
        {
            name: 'pra_cli_cas_id_de',
            fields: ['practice_id','client_id','case_id','is_deleted']
        },
        {
            name: 'pra_cli_cas_id_un',
            fields: ['practice_id','client_id','case_id','is_unarchived']
        },
        {
            name: 'pra_cli_cas_id',
            fields: ['practice_id','client_id','case_id']
        },
        {
            name: 'createdAt',
            fields: ['createdAt']
        },
        {
            name: 'is_deleted',
            fields: ['is_deleted']
        },
        {
            name: 'state',
            fields: ['state']
        }
    ]
});
