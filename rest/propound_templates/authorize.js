const {HTTPError} = require('../../utils/httpResp');

const adminInternalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const practiceRoles = ['lawyer', 'paralegal'];
const internalRoles = ['superAdmin', 'manager', 'operator'];
const superAdminRole = ['superAdmin'];
const normalUserRole = ['superAdmin', 'manager', 'operator'];

exports.authorizecreate = function (user) {
    if (!adminInternalRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetOne = function (user) {
    if (!adminInternalRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetAll = function (user) {
    if (!adminInternalRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeDestroy = function (user) {
    if (!adminInternalRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeCreatePropoundTemplates = function (user) {
    if (!adminInternalRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeSavePropoundTemplatesData = function (user) {
    if (!adminInternalRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetPropoundTemplatesQuestions = function (user) {
    if (!adminInternalRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};