const AWS = require('aws-sdk');
const { HTTPError } = require('../../utils/httpResp');

AWS.config.update({ region: process.env.REGION || 'us-west-2' });

const translate = new AWS.Translate();

const callTranslateApi = async (input) => {
    const params = {
        SourceLanguageCode: input.source,
        TargetLanguageCode: input.target, /* required */
        Text: input.text, /* required */
        // region: process.env.REGION || 'us-west-2',
    };

    return new Promise((resolve, reject) => {
        translate.translateText(params, (err, data) => {
            if (err) {
                reject(err.message);
            } else {
                resolve(data);
            }
        });
    });
};


const translateNow = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const data = await callTranslateApi(input);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                translateApiResponse: data,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not resolve.' }),
        };
    }
};


module.exports.translate = translateNow;
