const uuid = require('uuid');
const connectToDatabase = require('../../db');


const getVersionHistory = async (event) => {
    try {
        const { VersionHistory } = await connectToDatabase();
        const versionHistoryObject = await VersionHistory.findOne({
            where: {}
        });
        let versionJson = {
            version_date: new Date(),
            version: 1,
        };
        if (versionHistoryObject) {
            versionJson = versionHistoryObject.get({ plain: true });
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(versionJson),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Practices.' }),
        };
    }
};


const setVersionHistory = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { VersionHistory } = await connectToDatabase();
        const versionHistoryObject = await VersionHistory.findOne({
            where: {}
        });
        if (versionHistoryObject) {
            versionHistoryObject.version_date = new Date();
            versionHistoryObject.version = parseInt(input.version, 10);
            await versionHistoryObject.save();
        } else {
            const dataObject = Object.assign({
                version_date: new Date(),
                version: parseInt(input.version, 10),
            }, { id: uuid.v4() });
            await VersionHistory.create(dataObject);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'version updated',
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Practices.' }),
        };
    }
};


module.exports.getVersionHistory = getVersionHistory;
module.exports.setVersionHistory = setVersionHistory;

