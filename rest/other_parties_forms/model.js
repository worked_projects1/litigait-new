module.exports = (sequalize, type) => sequalize.define('OtherPartiesForms', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    case_id: type.STRING,
    practice_id: type.STRING,
    party_id: type.STRING, //Other Party ID.
    form_id: type.STRING,
    legalforms_id: type.STRING,
    document_type: type.STRING, // FROGS, SPROGS, RFPD, RFA
    connected_document_ids: type.TEXT('long'),  // RFA
    connected_form_document_type: type.STRING,
    duplicate_set_no: {
        type: type.INTEGER,
        defaultValue: 1,
        allowNull: false
    },
    is_duplicate: {
        type:type.BOOLEAN,
        defaultValue: false,
        allowNull: false
    },
    subgroup: type.BOOLEAN,
    is_consultation_set: {
        type:type.BOOLEAN,
        defaultValue: false,
        allowNull: false
    },
    consultation_set_no: {
        type: type.INTEGER,
        defaultValue: 1,
        allowNull: false
    },
    consultation_createdby: type.STRING,
    question_type: type.STRING, // only for initial discloser forms (FROGS, SPROGS, RFPD, RFA)
    question_id: type.FLOAT,
    question_number_text: type.STRING,
    question_number: type.FLOAT,
    question_text: type.TEXT,
    question_section_id: type.STRING,
    question_section: type.TEXT,
    question_section_text: type.TEXT,
    question_options: type.STRING,
    //question_number_sort: type.INTEGER,
    client_response_text: type.TEXT('long'),
    client_response_status: type.STRING, // (NotSetToClient, SentToClient, ClientResponseAvailable)
    share_attorney_response: type.BOOLEAN,
    client_modified_response: type.BOOLEAN,
    is_the_client_response_edited: type.BOOLEAN,
    file_upload_status: type.BOOLEAN,
    uploaded_documents: type.TEXT('long'),
    last_sent_to_client: type.DATE,
    last_updated_by_client: type.DATE,
    TargetLanguageCode: type.STRING,
});
