const AWS = require('aws-sdk');
const s3 = new AWS.S3({ region: 'us-east-1' });
const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const { sendEmail } = require('../../utils/mailModule');
const { sendSMS } = require('../../utils/smsModule');
const { generateRandomString } = require('../../utils/randomStringGenerator');
const moment = require("moment");
const timeZone = require("../../utils/timeStamp");

const docUrlGenerator = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const { s3_file_key, legalform_id, propoundforms_id, custom_template_id, document_type } = input;
    if (!s3_file_key) throw new HTTPError(400, 'S3 file key not provided');

    const { LegalForms, PropoundForms, PracticesTemplates } = await connectToDatabase();
    let urls = {};
    const Bucket = custom_template_id ? process.env.S3_BUCKET_FOR_TEMPLATES : process.env.S3_BUCKET_FOR_DOCUMENTS;

    const publicS3params = {
      Bucket,
      Key: s3_file_key,
    };

    if (!custom_template_id) publicS3params.Expires = 60 * 60 * 12;

    const publicUrl = await s3.getSignedUrlPromise('getObject', publicS3params);
    if (!publicUrl) throw new HTTPError(400, 'Can\'t generate public url');
    urls.doc_download_url = publicUrl;

    if (legalform_id) {
      if (!document_type) throw new HTTPError(400, 'Legal form Document type not provided');
      const legalformData = await LegalForms.findOne({
        where: {
          id: legalform_id,
          practice_id: event?.user?.practice_id,
          document_type: document_type
        }
      });

      if (!legalformData) throw new HTTPError(400, 'Legal form not found');

      legalformData.final_doc_edited_At = new Date();
      legalformData.final_document = publicUrl;
      urls.final_doc_edited_At = legalformData.final_doc_edited_At;
      urls.type = 'Responding';
      await legalformData.save();
    }

    if (propoundforms_id) {
      if (!document_type) throw new HTTPError(400, 'Propound form Document type not provided');
      const propoundFormData = await PropoundForms.findOne({
        where: {
          id: propoundforms_id,
          practice_id: event?.user?.practice_id,
          document_type: document_type
        }
      });

      if (!propoundFormData) throw new HTTPError(400, 'Propound form not found');

      propoundFormData.final_doc_edited_At = new Date();
      urls.final_doc_edited_At = propoundFormData.final_doc_edited_At;
      urls.type = 'Propounding';
      await propoundFormData.save();
    }

    if (custom_template_id && !document_type) {
      const customTemplateData = await PracticesTemplates.findOne({
        where: {
          id: custom_template_id,
          practice_id: event?.user?.practice_id,
        }
      });

      if (!customTemplateData) throw new HTTPError(400, 'Custom Template not found');

      customTemplateData.final_doc_edited_At = new Date();
      customTemplateData.custom_template = publicUrl;
      urls.final_doc_edited_At = customTemplateData.final_doc_edited_At;
      urls.type = 'Custom Template';
      await customTemplateData.save();
    }

    const s3Params = {
      Bucket,
      Key: s3_file_key,
      ContentType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
      Expires: 60 * 60 * 1,
      ACL: 'private',
    };

    const uploadURL = s3.getSignedUrl('putObject', s3Params);
    if (!uploadURL) throw new HTTPError(400, 'Can\'t generate upload url');
    urls.doc_upload_url = uploadURL;

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(urls),
    };
  } catch (err) {
    console.error(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Couldn\'t generate url' }),
    };
  }
}

const SaveDocSFDTkey = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const { legalforms_id, sfdt_s3key, propoundforms_id, custom_template_id, doc_version_id } = input;
    const { LegalForms, PropoundForms, PracticesTemplates, DocumentVersioning } = await connectToDatabase();
    let storedData;
    //Responding
    if (legalforms_id) {
      const legalFromsData = await LegalForms.findOne({
        where: {
          id: legalforms_id,
          practice_id: event.user.practice_id
        }
      });
      if (!legalFromsData) throw new HTTPError(400, 'LegalForms data not found');
      legalFromsData.sfdt_s3key = sfdt_s3key;
      storedData = await legalFromsData.save();
      const document_versioning_limit = 30;
    if(doc_version_id){
      const versionData = await DocumentVersioning.findOne({where:{ id: doc_version_id }});
      if(!versionData) throw new HTTPError(400,'version data not found');
      versionData.sfdt_s3key = sfdt_s3key;
      versionData.user_id = event.user.id;
      versionData.versioning_timeStamp = new Date();
      const LatestUpdatedData = await versionData.save();
      legalFromsData.final_document_s3_key = LatestUpdatedData.final_document_s3_key;
      if(legalFromsData?.pdf_s3_file_key)legalFromsData.pdf_s3_file_key = LatestUpdatedData.pdf_s3_file_key;
      if(LatestUpdatedData?.pdf_filename)legalFromsData.pdf_filename = LatestUpdatedData.pdf_filename;
      legalFromsData.doc_version_id = versionData.id;
      storedData = await legalFromsData.save(); 
    }

    if(!doc_version_id){
      const versionAllData = await DocumentVersioning.findAll({where:
        { legalforms_id: legalFromsData.id },
        order:[['versioning_timeStamp', 'DESC']],
        raw:true });
      for (let i = versionAllData.length ; i >= document_versioning_limit; i--){
            console.log(i);
            await DocumentVersioning.destroy({where:{ id: versionAllData[i-1].id }});
      }

      const versioningDataObject = Object.assign({},{
        id: uuid.v4(),
        case_id: legalFromsData.case_id,
        practice_id: legalFromsData.practice_id,
        client_id: legalFromsData.client_id,
        legalforms_id: legalFromsData.id,
        user_id: event.user.id,
        document_type: legalFromsData.document_type,
        s3_file_key: legalFromsData.s3_file_key,
        filename: legalFromsData.filename,
        pdf_filename: legalFromsData.pdf_filename,
        doc_filename: legalFromsData.doc_filename,
        pdf_s3_file_key: legalFromsData.pdf_s3_file_key,
        final_document: legalFromsData.final_document,
        final_document_s3_key: legalFromsData.final_document_s3_key,
        sfdt_s3key: sfdt_s3key,
        versioning_timeStamp: new Date()
     });

     const versioningData = await DocumentVersioning.create(versioningDataObject);
     const versionDataCount = await DocumentVersioning.count({where:{ legalforms_id: legalFromsData.id },raw:true });
     legalFromsData.doc_version_id = versioningData.id;
     storedData = await legalFromsData.save();
     storedData = storedData.get({plain: true});
     storedData.version_data_count = versionDataCount;
     storedData.recent_document_version_data = versioningData.get({plain: true}); 
     console.log(storedData);
    }
  }
    //Propounding
    if (propoundforms_id) {
      const propoundFromsData = await PropoundForms.findOne({
        where: {
          id: propoundforms_id,
          practice_id: event.user.practice_id
        }
      });
      if (!propoundFromsData) throw new HTTPError(400, 'propound form data not found');
      propoundFromsData.sfdt_s3key = sfdt_s3key;
      storedData = await propoundFromsData.save();
      storedData = storedData.get({plain: true});
    }
    //Custom_template
    if (custom_template_id) {
      const customTemplateData = await PracticesTemplates.findOne({
        where: {
          id: custom_template_id,
          practice_id: event.user.practice_id
        }
      });
      if (!customTemplateData) throw new HTTPError(400, 'custom template data not found');
      customTemplateData.sfdt_s3key = sfdt_s3key;
      storedData = await customTemplateData.save();
      storedData = storedData.get({plain: true});
    }
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(storedData),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not Save Document SFDT' }),
    };
  }
}

const fileRename = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const { s3_file_key, pdf_s3_file_key, legalform_id, propoundforms_id, case_id, document_type, filename } = input;

    const { LegalForms, PropoundForms } = await connectToDatabase();
    if(!filename) throw new HTTPError(400,'filename not provided');
    if (!s3_file_key) throw new HTTPError(400, 'S3 file key not provided');
    if (!pdf_s3_file_key) throw new HTTPError(400, 'Pdf S3 file key not provided');
    if (!case_id) throw new HTTPError(400, 'Case id not provided');
    const Bucket = process.env.S3_BUCKET_FOR_DOCUMENTS;

    let Keys = [];
    let output = {};
    output.filename = filename;
    output.case_id = case_id;
    let NewDate = timeZone.isoTime();
    if (legalform_id) {
      const legalformData = await LegalForms.findOne({
        where: {
          id: legalform_id,
          practice_id: event?.user?.practice_id,
          document_type: document_type,
        }
      });
      if (!legalformData) throw new HTTPError(400, 'Legal form not found');
      output.legalform_id = legalform_id
      if (legalformData?.s3_file_key) {
        const docKey = `${case_id}/final/${filename}_${NewDate}.docx`;
        console.log(docKey);
        Keys.push({ new: docKey, old: s3_file_key, type: 'docx' });
        console.log(Keys);
        output.new_s3_file_key = docKey;
        legalformData.final_document_s3_key = docKey;
      }
      if (legalformData?.pdf_s3_file_key) {
        const pdfKey = `${case_id}/final/${filename}_${NewDate}.pdf`;
        Keys.push({ new: pdfKey, old: pdf_s3_file_key, type: 'pdf' });
        output.new_pdf_s3_file_key = pdfKey;
        legalformData.pdf_s3_file_key = pdfKey;
      }
      legalformData.pdf_filename = filename;
      await legalformData.save();
    }

    if (propoundforms_id) {
      const propoundfromData = await PropoundForms.findOne({
        where: {
          id: propoundforms_id,
          practice_id: event?.user?.practice_id,
          document_type: document_type,
        }
      });
      if (!propoundfromData) throw new HTTPError(400, 'propound form not found');
      output.propoundforms_id = propoundforms_id;
      if (propoundfromData?.s3_file_key) {
        const docKey = `${case_id}/propound_doc/${filename}_${NewDate}.docx`;
        Keys.push({ new: docKey, old: s3_file_key, type: 'docx'});
        output.new_s3_file_key = docKey;
        propoundfromData.s3_file_key = docKey;
      }
      if (propoundfromData?.pdf_s3_file_key) {
        const pdfKey = `${case_id}/propound_doc/${filename}_${NewDate}.pdf`;
        Keys.push({ new: pdfKey, old: pdf_s3_file_key, type: 'pdf'});
        output.new_pdf_s3_file_key = pdfKey;
        propoundfromData.pdf_s3_file_key = pdfKey;
      };
      propoundfromData.pdf_filename = filename;
      await propoundfromData.save();
    }
    for (let i = 0; i < Keys.length; i++) {
      const copyBucket = {
        Bucket,
        CopySource: `${Bucket}/${Keys[i].old}`,
        Key: Keys[i].new
      }
      const deleteBucket = {
        Bucket,
        Key: Keys[i].old
      }

    await s3.copyObject(copyBucket).promise().then(() =>
        s3.deleteObject(deleteBucket).promise())
        .catch((e) => console.error(e))

      const publicS3params = {
        Bucket,
        Key: Keys[i].new,
      };

      const publicUrl = await s3.getSignedUrlPromise('getObject', publicS3params);
      if (!publicUrl) throw new HTTPError(400, `Can't generate public url for ${Keys[i].type}`);
      if(Keys[i].type == 'docx')output.docDownloadUrl = publicUrl;
      if(Keys[i].type == 'pdf')output.pdfDownloadUrl = publicUrl;
    }

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(output),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Issue in renaming this file' }),
    };
  }
}

const testEmail = async () => {
  try {
    await sendSMS(`9999999999`, `data`, `test`);
    await sendEmail(`test@gmail.com`, `data`, `test`);
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ message: 'email sent successfully' }),
    };
  } catch (err) {
    console.error(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Couldn\'t send email' }),
    };
  }
}

const NameRenameapi = async() => {
 try{
  
      const { LegalForms, PropoundForms, Op } = await connectToDatabase();

      // const data = await LegalForms.findAll({where: { pdf_s3_file_key:{[Op.not]:null}}});
      const data = await PropoundForms.findAll({where: { pdf_s3_file_key:{[Op.not]:null}}});
      for(let i = 0; i < data.length; i++) {
       const filename =  data[i].pdf_s3_file_key;
       const name = filename.split('/');
       console.log(name);
       const newName = name[2].split('.pdf');
        console.log(newName[0]);
          const PropoundFormsdata = await PropoundForms.findOne({where: { id: data[i].id }});
          // const legalForms = await LegalForms.findOne({where: { id: data[i].id }});
          // console.log(legalForms);
          console.log(PropoundFormsdata);
          // legalForms.pdf_filename = newName[0];
          PropoundFormsdata.pdf_filename = newName[0];
          await PropoundFormsdata.save();
          // await legalForms.save();

      }
      return {
        statusCode: 200,
        headers: {
          'Content-Type': 'text/plain',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true
        },
        body: JSON.stringify({ message: 'process completed successfully' }),
      };
    } catch (err) {
      console.error(err);
      return {
        statusCode: err.statusCode || 500,
        headers: {
          'Content-Type': 'text/plain',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true
        },
        body: JSON.stringify({ error: err.message || 'process failed' }),
      };
    }
};

const docPublicUrlGenerator = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const { sfdt_s3key } = input;
    if (!sfdt_s3key) throw new HTTPError(400, 'Document S3 file key not provided');

    const Bucket = process.env.S3_BUCKET_FOR_SFDT_DOC_JSON;

    const publicS3params = {
      Bucket,
      Key: sfdt_s3key,
    };

    const publicUrl = await s3.getSignedUrlPromise('getObject', publicS3params);
    if (!publicUrl) throw new HTTPError(400, 'Can\'t generate public url');
    
    const response = {
      sfdt_s3key,
      sfdtPublicUrl: publicUrl 
    };

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(response),
    };
  } catch (err) {
    console.error(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Couldn\'t generate url' }),
    };
  }
}
const DocToPdfFileRename = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const { s3_file_key, legalform_id, propoundforms_id, case_id, document_type } = input;

    if (!s3_file_key) throw new HTTPError(400, 'S3 file key not provided');
    if (!legalform_id && !propoundforms_id) throw new HTTPError(400, 'Form id not provided');
    if (!case_id) throw new HTTPError(400, 'Case id not provided');
    if (!document_type) throw new HTTPError(400, 'Document type not provided');
    const Bucket = process.env.S3_BUCKET_FOR_DOCUMENTS;

    const { Cases, LegalForms, PropoundForms,Op } = await connectToDatabase();
    const caseDetails = await Cases.findOne({
      where: {
        id: case_id,
        is_deleted: { [Op.not]: true },
        is_archived: { [Op.not]: true }
      }, raw: true
    });
    if (!caseDetails) throw new HTTPError(400, 'Case details not found');

    let docFilename = s3_file_key.split('/').pop();
    const docName = docFilename.split('.docx').join('').slice(0, -24);
    const docTimeStamp = docFilename.split('.docx').join('').split('_').pop();
    const pdfInitialNameCount = docName.replace(/[^a-zA-Z0-9 ]/g, '').split('').length;
    let filename = docName.replace(/[^a-zA-Z0-9 ]/g, '');
    if (pdfInitialNameCount > 210) {
      filename = `DFT ${caseDetails.case_defendant_name} ${document_type} TO PTF ${caseDetails.case_plaintiff_name}`.replace(/[^a-zA-Z0-9 ]/g, '');
      const pdfNewNameCount = filename.split('').length;
      if (pdfNewNameCount > 210) {
        filename = filename.slice(0, 210);
      }
    }
    let docNewName = `${filename}.docx`;
    let copyBucket = {
      Bucket,
      CopySource: `${Bucket}/${s3_file_key}`,
    }
    let response = {};
    console.log(filename);
    response.final_document_s3_key = s3_file_key;
    if (legalform_id) {
      const legalformsData = await LegalForms.findOne({ where: { id: legalform_id, practice_id: event.user.practice_id, case_id: case_id, document_type: document_type } });
      legalformsData.pdf_discovery_status = 'Processing';
      await legalformsData.save();
      copyBucket.Key = response.s3_file_key = `${case_id}/final/${legalform_id}_${docTimeStamp}/${docNewName}`;
      response.legalform_id = legalform_id;
    }

    if (propoundforms_id) {
      const propoundformsData = await PropoundForms.findOne({ where: { id: propoundforms_id, practice_id: event.user.practice_id, case_id: case_id, document_type: document_type } });
      propoundformsData.pdf_discovery_status = 'Processing';
      await propoundformsData.save();
      copyBucket.Key = response.s3_file_key = `${case_id}/propound_doc/${propoundforms_id}_${docTimeStamp}/${docNewName}`;
      response.propoundforms_id = propoundforms_id;
    }
    await s3.copyObject(copyBucket).promise().catch((e) => console.error(e));
    response.status = 'Success';
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(response),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'failed to rename this file' }),
    };
  }
}

module.exports.docUrlGenerator = docUrlGenerator;
module.exports.docPublicUrlGenerator = docPublicUrlGenerator;
module.exports.SaveDocSFDTkey = SaveDocSFDTkey;
module.exports.fileRename = fileRename;
module.exports.testEmail = testEmail;
module.exports.NameRenameapi = NameRenameapi;
module.exports.DocToPdfFileRename = DocToPdfFileRename;

