const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const connectToDatabase = require('../../db');
const { HTTPError, resInvalidPostDataWithInfo } = require('../../utils/httpResp');
const authMiddleware = require('../../auth');
const { QueryTypes } = require('sequelize');

const getAll_new_pagination = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let sortQuery = '';
        let searchQuery = '';
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;

        let codeSnip = '';
        let codeSnip2 = '';

        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY OtherPartiesFormsOtps.createdAt DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = `where OtherPartiesFormsOtps.document_type LIKE '%${searchKey}%' OR OtherPartiesFormsOtps.otp_code LIKE '%${searchKey}%' OR OtherPartiesFormsOtps.sending_type LIKE '%${searchKey}%' OR Practices.name LIKE '${searchKey}' OR Cases.case_title LIKE '${searchKey}' OR Cases.case_number LIKE '${searchKey}' OR OtherParties.name LIKE '${searchKey}' `;
        }

        if (searchQuery != '' && sortQuery != '') {
            codeSnip = searchQuery + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = searchQuery + sortQuery;
        } else if (searchQuery == '' && sortQuery != '') {
            codeSnip = sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = sortQuery;
        } else if (searchQuery != '' && sortQuery == '') {
            codeSnip = searchQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery == '') {
            codeSnip = ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = '';
        }

        let sqlQuery = ' SELECT OtherPartiesFormsOtps.id,OtherPartiesFormsOtps.case_id,OtherPartiesFormsOtps.TargetLanguageCode,' +
            ' OtherPartiesFormsOtps.party_id,OtherPartiesFormsOtps.legalforms_id,OtherPartiesFormsOtps.document_type,OtherPartiesFormsOtps.otp_code,OtherPartiesFormsOtps.otp_secret,OtherPartiesFormsOtps.createdAt,' +
            ' OtherPartiesFormsOtps.sending_type,Practices.name,Cases.case_title,OtherParties.name AS client_name,Cases.case_number from OtherPartiesFormsOtps INNER JOIN Cases ON OtherPartiesFormsOtps.case_id = Cases.id' +
            ' INNER JOIN OtherParties ON OtherPartiesFormsOtps.party_id = OtherParties.id' +
            ' INNER JOIN Practices ON Cases.practice_id = Practices.id ' + codeSnip;

        let sqlQueryCount = ' SELECT OtherPartiesFormsOtps.id,OtherPartiesFormsOtps.case_id,' +
            ' OtherPartiesFormsOtps.party_id,OtherPartiesFormsOtps.legalforms_id,OtherPartiesFormsOtps.document_type,OtherPartiesFormsOtps.otp_code,OtherPartiesFormsOtps.otp_secret,OtherPartiesFormsOtps.createdAt,' +
            ' OtherPartiesFormsOtps.sending_type,Practices.name,Cases.case_title,OtherParties.name AS client_name,Cases.case_number from OtherPartiesFormsOtps INNER JOIN Cases ON OtherPartiesFormsOtps.case_id = Cases.id' +
            ' INNER JOIN OtherParties ON OtherPartiesFormsOtps.party_id = OtherParties.id' +
            ' INNER JOIN Practices ON Cases.practice_id = Practices.id ' + codeSnip2;

        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });

        let totalPages, currentPage;
        if (query.limit && (serverData.length > 0)) {
            totalPages = Math.ceil(TableDataCount.length / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const serverDetails = {
            total_items: TableDataCount.length,
            response: serverData,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Form Otps.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let sortQuery = '';
        let searchQuery = '';
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;

        let codeSnip = '';
        let codeSnip2 = '';

        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY OtherPartiesFormsOtps.createdAt DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = `where OtherPartiesFormsOtps.document_type LIKE '%${searchKey}%' OR OtherPartiesFormsOtps.otp_code LIKE '%${searchKey}%' OR OtherPartiesFormsOtps.sending_type LIKE '%${searchKey}%' OR Practices.name LIKE '${searchKey}' OR Cases.case_title LIKE '${searchKey}' OR Cases.case_number LIKE '${searchKey}' OR OtherParties.name LIKE '${searchKey}' `;
        }

        if (searchQuery != '' && sortQuery != '') {
            codeSnip = searchQuery + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = searchQuery + sortQuery;
        } else if (searchQuery == '' && sortQuery != '') {
            codeSnip = sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = sortQuery;
        } else if (searchQuery != '' && sortQuery == '') {
            codeSnip = searchQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery == '') {
            codeSnip = ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = '';
        }

        let sqlQuery = ' SELECT OtherPartiesFormsOtps.id,OtherPartiesFormsOtps.case_id,OtherPartiesFormsOtps.TargetLanguageCode,' +
            ' OtherPartiesFormsOtps.party_id,OtherPartiesFormsOtps.legalforms_id,OtherPartiesFormsOtps.document_type,OtherPartiesFormsOtps.otp_code,OtherPartiesFormsOtps.otp_secret,OtherPartiesFormsOtps.createdAt,' +
            ' OtherPartiesFormsOtps.sending_type,Practices.name,Cases.case_title,OtherParties.name AS client_name,Cases.case_number from OtherPartiesFormsOtps INNER JOIN Cases ON OtherPartiesFormsOtps.case_id = Cases.id' +
            ' INNER JOIN OtherParties ON OtherPartiesFormsOtps.party_id = OtherParties.id' +
            ' INNER JOIN Practices ON Cases.practice_id = Practices.id ' + codeSnip;

        let sqlQueryCount = ' SELECT OtherPartiesFormsOtps.id,OtherPartiesFormsOtps.case_id,' +
            ' OtherPartiesFormsOtps.party_id,OtherPartiesFormsOtps.legalforms_id,OtherPartiesFormsOtps.document_type,OtherPartiesFormsOtps.otp_code,OtherPartiesFormsOtps.otp_secret,OtherPartiesFormsOtps.createdAt,' +
            ' OtherPartiesFormsOtps.sending_type,Practices.name,Cases.case_title,OtherParties.name AS client_name,Cases.case_number from OtherPartiesFormsOtps INNER JOIN Cases ON OtherPartiesFormsOtps.case_id = Cases.id' +
            ' INNER JOIN OtherParties ON OtherPartiesFormsOtps.party_id = OtherParties.id' +
            ' INNER JOIN Practices ON Cases.practice_id = Practices.id ' + codeSnip2;

        console.log(sqlQuery);
        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Form Otps.' }),
        };
    }
};

module.exports.getAll = getAll;