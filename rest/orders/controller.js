const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const connectToDatabase = require('../../db');
const { HTTPError, resInvalidPostDataWithInfo } = require('../../utils/httpResp');
const authMiddleware = require('../../auth');
const {
    validateCreateOrders,
    validateGetOneOrder,
    validateUpdateOrders,
    validateDeleteOrder,
    validateReadOrders,
} = require('./validation');

const { authorizeAdminGetAll } = require('./authorize');
const { QueryTypes } = require('sequelize');

const create = async (event) => {
    try {
        let id;
        const input = JSON.parse(event.body);
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, { id: id || uuid.v4() });
        validateCreateOrders(dataObject);
        const { Orders } = await connectToDatabase();
        const orders = await Orders.create(dataObject);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(orders),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the orders.' }),
        };
    }
};

const getOne = async (event) => {
    try {
        validateGetOneOrder(event.pathParameters);
        const { Orders } = await connectToDatabase();
        const orders = await Orders.findOne({ where: { id: event.pathParameters.id } });
        if (!orders) throw new HTTPError(404, `Orders with id: ${event.pathParameters.id} was not found`);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(orders),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Orders.' }),
        };
    }
};

const getAll_new_pagination = async (event) => {
    try {
        const { Orders, Clients, Cases, Op, Users } = await connectToDatabase();
        /* const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where); */
        let searchKey = '';
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        query.raw = true;
        let query2 = {};
        if (!query.where) {
            query.where = {};
            query2.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        query2.where.practice_id = event.user.practice_id;
        query.where.is_deleted = { [Op.not]: true };
        query2.where.is_deleted = { [Op.not]: true };
        validateReadOrders(query);
        query.order = [
            ['createdAt', 'DESC'],
        ];
        query2.order = [
            ['createdAt', 'DESC'],
        ];

        let orders = await Orders.findAll(query);
        const ordersCount = await Orders.count(query2);

        for (let i = 0; i < orders.length; i += 1) {
            const CaseDetails = await Cases.findOne({ where: { id: orders[i].case_id } });
            // orders[i].order_id = i + 1;
            let clientDetails;
            if (CaseDetails) {
                orders[i].case_title = CaseDetails.case_title;
                orders[i].case_number = CaseDetails.case_number;
                if (!orders[i].client_name) {
                    clientDetails = await Clients.findOne({ where: { id: CaseDetails.client_id } });
                    if (clientDetails) {
                        orders[i].client_name = clientDetails.name;
                    }
                }
            }
            if (!orders[i].order_by) {
                /* const UserDetailsObject = await Users.findOne({where:{id:orders[i].user_id); }}*/
                const UserDetailsObject = await Users.findOne({
                    where: {
                        id: orderDetails.user_id,
                        is_deleted: { [Op.not]: true }
                    }
                })
                if (UserDetailsObject) {
                    orders[i].order_by = UserDetailsObject.name;
                }
            }
            // if (!orders[i].case_title && orders[i].client_name) {

            // }
        }
        let totalCount = ordersCount;
        if (orders.length > 0) {
            const columns = Object.getOwnPropertyNames(orders[0]);
            if (searchKey != 'false' && searchKey.length >= 1) {
                let res = await filterRows(orders, columns, searchKey);
                orders = res;
                totalCount = orders.length;
            }
        }
        let totalPages, currentPage;
        if (query.limit && (orders.length > 0)) {
            totalPages = Math.ceil(ordersCount / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const ordersDetails = {
            total_items: ordersCount,
            response: orders,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': totalCount
            },
            body: JSON.stringify(ordersDetails),
        };
    } catch (err) {
        console.log('Errror Orders:');
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        const { Orders, Clients, Cases, Op, Users } = await connectToDatabase();
        /* const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where); */
        let searchKey = '';
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        query.raw = true;
        let query2 = {};
        if (!query.where) {
            query.where = {};
            query2.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        query2.where.practice_id = event.user.practice_id;
        query.where.is_deleted = { [Op.not]: true };
        query2.where.is_deleted = { [Op.not]: true };
        validateReadOrders(query);
        query.order = [
            ['createdAt', 'DESC'],
        ];
        query2.order = [
            ['createdAt', 'DESC'],
        ];

        let orders = await Orders.findAll(query);
        const ordersCount = await Orders.count(query2);

        for (let i = 0; i < orders.length; i += 1) {
            const CaseDetails = await Cases.findOne({ where: { id: orders[i].case_id } });
            // orders[i].order_id = i + 1;
            let clientDetails;
            if (CaseDetails) {
                orders[i].case_title = CaseDetails.case_title;
                orders[i].case_number = CaseDetails.case_number;
                if (!orders[i].client_name) {
                    clientDetails = await Clients.findOne({ where: { id: CaseDetails.client_id } });
                    if (clientDetails) {
                        orders[i].client_name = clientDetails.name;
                    }
                }
            }
            if (!orders[i].order_by) {
                /* const UserDetailsObject = await Users.findOne({where:{id:orders[i].user_id); }}*/
                const UserDetailsObject = await Users.findOne({
                    where: {
                        id: orderDetails.user_id,
                        is_deleted: { [Op.not]: true }
                    }
                })
                if (UserDetailsObject) {
                    orders[i].order_by = UserDetailsObject.name;
                }
            }
            // if (!orders[i].case_title && orders[i].client_name) {

            // }
        }
        let totalCount = ordersCount;
        const columns = Object.getOwnPropertyNames(orders[0]);
        if (searchKey != 'false' && searchKey.length >= 1) {
            let res = await filterRows(orders, columns, searchKey);
            orders = res;
            totalCount = orders.length;
            console.log(totalCount.length);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': totalCount
            },
            body: JSON.stringify(orders),
        };
    } catch (err) {
        console.log('Errror Orders:');
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
};

const PracticeordersSearch_new_pagination = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let filterQuery = '';
        let sortQuery = '';
        let searchQuery = '';
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        /* query.where.practice_id = event.user.practice_id; */
        let practice_id = event.user.practice_id;
        if (!practice_id) throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
        let codeSnip = '';
        let codeSnip2 = '';
        let where = `WHERE Orders.is_deleted IS NOT true AND Orders.practice_id = '${practice_id}' AND Orders.plan_type IN ('MONTHLY PROPOUNDING','YEARLY PROPOUNDING','MONTHLY','YEARLY','TEK AS-YOU-GO','FREE TRIAL','VIP') `;
        /**Filter**/
        if (query.filter != 'false') {
            query.filter = JSON.parse(query.filter);
            filterKey.product = query.filter.product;
            filterKey.type = query.filter.type;
            if (filterKey.product != 'all' && filterKey.product != '' && filterKey.product) {
                filterQuery += ` AND Orders.document_type LIKE '%${filterKey.product}%'`;
            }
            if (filterKey.type != 'all' && filterKey.type != '' && filterKey.type) {
                filterQuery += ` AND Orders.document_generation_type LIKE '%${filterKey.type}%'`;
            }
        }
        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY Orders.order_date DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` Users.name LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%' OR Cases.case_title LIKE '%${searchKey}%' OR Cases.case_number LIKE '%${searchKey}%' OR Orders.amount_charged LIKE '%${searchKey}%' OR Orders.filename LIKE '%${searchKey}%' OR Clients.name LIKE '%${searchKey}%' OR Orders.plan_type LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + sortQuery;
            codeSnip2 = where + filterQuery + sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')';
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery != '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')';
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery;
            codeSnip2 = where + filterQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where;
            codeSnip2 = where;
        }


        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }


        let sqlQuery = 'select Orders.id, Orders.order_date, Orders.filename, Orders.client_id,Clients.name AS client_name, Orders.order_id, Practices.name AS practice_name, Users.name AS user_name, Cases.case_number, Cases.case_title, ' +
            'Orders.document_type, Orders.document_generation_type, Orders.plan_type, Orders.amount_charged  from Orders ' +
            'INNER JOIN Practices ON Orders.practice_id = Practices.id ' +
            'INNER JOIN Users ON Orders.user_id = Users.id ' +
            'INNER JOIN Clients ON Orders.client_id = Clients.id ' +
            'INNER JOIN Cases ON Orders.case_id = Cases.id ' + codeSnip;

        let sqlQueryCount = 'select Orders.id, Orders.order_date, Orders.filename, Orders.client_id,Clients.name AS client_name, Orders.order_id, Practices.name AS practice_name, Users.name AS user_name, Cases.case_number, Cases.case_title, ' +
            'Orders.document_type, Orders.document_generation_type, Orders.plan_type, Orders.amount_charged from Orders ' +
            'INNER JOIN Practices ON Orders.practice_id = Practices.id ' +
            'INNER JOIN Users ON Orders.user_id = Users.id ' +
            'INNER JOIN Clients ON Orders.client_id = Clients.id ' +
            'INNER JOIN Cases ON Orders.case_id = Cases.id ' + codeSnip2;

        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });
        for (let i = 0; i < serverData.length; i++) {
            let plan_type = serverData[i].plan_type;
            if (plan_type == 'TEK AS-YOU-GO') {
                serverData[i].plan_type = 'AS-YOU-GO'
            }
        }

        let totalPages, currentPage;
        if (query.limit && (serverData.length > 0)) {
            totalPages = Math.ceil(TableDataCount.length / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const practiceOrdersDetails = {
            total_items: TableDataCount.length,
            response: serverData,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(practiceOrdersDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
};

const PracticeordersSearch = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let filterQuery = '';
        let sortQuery = '';
        let searchQuery = '';
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        /* query.where.practice_id = event.user.practice_id; */
        let practice_id = event.user.practice_id;
        if (!practice_id) throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
        let codeSnip = '';
        let codeSnip2 = '';
        let where = `WHERE Orders.is_deleted IS NOT true AND Orders.practice_id = '${practice_id}' AND Orders.plan_type IN ('MONTHLY PROPOUNDING','YEARLY PROPOUNDING','MONTHLY','YEARLY','TEK AS-YOU-GO','FREE TRIAL','VIP') `;
        /**Filter**/
        if (query.filter != 'false') {
            query.filter = JSON.parse(query.filter);
            filterKey.product = query.filter.product;
            filterKey.type = query.filter.type;
            if (filterKey.product != 'all' && filterKey.product != '' && filterKey.product) {
                filterQuery += ` AND Orders.document_type LIKE '%${filterKey.product}%'`;
            }
            if (filterKey.type != 'all' && filterKey.type != '' && filterKey.type) {
                filterQuery += ` AND Orders.document_generation_type LIKE '%${filterKey.type}%'`;
            }
        }
        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY Orders.order_date DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` Users.name LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%' OR Cases.case_title LIKE '%${searchKey}%' OR Cases.case_number LIKE '%${searchKey}%' OR Orders.amount_charged LIKE '%${searchKey}%' OR Orders.filename LIKE '%${searchKey}%' OR Clients.name LIKE '%${searchKey}%' OR Orders.plan_type LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + sortQuery;
            codeSnip2 = where + filterQuery + sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')';
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery != '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')';
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery;
            codeSnip2 = where + filterQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where;
            codeSnip2 = where;
        }


        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }


        let sqlQuery = 'select Orders.id, Orders.order_date, Orders.filename, Orders.client_id,Clients.name AS client_name, Orders.order_id, Practices.name AS practice_name, Users.name AS user_name, Cases.case_number, Cases.case_title, ' +
            'Orders.document_type, Orders.document_generation_type, Orders.plan_type, Orders.amount_charged  from Orders ' +
            'INNER JOIN Practices ON Orders.practice_id = Practices.id ' +
            'INNER JOIN Users ON Orders.user_id = Users.id ' +
            'INNER JOIN Clients ON Orders.client_id = Clients.id ' +
            'INNER JOIN Cases ON Orders.case_id = Cases.id ' + codeSnip;

        let sqlQueryCount = 'select Orders.id, Orders.order_date, Orders.filename, Orders.client_id,Clients.name AS client_name, Orders.order_id, Practices.name AS practice_name, Users.name AS user_name, Cases.case_number, Cases.case_title, ' +
            'Orders.document_type, Orders.document_generation_type, Orders.plan_type, Orders.amount_charged from Orders ' +
            'INNER JOIN Practices ON Orders.practice_id = Practices.id ' +
            'INNER JOIN Users ON Orders.user_id = Users.id ' +
            'INNER JOIN Clients ON Orders.client_id = Clients.id ' +
            'INNER JOIN Cases ON Orders.case_id = Cases.id ' + codeSnip2;

        /* console.log(sqlQuery); */
        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });
        for (let i = 0; i < serverData.length; i++) {
            let plan_type = serverData[i].plan_type;
            if (plan_type == 'TEK AS-YOU-GO') {
                serverData[i].plan_type = 'AS-YOU-GO'
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
};
const update = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateUpdateOrders(input);
        const { Orders } = await connectToDatabase();
        const orders = await Orders.findOne({ where: { id: event.pathParameters.id } });
        if (!orders) throw new HTTPError(404, `Orders with id: ${event.pathParameters.id} was not found`);
        const updatedModel = Object.assign(orders, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Orders.' }),
        };
    }
};

const destroy = async (event) => {
    try {
        validateDeleteOrder(event.pathParameters);
        const { Orders } = await connectToDatabase();
        const orders = await Orders.findOne({ where: { id: event.pathParameters.id } });
        if (!orders) throw new HTTPError(404, `Orders with id: ${event.pathParameters.id} was not found`);
        await orders.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(orders),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy the Orders.' }),
        };
    }
};

const adminGetAll = async (event) => {
    try {
        authorizeAdminGetAll(event.user);
        const { Orders, Clients, Cases, Op, Practices, Users } = await connectToDatabase();
        /* const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where); */


        let searchKey = '';
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        query.raw = true;
        let query2 = {};
        if (!query.where) {
            query.where = {};
            query2.where = {};
        }
        query.where.is_deleted = { [Op.not]: true };
        query2.where.is_deleted = { [Op.not]: true };
        if (query.practice_id) {
            query.where.practice_id = query.practice_id;
            query2.where.practice_id = query.practice_id;
        }

        validateReadOrders(query);
        query.order = [['createdAt', 'DESC'],];
        query2.order = [
            ['createdAt', 'DESC'],
        ];

        let orders = await Orders.findAll(query);
        const ordersCount = await Orders.count(query2);
        for (let i = 0; i < orders.length; i += 1) {
            const CaseDetails = await Cases.findOne({ where: { id: orders[i].case_id } });
            const practiceDetails = await Practices.findOne({ where: { id: orders[i].practice_id } });
            if (practiceDetails) {
                orders[i].practice_name = practiceDetails.name;
            }
            let clientDetails;
            if (CaseDetails) {
                orders[i].case_title = CaseDetails.case_title;
                orders[i].case_number = CaseDetails.case_number;
                if (!orders[i].client_name) {
                    clientDetails = await Clients.findOne({ where: { id: CaseDetails.client_id } });
                    if (clientDetails) {
                        orders[i].client_name = clientDetails.name;
                    }
                }
            }
            if (!orders[i].order_by) {
                /* const UserDetailsObject = await Users.findOne({where:{id:orders[i].user_id); }}*/
                const UserDetailsObject = await Users.findOne({
                    where: {
                        is_deleted: { [Op.not]: true },
                        id: orders[i].user_id
                    }
                });
                if (UserDetailsObject) {
                    orders[i].order_by = UserDetailsObject.name;
                }
            }
            // if (!orders[i].case_title && orders[i].client_name) {

            // }
        }
        let totalCount = ordersCount;
        const columns = Object.getOwnPropertyNames(orders[0]);
        if (searchKey != 'false' && searchKey.length >= 1) {
            let res = await filterRows(orders, columns, searchKey);
            orders = res;
            totalCount = orders.length;
            console.log(totalCount);
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': totalCount
            },
            body: JSON.stringify(orders),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
};
const ordersSearch = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let filterQuery = '';
        let sortQuery = '';
        let searchQuery = '';
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let codeSnip = '';
        let codeSnip2 = '';
        // let where = 'WHERE Orders.is_deleted IS NOT true';
        let where = `WHERE Orders.is_deleted IS NOT true AND Orders.plan_type IN ('MONTHLY','YEARLY','TEK AS-YOU-GO','FREE TRIAL','YEARLY PROPOUNDING','MONTHLY PROPOUNDING','VIP') `;
        /**Filter**/
        if (query.filter != 'false') {
            query.filter = JSON.parse(query.filter);
            filterKey.product = query.filter.product;
            filterKey.type = query.filter.type;
            if (filterKey.product != 'all' && filterKey.product != '' && filterKey.product) {
                filterQuery += ` AND Orders.document_type LIKE '%${filterKey.product}%'`;
            }
            if (filterKey.type != 'all' && filterKey.type != '' && filterKey.type) {
                filterQuery += ` AND Orders.document_generation_type LIKE '%${filterKey.type}%'`;
            }
        }
        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY Orders.order_date DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' AND Orders.createdAt >= "' + event?.query?.from_date + '" AND Orders.createdAt <= "' + event?.query?.to_date + '"';
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` Users.name LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%' OR Cases.case_title LIKE '%${searchKey}%' OR Cases.case_number LIKE '%${searchKey}%' OR Orders.amount_charged LIKE '%${searchKey}%' OR Orders.plan_type LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + sortQuery;
            codeSnip2 = where + filterQuery + sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')';
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery != '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')';
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery;
            codeSnip2 = where + filterQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where;
            codeSnip2 = where;
        }

        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let sqlQuery = 'select Orders.id, Orders.order_date, Orders.order_id, Practices.name AS practice_name, Users.name AS user_name, Cases.case_number, Cases.case_title, ' +
            'Orders.document_type, Orders.document_generation_type, Orders.plan_type, Orders.amount_charged, Orders.billing_type from Orders ' +
            'INNER JOIN Practices ON Orders.practice_id = Practices.id ' +
            'INNER JOIN Users ON Orders.user_id = Users.id ' +
            'INNER JOIN Cases ON Orders.case_id = Cases.id ' + codeSnip;

        let sqlQueryCount = 'select Orders.id, Orders.order_date, Orders.order_id, Practices.name AS practice_name, Users.name AS user_name, Cases.case_number, Cases.case_title, ' +
            'Orders.document_type, Orders.document_generation_type, Orders.plan_type, Orders.amount_charged, Orders.billing_type from Orders ' +
            'INNER JOIN Practices ON Orders.practice_id = Practices.id ' +
            'INNER JOIN Users ON Orders.user_id = Users.id ' +
            'INNER JOIN Cases ON Orders.case_id = Cases.id ' + codeSnip2;


        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });
        for (let i = 0; i < serverData.length; i++) {
            let plan_type = serverData[i].plan_type;
            if (plan_type == 'TEK AS-YOU-GO') {
                serverData[i].plan_type = 'AS-YOU-GO'
            }
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
};

const adminGetAll_new_pagination = async (event) => {
    try {
        authorizeAdminGetAll(event.user);
        const { Orders, Clients, Cases, Op, Practices, Users } = await connectToDatabase();
        /* const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where); */


        let searchKey = '';
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        query.raw = true;
        let query2 = {};
        if (!query.where) {
            query.where = {};
            query2.where = {};
        }
        query.where.is_deleted = { [Op.not]: true };
        query2.where.is_deleted = { [Op.not]: true };
        if (query.practice_id) {
            query.where.practice_id = query.practice_id;
            query2.where.practice_id = query.practice_id;
        }

        validateReadOrders(query);
        query.order = [['createdAt', 'DESC'],];
        query2.order = [
            ['createdAt', 'DESC'],
        ];

        let orders = await Orders.findAll(query);
        const ordersCount = await Orders.count(query2);
        for (let i = 0; i < orders.length; i += 1) {
            const CaseDetails = await Cases.findOne({ where: { id: orders[i].case_id } });
            const practiceDetails = await Practices.findOne({ where: { id: orders[i].practice_id } });
            if (practiceDetails) {
                orders[i].practice_name = practiceDetails.name;
            }
            let clientDetails;
            if (CaseDetails) {
                orders[i].case_title = CaseDetails.case_title;
                orders[i].case_number = CaseDetails.case_number;
                if (!orders[i].client_name) {
                    clientDetails = await Clients.findOne({ where: { id: CaseDetails.client_id } });
                    if (clientDetails) {
                        orders[i].client_name = clientDetails.name;
                    }
                }
            }
            if (!orders[i].order_by) {
                /* const UserDetailsObject = await Users.findOne({where:{id:orders[i].user_id); }}*/
                const UserDetailsObject = await Users.findOne({
                    where: {
                        is_deleted: { [Op.not]: true },
                        id: orders[i].user_id
                    }
                });
                if (UserDetailsObject) {
                    orders[i].order_by = UserDetailsObject.name;
                }
            }
            // if (!orders[i].case_title && orders[i].client_name) {

            // }
        }
        let totalCount = ordersCount;
        const columns = Object.getOwnPropertyNames(orders[0]);
        if (searchKey != 'false' && searchKey.length >= 1) {
            let res = await filterRows(orders, columns, searchKey);
            orders = res;
            totalCount = orders.length;
        }

        let totalPages, currentPage;
        if (query.limit && (orders.length > 0)) {
            totalPages = Math.ceil(ordersCount / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const adminOrderDetails = {
            total_items: ordersCount,
            response: orders,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': totalCount
            },
            body: JSON.stringify(adminOrderDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
};
const ordersSearch_new_pagination = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let filterQuery = '';
        let sortQuery = '';
        let searchQuery = '';
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let codeSnip = '';
        let codeSnip2 = '';
        // let where = 'WHERE Orders.is_deleted IS NOT true';
        let where = `WHERE Orders.is_deleted IS NOT true AND Orders.plan_type IN ('MONTHLY','YEARLY','TEK AS-YOU-GO','FREE TRIAL','YEARLY PROPOUNDING','MONTHLY PROPOUNDING','VIP') `;
        /**Filter**/
        if (query.filter != 'false') {
            query.filter = JSON.parse(query.filter);
            filterKey.product = query.filter.product;
            filterKey.type = query.filter.type;
            if (filterKey.product != 'all' && filterKey.product != '' && filterKey.product) {
                filterQuery += ` AND Orders.document_type LIKE '%${filterKey.product}%'`;
            }
            if (filterKey.type != 'all' && filterKey.type != '' && filterKey.type) {
                filterQuery += ` AND Orders.document_generation_type LIKE '%${filterKey.type}%'`;
            }
        }
        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY Orders.order_date DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' AND Orders.createdAt >= "' + event?.query?.from_date + '" AND Orders.createdAt <= "' + event?.query?.to_date + '"';
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` Users.name LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%' OR Cases.case_title LIKE '%${searchKey}%' OR Cases.case_number LIKE '%${searchKey}%' OR Orders.amount_charged LIKE '%${searchKey}%' OR Orders.plan_type LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + sortQuery;
            codeSnip2 = where + filterQuery + sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')';
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery != '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')';
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery;
            codeSnip2 = where + filterQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where;
            codeSnip2 = where;
        }

        if (!event?.query?.download) {
            codeSnip += ` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let sqlQuery = 'select Orders.id, Orders.order_date, Orders.order_id, Practices.name AS practice_name, Users.name AS user_name, Cases.case_number, Cases.case_title, ' +
            'Orders.document_type, Orders.document_generation_type, Orders.plan_type, Orders.amount_charged, Orders.billing_type from Orders ' +
            'INNER JOIN Practices ON Orders.practice_id = Practices.id ' +
            'INNER JOIN Users ON Orders.user_id = Users.id ' +
            'INNER JOIN Cases ON Orders.case_id = Cases.id ' + codeSnip;

        let sqlQueryCount = 'select Orders.id, Orders.order_date, Orders.order_id, Practices.name AS practice_name, Users.name AS user_name, Cases.case_number, Cases.case_title, ' +
            'Orders.document_type, Orders.document_generation_type, Orders.plan_type, Orders.amount_charged, Orders.billing_type from Orders ' +
            'INNER JOIN Practices ON Orders.practice_id = Practices.id ' +
            'INNER JOIN Users ON Orders.user_id = Users.id ' +
            'INNER JOIN Cases ON Orders.case_id = Cases.id ' + codeSnip2;


        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });
        for (let i = 0; i < serverData.length; i++) {
            let plan_type = serverData[i].plan_type;
            if (plan_type == 'TEK AS-YOU-GO') {
                serverData[i].plan_type = 'AS-YOU-GO'
            }
        }

        let totalPages, currentPage;
        if (query.limit && (serverData.length > 0)) {
            totalPages = Math.ceil(TableDataCount.length / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const OrdersDetails = {
            total_items: TableDataCount.length,
            response: serverData,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(OrdersDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
};

const filterRows = async (rows, columns, searchQuery) => {
    const filteredRows = [];
    if (searchQuery === null || searchQuery === '') {
        return rows;
    }
    rows.forEach(row => {
        columns.some(column => {
            if (row[column] !== undefined && row[column] !== null) {
                const rowValue = String(row[column]).toLowerCase();
                if (rowValue.length >= searchQuery.length && rowValue.indexOf(searchQuery.toLowerCase()) >= 0) {
                    filteredRows.push(row);
                    return true;
                }
            }
            return false;
        });
    });
    return filteredRows;
}
module.exports.create = middy(create).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAll = middy(getAll).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.adminGetAll = adminGetAll;
module.exports.ordersSearch = ordersSearch;
module.exports.PracticeordersSearch = PracticeordersSearch;
