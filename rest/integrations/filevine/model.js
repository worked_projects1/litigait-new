module.exports = (sequelize, type) => sequelize.define('Filevine', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    user_id: type.STRING,
    filevine_user_id: type.STRING,
    filevine_org_id: type.STRING,
    filevine_key: type.TEXT('long'),
    filevine_secret: type.TEXT('long'),
    filevine_refresh_token: type.TEXT('long'),
    filevine_hash: type.TEXT('long'),
    filevine_timeStamp: type.DATE,
    filevine_baseurl: type.TEXT('long'),
});