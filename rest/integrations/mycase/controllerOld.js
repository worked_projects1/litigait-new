const axios = require("axios");
const { Base64 } = require("js-base64");
const decode = require("urldecode");
const connectToDatabase = require("../../../db");
const uuid = require("uuid");
const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.REGION || "us-east-1" });
const s3 = new AWS.S3();
const { QueryTypes } = require("sequelize");
const timeZone = require("../../../utils/timeStamp");
const { HTTPError } = require("../../../utils/httpResp");
const request = axios.create({
  baseURL: process.env.MYCASE_URL,
});

const getMycaseAuthToken = async (event) => {
  try {
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    if (input && !input.code) throw new HTTPError(400, `Code not found.`);
    const code = decode(input.code);
    let result, client_id, client_secret, url, redirect_uri, host;

    const { Mycase, Op, sequelize } = await connectToDatabase();

    client_id = process.env.MYCASE_CLIENTID;
    client_secret = process.env.MYCASE_CLIENTSECRET;
    url = process.env.MYCASE_TOKENURL;
    redirect_uri = process.env.MYCASE_REDIRECTURL;
    host = process.env.MYCASE_HOST;

    const authcode = client_id + ":" + client_secret;
    const encrypted = Base64.btoa(authcode);

    let headers = {
      ContentType: "application/x-www-form-urlencoded",
      Authorization: "Basic " + encrypted,
      Accept: "*/*",
      Host: host,
    };
    let data = { code, grant_type: "authorization_code", redirect_uri, client_id, client_secret };

    await axios.request({ url: url, method: "post", headers, data }).then((response) => {
      result = response.data;
    });

    const dataObject = Object.assign(result, {
      id: uuid.v4(),
      practice_id: event.user.practice_id,
      auth_code: code,
      access_token: result.access_token,
      // access_token_timestamp: new Date(),
      refresh_token: result.refresh_token,
    });

    await Mycase.destroy({ where: { practice_id: event.user.practice_id } });
    await Mycase.create(dataObject);

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "mycase token details successfully fetched",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Can not fetch mycase token details",
      }),
    };
  }
};

const getMycaseData = async (event) => {
  try {
    const { Mycase } = await connectToDatabase();
    const params = event.query || event.pathParameters;
    const pageToken = params.page_token;
    if (!params.type) {
      throw new HTTPError(400, `client or case type not found`);
    }
    await validateToken(event);
    const tokenData = await Mycase.findOne({
      where: { practice_id: event.user.practice_id }
    });

    let clientDetails, dataObject, caseDetails;

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const headers = {
      headers: {
        ContentType: "application/json",
        Authorization: "Bearer " + tokenData.access_token,
      },
    };

    if (params.type == "Client") {
      if (!pageToken) {
        clientDetails = await request.get("/clients", headers);
      }
      if (pageToken) {
        clientDetails = await request.get(
          "/clients?page_token=" + pageToken,
          headers
        );
      }

      let clientHeaderObj = Object.assign({}, clientDetails.headers);
      const clientDataCount = parseInt(clientHeaderObj["item-count"]);

      if (clientDetails.headers.link) {
        const clientDataLink = clientDetails.headers.link.split("page_token=");
        const clientPageToken = clientDataLink[1].split(">;");
        clientDetails.data.page_token = clientPageToken[0];
        dataObject = Object.assign(
          {},
          {
            clientDetails: clientDetails.data,
            totalCount: clientDataCount,
            token: clientPageToken[0],
            hasMore: true,
          }
        );
      } else {
        dataObject = Object.assign(
          {},
          {
            clientDetails: clientDetails.data,
            totalCount: clientDataCount,
            hasMore: false,
          }
        );
      }
    }

    if (params.type == "Cases") {
      if (!pageToken) {
        caseDetails = await request.get("/cases", headers);
      }
      if (pageToken) {
        caseDetails = await request.get(
          "/cases?page_token=" + pageToken,
          headers
        );
      }

      let caseHeaderObj = Object.assign({}, caseDetails.headers);
      const caseDataCount = parseInt(caseHeaderObj["item-count"]);

      if (caseDetails.headers.link) {
        const caseDataLink = caseDetails.headers.link.split("page_token=");
        const casePageToken = caseDataLink[1].split(">;");
        caseDetails.data.page_token = casePageToken[0];
        dataObject = Object.assign(
          {},
          {
            caseDetails: caseDetails.data,
            totalCount: caseDataCount,
            token: casePageToken[0],
            hasMore: true,
          }
        );
      } else {
        dataObject = Object.assign(
          {},
          {
            caseDetails: caseDetails.data,
            totalCount: caseDataCount,
            hasMore: false,
          }
        );
      }
    }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(dataObject),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Can not fetch mycase details",
      }),
    };
  }
};

const createClientandCaseOld = async (event) => {
  try {
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const { Users, Clients, Cases, Mycase, Op } = await connectToDatabase();

    await validateToken(event);
    const tokenData = await Mycase.findOne({ where: { practice_id: event.user.practice_id } });
    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const headers = {
      headers: {
        ContentType: "application/json",
        Authorization: "Bearer " + tokenData.access_token,
      },
    };


    for (let i = 0; i < input.length; i++) {
      const clientDetails = input[i];
      const clientData = await Clients.findOne({
        where: {
          integration_client_id: input[i].client_id, is_deleted: { [Op.not]: true }, practice_id: event.user.practice_id,
        },
      });
      if (event.user.practice_id) {
        clientDetails.practice_id = event.user.practice_id;
      }
      let clientId;
      if (!clientData) {
        let importData = input[i];
        if (importData && !importData.client_id) throw new HTTPError(400, `client id not found.`);
        if (importData && !importData.name) throw new HTTPError(400, `client name not found.`);

        const dataObject = Object.assign(clientDetails, {
          id: uuid.v4(),
          mycase_client_created: new Date(),
          client_from: "mycase",
          integration_client_id: input[i].client_id,
          phone: input[i]?.cell_phone_number || null,
          name: input[i].name,
          address: input[i]?.address || null,
          dob: input[i]?.birthdate || null,
          email: input[i]?.email || null
        });
        const clientsObject = await Clients.create(dataObject);
        clientId = clientsObject.id;
      } else {
        clientId = clientData.id;
      }
      if (input[i].cases) {
        const cases = input[i].cases;
        const attorney = [];
        const casesDetails = cases[0];
        casesDetails.client_id = clientId;
        casesDetails.practice_id = event.user.practice_id;
        const staffDetails = casesDetails.staff;
        if (casesDetails && !casesDetails.id) throw new HTTPError(400, `case id not found.`);

        for (let k = 0; k < staffDetails.length; k++) {
          const mycaseUserDetails = await request.get(`/staff/${staffDetails[k].id}`, headers);
          const mycaseUserData = mycaseUserDetails?.data;
          if (mycaseUserData?.email) {
            const userData = await Users.findOne({
              where: {
                practice_id: event.user.practice_id,
                email: mycaseUserData.email,
                is_deleted: { [Op.not]: true },
              }, raw: true
            });
            if (userData) {
              attorney.push(userData.id)
            }
          }
        }
        const attorneys = attorney.toString();
        const caseData = await Cases.findOne({
          where: {
            integration_case_id: casesDetails.id,
            client_id: clientId,
            is_deleted: { [Op.not]: true },
          },
        });

        if (!caseData) {

          let opened_date = casesDetails.opened_date;
          opened_date = new Date(opened_date).toISOString();

          const casesDataObject = Object.assign(casesDetails, {
            id: uuid.v4(),
            integration_case_id: casesDetails.id,
            matter_id: casesDetails.id,
            case_title: casesDetails.name,
            case_number: casesDetails.case_number,
            date_of_loss: opened_date
          });
          if (attorneys) {
            casesDataObject.attorneys = attorneys;
          }
          const casesObject = await Cases.create(casesDataObject);
        }

      }
    }

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "Successfully Added Clients And Cases",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not create Clients and Cases.",
      }),
    };
  }
};

const createClientandCase = async (event) => {
  try {
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const { Users, Clients, Cases, Mycase, Op } = await connectToDatabase();

    await validateToken(event);
    const tokenData = await Mycase.findOne({ where: { practice_id: event.user.practice_id } });
    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const headers = {
      headers: {
        ContentType: "application/json",
        Authorization: "Bearer " + tokenData.access_token,
      },
    };


    for (let i = 0; i < input.length; i++) {
      const clientDetails = input[i];
      const clientData = await Clients.findOne({
        where: {
          integration_client_id: input[i].client_id, is_deleted: { [Op.not]: true }, practice_id: event.user.practice_id,
        },
      });
      if (event.user.practice_id) {
        clientDetails.practice_id = event.user.practice_id;
      }
      let clientId;
      if (!clientData) {
        let importData = input[i];
        if (importData && !importData.client_id) throw new HTTPError(400, `client id not found.`);
        if (importData && !importData.name) throw new HTTPError(400, `client name not found.`);

        const dataObject = Object.assign(clientDetails, {
          id: uuid.v4(),
          mycase_client_created: new Date(),
          client_from: "mycase",
          integration_client_id: input[i].client_id,
          phone: input[i]?.cell_phone_number || null,
          name: input[i].name,
          address: input[i]?.address || null,
          dob: input[i]?.birthdate || null,
          email: input[i]?.email || null
        });
        const clientsObject = await Clients.create(dataObject);
        clientId = clientsObject.id;
      } else {
        clientId = clientData.id;
      }
      if (input[i].cases) {
        const cases = input[i].cases;
        const attorney = [];
        const casesDetails = cases[0];
        casesDetails.client_id = clientId;
        casesDetails.practice_id = event.user.practice_id;
        const staffDetails = casesDetails.staff;
        if (casesDetails && !casesDetails.id) throw new HTTPError(400, `case id not found.`);

        for (let k = 0; k < staffDetails.length; k++) {
          const mycaseUserDetails = await request.get(`/staff/${staffDetails[k].id}`, headers);
          const mycaseUserData = mycaseUserDetails?.data;
          if (mycaseUserData?.email) {
            const userData = await Users.findOne({
              where: {
                practice_id: event.user.practice_id,
                email: mycaseUserData.email,
                is_deleted: { [Op.not]: true },
              }, raw: true
            });
            if (userData) {
              attorney.push(userData.id)
            }
          }
        }
        const attorneys = attorney.toString();
        const caseData = await Cases.findOne({
          where: {
            integration_case_id: casesDetails.id,
            client_id: clientId,
            is_deleted: { [Op.not]: true },
          },
        });

        if (!caseData) {

          let opened_date = casesDetails.opened_date;
          opened_date = new Date(opened_date).toISOString();

          const casesDataObject = Object.assign(casesDetails, {
            id: uuid.v4(),
            integration_case_id: casesDetails.id,
            matter_id: casesDetails.id,
            case_title: casesDetails.name,
            case_number: casesDetails.case_number,
            date_of_loss: opened_date
          });
          if (attorneys) {
            casesDataObject.attorneys = attorneys;
          }
          const casesObject = await Cases.create(casesDataObject);
        }

      }
    }

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "Successfully Added Clients And Cases",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not create Clients and Cases.",
      }),
    };
  }
};

const UpdateClientandCase = async (event) => {
  try {
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const { Users, Clients, Cases, Mycase, Op } = await connectToDatabase();

    await validateToken(event);
    const tokenData = await Mycase.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');
    const headers = {
      headers: {
        ContentType: "application/json",
        Authorization: "Bearer " + tokenData.access_token,
      },
    };

    for (let i = 0; i < input.length; i++) {
      const clientData = await Clients.findOne({
        where: {
          integration_client_id: input[i].client_id,
          is_deleted: { [Op.not]: true },
          practice_id: event.user.practice_id,
        },
      });
      let clientId;
      if (clientData) {
        let importData = input[i];
        if (importData && !importData.client_id) throw new HTTPError(400, `client id not found.`);
        if (importData && !importData.name) throw new HTTPError(400, `client name not found.`);

        clientData.phone = input[i]?.cell_phone_number || null;
        clientData.name = input[i]?.name || null;
        clientData.address = input[i]?.address || null;
        clientData.dob = input[i]?.birthdate || null;
        await clientData.save();
        clientId = clientData.id;

        if (input[i].cases) {
          const cases = input[i].cases;
            let attorney = [];
            const casesDetails = cases[j];
            if(!casesDetails.id){throw new HTTPError(400, `case id not found.`);}
            casesDetails.client_id = clientId;
            casesDetails.practice_id = event.user.practice_id;
            const staffDetails = casesDetails.staff
            const caseData = await Cases.findOne({
              where: {
                integration_case_id: casesDetails.id,
                client_id: clientId,
                is_deleted: { [Op.not]: true },
              },
            });

            if (caseData) {
              if (caseData.attorneys) {
                attorney = (caseData.attorneys).split(',');
              }
              for (let k = 0; k < staffDetails.length; k++) {
                const mycaseUserDetails = await request.get(`/staff/${staffDetails[k].id}`, headers);
                const mycaseUserData = mycaseUserDetails?.data;
                if (mycaseUserData?.email) {
                  const userData = await Users.findOne({
                    where: {
                      practice_id: event.user.practice_id,
                      email: mycaseUserData.email,
                      role: 'lawyer',
                      is_deleted: { [Op.not]: true },
                    }, raw: true
                  });
                  if (userData && (!attorney.includes(userData.id))) {
                    attorney.push(userData.id);
                  }
                }
              }

              const attorneys = attorney.toString();

              let opened_date = casesDetails.opened_date;
              opened_date = new Date(opened_date).toISOString();

              if (attorneys) {
                caseData.attorneys = attorneys;
              }
              caseData.integration_case_id = casesDetails.id;
              caseData.case_title = casesDetails.name;
              caseData.case_number = casesDetails.case_number;
              caseData.date_of_loss = opened_date;
              await caseData.save();
            }
        }
      }
    }

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "Successfully Added Clients And Cases",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not create Clients and Cases.",
      }),
    };
  }
};

const mycaseFileUpload = async (event) => {
  try {
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const { Mycase } = await connectToDatabase();
    await validateToken(event);
    const tokenData = await Mycase.findOne({
      where: { practice_id: event.user.practice_id }
    });
    if (!input || !input.s3_file_key)
      throw new HTTPError(404, `S3 file key not found.`);
    const s3_file_key = input.s3_file_key;
    const integration_case_id = input.integration_case_id;
    const filename = s3_file_key.split("/");
    const path = input.document_type + "_" + new Date().getTime();
    const body = Object.assign(
      {},
      {
        filename: filename[filename.length - 1],
        path: path,
        assigned_date: timeZone.isoTime(),
      }
    );
    const uploadDetails = await request
      .post(`/cases/${integration_case_id}/documents`, body, {
        headers: {
          Authorization: "Bearer " + tokenData.access_token,
        },
      })
      .then((re) => re.data);

    if (uploadDetails && uploadDetails.put_url) {
      let fileBody = await readDiscoveryFile(s3_file_key);
      const uploadUrl = (uploadDetails && uploadDetails.put_url) || false;
      const config = {
        method: "put",
        url: uploadUrl,
        headers: {
          "Content-Type": "application/octet-stream",
          "X-AMZ-ACL": "private",
        },
        data: fileBody,
      };
      await axios(config).then((re) => console.log(re));
    }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "Files successfully uploaded",
        uploadDetails,
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ error: err.message || "Could upload file" }),
    };
  }
};

const readDiscoveryFile = (filename) => {
  return new Promise(function (resolve, reject) {
    const s3BucketParams = {
      Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
      Key: filename,
    };
    s3.getObject(s3BucketParams, function (err, data) {
      if (err) {
        console.log(err);
        reject(err.message);
      } else {
        // var data = Buffer.from(data.Body).toString('utf8');
        resolve(data.Body);
      }
    });
  });
};

const destroyTokenData = async (event) => {
  try {
    const params = event.params || event.pathParameters;

    const practice_id = event.user.practice_id;

    const { Mycase } = await connectToDatabase();

    const mycasedetails = await Mycase.findOne({ where: { practice_id, id: params.id }, order: [['createdAt', 'DESC']] });

    if (!mycasedetails) {
      throw new HTTPError(400, "Details not found for this id");
    }

    await Mycase.destroy({ where: { practice_id, id: params.id } });

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "Mycase details deleted successfully",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not delete mycase details",
      }),
    };
  }
};


const validateToken = async (event) => {
  try { //once auth token will genereate using refresh token auth code will be empty.
    const { Mycase, Op, sequelize } = await connectToDatabase();

    let sqlQuery = `SELECT id, practice_id, CreatedAt, DATE_ADD(CreatedAt, INTERVAL 1 DAY) AS access_token_valid, ` +
      `DATE_ADD(CreatedAt, INTERVAL 13 DAY) AS refresh_token_valid FROM Mycases ` +
      `HAVING access_token_valid < CURDATE() AND refresh_token_valid > CURDATE() AND practice_id ='${event.user.practice_id}'`;
    // `HAVING refresh_token_valid > CURDATE() AND practice_id ='${event.user.practice_id}'`;

    const response = await sequelize.query(sqlQuery, {
      type: QueryTypes.SELECT,
    });
    if (response.length != 0) {
      const mycaseObj = await Mycase.findOne({
        where: { practice_id: event.user.practice_id },
        order: [["createdAt", "DESC"]],
      });
      let headers = { ContentType: "application/x-www-form-urlencoded" };
      let data = {
        client_id: process.env.MYCASE_CLIENTID,
        client_secret: process.env.MYCASE_CLIENTSECRET,
        grant_type: "refresh_token",
        refresh_token: mycaseObj.refresh_token,
      };
      let result;
      await axios.request({ url: process.env.MYCASE_TOKENURL, method: "post", headers, data, }).then((response) => {
        result = response.data;
      });
      if (result && result.access_token && result.refresh_token) {
        await Mycase.destroy({ where: { practice_id: event.user.practice_id } });
        const dataObject = Object.assign(result, {
          id: uuid.v4(),
          practice_id: event.user.practice_id,
          access_token: result.access_token,
          // access_token_timestamp: new Date(),
          refresh_token: result.refresh_token,
        });
        await Mycase.create(dataObject);
      }
    }
  } catch (err) {
    console.log(err);
  }
}

const updateClientToMycase = async (event) => {
  try {

    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;

    const { Clients, Mycase, Op } = await connectToDatabase();

    await validateToken(event);

    const tokenData = await Mycase.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const { integration_client_id, name, dob, email, phone, address } = input;

    const clientData = await Clients.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_client_id: integration_client_id,
      }
    });

    if (!clientData) { throw new HTTPError(400, 'client not found') };

    const headers = {
      ContentType: "application/json",
      Authorization: "Bearer " + tokenData.access_token,
    };

    let url = `${process.env.MYCASE_URL}/clients/${integration_client_id}`

    let data = {
      first_name: input.first_name,
      last_name: input.last_name,
      cell_phone_number: phone
    };

    if(input.middle_name){ data.middle_initial = input.middle_name };
    clientData.name = name;
    clientData.phone = phone;
    if (address) { clientData.address = address }
    if (dob) {
      clientData.dob = dob;
      data.birthdate = dob;
    }
    if (email) {
      clientData.email = email;
      data.email = email;
    }
    let updatedClientData = await clientData.save();
    let plaintext = updatedClientData.get({ plain: true });

    const mycaseDetails = await axios.request({ url: url, method: "put", headers, data })


    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "client details updated to mycase successfully",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not update client details",
      }),
    };
  }
}

const updateCaseToMycase = async (event) => {
  try {

    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;

    const { Cases, Mycase, Op } = await connectToDatabase();

    await validateToken(event);

    const tokenData = await Mycase.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const { integration_case_id, case_title, case_number, date_of_loss } = input;

    const caseData = await Cases.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_case_id: integration_case_id,
      }
    });

    if (!caseData) { throw new HTTPError(400, 'case not found') };

    caseData.case_title = case_title;
    caseData.case_number = case_number;
    caseData.date_of_loss = date_of_loss;
    await caseData.save();

    const headers = {
      ContentType: "application/json",
      Authorization: "Bearer " + tokenData.access_token,
    };

    let url = `${process.env.MYCASE_URL}/cases/${integration_case_id}`
    let data = {
      name: case_title,
      case_number: case_number,
      opened_date: date_of_loss
    };

    const mycaseDetails = await axios.request({ url: url, method: "put", headers, data })


    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "client details updated to mycase successfully",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not update case details",
      }),
    };
  }
}

module.exports.getMycaseAuthToken = getMycaseAuthToken;
module.exports.createClientandCase = createClientandCase;
module.exports.getMycaseData = getMycaseData;
module.exports.mycaseFileUpload = mycaseFileUpload;
module.exports.destroyTokenData = destroyTokenData;
module.exports.UpdateClientandCase = UpdateClientandCase;
module.exports.updateCaseToMycase = updateCaseToMycase;
module.exports.updateClientToMycase = updateClientToMycase;
