const {HTTPError} = require('../../../utils/httpResp');

const adminInternalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const practiceRoles = ['lawyer', 'paralegal'];
const internalRoles = ['superAdmin', 'manager', 'operator', 'QualityTechnician'];
const superAdminRole = ['superAdmin'];
const normalUserRole = ['superAdmin', 'manager', 'operator'];

exports.authorizeGetClioAuthToken = function (user) {
    if (!practiceRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetClioData = function (user) {
    if (!practiceRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeCreateClientandCase = function (user) {
    if (!practiceRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdateClientandCase = function (user) {
    if (!practiceRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeDestroyTokenData = function (user) {
    if (!practiceRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdateCaseToClio = function (user) {
    if (!practiceRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdateClientToClio = function (user) {
    if (!practiceRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};