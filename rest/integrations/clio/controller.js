const axios = require("axios");
const decode = require("urldecode");
const connectToDatabase = require("../../../db");
const encode = require('encodeurl');
const uuid = require("uuid");
const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.REGION || "us-east-1" });
const s3 = new AWS.S3();
const { QueryTypes } = require("sequelize");
const timeZone = require("../../../utils/timeStamp");
const { HTTPError } = require("../../../utils/httpResp");
const moment = require("moment");
const Baseurl = "https://app.clio.com/";

const { authorizeGetClioAuthToken,
  authorizeGetClioData,
  authorizeCreateClientandCase,
  authorizeUpdateClientandCase,
  authorizeDestroyTokenData,
  authorizeUpdateCaseToClio,
  authorizeUpdateClientToClio
} = require("./authorize");

const { getSecrets } = require('../../helpers/secrets.helper')

const getClioAuthToken = async (event) => {
  try {
    authorizeGetClioAuthToken(event.user);
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    if (input && !input.code) throw new HTTPError(400, `Code not found.`);
    // const code = decode(input.code);
    const code = input.code;
    let client_id, client_secret, url, redirect_uri;
    const { Clio } = await connectToDatabase();

    client_id = process?.env?.CLIO_CLIENTID ? process?.env?.CLIO_CLIENTID : await getSecrets('CLIO_CLIENTID');
    client_secret = process?.env?.CLIO_CLIENTSECRET ? process?.env?.CLIO_CLIENTSECRET : await getSecrets('CLIO_CLIENTSECRET');
    url = Baseurl + 'oauth/token';
    redirect_uri = process?.env?.CLIO_REDIRECTURL ? process?.env?.CLIO_REDIRECTURL : await getSecrets('CLIO_REDIRECTURL');;

    let headers = {
      Accept: "*/*",
    };
    let params = { code, grant_type: "authorization_code", redirect_uri, client_id, client_secret };

    const result = await axios.request({ url: url, params, method: "post", headers }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`Clio integration failed: ${error.message}`);
      });

    const dataObject = Object.assign(result, {
      id: uuid.v4(),
      practice_id: event.user.practice_id,
      auth_code: code,
      access_token: result.access_token,
      access_token_timestamp: new Date(),
      refresh_token: result.refresh_token,
    });

    await Clio.destroy({ where: { practice_id: event.user.practice_id } });
    const clioDetails = await Clio.create(dataObject);

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "clio token details successfully fetched",
        clioDetails
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Can't fetch token details from Clio",
      }),
    };
  }
};

const getClioData = async (event) => {
  try {
    authorizeGetClioData(event.user);
    const { Clio } = await connectToDatabase();
    const params = event.query || event.pathParameters;
    const pageToken = params.page_token;
    if (!params.type) {
      throw new HTTPError(400, `type not found found in params`); //client or case
    }
    validateToken(event);
    const tokenData = await Clio.findOne({
      where: { practice_id: event.user.practice_id }
    });

    let clientDetails, dataObject, caseDetails;

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const headers = {
      ContentType: "application/json",
      Authorization: "Bearer " + tokenData.access_token,
    };

    if (params.type == "Client") {
      let fields = `fields=id,name,first_name,middle_name,last_name,date_of_birth,type,primary_address{street,city,province,postal_code,country,name,id},primary_email_address{id, etag, address, name},primary_phone_number{id, number, name},email_addresses{id, primary},phone_numbers{id, primary},is_client,created_at&limit=230&type=Person`; //limit = 200
      if (pageToken) {
        fields += `&page_token=${pageToken}`;
      }

      let url = `${Baseurl}api/v4/contacts.json?${fields}`;
      clientDetails = await axios.request({ url: url, method: "get", headers}).then((response) => response.data)
        .catch((error) => {
          console.log(error);
          throw new Error(`Clio integration failed: ${error.message}`);
        });
      const clientDataCount = clientDetails?.meta?.records;
        console.log(clientDetails?.meta);
      if (clientDetails?.meta?.paging?.next) {
        const clientPageTokenDetails = clientDetails.meta.paging.next.split("page_token=");
        const clientPageToken = clientPageTokenDetails[1].split("&");
        clientDetails.page_token = clientPageToken[0];
        dataObject = Object.assign(
          {},
          {
            clientDetails: clientDetails,
            totalCount: clientDataCount,
            token: clientPageToken[0],
            hasMore: true,
          }
        );
      } else {
        dataObject = Object.assign(
          {},
          {
            clientDetails: clientDetails,
            totalCount: clientDataCount,
            hasMore: false,
          }
        );
      }
    }
    if (params.type == "Cases") {
      let fields = `fields=id,close_date,description,display_number,client{first_name,id,last_name,middle_name,name,primary_email_address,primary_phone_number,type,is_client,created_at},status,originating_attorney{enabled,first_name,id,last_name,name,email},responsible_attorney{enabled,first_name,id,last_name,name,email},&limit=230&status=Open&type=Person`; //limit = 200
      if (pageToken) {
        fields += `&page_token=${pageToken}`;
      }
      let url = `${Baseurl}api/v4/matters.json?${fields}`;
      caseDetails = await axios.request({ url: url, method: "get", headers}).then((response) => response.data);
      const caseDataCount = caseDetails?.meta?.records;
      if (caseDetails?.meta?.paging?.next) {
        const casePageTokenDetails = caseDetails.meta.paging.next.split("page_token=");
        const casePageToken = casePageTokenDetails[1].split("&");
        caseDetails.data.page_token = casePageToken[0];
        dataObject = Object.assign(
          {},
          {
            caseDetails: caseDetails,
            totalCount: caseDataCount,
            token: casePageToken[0],
            hasMore: true,
          }
        );
      } else {
        dataObject = Object.assign(
          {},
          {
            caseDetails: caseDetails,
            totalCount: caseDataCount,
            hasMore: false,
          }
        );
      }
    }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(dataObject),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Can't fetch details from Clio",
      }),
    };
  }
};

const destroyTokenData = async (event) => {
  try {
    authorizeDestroyTokenData(event.user);
    const params = event.params || event.pathParameters;

    const practice_id = event.user.practice_id;

    const { Clio } = await connectToDatabase();

    const clioDetails = await Clio.findOne({ where: { practice_id, id: params.id } });

    if (!clioDetails) {
      throw new HTTPError(400, "Details not found for this id");
    }

    await Clio.destroy({ where: { practice_id, id: params.id } });

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "Clio details Destroyed successfully",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't Destroy Clio details",
      }),
    };
  }
};

const validateToken = async (event) => {
  try {
    const { Clio, Op, sequelize } = await connectToDatabase();

    let sqlQuery = `SELECT id, practice_id, CreatedAt,` + ` DATE_ADD(CreatedAt, INTERVAL 30 DAY) AS access_token_valid, now() FROM Clios ` + `where DATE_ADD(CreatedAt, INTERVAL 30 DAY) <= now() AND practice_id ='${event.user.practice_id}'`;

    const response = await sequelize.query(sqlQuery, {
      type: QueryTypes.SELECT,
    });

    if (response.length != 0) {
      const ClioObj = await Clio.findOne({
        where: { practice_id: event.user.practice_id }
      });
      const client_id = process?.env?.CLIO_CLIENTID ? process?.env?.CLIO_CLIENTID : await getSecrets('CLIO_CLIENTID');
      const client_secret = process?.env?.CLIO_CLIENTSECRET ? process?.env?.CLIO_CLIENTSECRET : await getSecrets('CLIO_CLIENTSECRET');
      let url = Baseurl + 'oauth/token';
      let redirect_uri = process?.env?.CLIO_REDIRECTURL ? process?.env?.CLIO_REDIRECTURL : await getSecrets('CLIO_REDIRECTURL');
      let refresh_token = ClioObj.refresh_token;


      let headers = {
        Accept: "*/*",
      };
      let params = { refresh_token, "Content-Type": "application/x-www-form-urlencoded", grant_type: "refresh_token", redirect_uri, client_id, client_secret };

      const result = await axios.request({ url: url, params, method: "post", headers }).then((response) => response.data)
        .catch((error) => {
          console.log(error);
          throw new Error(`Clio integration failed: ${error.message}`);
        });
      console.log(result);
      if (result?.access_token) {
        await Clio.destroy({ where: { practice_id: event.user.practice_id } });
        const dataObject = Object.assign(result, {
          id: uuid.v4(),
          practice_id: event.user.practice_id,
          auth_code: ClioObj.auth_code,
          access_token: result.access_token,
          access_token_timestamp: new Date(),
          refresh_token: ClioObj.refresh_token,
        });
        await Clio.create(dataObject);
      }
    }
  } catch (err) {
    console.log(err);
    throw new Error(`Clio token validation failed : ${err.message}`);
  }
}

const createClientandCase = async (event) => {
  try {
    authorizeCreateClientandCase(event.user);
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const { Clients, Cases, Users, Op } = await connectToDatabase();
    let clientId;
    for (let i = 0; i < input.length; i++) {
      const clientDetails = input[i];

      if (clientDetails && !clientDetails.client_id) throw new HTTPError(400, `client id not found.`);
      if (clientDetails && !clientDetails.name) throw new HTTPError(400, `client name not found.`);

      const clientData = await Clients.findOne({
        where: {
          integration_client_id: clientDetails.client_id,
          is_deleted: { [Op.not]: true },
          practice_id: event.user.practice_id,
          client_from: "clio"
        },
      });

      if (event.user.practice_id) {
        clientDetails.practice_id = event.user.practice_id;
      }
      if (!clientData) {
        const ClientDataObject = Object.assign(clientDetails, {
          id: uuid.v4(),
          clio_client_created: new Date(),
          client_from: 'clio',
          integration_client_id: clientDetails.client_id,
          name: clientDetails.name,
          first_name: clientDetails.first_name || null,
          middle_name: clientDetails.middle_name || null,
          last_name: clientDetails.last_name || null,
          email: clientDetails?.email || null,
          phone: clientDetails?.phone || null,
          dob: clientDetails?.dob || null,
          address: clientDetails?.address || null,
          street: clientDetails?.primary_address?.street || null,
          city: clientDetails?.primary_address?.city || null,
          state: clientDetails?.primary_address?.province || null,
          zip_code: clientDetails?.primary_address?.postal_code || null,
          country: clientDetails?.primary_address?.country || null,
        });

        const clientsObject = await Clients.create(ClientDataObject);
        clientId = await clientsObject.id;
      } else { clientId = await clientData.id; }

      if (input[i].cases) {
        const cases = input[i].cases;
        const attorney = [];
        const attorneyEmail = [];
        const casesDetails = cases[0];
        casesDetails.client_id = clientId;
        casesDetails.practice_id = event.user.practice_id;
        if (casesDetails?.originating_attorney?.email) { attorneyEmail.push(casesDetails.originating_attorney.email) };
        if (casesDetails?.responsible_attorney?.email) { attorneyEmail.push(casesDetails.responsible_attorney.email) };
        if (attorneyEmail.length != 0) {
          const userData = await Users.findAll({
            where: {
              practice_id: event.user.practice_id,
              email: { [Op.in]: attorney },
              is_deleted: { [Op.not]: true },
            }, raw: true
          });

          for (let j = 0; j < userData.length; j++) {
            attorney.push(userData.id);
          }
        }
        const attorneys = attorney.toString();
        const caseData = await Cases.findOne({
          where: {
            integration_case_id: casesDetails.id,
            client_id: clientId,
            is_deleted: { [Op.not]: true },
            case_from: "clio"
          },
        });
        if (!caseData) {
          const caseDataObject = Object.assign(casesDetails, {
            id: uuid.v4(),
            matter_id: clientDetails.case_id,
            integration_case_id: clientDetails.case_id,
            integration_case_data: JSON.stringify(clientDetails),
            case_from: "clio",
            case_title: clientDetails?.case_title || null,
          });

          if (attorneys) {
            caseDataObject.attorneys = attorneys;
          }

          const casesObject = await Cases.create(caseDataObject);
        }
      }
    }
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        status: 'ok',
        message: "Client and Case created successfully using clio details",
      }),
    };
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || "Couldn't create Clients and Cases using Clio details" }),
    };
  }
}

const UpdateClientandCase = async (event) => {
  try {
    authorizeUpdateClientandCase(event.user);
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const { Users, Clients, Cases, Clio, Op } = await connectToDatabase();

    let clientId;

    const tokenData = await Clio.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    for (let i = 0; i < input.length; i++) {
      let clientDetails = input[i];
      if (!clientDetails.client_id) throw new HTTPError(400, `client id not found.`);
      if (!clientDetails.name) throw new HTTPError(400, `client name not found.`);

      const clientData = await Clients.findOne({
        where: {
          integration_client_id: clientDetails.client_id,
          is_deleted: { [Op.not]: true },
          practice_id: event.user.practice_id,
          client_from: "clio"
        }
      });

      if (clientData) {
        clientData.name = clientDetails?.name || null;
        clientData.first_name = clientDetails?.first_name || null;
        clientData.middle_name = clientDetails?.middle_name || null;
        clientData.last_name = clientDetails?.last_name || null;
        clientData.email = clientDetails?.email || null;
        clientData.phone = clientDetails?.phone || null;
        clientData.address = clientDetails?.address || null;
        clientData.dob = clientDetails?.dob || null;
        clientData.street = clientDetails?.primary_address?.street || null,
          clientData.city = clientDetails?.primary_address?.city || null,
          clientData.state = clientDetails?.primary_address?.province || null,
          clientData.zip_code = clientDetails?.primary_address?.postal_code || null,
          clientData.country = clientDetails?.primary_address?.country || null
      }
      await clientData.save();
      clientId = clientData.id;
      if (input[i].cases) {
        const cases = input[i].cases;
        const casesDetails = cases[0];
        if (!casesDetails.id) { throw new HTTPError(400, `case id not found`); }
        const caseData = await Cases.findOne({
          where: {
            integration_case_id: input[i].case_id,
            client_id: clientId,
            is_deleted: { [Op.not]: true },
            case_from: "clio"
          },
        });
        if (caseData) {
          let attorney = [];
          const attorneyEmail = [];
          casesDetails.client_id = clientId;
          casesDetails.practice_id = event.user.practice_id;
          if (casesDetails?.originating_attorney?.email) { attorneyEmail.push(casesDetails.originating_attorney.email) };
          if (casesDetails?.responsible_attorney?.email) { attorneyEmail.push(casesDetails.responsible_attorney.email) };

          if (caseData.attorneys) {
            attorney = (caseData.attorneys).split(',');
          }

          if (attorneyEmail.length != 0) {
            const userData = await Users.findAll({
              where: {
                practice_id: event.user.practice_id,
                email: { [Op.in]: attorney },
                is_deleted: { [Op.not]: true },
              }, raw: true
            });

            for (let j = 0; j < userData.length; j++) {
              if (userData[j]?.id && (!attorney.includes(userData[j].id))) {
                attorney.push(userData[j].id);
              }
            }
          }
          const attorneys = attorney.toString();
          caseData.case_title = input[i]?.case_title || null;
          caseData.attorneys = attorneys || null;
          caseData.integration_case_data = JSON.stringify(input[i]);
          await caseData.save();
        }
      }
    }
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        status: 'ok',
        message: 'Client and Case successfully updated using clio details',
      }),
    };
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || "Couldn't update clients and cases using clio details." }),
    };
  }
}

const updateCaseToClio = async (event) => {
  try {
    authorizeUpdateCaseToClio(event.user);
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;

    const { Cases, Clio, Op } = await connectToDatabase();

    await validateToken(event);

    const tokenData = await Clio.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const { integration_case_id, case_title, case_number, date_of_loss } = input;

    const caseData = await Cases.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_case_id: integration_case_id,
      }
    });

    if (!caseData) { throw new HTTPError(400, 'case not found') };

    caseData.case_title = case_title;
    caseData.case_number = case_number;
    caseData.date_of_loss = date_of_loss;
    await caseData.save();

    const headers = {
      ContentType: "application/json",
      Authorization: "Bearer " + tokenData.access_token,
    };

    let caseDetails = {
      data: {
        description: case_title,
      }
    };

    let url = `${Baseurl}api/v4/matters/${integration_case_id}.json`;

    const clioDetails = await axios.request({ url: url, method: "patch", headers, data: caseDetails }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`Clio integration failed: ${error.message}`);
      });

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "client details successfully updated to clio",
        clioDetails
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't update case details to clio",
      }),
    };
  }
}

const updateClientToClio = async (event) => {
  try {
    authorizeUpdateClientToClio(event.user);
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;

    const { Clients, Clio, Op, Cases } = await connectToDatabase();

    await validateToken(event);

    const tokenData = await Clio.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const { integration_client_id, first_name, last_name, middle_name, dob, email, phone, address, street, city, state, zip_code } = input;
    if (!integration_client_id) { throw new HTTPError(400, 'Clio Integration client id not provided') };
    const clientData = await Clients.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_client_id: integration_client_id,
        client_from: "clio",
        is_deleted: { [Op.not]: true }
      }
    });

    if (!clientData) { throw new HTTPError(400, 'client not found') };

    const caseData = await Cases.findOne({
      where: {
        practice_id: event.user.practice_id,
        client_id: clientData.id,
        case_from: "clio",
        is_deleted: { [Op.not]: true }
      }
    });

    if (!caseData) { throw new HTTPError(400, 'case not found') };
    const integrationCaseData = JSON.parse(caseData?.integration_case_data);

    const headers = {
      ContentType: "application/json",
      Authorization: "Bearer " + tokenData.access_token,
    };

    let url = `${Baseurl}api/v4/contacts/${integration_client_id}.json`;
    let name;
    if (first_name) name = first_name;
    if (middle_name) name += ' ' + middle_name;
    if (last_name) name += ' ' + last_name;

    let clientDetails = {
      data: {
        name: name,
        phone_numbers: {
          number: phone,
        }
      }
    };

    clientData.name = name;
    clientData.phone = phone;
    if (address) { clientData.address = address }
    if (dob) {
      clientData.dob = dob;
      clientDetails.data.date_of_birth = moment(dob).format('YYYY-MM-DD');
    }
    if (email) {
      clientData.email = email;
      clientDetails.data.email_addresses = {
        address: email
      };
      if (integrationCaseData.clio_email_reference_id) { clientDetails.data.email_addresses.id = integrationCaseData.clio_email_reference_id };
    }
    if (street) {
      clientDetails.data.addresses = {
        street: street,
        city: city,
        province: state,
        postal_code: zip_code
      };
      if (integrationCaseData.clio_address_reference_id) {
        clientDetails.data.addresses.id = integrationCaseData.clio_address_reference_id;
      }
      console.log(integrationCaseData);
      clientDetails.data.addresses.country = integrationCaseData?.primary_address?.country || null;

      clientData.street = street;
      clientData.city = city;
      clientData.state = state;
      clientData.zip_code = zip_code;
    }
    console.log(clientDetails);
    if (integrationCaseData.clio_phone_reference_id) { clientDetails.data.phone_numbers.id = integrationCaseData.clio_phone_reference_id };

    await clientData.save();

    const clioDetails = await axios.request({ url: url, method: "patch", headers, data: clientDetails }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`Clio integration failed: ${error.message}`);
      });

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "client details successfully updated to clio",
        clioDetails
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't update client details to clio",
      }),
    };
  }
}

const clioFileUpload = async (event) => {
  try {
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;

    if (!input || !input.s3_file_key) throw new HTTPError(404, `S3 file key not found.`);
    const { Clients, Clio, Op } = await connectToDatabase();

    await validateToken(event);

    const tokenData = await Clio.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const s3_file_key = input.s3_file_key;
    const integration_case_id = input.integration_case_id;
    if (!integration_case_id) throw new HTTPError(404, `Integration case id not found in input`);
    const filenameArr = s3_file_key.split("/");
    const filename = filenameArr[filenameArr.length - 1];

    const fileUploadData = {
      data: {
        name: filename,
        parent: {
          id: integration_case_id,
          type: "Matter"
        }
      }
    };

    const headers = {
      ContentType: "application/json",
      Authorization: "Bearer " + tokenData.access_token,
    };

    let statusUploadDetails;

    const uploadUrldetails = await axios.request({
      url: `${Baseurl}api/v4/documents?fields=id,latest_document_version{uuid,put_url,put_headers}`,
      method: "post", headers, data: fileUploadData
    }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`An error occurred during the Clio request while document uploading process : ${error.message}`);
      });

    let fileBody = await readDiscoveryFile(s3_file_key);
    const uploadUrl = uploadUrldetails?.data?.latest_document_version?.put_url || false;

    const config = {
      method: "put",
      url: uploadUrl,
      headers: {
        "Content-Type": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "x-amz-server-side-encryption": "AES256"
      },
      data: fileBody,
    };

    const putData = await axios(config).then((re) => re.status);
    if (putData == '200') {
      const statusUpdateData = {
        data: {
          uuid: uploadUrldetails?.data?.latest_document_version?.uuid,
          fully_uploaded: "true"
        }
      }
      statusUploadDetails = await axios.request({
        url: `${Baseurl}api/v4/documents/${uploadUrldetails?.data?.id}?fields=id,latest_document_version{fully_uploaded}`,
        method: "patch", headers, data: statusUpdateData
      }).then((response) => response.data)
        .catch((error) => {
          console.log(error);
          throw new Error(`An error occurred during the Clio request while document upload status updating process : ${error.message}`);
        });
    }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "client details successfully updated to clio",
        statusUploadDetails
      }),
    };
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Origin": true,
      },
      body: JSON.stringify({
        error: err.message || "Document upload to clio failed"
      })
    };
  }
}

const readDiscoveryFile = (filename) => {
  return new Promise(function (resolve, reject) {
    const s3BucketParams = {
      Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
      Key: filename,
    };
    s3.getObject(s3BucketParams, function (err, data) {
      if (err) {
        console.log(err);
        reject(err.message);
      } else {
        //var data = Buffer.from(data.Body).toString('utf8');
        resolve(data.Body);
      }
    });
  });
};

const commonUpdateClio = async (event) => {
  try {
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;

    const { Clients, Clio, Op, Cases } = await connectToDatabase();

    await validateToken(event);

    const tokenData = await Clio.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const { integration_client_id, client_phone } = input;
    if (!integration_client_id) { throw new HTTPError(400, 'Clio Integration client id not provided') };
    if (client_phone) {
      const clientData = await Clients.findOne({
        where: {
          practice_id: event.user.practice_id,
          integration_client_id: integration_client_id,
          client_from: "clio",
          is_deleted: { [Op.not]: true }
        }
      });
      if (!clientData) { throw new HTTPError(400, 'client not found') };

      const caseData = await Cases.findOne({
        where: {
          practice_id: event.user.practice_id,
          client_id: clientData.id,
          case_from: "clio",
          is_deleted: { [Op.not]: true }
        }
      });

      if (!caseData) { throw new HTTPError(400, 'case not found') };

      const headers = {
        ContentType: "application/json",
        Authorization: "Bearer " + tokenData.access_token,
      };

      let url = `${Baseurl}api/v4/contacts/${integration_client_id}.json`;

      let clientDetails = {
        data: {
          phone_numbers: {
            number: client_phone,
          }
        }
      };

      const integrationCaseData = JSON.parse(caseData?.integration_case_data);
      if (integrationCaseData.clio_phone_reference_id) { clientDetails.data.phone_numbers.id = integrationCaseData.clio_phone_reference_id };

      const clioDetails = await axios.request({ url: url, method: "patch", headers, data: clientDetails }).then((response) => response.data)
        .catch((error) => {
          console.log(error);
          throw new Error(`Clio integration failed: ${error.message}`);
        });
    }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "client details successfully updated to clio",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't update client details to clio",
      }),
    };
  }
}

const updateGetOneClioData = async (event) => {
  try {
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;

    const { Clients, Clio, Op, Cases } = await connectToDatabase();

    await validateToken(event);

    const tokenData = await Clio.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const { integration_client_id, integration_case_id,case_id, client_id } = input;
    if (!integration_client_id) { throw new HTTPError(400, 'Clio Integration client id not provided') };
    if (case_id.length == 0) { throw new HTTPError(400, 'Case id not provided') };
    if (!client_id) { throw new HTTPError(400, 'Client id not provided') };
    if (integration_case_id.length == 0) { throw new HTTPError(400, 'Integration case id not provided') };
    
    const clientData = await Clients.findOne({
      where: {
        id: client_id,
        integration_client_id: integration_client_id,
        practice_id: event?.user?.practice_id,
        client_from: "clio",
        is_deleted: { [Op.not]: true }
      }
    });
    if (!clientData) { throw new HTTPError(400, 'Case Data not found') };

    let fields = `fields=id,name,first_name,middle_name,last_name,date_of_birth,type,primary_address{street,city,province,postal_code,country,name,id},primary_email_address{id, etag, address, name},primary_phone_number{id, number, name},email_addresses{id, primary},phone_numbers{id, primary},is_client,created_at&type=Person`;

      const url = `${Baseurl}api/v4/contacts/${integration_client_id}.json?${fields}`;

      const headers = {
        ContentType: "application/json",
        Authorization: "Bearer " + tokenData.access_token,
      };

      const clioClientDetails = await axios.request({ url: url, method: "get", headers}).then((response) => response.data.data)
        .catch((error) => {
          console.log(error);
          throw new Error(`Clio integration failed: ${error.message}`);
        });
    if (!clioClientDetails) { throw new HTTPError(400, 'Client data not found in Clio') };

    const caseData = await Cases.findOne({
      where: {
        id: case_id,
        client_id: client_id,
        integration_case_id: integration_case_id,
        practice_id: event?.user?.practice_id,
        case_from: "clio",
        is_deleted: { [Op.not]: true }
      }
    });
    if (!caseData) { throw new HTTPError(400, 'Case Data not found') };

    console.log(clioClientDetails);
    if(caseData?.integration_case_data && clioClientDetails?.id){
      const integrationCaseData = JSON.parse(caseData.integration_case_data);
      if (clioClientDetails?.email_addresses.length > 0) {
        integrationCaseData.email_addresses = clioClientDetails.email_addresses;
        const emailDatas = clioClientDetails?.email_addresses;
        emailDatas.map(e =>{
          if(e.primary == true){
            integrationCaseData.clio_email_reference_id = e.id;
          }
        });
      }
      if (clioClientDetails?.primary_address?.id) {
        integrationCaseData.clio_address_reference_id = clioClientDetails.primary_address.id;
        integrationCaseData.primary_address = clioClientDetails.primary_address;
        if(!clientData.street){
          const address = clioClientDetails?.primary_address?.street + ', ' + clioClientDetails?.primary_address?.city + ', ' + clioClientDetails?.primary_address?.province + ' - ' + clioClientDetails?.primary_address?.postal_code;
          clientData.address = address;
          clientData.street = clioClientDetails?.primary_address?.street;
          clientData.city = clioClientDetails?.primary_address?.city;
          clientData.state = clioClientDetails?.primary_address?.province;
          clientData.zip_code = clioClientDetails?.primary_address?.postal_code;
          clientData.country = clioClientDetails?.primary_address?.country;
       }
      }
      if (clioClientDetails?.phone_numbers.length > 0) {
        integrationCaseData.phone_numbers = clioClientDetails.phone_numbers;
        const phoneNumberDatas = clioClientDetails?.phone_numbers;
        phoneNumberDatas.map( p =>{
          if(p.primary == true){
            integrationCaseData.clio_phone_reference_id = p.id;
          }
        });
      }
      caseData.integration_case_data = JSON.stringify(integrationCaseData);
      await caseData.save();
      await clientData.save();
    }

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "client details successfully updated to our database",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't update client details our database",
      }),
    };
  }
}

module.exports.getClioAuthToken = getClioAuthToken;
module.exports.getClioData = getClioData;
module.exports.destroyTokenData = destroyTokenData;
module.exports.createClientandCase = createClientandCase;
module.exports.UpdateClientandCase = UpdateClientandCase;
module.exports.updateCaseToClio = updateCaseToClio;
module.exports.updateClientToClio = updateClientToClio;
module.exports.clioFileUpload = clioFileUpload;
module.exports.commonUpdateClio = commonUpdateClio;
module.exports.updateGetOneClioData = updateGetOneClioData;