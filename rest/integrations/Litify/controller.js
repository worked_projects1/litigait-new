const axios = require("axios");
const connectToDatabase = require("../../../db");
const uuid = require("uuid");
const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.REGION || "us-east-1" });
const s3 = new AWS.S3();
const { HTTPError } = require("../../../utils/httpResp");
const { litifyAuthToken } = require("./access_token");
const moment = require("moment");
const Baseurl = "https://esquiretek2.my.salesforce.com/services/data/v57.0/"

const getLitifyData = async (event) => {
  try {
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    if (!input.integration_case_id) { throw new HTTPError(400, 'integration case id not found') };

    const { Clients, Cases, States, Op } = await connectToDatabase();
    const case_url = `${Baseurl}sobjects/litify_pm__Matter__c/${input.integration_case_id}`;

    const accessToken = await litifyAuthToken()
    const headers = {
      Authorization: "Bearer " + accessToken
    };
    let ResponseData = {}, clientData, states;

    const caseDetails = await axios.request({ url: case_url, method: "get", headers }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`Litify integration failed: ${error.message}`);
      });
    if (!caseDetails) { throw new HTTPError(400, 'case details not found in Litify') };

    let stateCode;
    if (caseDetails?.litify_pm__Matter_State__c) {
      let stateCodeArr = caseDetails.litify_pm__Matter_State__c.split(' ');
      stateCode = stateCodeArr[0];
      states = await States.findOne({ where: { state_code: stateCode } });
    }

    if (!(states || caseDetails?.litify_pm__Matter_State__c == null)) {
      throw new HTTPError(400, 'Unsupported state in EsquireTek.');
    }
    const litifyClientId = caseDetails.litify_pm__Client__c;

    const client_url = `${Baseurl}sobjects/Account/${litifyClientId}`;

    const clientDetails = await axios.request({ url: client_url, method: "get", headers }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`Litify integration failed: ${error.message}`);
      });
    if (!clientDetails) { throw new HTTPError(400, 'client details not found in Litify') };

    const clientDatas = await Clients.findOne({
      where: {
        integration_client_id: clientDetails?.Id,
        practice_id: event.user.practice_id,
        client_from: "litify",
        is_deleted: { [Op.not]: true },
      }
    });

    if (!clientDatas && caseDetails && clientDetails && (states || caseDetails?.litify_pm__Matter_State__c == null)) {
      const clientDataObj = Object.assign({}, {
        id: uuid.v4(),
        practice_id: event.user.practice_id,
        phone: clientDetails.litify_pm__Phone_Mobile__c || null,
        dob: clientDetails.litify_pm__Date_of_birth__c || null,
        email: clientDetails.litify_pm__Email__c || null,
        integration_client_id: clientDetails.Id,
        client_from: "litify"
      });
      let name = '';
      if (clientDetails.Name && !clientDetails?.litify_pm__First_Name__c && !clientDetails?.litify_pm__Last_Name__c) {
        name += clientDetails.Name;
        let names = name.split(' ');
        clientDataObj.first_name = names[0];
        if (names.length > 1) {
          names.shift();
          clientDataObj.last_name = names.join(' ');
        }
      };
      if (clientDetails?.litify_pm__First_Name__c) {
        name += clientDetails.litify_pm__First_Name__c;
        clientDataObj.first_name = clientDetails.litify_pm__First_Name__c;
      }
      if (clientDetails?.litify_pm__Last_Name__c) {
        if (clientDetails.litify_pm__First_Name__c) name = name + ' '
        name += clientDetails?.litify_pm__Last_Name__c;
        clientDataObj.last_name = clientDetails.litify_pm__Last_Name__c;
      }
      clientDataObj.name = name;
      clientData = await Clients.create(clientDataObj);
      if (!clientData) { throw new HTTPError(400, 'client data not found') };
      ResponseData.client = clientData;
      const integrationCaseData = Object.assign({}, clientDetails, caseDetails);
      const caseDataObj = Object.assign({}, {
        id: uuid.v4(),
        practice_id: event.user.practice_id,
        client_id: clientData.id,
        integration_case_id: caseDetails.Id,
        case_title: caseDetails.litify_pm__Case_Title__c,
        case_plaintiff_name: clientDataObj.name,
        county: caseDetails.litify_pm__Matter_City__c || null,
        state: stateCode || null,
        date_of_loss: caseDetails.litify_pm__Incident_date__c,
        integration_case_data: JSON.stringify(integrationCaseData),
        case_from: "litify"
      });
      if (caseDetails?.litify_pm__OpposingParty__c) {
        caseDataObj.case_defendant_name = caseDetails.litify_pm__OpposingParty__c;
      };
      const caseData = await Cases.create(caseDataObj);
      caseData.integration_case_data = JSON.parse(caseData.integration_case_data);
      ResponseData.case = caseData;
    };
    if (clientDatas && caseDetails && clientDetails && (states || caseDetails?.litify_pm__Matter_State__c == null)) {
      let name = '';
      if (clientDetails?.Name && !clientDetails?.litify_pm__First_Name__c && !clientDetails?.litify_pm__Last_Name__c) {
        name += clientDetails.Name;
        let names = name.split(' ');
        clientDatas.first_name = names[0];
        if (names.length > 1) {
          names.shift();
          clientDatas.last_name = names.join(' ');
        }
      };
      if (clientDetails?.litify_pm__First_Name__c) {
        name += clientDetails.litify_pm__First_Name__c;
        clientDatas.first_name = clientDetails.litify_pm__First_Name__c;
      }
      if (clientDetails?.litify_pm__Last_Name__c) {
        if (clientDetails.litify_pm__First_Name__c) name = name + ' '
        name += clientDetails?.litify_pm__Last_Name__c;
        clientDatas.last_name = clientDetails.litify_pm__Last_Name__c;
      }
      clientDatas.name = name;
      clientDatas.phone = clientDetails.litify_pm__Phone_Mobile__c || null,
        clientDatas.dob = clientDetails.litify_pm__Date_of_birth__c || null,
        clientDatas.email = clientDetails.litify_pm__Email__c || null,
        await clientDatas.save();
      ResponseData.client = clientDatas;

      const caseDatas = await Cases.findOne({
        where: {
          integration_case_id: caseDetails?.Id,
          practice_id: event.user.practice_id,
          client_id: clientDatas.id,
          is_deleted: { [Op.not]: true },
          case_from: "litify"
        }
      });

      if (caseDatas) {
        const integrationCaseData = Object.assign({}, clientDetails, caseDetails);
        caseDatas.case_title = caseDetails.litify_pm__Case_Title__c;
        caseDatas.date_of_loss = caseDetails.litify_pm__Incident_date__c;
        caseDatas.state = stateCode || null;
        caseDatas.county = caseDetails.litify_pm__Matter_City__c || null;
        caseDatas.integrationCaseData = JSON.stringify(integrationCaseData);
        if (caseDetails?.litify_pm__OpposingParty__c) {
          caseDatas.case_defendant_name = caseDetails.litify_pm__OpposingParty__c;
        };
        caseDatas.case_plaintiff_name = clientDatas.name;
        await caseDatas.save();
        caseDatas.integration_case_data = JSON.parse(caseDatas.integration_case_data);
        ResponseData.case = caseDatas;
      }
      if (!caseDatas) {
        const integrationCaseData = Object.assign({}, clientDetails, caseDetails);
        const caseDataObj = Object.assign({}, {
          id: uuid.v4(),
          practice_id: event.user.practice_id,
          client_id: clientDatas.id,
          integration_case_id: caseDetails.Id,
          case_title: caseDetails.litify_pm__Case_Title__c,
          county: caseDetails.litify_pm__Matter_City__c || null,
          state: stateCode,
          date_of_loss: caseDetails.litify_pm__Incident_date__c,
          integration_case_data: JSON.stringify(integrationCaseData),
          case_from: "litify"
        });
        const caseData = await Cases.create(caseDataObj);
        caseData.integration_case_data = JSON.parse(caseData.integration_case_data);
        ResponseData.case = caseData;
      }
    };

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ ResponseData }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't fetch litify data  ",
      }),
    };
  }
}

const updateClientToLitify = async (event) => {
  try {
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    if (!input) throw new Error(400, 'input data not found');
    const { Clients, Cases, Op } = await connectToDatabase();
    const { integration_client_id, first_name, last_name, phone, dob, email } = input;
    const url = `${Baseurl}sobjects/Account/${integration_client_id}`;
    const accessToken = await litifyAuthToken();
    const headers = {
      Authorization: "Bearer " + accessToken,
      "Content-Type": "application/json"
    };
    const IntakeQuery = `SELECT id, Name, litify_pm__Case_Type__c,litify_pm__Client__c, litify_pm__Case_Type__r.litify_pm__Is_Available__c, litify_pm__Case_Type__r.Name FROM litify_pm__Intake__c WHERE litify_pm__Client__c IN ('${integration_client_id}')`;
    const dataUrl = `${Baseurl}query/?q=${encodeURIComponent(IntakeQuery)}`;

    const IntakeDetails = await axios.request({ url: dataUrl, method: "get", headers }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`Litify integration failed: ${error.message}`);
      });

    const errorCaseType = [];
    if (IntakeDetails?.totalSize != 0 || IntakeDetails.records) {
      const IntakeRecords = IntakeDetails.records
      for (let i = 0; i < IntakeRecords.length; i++) {
        if (IntakeRecords[i]?.litify_pm__Case_Type__r || IntakeRecords[i]?.litify_pm__Case_Type__r?.litify_pm__Is_Available__c) {
          if (!IntakeRecords[i]?.litify_pm__Case_Type__r?.litify_pm__Is_Available__c) {
            if (!errorCaseType.includes(IntakeRecords[i].litify_pm__Case_Type__r.Name)) {
              errorCaseType.push(IntakeRecords[i].litify_pm__Case_Type__r.Name)
            }
          }
        }
      }
      const errorFrom = Object.assign({}, { error_from: 'client', case_type: errorCaseType });
      if (errorCaseType.length != 0) throw new HTTPError(500, errorFrom);
    }
    const client_url = `${Baseurl}sobjects/Account/${integration_client_id}`;
    const clientDetails = await axios.request({ url: client_url, method: "get", headers }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`An error occurred during the Litify request while fetching client details : ${error.message}`);
      });
    if (!clientDetails) { throw new HTTPError(400, 'client details not found in Litify') };

    const clientData = await Clients.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_client_id: integration_client_id,
        is_deleted: { [Op.not]: true },
        client_from: 'litify'
      }
    });

    if (!clientData) { throw new HTTPError(400, 'client data not found') };

    clientData.first_name = first_name;
    clientData.last_name = last_name;
    clientData.phone = phone;
    clientData.dob = dob;
    clientData.email = email;
    await clientData.save();

    const dateOfBirth = dob && moment(dob).format('YYYY-MM-DD');
    let data = {
      litify_pm__Phone_Mobile__c: phone,
      litify_pm__Date_of_birth__c: dateOfBirth,
      litify_pm__Email__c: email
    };

    if (clientDetails?.Name && !clientDetails?.litify_pm__First_Name__c && !clientDetails?.litify_pm__Last_Name__c) {
      data.Name = first_name + ' ' + last_name;
    };
    if (clientDetails?.litify_pm__First_Name__c) {
      data.litify_pm__First_Name__c = first_name;
    }
    if (clientDetails?.litify_pm__Last_Name__c) {
      data.litify_pm__Last_Name__c = last_name;
    }

    const litifyDetails = await axios.request({ url: url, method: "patch", headers, data }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`An error occurred during the Litify request while fetching case details : ${error.message}`);
      });

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ message: "client details updated to litify successfully", IntakeDetails }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't fetch litify data",
      }),
    };
  }
}

const updateCaseToLitify = async (event) => {
  try {
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const { Clients, Cases, Op } = await connectToDatabase();
    const { integration_case_id, case_title, county, state, date_of_loss } = input;
    const url = `${Baseurl}sobjects/litify_pm__Matter__c/${integration_case_id}`;
    const accessToken = await litifyAuthToken();

    const headers = {
      Authorization: "Bearer " + accessToken,
      "Content-Type": "application/json"
    };

    const CaseTypeQuery = `SELECT id, Name, litify_pm__Case_Type__c, litify_pm__Client__c, litify_pm__Case_Type__r.litify_pm__Is_Available__c, litify_pm__Case_Type__r.Name FROM litify_pm__Matter__c WHERE id IN ('${integration_case_id}')`;

    const caseTypeDataUrl = `${Baseurl}query/?q=${encodeURIComponent(CaseTypeQuery)}`;
    const caseTypeDetails = await axios.request({ url: caseTypeDataUrl, method: "get", headers }).then((response) => response.data)
    if (!caseTypeDetails?.records[0]?.litify_pm__Case_Type__r?.litify_pm__Is_Available__c && caseTypeDetails?.records[0]?.litify_pm__Case_Type__r != null) {
      const errorFrom = Object.assign({}, { error_from: 'case', case_type: [`${caseTypeDetails?.records[0]?.litify_pm__Case_Type__r?.Name}`] });

      throw new HTTPError(500, errorFrom);
    }

    const caseData = await Cases.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_case_id: integration_case_id,
        is_deleted: { [Op.not]: true },
        case_from: 'litify'
      }
    });

    if (!caseData) { throw new HTTPError(400, 'case not found') };

    let states = state && state == "CA" ? "CA California" : state == "TN" ? "TN Tennessee" : state == "TX" ? "TX Texas" : state == "FL" ? "FL Florida" : state == "NV" ? "NV Nevada" : state == "IN" ? "IN Indiana" : state == "MN" ? "MN Minnesota" : state == "GA" ? "GA Georgia" : state == "IA" ? "IA Iowa" : state == "AZ" ? "AZ Arizona" : state == "WA" ? "WA Washington" : state == "MI" ? "MI Michigan" : null;

    caseData.case_title = case_title;
    caseData.case_title = date_of_loss;
    await caseData.save();

    const data = {
      litify_pm__Case_Title__c: case_title,
      litify_pm__Matter_City__c: county,
      litify_pm__Matter_State__c: states,
      litify_pm__Incident_date__c: date_of_loss
    };

    const litifyDetails = await axios.request({ url: url, method: "patch", headers, data }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`An error occurred during the Litify request while fetching case details : ${error.message}`);
      });

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ message: "case details updated to litify successfully", litifyDetails }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't fetch litify data",
      }),
    };
  }
}

const commonUpdateLitify = async (event) => {
  try {
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    if (!input) throw new Error(400, 'input data not found');
    const { Clients, Cases, Op } = await connectToDatabase();
    const { integration_client_id, integration_case_id, client_phone, state, county, case_title } = input;
    const client_url = `${Baseurl}sobjects/Account/${integration_client_id}`;
    const case_url = `${Baseurl}sobjects/litify_pm__Matter__c/${integration_case_id}`;

    const IntakeQuery = `SELECT id, Name, litify_pm__Case_Type__c,litify_pm__Client__c, litify_pm__Case_Type__r.litify_pm__Is_Available__c, litify_pm__Case_Type__r.Name FROM litify_pm__Intake__c WHERE litify_pm__Client__c IN ('${integration_client_id}')`;
    const dataUrl = `${Baseurl}query/?q=${encodeURIComponent(IntakeQuery)}`;
    const accessToken = await litifyAuthToken();
    const headers = {
      Authorization: "Bearer " + accessToken,
      "Content-Type": "application/json"
    };

    const IntakeDetails = await axios.request({ url: dataUrl, method: "get", headers }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`Litify integration failed: ${error.message}`);
      });

    const errorCaseType = [];
    if (IntakeDetails?.totalSize != 0 || IntakeDetails.records) {
      const IntakeRecords = IntakeDetails.records
      for (let i = 0; i < IntakeRecords.length; i++) {
        if (IntakeRecords[i]?.litify_pm__Case_Type__r || IntakeRecords[i]?.litify_pm__Case_Type__r?.litify_pm__Is_Available__c) {
          if (!IntakeRecords[i]?.litify_pm__Case_Type__r?.litify_pm__Is_Available__c) {
            if (!errorCaseType.includes(IntakeRecords[i].litify_pm__Case_Type__r.Name)) {
              errorCaseType.push(IntakeRecords[i].litify_pm__Case_Type__r.Name)
            }
          }
        }
      }
      const errorFrom = Object.assign({}, { error_from: 'client', case_type: errorCaseType });
      if (errorCaseType.length != 0) throw new HTTPError(500, errorFrom);
    }

    const CaseTypeQuery = `SELECT id, Name, litify_pm__Case_Type__c, litify_pm__Client__c, litify_pm__Case_Type__r.litify_pm__Is_Available__c, litify_pm__Case_Type__r.Name FROM litify_pm__Matter__c WHERE id IN ('${integration_case_id}')`;

    const caseTypeDataUrl = `${Baseurl}query/?q=${encodeURIComponent(CaseTypeQuery)}`;
    const caseTypeDetails = await axios.request({ url: caseTypeDataUrl, method: "get", headers }).then((response) => response.data)
    if (!caseTypeDetails?.records[0]?.litify_pm__Case_Type__r?.litify_pm__Is_Available__c && caseTypeDetails?.records[0]?.litify_pm__Case_Type__r != null) {
      const errorFrom = Object.assign({}, { error_from: 'case', case_type: [`${caseTypeDetails?.records[0]?.litify_pm__Case_Type__r?.Name}`] });
      throw new HTTPError(500, errorFrom);
    }

    const clientData = await Clients.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_client_id: integration_client_id,
        is_deleted: { [Op.not]: true },
        client_from: 'litify'
      }
    });
    if (!clientData) { throw new HTTPError(400, 'client data not found') };

    const caseData = await Cases.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_case_id: integration_case_id,
        is_deleted: { [Op.not]: true },
        case_from: 'litify'
      }
    });
    if (!caseData) { throw new HTTPError(400, 'case not found') };

    if (client_phone) {
      const client_data = {
        litify_pm__Phone_Mobile__c: client_phone,
      };

      const litifyClientDetails = await axios.request({ url: client_url, method: "patch", headers, data: client_data }).then((response) => response.data)
        .catch((error) => {
          console.log(error);
          throw new Error(`An error occurred during the Litify request while updating case details : ${error.message}`);
        });
    }

    if (state || county || case_title) {
      const case_data = {};
      if (state) {
        let states = state && state == "CA" ? "CA California" : state == "TN" ? "TN Tennessee" : state == "TX" ? "TX Texas" : state == "FL" ? "FL Florida" : state == "NV" ? "NV Nevada" : state == "IN" ? "IN Indiana" : state == "MN" ? "MN Minnesota" : state == "GA" ? "GA Georgia" : state == "IA" ? "IA Iowa" : state == "AZ" ? "AZ Arizona" : state == "WA" ? "WA Washington" : state == "MI" ? "MI Michigan" : null;
        case_data.litify_pm__Matter_State__c = states;
      }
      if (county) case_data.litify_pm__Matter_City__c = county;
      if (case_title) case_data.litify_pm__Case_Title__c = case_title;

      const litifyCaseDetails = await axios.request({ url: case_url, method: "patch", headers, data: case_data }).then((response) => response.data)
        .catch((error) => {
          console.log(error);
          throw new Error(`An error occurred during the Litify request while updating case details : ${error.message}`);
        });
    }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ message: "client details updated to litify successfully" }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't fetch litify data",
      }),
    };
  }
}

const litifyFileUpload = async (event) => {
  try {
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;

    if (!input || !input.s3_file_key) throw new HTTPError(404, `S3 file key not found.`);
    const { s3_file_key, integration_case_id, case_plaintiff_name, case_defendant_name, document_type } = input;

    if (!integration_case_id) throw new HTTPError(404, `Integration case id not provided.`);
    if (!case_plaintiff_name) throw new HTTPError(404, `plaintiff name not provided.`);
    if (!case_defendant_name) throw new HTTPError(404, `defendant name not provided.`);
    if (!document_type) throw new HTTPError(404, `document_type not provided.`);

    let filename = `DFT ${case_plaintiff_name}'S ${document_type.toUpperCase()} TO PLTF ${case_defendant_name}`;
    const filenamelength = filename.split('').length;
    if (filenamelength > 76) filename = filenamelength.slice(0, 76);

    const accessToken = await litifyAuthToken();
    const headers = {
      Authorization: "Bearer " + accessToken,
      "Content-Type": "application/json"
    };

    const bucketdetails = await axios.request({ url: "https://2xm5rfl2q0.execute-api.us-east-2.amazonaws.com/v1/orginfo", method: "get", headers }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`An error occurred during the Litify request while fetching case details : ${error.message}`);
      });

    const documentData = [{
      Name: filename,
      litify_docs__Related_To_Api_Name__c: "litify_pm__Matter__c",
      litify_docs__Related_To__c: integration_case_id,
      litify_docs__File_Type__c: "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    }]
    let uploadedFileDetails;
    const signedUrldetails = await axios.request({ url: `${bucketdetails.AwsApiPrefix}/files`, method: "post", headers, data: documentData }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`An error occurred during the Litify request while fetching case details : ${error.message}`);
      });
    console.log(signedUrldetails);
    console.log(signedUrldetails[filename]);
    if (signedUrldetails && signedUrldetails[filename]?.SignedUrl) {
      let fileBody = await readDiscoveryFile(s3_file_key);
      const uploadUrl = (signedUrldetails && signedUrldetails[filename]?.SignedUrl) || false;
      const config = {
        method: "put",
        url: uploadUrl,
        headers: {
          "Content-Type": "application/pdf",
          "Accept": "*/*",
        },
        data: fileBody,
      };
      const putData = await axios(config).then((re) => re.status).catch((error) => {
        console.log(error);
      });
      if (putData == '200') {
        const uploadedId = {
          "Ids": [signedUrldetails[filename]?.Id]
        }
        uploadedFileDetails = await axios.request({ url: `${bucketdetails.AwsApiPrefix}/files/complete`, method: "post", headers, data: uploadedId }).then((response) => response.data)
          .catch((error) => {
            console.log(error);
            throw new Error(`An error occurred during the Litify request while fetching case details : ${error.message}`);
          });
      } else {
        throw new HTTPError(500, "failed to set upload status");
      }
    } else {
      throw new HTTPError(500, "failed to process signed url");
    }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "Files successfully uploaded",
        uploadedFileDetails,
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ error: err.message || "Could upload file" }),
    };
  }
};

const readDiscoveryFile = (filename) => {
  return new Promise(function (resolve, reject) {
    const s3BucketParams = {
      Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
      Key: filename,
    };
    s3.getObject(s3BucketParams, function (err, data) {
      if (err) {
        console.log(err);
        reject(err.message);
      } else {
        //var data = Buffer.from(data.Body).toString('utf8');
        resolve(data.Body);
      }
    });
  });
};


const getMultipleRecords = async (event) => {
  try {
    const accessToken = await litifyAuthToken()
    const headers = {
      Authorization: "Bearer " + accessToken
    };
    const query = event.queryStringParameters || event.query || {};
    if (query.offset) query.offset = parseInt(query.offset, 10);
    if (query.limit) query.limit = parseInt(query.limit, 10);

    const Where = `WHERE litify_pm__Status__c IN ('Open','Pending') AND litify_pm__Matter_State__c IN ('CA California','TN Tennessee','TX Texas','FL Florida','NV Nevada','IN Indiana','MN Minnesota','GA Georgia','IA Iowa','AZ Arizona','WA Washington','MI Michigan',null)`;

    const dataQuery = `SELECT Id, OwnerId, litify_pm__Status__c, litify_pm__Case_Title__c, litify_pm__Matter_City__c, litify_pm__Matter_State__c, litify_pm__Incident_date__c, litify_pm__OpposingParty__c, litify_pm__Client__c, CreatedDate,litify_pm__Client__r.Name,  litify_pm__Client__r.litify_pm__Phone_Mobile__c, litify_pm__Client__r.litify_pm__Date_of_birth__c, litify_pm__Client__r.litify_pm__Email__c, litify_pm__Client__r.litify_pm__First_Name__c, litify_pm__Client__r.litify_pm__Last_Name__c, litify_pm__Client__r.litify_pm__Gender__c FROM litify_pm__Matter__c ${Where} LIMIT ${query?.limit || 100} OFFSET ${query?.offset}`;

    const countQuery = `SELECT COUNT() FROM litify_pm__Matter__c ${Where}`;

    const dataUrl = `${Baseurl}query/?q=${encodeURIComponent(dataQuery)}`;
    const CountUrl = `${Baseurl}query/?q=${encodeURIComponent(countQuery)}`;
    const caseDetails = await axios.request({ url: dataUrl, method: "get", headers }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`Litify integration failed: ${error.message}`);
      });
    const caseCount = await axios.request({ url: CountUrl, method: "get", headers }).then((response) => response.data)
      .catch((error) => {
        console.log(error);
        throw new Error(`Litify integration failed: ${error.message}`);
      });

    let totalPages, currentPage, hasMore = true;
    if (query?.limit && caseCount?.totalSize) {
      totalPages = Math.ceil(caseCount.totalSize / query.limit);
      if (query.offset && query.offset != 0) {
        currentPage = Math.ceil((query.limit + query.offset) / query.limit);
      } else {
        currentPage = 1;
      }
    }
    if (totalPages && currentPage && totalPages == currentPage) hasMore = false;

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        hasMore,
        caseDetails: caseDetails,
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ error: err.message || "Couldn't fetch details" }),
    };
  }
};


const bulkCreate = async (event) => {
  try {
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;

    const { Clients, Cases, Op } = await connectToDatabase();

    for (let i = 0; i < input?.length; i++) {
      const inputData = input[i];
      let clientId, clientName;
      const clientDatas = await Clients.findOne({
        where: {
          integration_client_id: inputData?.litify_pm__Client__c,
          practice_id: event.user.practice_id,
          client_from: "litify",
          is_deleted: { [Op.not]: true },
        }
      });
      if (!clientDatas) {
        const clientDataObj = Object.assign({}, {
          id: uuid.v4(),
          practice_id: event.user.practice_id,
          phone: inputData.litify_pm__Phone_Mobile__c || null,
          dob: inputData.litify_pm__Date_of_birth__c || null,
          email: inputData.litify_pm__Email__c || null,
          integration_client_id: inputData.litify_pm__Client__c,
          client_from: "litify"
        });
        let name = '';
        if (inputData.Name && !inputData?.litify_pm__First_Name__c && !inputData?.litify_pm__Last_Name__c) {
          name += inputData.Name;
          let names = name.split(' ');
          clientDataObj.first_name = names[0];
          if (names.length > 1) {
            names.shift();
            clientDataObj.last_name = names.join(' ');
          }
        };
        if (inputData?.litify_pm__First_Name__c) {
          name += inputData.litify_pm__First_Name__c;
          clientDataObj.first_name = inputData.litify_pm__First_Name__c;
        }
        if (inputData?.litify_pm__Last_Name__c) {
          if (inputData.litify_pm__First_Name__c) name = name + ' '
          name += inputData?.litify_pm__Last_Name__c;
          clientDataObj.last_name = inputData.litify_pm__Last_Name__c;
        }
        clientDataObj.name = name;
        const clientData = await Clients.create(clientDataObj);
        if (!clientData) throw new HTTPError(400, 'client not created');
        clientId = clientData.id;
        clientName = clientData.name;
      } else {
        clientId = clientDatas.id;
        clientName = clientDatas.name;
      }
      let stateCode;
      if (inputData?.litify_pm__Matter_State__c) {
        let stateCodeArr = inputData.litify_pm__Matter_State__c.split(' ');
        stateCode = stateCodeArr[0];
      }
      const integrationCaseData = Object.assign({}, inputData);
      const caseDataObj = Object.assign({}, {
        id: uuid.v4(),
        practice_id: event.user.practice_id,
        client_id: clientId,
        integration_case_id: inputData.Id,
        case_title: inputData.litify_pm__Case_Title__c || null,
        case_plaintiff_name: clientName,
        county: inputData.litify_pm__Matter_City__c || null,
        state: stateCode || null,
        date_of_loss: inputData.litify_pm__Incident_date__c || null,
        integration_case_data: JSON.stringify(integrationCaseData),
        case_from: "litify"
      });
      if (inputData?.litify_pm__OpposingParty__c) {
        caseDataObj.case_defendant_name = inputData.litify_pm__OpposingParty__c;
      };
      const caseData = await Cases.create(caseDataObj);
      if (!caseData) throw new HTTPError(400, 'case not created');
    };
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ message: "Client and case successfully created" }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ error: err.message || "Couldn't create client and case" }),
    };
  }
};

const bulkUpdate = async (event) => {
  try {

    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;

    const { Clients, Cases, Op } = await connectToDatabase();

    for (let i = 0; i < input?.length; i++) {
      const inputData = input[i];

      const clientDatas = await Clients.findOne({
        where: {
          integration_client_id: inputData?.litify_pm__Client__c,
          practice_id: event.user.practice_id,
          client_from: "litify",
          is_deleted: { [Op.not]: true },
        }
      });

      if (!clientDatas) throw new HTTPError(400, 'client not found');

      let name = '';
      if (inputData?.Name && !inputData?.litify_pm__First_Name__c && !inputData?.litify_pm__Last_Name__c) {
        name += inputData.Name;
        let names = name.split(' ');
        clientDatas.first_name = names[0];
        if (names.length > 1) {
          names.shift();
          clientDatas.last_name = names.join(' ');
        }
      };
      if (inputData?.litify_pm__First_Name__c) {
        name += inputData.litify_pm__First_Name__c;
        clientDatas.first_name = inputData.litify_pm__First_Name__c;
      }
      if (inputData?.litify_pm__Last_Name__c) {
        if (inputData.litify_pm__First_Name__c) name = name + ' '
        name += inputData?.litify_pm__Last_Name__c;
        clientDatas.last_name = inputData.litify_pm__Last_Name__c;
      }
      clientDatas.name = name;
      clientDatas.phone = inputData.litify_pm__Phone_Mobile__c || null,
        clientDatas.dob = inputData.litify_pm__Date_of_birth__c || null,
        clientDatas.email = inputData.litify_pm__Email__c || null,
        await clientDatas.save();

      const caseDatas = await Cases.findOne({
        where: {
          integration_case_id: inputData?.Id,
          practice_id: event.user.practice_id,
          client_id: clientDatas.id,
          is_deleted: { [Op.not]: true },
          case_from: "litify"
        }
      });
      let stateCode;
      if (inputData?.litify_pm__Matter_State__c) {
        let stateCodeArr = inputData.litify_pm__Matter_State__c.split(' ');
        stateCode = stateCodeArr[0];
      }
      const integrationCaseData = Object.assign({}, inputData);
      caseDatas.case_title = inputData.litify_pm__Case_Title__c || null;
      caseDatas.date_of_loss = inputData.litify_pm__Incident_date__c || null;
      caseDatas.state = stateCode || null;
      caseDatas.county = inputData.litify_pm__Matter_City__c || null;
      caseDatas.integrationCaseData = JSON.stringify(integrationCaseData);
      if (inputData?.litify_pm__OpposingParty__c) {
        caseDatas.case_defendant_name = inputData.litify_pm__OpposingParty__c;
      };
      caseDatas.case_plaintiff_name = clientDatas.name;
      await caseDatas.save();
    }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ message: "Client and case successfully updated" }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ error: err.message || "Couldn't update client and case" }),
    };
  }
};


module.exports.getLitifyData = getLitifyData;
module.exports.updateCaseToLitify = updateCaseToLitify;
module.exports.updateClientToLitify = updateClientToLitify;
module.exports.commonUpdateLitify = commonUpdateLitify;
module.exports.litifyFileUpload = litifyFileUpload;
module.exports.getMultipleRecords = getMultipleRecords;
module.exports.bulkCreate = bulkCreate;
module.exports.bulkUpdate = bulkUpdate;