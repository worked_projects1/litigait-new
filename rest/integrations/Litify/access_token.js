const jwt = require('jsonwebtoken');
const axios = require('axios');

// Salesforce Connected App credentials
const privateKey = `-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCGMPF9n+Iam6Rb
5kU/xyLEIW+BQ41LZ989xuhH1I8UmX4lqalHela8CnFD3EADFF7shZSOCKYb9d1f
HmNVyMamsWiM60tJ3hwzIKrXsQVX8MYSji2g2aigzFFIMqmDMUfdAKy5tWngLMnP
bSsUVzCwlDRVuMiws0mwp58+6WpELfk/QUbUXuKlbc6uU9AbxsjkIkCUM0BjfR+N
4L6mKEDAF/qHAzyXkUuW714JihwyPIkgsq+0GXsFm/novKWn/CS4cFCEBxWDU0Xo
zIUo6l+QZTn4gUfNandxPbww35dEJ5IucPeFHnk9tVmzYT9lTuYflf/JFRlS3mcW
xIruW6/nAgMBAAECggEAY0H4no+Q01Wk8UUWm+JUWPbSX67z6H7gpfVnAxq66uXO
ZVVlnTVkypp+xU85Gg4LWLBF/flvxObL4/u6Rk3IgqxfRT0V54GfIkJbnEeYSEAU
gs0f+waEsMvwo5K1yBC/sz04hfUmZ2c5r3B0mQsVPMejHnUcHXfAXibep85HiD3O
4hcRRsNAMCw+mEw5Ii6Y9zSe33SaP57tVlL4p2mTWSAFFORFa7VM3aZ5+sn9s3Hg
IqwY0oC9e4T+NVnGhVImqF3EaDtSOGqnVUBTItM6fqREgRD4BiSdq3riCk8ARI0P
C/tzqK4o9URLnYBabX1L1Ex9R1g6qj85OkNwk3ZCQQKBgQDsnVWrNmjcQ/Hek0oJ
aTD8lEX7j7IhMJRzPvKHBgqymR4C90HezYNdcIu8F0XSEVAMgy3Xn+RDOnWY4SW8
iRKTM/yfZMgYVU0gU9kWamuLYxj6vNZa5kPvMx4yoXoaIIK9FO2u3qLqqiF0edfp
3Rzcr69prkPWBHY2SyiGn+91yQKBgQCRL2szBe4Lr5kNl3bEh79VBBoWkNqiwc44
kGmxDczDWVbLczsiF+cPMXYpAW8QEpqiUYkCFJ9CgIGalGwdR8MqVc7mjgrHa8Ox
Rj7HgtREkS2O/V2bOL+jjiiXaT3DKsxAbKoMRxPkvmOfbAs0NcAhTHQF+kHytKhY
tipKyASQLwKBgCrgh/myrmXb2iSfRDdYXIwowlXHwFvLVdtceM6HBcE/hfS7obAi
Rb/U+T1oajqa8IqT6ua8Dr0wBsdhZVk47ajHLuwjcy8VphhJmXk6Z6qmlow5VA68
02qxNLOwF1RMt+RTuo6JwbQMiGt2+O1kKCrFm1/nOFu8Cz/jdsCOkvIhAoGAYX+R
ouspiCl9RBZiSOrMf6BIkLzoRoKWJYA9qgwVZV6anfgdTvD0V0ylCDaRSh7kP3s3
4fDNXtCDuLZRwHwPwV9smYHsFS32dhcxU7Q1LnK8QJk/glzivUhbVAr10hnsnpsa
uzIEVwnQRyA8iexKwVHhXJ5KKV5O63wx/JREFDsCgYAeF7u9Wcx1ZrS7EMJBe0Qh
1xm+dF6nkmcrCTWHwnBgnXPXWphXObBFxPDB1bT5vkgFRlRmvTIs6l8Ipt5RpVKn
isJKKzYbtnjrcd5A+w6mVyJIyrp3H8HiujWsWUSxSSmEscQblSEguBgk5dKV7AUy
AgwBwqW6uvoxlHIb/7MwFg==
-----END PRIVATE KEY-----`;

const litifyAuthToken = async () => {
  try {
    const jwtClaims = {
      iss: "3MVG93inh8Bkz5na_tLWSUzJprDEw2SsVR22.AXzoQ984R5W7.lFew7fh2VmWk_2qT4yupc.3AutSLkuTi.8I",
      sub: "esquiretek@litifysandbox.com",
      aud: `https://login.salesforce.com`,
      exp: Math.floor(Date.now() / 1000) + (60 * 1) // Token Validity Duration: 1 min
    };

    const jwtOptions = {
      algorithm: 'RS256'
    };

    const signedJwt = jwt.sign(jwtClaims, privateKey, jwtOptions);

    // Request an access token
    const params = new URLSearchParams();
    params.append('grant_type', 'urn:ietf:params:oauth:grant-type:jwt-bearer');
    params.append('assertion', signedJwt);
    // Salesforce token endpoint
    const accessTokenData = await axios.post(`https://login.salesforce.com/services/oauth2/token`, params).then((response) => response.data)
    .catch((error) => {
      console.log(error);
      throw new Error(`Litify token generation failed: ${error.message}`);
    });
    const accessToken = accessTokenData.access_token;
    return accessToken;
  } catch (err) {
    console.log(err);
    throw new Error('An error occurred during the Litify Token Request.')
  }
}

module.exports = { litifyAuthToken };