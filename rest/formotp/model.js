module.exports = (sequelize, type) => sequelize.define('Formotp', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    otp_code: type.STRING,
    otp_secret: type.STRING,
    client_id: type.STRING,
    document_type: type.STRING, // FROGS, SPROGS, RFPD, RFA
    case_id: type.STRING,
    legalforms_id: type.STRING,
    sending_type: type.STRING,
    question_ids: type.TEXT('long'),
    sent_by: type.STRING,
    scheduler_email: type.BOOLEAN,
    TargetLanguageCode: type.STRING,
    latest_client_response_date: type.DATE,
    is_client_answered_all_questions: type.BOOLEAN,
    reminder_meantime: type.STRING,
    latest_reminder_send_date: type.DATE,
    reminder_till_date: type.DATE,
});