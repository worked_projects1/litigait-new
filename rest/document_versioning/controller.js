const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const { encryptedwithoutBase64 } = require('../../rest/helpers/filevine.helper');
const { sendEmail } = require('../../utils/mailModule');
const { getSecrets } = require('../helpers/secrets.helper')

const axios = require("axios");
const moment = require("moment");

const getOne = async (event) => {
  try {
    const { DocumentVersioning, Op } = await connectToDatabase();
    const params = event.params || event.pathParameters;
    const { id } = params;

    const versionData = await DocumentVersioning.findOne({ where: { id: id }, raw: true });
    if (!versionData) throw new HTTPError(400, `version data not found`);

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(versionData),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not get document version data' }),
    };
  }
};

const getAll = async (event) => {
  try {
    const { DocumentVersioning, Op } = await connectToDatabase();
    const params = event.query || event.pathParameters;
    const { legalforms_id } = params;
    const versionData = await DocumentVersioning.findAll({
      where:
        { legalforms_id: legalforms_id },
      order: [['versioning_timeStamp', 'DESC']],
      raw: true
    });
    versionData.totalCount = versionData.length;
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(versionData),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not get document versions' }),
    };
  }
};

const update = async (event) => {
  try {
    const { DocumentVersioning, LegalForms, Op } = await connectToDatabase();
    const { id, legalforms_id } = input;
    if (id) throw new HTTPError(400, `Document version id not provided`);
    const legalFormsData = await LegalForms.findOne({ where: { id: legalforms_id } });
    if (!legalFormsData) throw new HTTPError(400, `Legalforms data not found`);
    const versionData = await DocumentVersioning.findOne({ where: { id: id } });
    if (!versionData) throw new HTTPError(400, `version data not found`);
    versionData.versioning_timeStamp = new Date();
    const LatestUpdatedData = await versionData.save();
    legalFormsData.final_document_s3_key = LatestUpdatedData.final_document_s3_key;
    if (LatestUpdatedData?.pdf_s3_file_key) legalFormsData.pdf_s3_file_key = LatestUpdatedData.pdf_s3_file_key;
    if (LatestUpdatedData?.pdf_filename) legalFormsData.pdf_filename = LatestUpdatedData.pdf_filename;
    legalFormsData.doc_filename = LatestUpdatedData.doc_filename;
    await legalFormsData.save();

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(versionData),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not update document versioning datas' }),
    };
  }
};

const destroy = async (event) => {
  try {
    const { DocumentVersioning, Op } = await connectToDatabase();
    const params = event.params || event.pathParameters;
    const { id } = params;

    const versionData = await DocumentVersioning.findOne({ where: { id: id } });
    if (!versionData) throw new HTTPError(400, `version data not found`);
    await DocumentVersioning.destroy({ where: { id: id } });

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ message: 'Document version Data destroyed successfully' }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not destory document versions' }),
    };
  }
};

const pdfConversion = async (event) => {
  try {
    const body = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const { data, legalforms_id, s3_file_key, practice_id, propound_id, final_document_s3_key } = body;
    const { LegalForms, PropoundForms, Practices, Op } = await connectToDatabase();

    let formData;
    let email = '';
    const mailId = 'prithiviraj.rifluxyss@gmail.com,r.aravinth.rifluxyss@gmail.com,s.manikandan.rifluxyss@gmail.com';
    if (!legalforms_id && !propound_id) throw new HTTPError(400, "form id not provided");
    if (!practice_id) throw new HTTPError(400, "Practice id not provided");
    if (!data) throw new HTTPError(400, "Encrypted data not provided");
    if (!s3_file_key) throw new HTTPError(400, "S3 file key data not provided");
    // if (!final_document_s3_key) throw new HTTPError(400, " Final document s3 file key data not provided");
    const practiceData = await Practices.findOne({ where: { id: practice_id }, raw: true });
    if (!practiceData) throw new HTTPError(400, "Practice not found");
    if (legalforms_id) {
      formData = await LegalForms.findOne({ where: { id: legalforms_id, practice_id: practice_id } });
      if (!formData) throw new HTTPError(400, "Legalform data not found");
      console.log("legalformData updated");
      if (final_document_s3_key) formData.final_document_s3_key = final_document_s3_key;
      formData.pdf_discovery_status = 'Processing';
      await formData.save();
      email += 'Form-type: Legalforms<br>Legal-Form Id: ' + legalforms_id + '<br>Practice Name: ' + practiceData.name + '<br>Practice Id: ' + practice_id + '<br>';
    }

    if (propound_id) {
      formData = await PropoundForms.findOne({ where: { id: propound_id, practice_id: practice_id } });
      if (!formData) throw new HTTPError(400, "Propound form data not found");
      console.log("Propound form updated");
      if (final_document_s3_key) formData.s3_file_key = final_document_s3_key;
      formData.pdf_discovery_status = 'Processing';
      await formData.save();
      email += 'Form-type: Propoundforms<br>Propound-Form Id: ' + propound_id + '<br>Practice Name: ' + practiceData.name + '<br>Practice Id: ' + practice_id + '<br>';
    }

    // const apiUrl = 'https://pdf2doc.esquiretek.com/conversion/doc-pdf';
    const apiUrl = process?.env?.DOC_TO_PDF_API_URL ? process?.env?.DOC_TO_PDF_API_URL : await getSecrets('DOC_TO_PDF_API_URL');
    const axiosConfig = {
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      
    };
    
    const TimeStamp = new Date();
    const date = moment(TimeStamp).format("YYYY-MM-DD'T'HH:mm:ss");

    const timeoutinsec = 1000 * 60 * 3;
    const source = axios.CancelToken.source();
    const timeout = setTimeout(() => {
      source.cancel();
    }, timeoutinsec);

    axios.post(apiUrl, { data }, { cancelToken: source.token, ...axiosConfig }).then(async (res) => {
      clearTimeout(timeout);
      console.log('API call successful');
      email += 'Pdf-conversion: Completed<br>Time-stamp: ' + date + '<br>';
      const pdfData = res.data;
      if (pdfData?.pdf_s3_key) {
        const pdfS3FileKey = pdfData?.pdf_s3_key;
        formData.pdf_s3_file_key = pdfS3FileKey;
        const pdfName = pdfS3FileKey.split('.pdf').join('').split('/').pop();
        formData.pdf_filename = pdfName;
        formData.pdf_s3_file_key = pdfData?.pdf_s3_key;
      }
      formData.pdf_discovery_status = 'Completed';
      await formData.save();
      await sendEmail(mailId, 'Important: PDF-Conversion-Success', email, 'Conversion API Status');
    }).catch(async (error) => {
      email += 'Pdf-conversion: Not Completed<br>Time-stamp:' + date + '<br>Error:' + error;
      console.error('Error making API call:', error.message);
      formData.pdf_discovery_status = 'Failed';
      await formData.save();
      await sendEmail(mailId, 'Important: PDF-Conversion-Failure', email, 'Conversion API Status');
    });

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ message: 'success' }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'failed' }),
    };
  }
}

const pdfStatusGetOne = async (event) => {
  try {
    const { LegalForms, PropoundForms, Op } = await connectToDatabase();
    const params = event.params || event.pathParameters;
    const { id, type } = params;
    if (!id) throw new HTTPError(400, 'id not provided');
    if (!type) throw new HTTPError(400, 'type not provided');
    const formTypeArr = ['responding', 'propounding'];
    const form_type = type.toLowerCase();
    if (!formTypeArr.includes(form_type)) throw new HTTPError(400, 'Provided type doesn\'t match');

    let response = {};
    if (form_type == 'responding') {
      const pdfStatusData = await LegalForms.findOne({
        where: { id: id },
        // attributes: ['id','pdf_filename','pdf_s3_file_key','pdf_discovery_status'],
        raw: true
      });
      if (!pdfStatusData) throw new HTTPError(400, `Legalforms data not found`);
      response = Object.assign(response, pdfStatusData);
    }
    if (form_type == 'propounding') {
      const pdfStatusData = await PropoundForms.findOne({
        where: { id: id },
        // attributes: ['id','pdf_filename','pdf_s3_file_key','pdf_discovery_status'],
        raw: true
      });
      if (!pdfStatusData) throw new HTTPError(400, `Legalforms data not found`);
      response = Object.assign(response, pdfStatusData);
    }
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(response),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not get document version data' }),
    };
  }
};

module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;
module.exports.pdfConversion = pdfConversion;
module.exports.pdfStatusGetOne = pdfStatusGetOne;