module.exports = (sequelize, type) => sequelize.define('DocumentVersioning', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    case_id: type.STRING,
    practice_id: type.STRING,
    client_id: type.STRING,
    legalforms_id: type.STRING,
    user_id: type.STRING,
    propound_form_id: type.STRING,
    document_type: type.STRING,
    s3_file_key: type.STRING,
    filename: type.STRING,
    pdf_filename: type.STRING,
    doc_filename: type.STRING,
    pdf_s3_file_key: type.STRING,
    final_document: type.TEXT,
    final_document_s3_key: type.TEXT,
    sfdt_s3key: type.TEXT('long'),
    versioning_timeStamp: type.DATE
});