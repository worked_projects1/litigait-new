module.exports = (sequelize, type) => sequelize.define('PropoundDocumentUploadProgress', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    propound_template_id: type.STRING,
    document_type: type.STRING,
    status: type.STRING, // pending, complete
    s3_file_key: type.STRING,
    uploaded_by: type.STRING,
});