const {HTTPError} = require('../../utils/httpResp');

const adminInternalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const internalRoles = ['superAdmin', 'manager', 'operator'];
const superAdminRole = ['superAdmin'];
const normalUserRole = ['superAdmin', 'manager', 'operator'];

exports.authorizeCreate = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetOne = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};
exports.authorizeGetAll = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdate = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeDestroy = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizePushToAllPractices = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};