const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');

exports.validateCreateParties = (data) => {
    const rules = {
        case_id: 'required',
        practice_id: 'required',
        email: 'email|min:4|max:64',
        phone: 'required|numeric',
        address: 'min:4|max:64',
    };
    const validation = new Validator(data, rules);
    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
}
exports.validateGetOneParty = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateUpdateParty = function (data) {
    const rules = {
        case_id: 'required',
        name: 'min:4|max:64',
        email: 'email|min:4|max:64',
        phone: 'numeric',
        address: 'min:4|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};
exports.validateDeleteParty = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};
  