const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const authMiddleware = require('../../auth');

const {
    validateCreateParties,
    validateGetOneParty,
    validateUpdateParty,
    validateDeleteParty
} = require('./validation');
const { QueryTypes } = require('sequelize');
const create = async (event) => {
    try {
        let id;
        const input = event.body;
        if (event.user.practice_id) {
            input.practice_id = event.user.practice_id;
        }
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        let address;
        let name = '';
        if (input?.first_name) {
            name = input?.first_name;
        }
        if (input.middle_name) {
            name = name + ' ' + input.middle_name;
        }
        if (input.last_name) {
            name = name + ' ' + input.last_name;
        }
        if (input.street) {
            address = input.street + ', ' + input.city + ', ' + input.state + ' - ' + input.zip_code;
        }
        const dataObject = Object.assign(input, { id: id || uuid.v4(), name: name, address: address });
        validateCreateParties(dataObject);

        const { OtherParties, Cases, Clients, Users, Op, Forms } = await connectToDatabase();
        const OtherPartiesCreateResp = await OtherParties.create(dataObject);
        const OtherPartiesModelObject = OtherPartiesCreateResp.get({ plain: true });
        if (!OtherPartiesModelObject) throw new HTTPError(400, `Cannot create Otherparties deatails`);
        const savedData = await OtherParties.findOne({ where: { id: OtherPartiesModelObject.id }, raw: true });
        const CasesObj = await Cases.findOne({ where: { id: input.case_id }, raw: true });
        const ClientsObj = await Clients.findOne({ where: { id: CasesObj.client_id }, raw: true });
        savedData.case_title = CasesObj.case_title;
        savedData.case_number = CasesObj.case_number;
        savedData.client_name = ClientsObj.name;
        savedData.client_id = CasesObj.client_id;
        let attorneyName = [];
        if (CasesObj.attorneys != null && CasesObj.attorneys) {
            let attorneysIds = CasesObj.attorneys.split(",")
            for (let l = 0; l < attorneysIds.length; l++) {
                if (attorneysIds[l]) {
                    const UsersObj = await Users.findAll({
                        where: { id: attorneysIds[l], is_deleted: { [Op.not]: true } },
                        raw: true
                    });
                    if (UsersObj[0]?.name) attorneyName.push(UsersObj[0].name);
                }
            }
        }
        savedData.attorney_name = attorneyName.toString();
        const FormsModelObj = await Forms.findAll({
            where: { case_id: input.case_id, practice_id: input.practice_id },
            logging: console.log
        });
        if (FormsModelObj.length <= 0) {
            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify(savedData),
            };
        } else {
            let res = await createOtherparyForms(OtherPartiesModelObject);
            if (res.length > 0) {
                return {
                    statusCode: 200,
                    headers: {
                        'Content-Type': 'text/plain',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Credentials': true
                    },
                    body: JSON.stringify(savedData),
                };
            } else {
                return {
                    statusCode: 500,
                    headers: {
                        'Content-Type': 'text/plain',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Credentials': true
                    },
                    body: JSON.stringify(res),
                };
            }
        }

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the Parties.' }),
        };
    }
};

const createOtherparyForms = async (OtherPartidata) => {
    try {
        console.log(OtherPartidata);
        const caseId = OtherPartidata.case_id;
        const partyId = OtherPartidata.id;
        const practice_id = OtherPartidata.practice_id;
        console.log(caseId);
        console.log(partyId);
        console.log(practice_id);
        const { Forms, Cases, OtherPartiesForms } = await connectToDatabase();
        const query = {};
        query.where = {};
        query.where.case_id = caseId;
        query.where.practice_id = practice_id;
        query.logging = console.log;

        console.log(query);

        const FormsModelObj = await Forms.findAll(query);
        const otherPartiesFormsData = [];
        for (let i = 0; i < FormsModelObj.length; i++) {
            const dataObject = {
                id: uuid.v4(),
                case_id: caseId,
                practice_id: FormsModelObj[i].practice_id,
                party_id: partyId,
                legalforms_id: FormsModelObj[i].legalforms_id,
                form_id: FormsModelObj[i].id,
                subgroup: FormsModelObj[i].subgroup,
                document_type: FormsModelObj[i].document_type, // FROGS, SPROGS, RFPD, RFA
                question_type: FormsModelObj[i].question_type, // only for initial discloser forms (FROGS, SPROGS, RFPD, RFA)
                question_number: FormsModelObj[i].question_number,
                question_number_text: FormsModelObj[i].question_number_text,
                question_id: FormsModelObj[i].question_id,
                question_text: FormsModelObj[i].question_text,
                question_section_id: FormsModelObj[i].question_section_id,
                question_section: FormsModelObj[i].question_section,
                question_section_text: FormsModelObj[i].question_section_text,
                question_options: FormsModelObj[i].question_options,
                is_consultation_set: FormsModelObj[i].is_consultation_set,
                consultation_set_no: FormsModelObj[i].consultation_set_no,
                client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
            };
            otherPartiesFormsData.push(dataObject);
        }
        let savedResponse = await OtherPartiesForms.bulkCreate(otherPartiesFormsData);
        if (savedResponse.length == FormsModelObj.length) {
            return savedResponse;
        } else {
            throw new HTTPError(400, `Some questions are missing`);
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the Other Parties.Forms' }),
        };
    }
}


const getAll_new = async (event) => {
    try {
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        query.order = [
            ['createdAt', 'DESC'],
        ];
        const { OtherParties, Op, Cases, Clients, Users } = await connectToDatabase();
        query.where.is_deleted = { [Op.not]: true };
        query.raw = true;
        const OtherPartiesModelObject = await OtherParties.findAll(query);
        const OtherPartiesCount = await OtherParties.count({ where: { practice_id: event.user.practice_id } });

        let responseArr = [];
        for (let i = 0; i < OtherPartiesModelObject.length; i++) {
            const CasesObj = await Cases.findAll({ where: { id: OtherPartiesModelObject[i].case_id }, raw: true });
            let attorneyName = [];
            if (CasesObj[0]) {
                let client_id = CasesObj[0].client_id;
                const ClientsObj = await Clients.findAll({ where: { id: client_id }, raw: true });
                if (CasesObj[0].attorneys != null && CasesObj[0].attorneys) {
                    let attorneysIds = CasesObj[0].attorneys.split(",")
                    for (let l = 0; l < attorneysIds.length; l++) {
                        if (attorneysIds[l]) {
                            const UsersObj = await Users.findAll({
                                where: {
                                    id: attorneysIds[l],
                                    is_deleted: { [Op.not]: true }
                                }, raw: true
                            });
                            if (UsersObj.length != 0) attorneyName.push(UsersObj[0].name);
                        }
                    }
                }

                if (ClientsObj) {
                    let newData = {
                        id: OtherPartiesModelObject[i].id,
                        case_id: OtherPartiesModelObject[i].case_id,
                        practice_id: OtherPartiesModelObject[i].practice_id,
                        name: OtherPartiesModelObject[i].name,
                        email: OtherPartiesModelObject[i].email,
                        phone: OtherPartiesModelObject[i].phone,
                        address: OtherPartiesModelObject[i].address,
                        dob: OtherPartiesModelObject[i].dob,
                        is_deleted: OtherPartiesModelObject[i].is_deleted,
                        createdAt: OtherPartiesModelObject[i].createdAt,
                        updatedAt: OtherPartiesModelObject[i].updatedAt,
                        clinet_id: ClientsObj[0].id,
                        client_name: ClientsObj[0].name,
                        case_id: CasesObj[0].id,
                        case_title: CasesObj[0].case_title,
                        case_number: CasesObj[0].case_number,
                        attorney_name: attorneyName.toString()
                    };
                    responseArr[responseArr.length] = newData;
                }
            }
        }
        let totalPages, currentPage;
        if (query.limit && (responseArr.length > 0)) {
            totalPages = Math.ceil(OtherPartiesCount / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const OtherPartiesDetails = {
            total_items: OtherPartiesCount,
            response: responseArr,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(OtherPartiesDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Parties.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        const input = event.body;
        let header = event.headers;
        const query = event.queryStringParameters || {};
        if (header.offset) query.offset = parseInt(header.offset, 10);
        if (header.limit) query.limit = parseInt(header.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        query.order = [
            ['createdAt', 'DESC'],
        ];
        const { OtherParties, Op, Cases, Clients, Users } = await connectToDatabase();
        query.where.is_deleted = { [Op.not]: true };
        query.raw = true;
        const OtherPartiesModelObject = await OtherParties.findAll(query);

        let responseArr = [];
        for (let i = 0; i < OtherPartiesModelObject.length; i++) {
            const CasesObj = await Cases.findAll({ where: { id: OtherPartiesModelObject[i].case_id }, raw: true });
            let attorneyName = [];
            if (CasesObj[0]) {
                let client_id = CasesObj[0].client_id;
                const ClientsObj = await Clients.findAll({ where: { id: client_id }, raw: true });
                if (CasesObj[0].attorneys != null && CasesObj[0].attorneys) {
                    let attorneysIds = CasesObj[0].attorneys.split(",")
                    for (let l = 0; l < attorneysIds.length; l++) {
                        if (attorneysIds[l]) {
                            const UsersObj = await Users.findAll({
                                where: {
                                    id: attorneysIds[l],
                                    is_deleted: { [Op.not]: true }
                                }, raw: true
                            });
                            if (UsersObj.length != 0) attorneyName.push(UsersObj[0].name);
                        }
                    }
                }

                if (ClientsObj) {
                    let newData = {
                        id: OtherPartiesModelObject[i].id,
                        case_id: OtherPartiesModelObject[i].case_id,
                        practice_id: OtherPartiesModelObject[i].practice_id,
                        name: OtherPartiesModelObject[i].name,
                        email: OtherPartiesModelObject[i].email,
                        phone: OtherPartiesModelObject[i].phone,
                        address: OtherPartiesModelObject[i].address,
                        dob: OtherPartiesModelObject[i].dob,
                        is_deleted: OtherPartiesModelObject[i].is_deleted,
                        createdAt: OtherPartiesModelObject[i].createdAt,
                        updatedAt: OtherPartiesModelObject[i].updatedAt,
                        clinet_id: ClientsObj[0].id,
                        client_name: ClientsObj[0].name,
                        case_id: CasesObj[0].id,
                        case_title: CasesObj[0].case_title,
                        case_number: CasesObj[0].case_number,
                        attorney_name: attorneyName.toString()
                    };
                    responseArr[responseArr.length] = newData;
                }
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(responseArr),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Parties.' }),
        };
    }
};
/* list all start */
// const getAll = async (event) => {
//   try {
//     const { sequelize } = await connectToDatabase();
//       let searchKey ='';
//       const sortKey = {};
//       let sortQuery = '';
//       let searchQuery = '';
//       const query = event.headers;
//       if ( !query.offset && !query.limit){
//         let otherPartiesRes = getAllTotalParties(event);
//         return otherPartiesRes;
//       }
//       if (query.offset) query.offset = parseInt(query.offset, 10);
//       if (query.limit) query.limit = parseInt(query.limit, 10);
//       if (query.search) searchKey = query.search;
//       let practice_id = event.user.practice_id;
//       if(!practice_id) throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
//       let codeSnip = '';
//       let codeSnip2 = '';
//       let where = `WHERE OtherParties.is_deleted IS NOT true AND OtherParties.practice_id = '${practice_id}'`;

//       /**Sort**/
//       if(query.sort == 'false'){
//           sortQuery += ` ORDER BY OtherParties.createdAt DESC`
//       }else if (query.sort != 'false'){
//         query.sort = JSON.parse(query.sort);
//         sortKey.column = query.sort.column;
//         sortKey.type = query.sort.type;
//         if(sortKey.column != 'all' && sortKey.column != '' && sortKey.column){
//           sortQuery += ` ORDER BY ${sortKey.column}`;
//         }
//         if(sortKey.type != 'all' && sortKey.type != '' && sortKey.type){
//           sortQuery += ` ${sortKey.type}`;
//         }
//       }

//       /**Search**/
//       if( searchKey != 'false'  && searchKey.length>=1 && searchKey !='' ){
//         searchQuery = ` OtherParties.name LIKE '%${searchKey}%' OR OtherParties.email LIKE '%${searchKey}%' OR OtherParties.phone LIKE '%${searchKey}%' OR OtherParties.address LIKE '%${searchKey}%' OR Clients.name LIKE '%${searchKey}%' OR Cases.case_title LIKE '%${searchKey}%'`;
//       }

//       if( searchQuery != '' && sortQuery !=''){
//         codeSnip = where + ' AND ('+searchQuery+')' + sortQuery +` LIMIT ${query.limit} OFFSET ${query.offset}`;
//         codeSnip2 = where + ' AND ('+searchQuery+')' + sortQuery ;
//       }else if ( searchQuery == '' && sortQuery !=''){
//         codeSnip = where + sortQuery +` LIMIT ${query.limit} OFFSET ${query.offset}`;
//         codeSnip2 = where + sortQuery ;
//       }else if( searchQuery != '' && sortQuery =='' ){
//         codeSnip = where +' AND ('+searchQuery+')'+ ` LIMIT ${query.limit} OFFSET ${query.offset}`;
//         codeSnip2 = where +' AND ('+searchQuery+')';
//       }else if( searchQuery =='' && sortQuery =='' ){
//         codeSnip = where + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
//         codeSnip2 = where ;
//       }


//       let sqlQuery = 'select OtherParties.*,Cases.case_title AS case_title,Clients.name AS client_name, Clients.id AS clinet_id from OtherParties '+
//       'INNER JOIN Cases ON Cases.id = OtherParties.case_id '+
//       'INNER JOIN Clients ON Clients.id = Cases.client_id '+ codeSnip ;

//       let sqlQueryCount = 'select OtherParties.*,Cases.case_title AS case_title,Clients.name AS client_name, Clients.id AS clinet_id from OtherParties '+
//       'INNER JOIN Cases ON Cases.id = OtherParties.case_id '+
//       'INNER JOIN Clients ON Clients.id = Cases.client_id '+ codeSnip2 ;

//       console.log(sqlQuery);
//     const serverData = await sequelize.query(sqlQuery, {
//       type: QueryTypes.SELECT
//     });

//     const TableDataCount = await sequelize.query(sqlQueryCount, {
//       type: QueryTypes.SELECT
//     });


//     return {
//       statusCode: 200,
//       headers: { 'Content-Type': 'text/plain',
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Credentials': true,
//         'Access-Control-Expose-Headers': 'totalPageCount',
//         'totalPageCount':TableDataCount.length
//       },
//       body: JSON.stringify(serverData),
//     };
//   } catch (err) {
//     console.log(err);
//     return {
//       statusCode: err.statusCode || 500,
//       headers: { 'Content-Type': 'text/plain',
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Credentials': true },
//       body: JSON.stringify({ error: err.message || 'Could not fetch the clientss.' }),
//     };
//   }
// }
// const getAllTotalParties = async (event) =>{
//   try {
//     const { sequelize } = await connectToDatabase();
//     const practice_id = "'"+event.user.practice_id+"'";

//     /* const OtherPartiesModelObject = await OtherParties.findAll(query); */
//     const sqlQuery ='select OtherParties.*,Cases.case_title AS case_title,Clients.name AS client_name, Clients.id AS clinet_id from OtherParties '+
//     'INNER JOIN Cases ON Cases.id = OtherParties.case_id '+
//     'INNER JOIN Clients ON Clients.id = Cases.client_id WHERE OtherParties.is_deleted IS NOT true AND OtherParties.practice_id = '+practice_id+' ORDER BY OtherParties.createdAt DESC';
//     const serverData = await sequelize.query(sqlQuery, {
//       type: QueryTypes.SELECT
//     });
//     console.log(sqlQuery);
//     return {
//       statusCode: 200,
//       headers: { 'Content-Type': 'text/plain',
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Credentials': true },
//       body: JSON.stringify(serverData),
//     };
//   } catch (err) {
//     console.log(err);
//     return {
//       statusCode: err.statusCode || 500,
//       headers: { 'Content-Type': 'text/plain',
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Credentials': true },
//       body: JSON.stringify({ error: err.message || 'Could not fetch the Parties.' }),
//     };
//   }
// };
/* list all end */
const getOne = async (event) => {
    try {

        const input = event.params;
        validateGetOneParty(event.params);
        const { OtherParties, Cases, Clients } = await connectToDatabase();
        const OtherPartiesModelObject = await OtherParties.findOne({ where: { id: event.params.id } });
        if (!OtherPartiesModelObject) throw new HTTPError(404, `OtherParty with id: ${event.params.id} was not found`);
        const plainParties = OtherPartiesModelObject.get({ plain: true });
        const casesObj = await Cases.findOne({ where: { id: OtherPartiesModelObject.case_id }, raw: true });
        const clientObj = await Clients.findOne({ where: { id: casesObj.client_id }, raw: true });
        plainParties.totalCases = await Cases.count({ where: { id: OtherPartiesModelObject.case_id } });
        plainParties.case_title = casesObj.case_title;
        plainParties.client_name = clientObj.name;
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainParties),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the OtherParties.' }),
        };
    }
}
const update = async (event) => {
    try {
        const input = event.body;
        validateUpdateParty(input);
        const { OtherParties, Cases, Clients, Users, Op } = await connectToDatabase();
        const OtherPartiesModelObject = await OtherParties.findOne({ where: { id: event.params.id } });
        if (!OtherPartiesModelObject) throw new HTTPError(404, ` with id: ${event.params.id} was not found`);
        let address;
        let name = '';
        if (input?.first_name) {
            name = input?.first_name;
        }
        if (input.middle_name) {
            name = name + ' ' + input.middle_name;
        }
        if (input.last_name) {
            name = name + ' ' + input.last_name;
        }
        if (input.street) {
            address = input.street + ', ' + input.city + ', ' + input.state + ' - ' + input.zip_code;
        }
        input.name = name;
        input.address = address;
        const updatedModel = Object.assign(OtherPartiesModelObject, input);
        await updatedModel.save();
        const plainCase = await updatedModel.get({ plain: true });
        const CasesObj = await Cases.findOne({ where: { id: input.case_id }, raw: true });
        const ClientsObj = await Clients.findOne({ where: { id: CasesObj.client_id }, raw: true });
        let attorneyName = [];
        if (CasesObj.attorneys != null && CasesObj.attorneys) {
            let attorneysIds = CasesObj.attorneys.split(",")
            for (let l = 0; l < attorneysIds.length; l++) {
                if (attorneysIds[l]) {
                    const UsersObj = await Users.findAll({
                        where: { id: attorneysIds[l], is_deleted: { [Op.not]: true } },
                        raw: true
                    });
                    if (UsersObj[0]?.name) attorneyName.push(UsersObj[0].name);
                }
            }
        }
        plainCase.case_title = CasesObj.case_title;
        plainCase.case_number = CasesObj.case_number;
        plainCase.client_name = ClientsObj.name;
        plainCase.client_id = CasesObj.client_id;
        plainCase.attorney_name = attorneyName.toString();

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainCase),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Party.' }),
        };
    }
};
const destroy = async (event) => {
    try {
        validateDeleteParty(event.params);
        const { OtherParties, OtherPartiesForms, OtherPartiesFormsOtp } = await connectToDatabase();
        const OtherPartiesModelObject = await OtherParties.findOne({ where: { id: event.params.id } });
        if (!OtherPartiesModelObject) throw new HTTPError(404, `Party with id: ${event.params.id} was not found`);
        await OtherParties.destroy({ where: { id: event.params.id } });
        await OtherPartiesForms.destroy({
            where: {
                party_id: event.params.id
            }
        });
        await OtherPartiesFormsOtp.destroy({
            where: {
                party_id: event.params.id
            }
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(OtherPartiesModelObject),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy fetch the Clients.' }),
        };
    }
};

module.exports.create = create
module.exports.getAll = getAll;
module.exports.getOne = getOne;
module.exports.update = update;
module.exports.destroy = destroy;
