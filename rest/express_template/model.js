module.exports = (sequelize, type) => sequelize.define('CustomerObjections', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    objection_title: type.STRING,
    objection_text: type.TEXT,
});
