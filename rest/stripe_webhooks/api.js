const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY, {apiVersion: ''});
const connectToDatabase = require('../../db');
const { QueryTypes } = require("sequelize");

//get subscription details by id
const getSubscriptionDetails = async (event) =>{
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { subscription_id } = input;
        const subscription = await stripe.subscriptions.retrieve(subscription_id);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(subscription),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the subscription details.' }),
        };
    }
}

//get invoice details by id
const getInvoiceDetails = async (event) =>{
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { invoice_id } = input;
        const invoiceObj = await stripe.invoices.retrieve(invoice_id);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(invoiceObj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Invoice details.' }),
        };
    }
}

//get charges details by id
const getChargeDetails = async (event) =>{
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { charge_id } = input;
        const chargeObj = await stripe.charges.retrieve(charge_id);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(chargeObj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Charge details.' }),
        };
    }
}

const updateSubscriptionTables = async (event) => {
    try {
        const { Subscriptions, Op, sequelize } = await connectToDatabase();
        const arr = [];
        const subscriptionObj = await Subscriptions.findAll({raw:true});
        for (let i = 0; i < subscriptionObj.length; i++) {
            console.log('i : '+i);
            const element = subscriptionObj[i];
            console.log('id : '+element?.id);
            const stripe_obj = JSON.parse(element.stripe_subscription_data);
            let invoiceObj = undefined;
            let chargeObj = undefined;
            try {
                invoiceObj = stripe_obj?.latest_invoice ? await stripe.invoices.retrieve(stripe_obj?.latest_invoice) : undefined;    
            } catch (err) {
                console.log('err : '+err);
            }
            
            try {
                chargeObj = invoiceObj?.charge ?await stripe.charges.retrieve(invoiceObj?.charge) : undefined;
            } catch (err) {
                console.log('err : '+err);
            }

            if(invoiceObj?.id && chargeObj?.id){
                const whereCon = {
                    invoice_id: invoiceObj?.id || null,
                    payment_attempt_count: invoiceObj?.attempt_count || null, charge_id: invoiceObj?.charge || null,
                    charge_response: chargeObj?.outcome ? JSON.stringify(chargeObj?.outcome) : null
                }
                await Subscriptions.update(whereCon,{where:{id:element.id}});
            }else{
                arr.push(element.id);
            }
        }
        console.log(arr);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({status: 'okay.'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Charge details.' }),
        };
    }
}

module.exports.getSubscriptionDetails = getSubscriptionDetails;
module.exports.getInvoiceDetails = getInvoiceDetails;
module.exports.getChargeDetails = getChargeDetails;
module.exports.updateSubscriptionTables = updateSubscriptionTables;