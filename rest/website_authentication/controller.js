const { HTTPError } = require("../../utils/httpResp");

const siteAuthentication = async (event) => {
    try {
        if (process.env.CODE_ENV == 'local' || process.env.CODE_ENV == 'staging') {
            const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
            const password = input.password;
            if (!password) throw new HTTPError(500, "password does not exists in request");
            if (password != "EsquireTek321") throw new HTTPError(500, "password does not match");
        }
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({ message: "authentication successful" }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "authentication failed",
            }),
        };
    }
}

module.exports.siteAuthentication = siteAuthentication;