module.exports = (sequelize, type) => sequelize.define('FeeTerms', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    terms_text: type.TEXT,
});
