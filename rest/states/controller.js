const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const create = async (event) => {
    try {
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

        const dataObject = Object.assign(input, { id: id || uuid.v4() });
        const { States, AdminObjections, CustomerObjections, Practices, Op } = await connectToDatabase();

        const stateDatas = {
            id: dataObject.id,
            state_name: dataObject.state_name,
            state_code: dataObject.state_code,
            document_type: dataObject.document_type,
        }
        const statesDeatails = await States.create(stateDatas);


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(statesDeatails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the State Deatails.' }),
        };
    }
}
const getOne = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;

        const { States } = await connectToDatabase();
        const statesObject = await States.findOne({ where: { id: params.id } });
        if (!statesObject) throw new HTTPError(404, `State with id: ${params.id} was not found`);
        const plaintext = statesObject.get({ plain: true });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plaintext),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not get the State Deatails.' }),
        };
    }
}

const getAll_new_pagination = async (event) => {
    try {
        const query = event.queryStringParameters || event.query || {};
        const { Op } = await connectToDatabase();
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.order = [
            ['createdAt', 'ASC'],
        ];
        query.raw = true;
        query.where.is_deleted = { [Op.not]: true };

        const { States } = await connectToDatabase();
        const plaintext = await States.findAll(query);
        const plaintextCount = await States.count();

        let totalPages, currentPage;
        if (query.limit && (plaintext.length > 0)) {
            totalPages = Math.ceil(plaintextCount / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const plaintextDetails = {
            total_items: plaintextCount,
            response: plaintext,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plaintextDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not get the State Deatails.' }),
        };
    }
}

const getAll = async (event) => {
    try {
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.order = [
            ['createdAt', 'ASC'],
        ];
        query.raw = true;

        const { States } = await connectToDatabase();
        const plaintext = await States.findAll(query);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plaintext),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not get the State Deatails.' }),
        };
    }
}

const update = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;

        const { States } = await connectToDatabase();
        const statesObject = await States.findOne({ where: { id: params.id } });
        if (!statesObject) throw new HTTPError(404, `State with id: ${params.id} was not found`);
        const stateModel = Object.assign(statesObject, input);
        await stateModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(stateModel),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the State.' }),
        };
    }
}
const destroy = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;

        const { States } = await connectToDatabase();
        const stateModelObject = await States.findOne({ where: { id: params.id } });
        if (!stateModelObject) throw new HTTPError(404, `States with id: ${params.id} was not found`);
        const stateModel = Object.assign(stateModelObject, input);
        await stateModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ message: "State destroyed" }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not destroy fetch state.' }),
        };
    }
}
module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;