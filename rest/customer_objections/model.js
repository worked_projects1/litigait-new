module.exports = (sequelize, type) => sequelize.define('CustomerObjections', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    adminobjection_id: type.STRING,
    practice_id: type.STRING,
    objection_title: type.STRING,
    objection_text: type.TEXT,
    state: type.STRING,
    objection_view: type.BOOLEAN,
    attorneys: type.TEXT('long'),

}, {
    indexes: [
        {
        name: 'apid',
        fields: ['adminobjection_id', 'practice_id']
    },
    {
        name: 'pid',
        fields: ['practice_id']
    },
    {
        name: 'ste',
        fields: ['state']
    },
    {
        name: 'pistate',
        fields: ['practice_id','state']
    },
    {
        name: 'apidst',
        fields: ['adminobjection_id','practice_id','state']
    }
    ]
}
);