const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const {
    validateCreateCustomerObjections,
    validateGetOneCustomerObjection,
    validateUpdateCustomerObjections,
    validateDeleteCustomerObjection,
    validateReadCustomerObjections,
} = require('./validation');

const create = async (event) => {
    try {
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        if (event.user.practice_id) {
            input.practice_id = event.user.practice_id;
        }
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, { id: id || uuid.v4() });
        validateCreateCustomerObjections(dataObject);
        const { CustomerObjections } = await connectToDatabase();
        const customerObjectionss = await CustomerObjections.create(dataObject);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(customerObjectionss),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the customerObjectionss.' }),
        };
    }
};


const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        //validateGetOneCustomerObjection(params);
        const { CustomerObjections, Users, Op } = await connectToDatabase();
        const customerObjectionss = await CustomerObjections.findOne({ where: { id: params.id } });
        if (!customerObjectionss) throw new HTTPError(404, `CustomerObjections with id: ${params.id} was not found`);
        const plainCustomerObjection = customerObjectionss.get({ plain: true });

        let attorneysNames = '';
        if (plainCustomerObjection?.attorneys) {
            const attorneysList = plainCustomerObjection.attorneys.split(',')
            for (let i = 0; i < attorneysList.length; i++) {
                let user = await Users.findOne({ where: { id: attorneysList[i], is_deleted: { [Op.not]: true } } });
                if (user.name) {
                    attorneysNames += user.name + ','
                }
            }
            plainCustomerObjection.attorneys_name = attorneysNames.slice(0, -1);
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainCustomerObjection),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the CustomerObjections.' }),
        };
    }
};

const getAll_new = async (event) => {
    try {
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        query.logging = console.log;
        query.order = [
            ['objection_title', 'ASC'],
        ];
        validateReadCustomerObjections(query);
        const { CustomerObjections } = await connectToDatabase();
        const customerObjectionsss = await CustomerObjections.findAll(query);
        const customerObjectionCount = await CustomerObjections.count({ where: { practice_id: event.user.practice_id } });
        let totalPages, currentPage;
        if (query.limit && (customerObjectionsss.length > 0)) {
            totalPages = Math.ceil(customerObjectionCount / query.limit);
            if (query.offset) {
                currentPage = Math.ceil((query.limit + query.offset) / query.limit);
            } else {
                currentPage = 1;
            }
        }

        const customerObjectionDetails = {
            total_items: customerObjectionCount,
            response: customerObjectionsss,
            total_pages: totalPages,
            current_page: currentPage
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(customerObjectionDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the customerObjectionsss.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        const query = event.headers || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        query.order = [
            ['objection_title', 'ASC'],
        ];
        validateReadCustomerObjections(query);
        const { CustomerObjections } = await connectToDatabase();
        const customerObjectionsss = await CustomerObjections.findAll(query);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(customerObjectionsss),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the customerObjectionsss.' }),
        };
    }
};

const update = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        validateUpdateCustomerObjections(input);
        const { CustomerObjections } = await connectToDatabase();
        const customerObjectionss = await CustomerObjections.findOne({ where: { id: params.id } });
        if (!customerObjectionss) throw new HTTPError(404, `CustomerObjections with id: ${params.id} was not found`);
        const updatedModel = Object.assign(customerObjectionss, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the CustomerObjections.' }),
        };
    }
};

const updateMany = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        validateUpdateCustomerObjections(input);
        const { CustomerObjections } = await connectToDatabase();
        for (let i = 0; i < input.length; i += 1) {
            const dataObject = input[i];
            const customerObjectionObject = await CustomerObjections.findOne({ where: { id: dataObject.id } });
            if (!customerObjectionObject) throw new HTTPError(404, `Customer Objection with id: ${customerObjectionObject.id} was not found`);
            const updatedModel = Object.assign(customerObjectionObject, dataObject);
            await updatedModel.save();
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'All records successfully updated',
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the CustomerObjections.' }),
        };
    }
};

const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateDeleteCustomerObjection(params);
        const { CustomerObjections } = await connectToDatabase();
        const customerObjectionsDetails = await CustomerObjections.findOne({ where: { id: params.id } });
        if (!customerObjectionsDetails) throw new HTTPError(404, `CustomerObjections with id: ${params.id} was not found`);
        await customerObjectionsDetails.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(customerObjectionsDetails),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy fetch the CustomerObjections.' }),
        };
    }
};

const getAllObjectionByState = async (event) => {
    try {
        const query = event.queryStringParameters || event.query || {};
        const { CustomerObjections, Op } = await connectToDatabase();
        const { state } = query;
        if (!state) throw new HTTPError(400, 'State not provided');
        const practice_id = event.user.practice_id;

        const customerObjectionsDetails = await CustomerObjections.findAll({
            where: { practice_id: practice_id, state: state, objection_text: { [Op.not]: null } },
            order: [['objection_title', 'ASC']]
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(customerObjectionsDetails),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Failed to fetch customer objections' }),
        };
    }
}

module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;
module.exports.updateMany = updateMany;
module.exports.getAllObjectionByState = getAllObjectionByState;
